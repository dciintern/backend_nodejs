PGDMP                         y            Eparts    12.2    12.2 :   �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            �           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            �           1262    180474    Eparts    DATABASE     �   CREATE DATABASE "Eparts" WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'English_United States.1252' LC_CTYPE = 'English_United States.1252';
    DROP DATABASE "Eparts";
                e-parts-admin    false            b           1259    236513    login_user_type    TABLE     �   CREATE TABLE public.login_user_type (
    login_user_type_id integer NOT NULL,
    login_user_type_name character varying(50),
    status boolean,
    created_at date,
    created_by integer,
    modified_at date,
    modified_by integer
);
 #   DROP TABLE public.login_user_type;
       public         heap    postgres    false            a           1259    236511 &   Login_user_type_Login_user_type_id_seq    SEQUENCE     �   CREATE SEQUENCE public."Login_user_type_Login_user_type_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ?   DROP SEQUENCE public."Login_user_type_Login_user_type_id_seq";
       public          postgres    false    354            �           0    0 &   Login_user_type_Login_user_type_id_seq    SEQUENCE OWNED BY     s   ALTER SEQUENCE public."Login_user_type_Login_user_type_id_seq" OWNED BY public.login_user_type.login_user_type_id;
          public          postgres    false    353            $           1259    186813    Rating_review    TABLE     r  CREATE TABLE public."Rating_review" (
    id integer NOT NULL,
    seller_name character varying,
    rate_seller character varying,
    review_seller character varying,
    status character varying,
    action character varying,
    created_at timestamp without time zone,
    created_by integer,
    modified_at timestamp without time zone,
    modified_by integer
);
 #   DROP TABLE public."Rating_review";
       public         heap    postgres    false            #           1259    186811    Rating_review_id_seq    SEQUENCE     �   CREATE SEQUENCE public."Rating_review_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public."Rating_review_id_seq";
       public          postgres    false    292            �           0    0    Rating_review_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE public."Rating_review_id_seq" OWNED BY public."Rating_review".id;
          public          postgres    false    291            �            1259    186518    application_user    TABLE     �  CREATE TABLE public.application_user (
    id integer NOT NULL,
    name character varying,
    phone_no character varying,
    email_id character varying,
    user_type character varying,
    password character varying,
    designation character varying,
    status character varying,
    created_at timestamp without time zone,
    created_by integer,
    modified_at timestamp without time zone,
    modified_by integer
);
 $   DROP TABLE public.application_user;
       public         heap    postgres    false            �            1259    186516    application_user_id_seq    SEQUENCE     �   CREATE SEQUENCE public.application_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.application_user_id_seq;
       public          postgres    false    237            �           0    0    application_user_id_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE public.application_user_id_seq OWNED BY public.application_user.id;
          public          postgres    false    236            8           1259    186923    audit_trail    TABLE     �  CREATE TABLE public.audit_trail (
    id integer NOT NULL,
    module_name character varying,
    field_name character varying,
    value character varying,
    user_name character varying,
    time_stamp character varying,
    status character varying,
    action character varying,
    created_at timestamp without time zone,
    created_by integer,
    modified_at timestamp without time zone,
    modified_by integer
);
    DROP TABLE public.audit_trail;
       public         heap    postgres    false            7           1259    186921    audit_trail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.audit_trail_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.audit_trail_id_seq;
       public          postgres    false    312            �           0    0    audit_trail_id_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE public.audit_trail_id_seq OWNED BY public.audit_trail.id;
          public          postgres    false    311            B           1259    195194 
   banner_cms    TABLE     .  CREATE TABLE public.banner_cms (
    title character varying(50),
    content character varying(250),
    image character varying(255) NOT NULL,
    status boolean,
    created_at date,
    modified_at date,
    created_by numeric NOT NULL,
    modified_by numeric NOT NULL,
    id integer NOT NULL
);
    DROP TABLE public.banner_cms;
       public         heap    e-parts-admin    false            A           1259    195192    banner_cms_id_seq    SEQUENCE     �   CREATE SEQUENCE public.banner_cms_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.banner_cms_id_seq;
       public          e-parts-admin    false    322            �           0    0    banner_cms_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.banner_cms_id_seq OWNED BY public.banner_cms.id;
          public          e-parts-admin    false    321            �            1259    186545    brands    TABLE     h  CREATE TABLE public.brands (
    id integer NOT NULL,
    brand_id integer NOT NULL,
    brand_name character varying,
    brand_api_id character varying,
    brand_image character varying,
    status character varying,
    created_at timestamp without time zone,
    created_by integer,
    modified_at timestamp without time zone,
    modified_by integer
);
    DROP TABLE public.brands;
       public         heap    postgres    false            �            1259    186543    brands_brand_id_seq    SEQUENCE     �   CREATE SEQUENCE public.brands_brand_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.brands_brand_id_seq;
       public          postgres    false    243            �           0    0    brands_brand_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE public.brands_brand_id_seq OWNED BY public.brands.brand_id;
          public          postgres    false    242            �            1259    186541    brands_id_seq    SEQUENCE     �   CREATE SEQUENCE public.brands_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE public.brands_id_seq;
       public          postgres    false    243            �           0    0    brands_id_seq    SEQUENCE OWNED BY     ?   ALTER SEQUENCE public.brands_id_seq OWNED BY public.brands.id;
          public          postgres    false    241            �            1259    186452    buyer_management    TABLE     �  CREATE TABLE public.buyer_management (
    id integer NOT NULL,
    login_id character varying,
    password character varying,
    buyer_name character varying,
    email_id character varying,
    contact_no character varying,
    address character varying,
    status boolean,
    action character varying,
    created_at timestamp without time zone,
    created_by integer,
    modified_at timestamp without time zone,
    modified_by integer
);
 $   DROP TABLE public.buyer_management;
       public         heap    postgres    false            �            1259    186450    buyer_management_id_seq    SEQUENCE     �   CREATE SEQUENCE public.buyer_management_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.buyer_management_id_seq;
       public          postgres    false    225            �           0    0    buyer_management_id_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE public.buyer_management_id_seq OWNED BY public.buyer_management.id;
          public          postgres    false    224                       1259    186769    buyer_saved_quote    TABLE     �  CREATE TABLE public.buyer_saved_quote (
    id integer NOT NULL,
    buyer_id character varying,
    buyer_name character varying,
    quote_id character varying,
    customer_name character varying,
    email character varying,
    phone character varying,
    items character varying,
    status_of_quote character varying,
    quantity character varying,
    price character varying,
    margin_per character varying,
    price_with_margin character varying,
    tax_code character varying,
    tax_amount character varying,
    final_price character varying,
    cusotmer_detail_name character varying,
    address character varying,
    car_details_brand character varying,
    model character varying,
    status character varying,
    action character varying,
    created_at timestamp without time zone,
    created_by integer,
    modified_at timestamp without time zone,
    modified_by integer
);
 %   DROP TABLE public.buyer_saved_quote;
       public         heap    postgres    false                       1259    186767    buyer_saved_quote_id_seq    SEQUENCE     �   CREATE SEQUENCE public.buyer_saved_quote_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 /   DROP SEQUENCE public.buyer_saved_quote_id_seq;
       public          postgres    false    284            �           0    0    buyer_saved_quote_id_seq    SEQUENCE OWNED BY     U   ALTER SEQUENCE public.buyer_saved_quote_id_seq OWNED BY public.buyer_saved_quote.id;
          public          postgres    false    283            J           1259    211637    buyer_seller_cms    TABLE     ,  CREATE TABLE public.buyer_seller_cms (
    id integer NOT NULL,
    buyer_title character varying(50) NOT NULL,
    buyer_content character varying(5000) NOT NULL,
    buyer_link character varying(255) NOT NULL,
    buyer_image character varying(255) NOT NULL,
    seller_title character varying(50) NOT NULL,
    seller_content character varying(5000) NOT NULL,
    seller_link character varying(255) NOT NULL,
    seller_image character varying(255) NOT NULL,
    created_by integer,
    modified_at date,
    modified_by integer,
    created_at date
);
 $   DROP TABLE public.buyer_seller_cms;
       public         heap    e-parts-admin    false            I           1259    211635    buyer_seller_cms_id_seq    SEQUENCE     �   CREATE SEQUENCE public.buyer_seller_cms_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.buyer_seller_cms_id_seq;
       public          e-parts-admin    false    330            �           0    0    buyer_seller_cms_id_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE public.buyer_seller_cms_id_seq OWNED BY public.buyer_seller_cms.id;
          public          e-parts-admin    false    329                       1259    186714    car_management    TABLE     �  CREATE TABLE public.car_management (
    id integer NOT NULL,
    buyer_name character varying,
    car_id character varying,
    brand character varying,
    model character varying,
    vin_no character varying,
    action character varying,
    status character varying,
    created_at timestamp without time zone,
    created_by integer,
    modified_at timestamp without time zone,
    modified_by integer
);
 "   DROP TABLE public.car_management;
       public         heap    postgres    false                       1259    186712    car_management_id_seq    SEQUENCE     �   CREATE SEQUENCE public.car_management_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public.car_management_id_seq;
       public          postgres    false    274            �           0    0    car_management_id_seq    SEQUENCE OWNED BY     O   ALTER SEQUENCE public.car_management_id_seq OWNED BY public.car_management.id;
          public          postgres    false    273            �            1259    186582    cars    TABLE     �  CREATE TABLE public.cars (
    id integer NOT NULL,
    car_id character varying,
    brand_name character varying,
    model_name character varying,
    parameters_name character varying,
    parameters_value character varying,
    status character varying,
    created_at timestamp without time zone,
    created_by integer,
    modified_at timestamp without time zone,
    modified_by integer
);
    DROP TABLE public.cars;
       public         heap    postgres    false            �            1259    186580    cars_id_seq    SEQUENCE     �   CREATE SEQUENCE public.cars_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 "   DROP SEQUENCE public.cars_id_seq;
       public          postgres    false    250            �           0    0    cars_id_seq    SEQUENCE OWNED BY     ;   ALTER SEQUENCE public.cars_id_seq OWNED BY public.cars.id;
          public          postgres    false    249                       1259    186758    category_request_management    TABLE     �  CREATE TABLE public.category_request_management (
    id integer NOT NULL,
    category_code character varying,
    category_name character varying,
    description character varying,
    reason_for_reject character varying,
    status character varying,
    action character varying,
    created_at timestamp without time zone,
    created_by integer,
    modified_at timestamp without time zone,
    modified_by integer
);
 /   DROP TABLE public.category_request_management;
       public         heap    postgres    false                       1259    186756 "   category_request_management_id_seq    SEQUENCE     �   CREATE SEQUENCE public.category_request_management_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 9   DROP SEQUENCE public.category_request_management_id_seq;
       public          postgres    false    282            �           0    0 "   category_request_management_id_seq    SEQUENCE OWNED BY     i   ALTER SEQUENCE public.category_request_management_id_seq OWNED BY public.category_request_management.id;
          public          postgres    false    281            R           1259    223384    category_request_status    TABLE       CREATE TABLE public.category_request_status (
    cat_req_status_id integer NOT NULL,
    cat_req_status_name character varying,
    remarks character varying,
    created_at date,
    created_by integer,
    modified_at date,
    modified_by integer,
    status boolean
);
 +   DROP TABLE public.category_request_status;
       public         heap    postgres    false            Q           1259    223382 -   category_request_status_cat_req_status_id_seq    SEQUENCE     �   CREATE SEQUENCE public.category_request_status_cat_req_status_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 D   DROP SEQUENCE public.category_request_status_cat_req_status_id_seq;
       public          postgres    false    338            �           0    0 -   category_request_status_cat_req_status_id_seq    SEQUENCE OWNED BY        ALTER SEQUENCE public.category_request_status_cat_req_status_id_seq OWNED BY public.category_request_status.cat_req_status_id;
          public          postgres    false    337            >           1259    186971    city_details    TABLE       CREATE TABLE public.city_details (
    id integer NOT NULL,
    country_id integer,
    state_id integer,
    city_name character varying,
    created_at timestamp without time zone,
    created_by integer,
    modified_at timestamp without time zone,
    modified_by integer
);
     DROP TABLE public.city_details;
       public         heap    postgres    false            =           1259    186969    city_details_id_seq    SEQUENCE     �   CREATE SEQUENCE public.city_details_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.city_details_id_seq;
       public          postgres    false    318            �           0    0    city_details_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE public.city_details_id_seq OWNED BY public.city_details.id;
          public          postgres    false    317                       1259    186681    commission_managemnet    TABLE     �  CREATE TABLE public.commission_managemnet (
    id integer NOT NULL,
    product_type character varying,
    commission_precentage character varying,
    effective_date character varying,
    action character varying,
    status character varying,
    created_at timestamp without time zone,
    created_by integer,
    modified_at timestamp without time zone,
    modified_by integer
);
 )   DROP TABLE public.commission_managemnet;
       public         heap    postgres    false                       1259    186679    commission_managemnet_id_seq    SEQUENCE     �   CREATE SEQUENCE public.commission_managemnet_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 3   DROP SEQUENCE public.commission_managemnet_id_seq;
       public          postgres    false    268            �           0    0    commission_managemnet_id_seq    SEQUENCE OWNED BY     ]   ALTER SEQUENCE public.commission_managemnet_id_seq OWNED BY public.commission_managemnet.id;
          public          postgres    false    267            h           1259    236620    configuration_details    TABLE     |  CREATE TABLE public.configuration_details (
    id integer NOT NULL,
    config_name character varying(150),
    "config_URL" character varying(255),
    "Config_username" character varying(150),
    config_password character varying(100),
    config_port integer,
    config_parameter character varying(1000),
    "config_MobileNumber" integer,
    config_mtype integer,
    config_message integer,
    config_secretkey character varying(100),
    status integer,
    remarks character varying(1000),
    created_at timestamp with time zone,
    created_by integer,
    modified_at timestamp with time zone,
    modified_by integer
);
 )   DROP TABLE public.configuration_details;
       public         heap    postgres    false            g           1259    236618    configuration_details_id_seq    SEQUENCE     �   CREATE SEQUENCE public.configuration_details_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 3   DROP SEQUENCE public.configuration_details_id_seq;
       public          postgres    false    360            �           0    0    configuration_details_id_seq    SEQUENCE OWNED BY     ]   ALTER SEQUENCE public.configuration_details_id_seq OWNED BY public.configuration_details.id;
          public          postgres    false    359            �            1259    186408    country_details    TABLE     +  CREATE TABLE public.country_details (
    id integer NOT NULL,
    country_name character varying,
    created_at timestamp without time zone,
    created_by integer,
    modified_at timestamp without time zone,
    modified_by integer,
    status boolean,
    country_code character varying(50)
);
 #   DROP TABLE public.country_details;
       public         heap    postgres    false            �            1259    186406    country_details_id_seq    SEQUENCE     �   CREATE SEQUENCE public.country_details_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.country_details_id_seq;
       public          postgres    false    217            �           0    0    country_details_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE public.country_details_id_seq OWNED BY public.country_details.id;
          public          postgres    false    216            �            1259    186397    currency_details    TABLE       CREATE TABLE public.currency_details (
    id integer NOT NULL,
    currency_name character varying,
    created_at timestamp without time zone,
    created_by integer,
    modified_at timestamp without time zone,
    modified_by integer,
    status boolean
);
 $   DROP TABLE public.currency_details;
       public         heap    postgres    false            �            1259    186395    currency_details_id_seq    SEQUENCE     �   CREATE SEQUENCE public.currency_details_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.currency_details_id_seq;
       public          postgres    false    215            �           0    0    currency_details_id_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE public.currency_details_id_seq OWNED BY public.currency_details.id;
          public          postgres    false    214            <           1259    186945    email_template    TABLE     	  CREATE TABLE public.email_template (
    id integer NOT NULL,
    title character varying,
    email_type character varying,
    encoding_type character varying,
    iso_code character varying,
    description character varying,
    subject character varying,
    email_content character varying,
    attachment character varying,
    action character varying,
    created_at timestamp without time zone,
    created_by integer,
    modified_at timestamp without time zone,
    modified_by integer,
    status boolean
);
 "   DROP TABLE public.email_template;
       public         heap    postgres    false            ;           1259    186943    email_temp_id_seq    SEQUENCE     �   CREATE SEQUENCE public.email_temp_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.email_temp_id_seq;
       public          postgres    false    316            �           0    0    email_temp_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE public.email_temp_id_seq OWNED BY public.email_template.id;
          public          postgres    false    315            D           1259    195205    features_cms    TABLE     h  CREATE TABLE public.features_cms (
    id integer NOT NULL,
    title character varying(50) NOT NULL,
    content character varying(250) NOT NULL,
    icon character varying(255) NOT NULL,
    status boolean NOT NULL,
    created_at date NOT NULL,
    modified_at date NOT NULL,
    created_by numeric(20,0) NOT NULL,
    modified_by numeric(20,0) NOT NULL
);
     DROP TABLE public.features_cms;
       public         heap    e-parts-admin    false            C           1259    195203    features_cms_id_seq    SEQUENCE     �   CREATE SEQUENCE public.features_cms_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.features_cms_id_seq;
       public          e-parts-admin    false    324            �           0    0    features_cms_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE public.features_cms_id_seq OWNED BY public.features_cms.id;
          public          e-parts-admin    false    323            �            1259    186593    group_management    TABLE     �  CREATE TABLE public.group_management (
    id integer NOT NULL,
    car_id character varying,
    brand character varying,
    model character varying,
    group_code character varying,
    group_name character varying,
    status character varying,
    created_at timestamp without time zone,
    created_by integer,
    modified_at timestamp without time zone,
    modified_by integer
);
 $   DROP TABLE public.group_management;
       public         heap    postgres    false            �            1259    186591    group_management_id_seq    SEQUENCE     �   CREATE SEQUENCE public.group_management_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.group_management_id_seq;
       public          postgres    false    252            �           0    0    group_management_id_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE public.group_management_id_seq OWNED BY public.group_management.id;
          public          postgres    false    251            6           1259    186912    help_management    TABLE     G  CREATE TABLE public.help_management (
    id integer NOT NULL,
    code character varying,
    subject_name character varying,
    status character varying,
    action character varying,
    created_at timestamp without time zone,
    created_by integer,
    modified_at timestamp without time zone,
    modified_by integer
);
 #   DROP TABLE public.help_management;
       public         heap    postgres    false            5           1259    186910    help_management_id_seq    SEQUENCE     �   CREATE SEQUENCE public.help_management_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.help_management_id_seq;
       public          postgres    false    310            �           0    0    help_management_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE public.help_management_id_seq OWNED BY public.help_management.id;
          public          postgres    false    309            H           1259    211626    hero_section_cms    TABLE     O  CREATE TABLE public.hero_section_cms (
    title character varying(50),
    sub_title character varying(150),
    content character varying(5000),
    link character varying(255),
    id integer NOT NULL,
    created_by integer NOT NULL,
    created_at date NOT NULL,
    modified_by integer NOT NULL,
    modified_at date NOT NULL
);
 $   DROP TABLE public.hero_section_cms;
       public         heap    e-parts-admin    false            G           1259    211624    hero_section_cms_id_seq    SEQUENCE     �   CREATE SEQUENCE public.hero_section_cms_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.hero_section_cms_id_seq;
       public          e-parts-admin    false    328            �           0    0    hero_section_cms_id_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE public.hero_section_cms_id_seq OWNED BY public.hero_section_cms.id;
          public          e-parts-admin    false    327            ?           1259    195183    info_cms    TABLE     �  CREATE TABLE public.info_cms (
    contact numeric(10,0) NOT NULL,
    email character varying(50) NOT NULL,
    created_at date,
    modified_at date,
    created_by integer,
    modified_by integer,
    id integer NOT NULL,
    company_name character varying(50),
    description character varying(5000),
    logo character varying(255),
    playstore_link character varying(255),
    applestore_link character varying(255)
);
    DROP TABLE public.info_cms;
       public         heap    e-parts-admin    false            @           1259    195186    info_cms_id_seq    SEQUENCE     �   CREATE SEQUENCE public.info_cms_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.info_cms_id_seq;
       public          e-parts-admin    false    319            �           0    0    info_cms_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE public.info_cms_id_seq OWNED BY public.info_cms.id;
          public          e-parts-admin    false    320            �            1259    186507    intermediator    TABLE     i  CREATE TABLE public.intermediator (
    id integer NOT NULL,
    firstname character varying,
    lastname character varying,
    user_name character varying,
    email_id character varying,
    passwork character varying,
    role character varying,
    phone_number character varying,
    mobile_number character varying,
    status character varying,
    image character varying,
    country character varying,
    language character varying,
    address character varying,
    created_at timestamp without time zone,
    created_by integer,
    modified_at timestamp without time zone,
    modified_by integer
);
 !   DROP TABLE public.intermediator;
       public         heap    postgres    false            �            1259    186505    intermediator_id_seq    SEQUENCE     �   CREATE SEQUENCE public.intermediator_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.intermediator_id_seq;
       public          postgres    false    235            �           0    0    intermediator_id_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.intermediator_id_seq OWNED BY public.intermediator.id;
          public          postgres    false    234            �            1259    186496    intermediator_management    TABLE     �  CREATE TABLE public.intermediator_management (
    id integer NOT NULL,
    intermdidator_name character varying,
    email_id character varying,
    contact_no character varying,
    role character varying,
    status boolean,
    action character varying,
    created_at timestamp without time zone,
    created_by integer,
    modified_at timestamp without time zone,
    modified_by integer
);
 ,   DROP TABLE public.intermediator_management;
       public         heap    postgres    false            �            1259    186494    intermediator_management_id_seq    SEQUENCE     �   CREATE SEQUENCE public.intermediator_management_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 6   DROP SEQUENCE public.intermediator_management_id_seq;
       public          postgres    false    233            �           0    0    intermediator_management_id_seq    SEQUENCE OWNED BY     c   ALTER SEQUENCE public.intermediator_management_id_seq OWNED BY public.intermediator_management.id;
          public          postgres    false    232            4           1259    186901    inventory_manangement    TABLE     �  CREATE TABLE public.inventory_manangement (
    id integer NOT NULL,
    ware_house character varying,
    seller_name character varying,
    product character varying,
    price character varying,
    onhand character varying,
    e_part_qnty character varying,
    buffer_qnty character varying,
    re_order_level character varying,
    threshold_limit character varying,
    sold character varying,
    balance character varying,
    bin_loc character varying,
    status character varying,
    action character varying,
    created_at timestamp without time zone,
    created_by integer,
    modified_at timestamp without time zone,
    modified_by integer
);
 )   DROP TABLE public.inventory_manangement;
       public         heap    postgres    false            3           1259    186899    inventory_manangement_id_seq    SEQUENCE     �   CREATE SEQUENCE public.inventory_manangement_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 3   DROP SEQUENCE public.inventory_manangement_id_seq;
       public          postgres    false    308            �           0    0    inventory_manangement_id_seq    SEQUENCE OWNED BY     ]   ALTER SEQUENCE public.inventory_manangement_id_seq OWNED BY public.inventory_manangement.id;
          public          postgres    false    307            T           1259    223397    make_offer_status    TABLE       CREATE TABLE public.make_offer_status (
    make_offer_status_id integer NOT NULL,
    offer_status_name character varying,
    remarks character varying,
    created_at date,
    created_by integer,
    modified_at date,
    modified_by integer,
    status boolean
);
 %   DROP TABLE public.make_offer_status;
       public         heap    postgres    false            S           1259    223395 *   make_offer_status_make_offer_status_id_seq    SEQUENCE     �   CREATE SEQUENCE public.make_offer_status_make_offer_status_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 A   DROP SEQUENCE public.make_offer_status_make_offer_status_id_seq;
       public          postgres    false    340            �           0    0 *   make_offer_status_make_offer_status_id_seq    SEQUENCE OWNED BY     y   ALTER SEQUENCE public.make_offer_status_make_offer_status_id_seq OWNED BY public.make_offer_status.make_offer_status_id;
          public          postgres    false    339            j           1259    236634    mobile_section_cms    TABLE       CREATE TABLE public.mobile_section_cms (
    title character varying(150),
    id integer NOT NULL,
    content character varying(300),
    status boolean,
    created_at date,
    created_by integer,
    modified_at date,
    image character varying,
    modified_by integer
);
 &   DROP TABLE public.mobile_section_cms;
       public         heap    e-parts-admin    false            i           1259    236632    mobile_section_id_seq    SEQUENCE     �   CREATE SEQUENCE public.mobile_section_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public.mobile_section_id_seq;
       public          e-parts-admin    false    362            �           0    0    mobile_section_id_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE public.mobile_section_id_seq OWNED BY public.mobile_section_cms.id;
          public          e-parts-admin    false    361            �            1259    186559    models    TABLE     �  CREATE TABLE public.models (
    id integer NOT NULL,
    brand_name integer NOT NULL,
    model_id character varying,
    model_name character varying,
    model_api_id character varying,
    model_image character varying,
    status character varying,
    created_at timestamp without time zone,
    created_by integer,
    modified_at timestamp without time zone,
    modified_by integer
);
    DROP TABLE public.models;
       public         heap    postgres    false            �            1259    186557    models_brand_name_seq    SEQUENCE     �   CREATE SEQUENCE public.models_brand_name_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public.models_brand_name_seq;
       public          postgres    false    246            �           0    0    models_brand_name_seq    SEQUENCE OWNED BY     O   ALTER SEQUENCE public.models_brand_name_seq OWNED BY public.models.brand_name;
          public          postgres    false    245            �            1259    186555    models_id_seq    SEQUENCE     �   CREATE SEQUENCE public.models_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE public.models_id_seq;
       public          postgres    false    246            �           0    0    models_id_seq    SEQUENCE OWNED BY     ?   ALTER SEQUENCE public.models_id_seq OWNED BY public.models.id;
          public          postgres    false    244            P           1259    223376    nav_links_cms    TABLE     0  CREATE TABLE public.nav_links_cms (
    id integer NOT NULL,
    label character varying(15) NOT NULL,
    link character varying(100) NOT NULL,
    created_by integer NOT NULL,
    created_at date NOT NULL,
    modified_by integer NOT NULL,
    modified_at date NOT NULL,
    status boolean NOT NULL
);
 !   DROP TABLE public.nav_links_cms;
       public         heap    e-parts-admin    false            O           1259    223374    nav_links_cms_id_seq    SEQUENCE     �   CREATE SEQUENCE public.nav_links_cms_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.nav_links_cms_id_seq;
       public          e-parts-admin    false    336            �           0    0    nav_links_cms_id_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.nav_links_cms_id_seq OWNED BY public.nav_links_cms.id;
          public          e-parts-admin    false    335            �            1259    186463    new_buyer_details    TABLE     �  CREATE TABLE public.new_buyer_details (
    id integer NOT NULL,
    login_id character varying,
    password character varying,
    business_name character varying,
    business_com_logo character varying,
    business_subscrip_plan integer,
    business_status integer,
    billing_country integer,
    billing_state integer,
    billing_city character varying,
    b_address1 character varying,
    b_address2 character varying,
    b_displayname character varying,
    b_phonenumber numeric,
    b_country_code numeric,
    shipping_country integer,
    shipping_state integer,
    shipping_city character varying,
    s_address1 character varying,
    s_address2 character varying,
    s_displayname character varying,
    s_phonenumber numeric,
    s_country_code numeric,
    s_phoneno_verified character varying,
    pickup_address character varying,
    payment_info json,
    product_type integer,
    trading_type integer,
    iv_locate integer,
    iv_licenese character varying,
    iv_national character varying,
    iv_exp_date timestamp without time zone,
    iv_country_issue integer,
    iv_f_name character varying,
    iv_m_name character varying,
    iv_l_name character varying,
    iv_dob timestamp without time zone,
    iv_r_bussiness_name character varying,
    iv_licenese_no character varying,
    iv_r_address character varying,
    id_doc character varying,
    bus_doc_license character varying,
    bus_doc_r_status character varying,
    bus_doc_fail_reason integer,
    vat_info_number character varying,
    vat_r_certificate character varying,
    bank_benefi_name character varying,
    bank_name character varying,
    bank_ac_no character varying,
    back_iban character varying,
    swift_code character varying,
    bank_currency integer,
    bank_browse_file character varying,
    created_at timestamp without time zone,
    created_by integer,
    modified_at timestamp without time zone,
    modified_by integer
);
 %   DROP TABLE public.new_buyer_details;
       public         heap    postgres    false            �            1259    186461    new_buyer_details_id_seq    SEQUENCE     �   CREATE SEQUENCE public.new_buyer_details_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 /   DROP SEQUENCE public.new_buyer_details_id_seq;
       public          postgres    false    227            �           0    0    new_buyer_details_id_seq    SEQUENCE OWNED BY     U   ALTER SEQUENCE public.new_buyer_details_id_seq OWNED BY public.new_buyer_details.id;
          public          postgres    false    226            �            1259    186485    new_seller_details    TABLE     �	  CREATE TABLE public.new_seller_details (
    id integer NOT NULL,
    login_id character varying,
    password character varying,
    b_multi_branch character varying,
    business_name character varying,
    business_com_logo character varying,
    business_subscrip_plan integer,
    payout_shed integer,
    business_status integer,
    billing_country integer,
    billing_state integer,
    billing_city character varying,
    b_address1 character varying,
    b_address2 character varying,
    b_displayname character varying,
    b_phonenumber numeric,
    b_country_code numeric,
    shipping_country integer,
    shipping_state integer,
    shipping_city character varying,
    s_address1 character varying,
    s_address2 character varying,
    s_displayname character varying,
    s_phonenumber numeric,
    s_country_code numeric,
    s_phoneno_verified character varying,
    pickup_address character varying,
    payment_info json,
    product_type integer,
    trading_type integer,
    iv_locate integer,
    iv_licenese character varying,
    iv_national character varying,
    iv_exp_date timestamp without time zone,
    iv_country_issue integer,
    iv_f_name character varying,
    iv_m_name character varying,
    iv_l_name character varying,
    iv_dob timestamp without time zone,
    iv_r_bussiness_name character varying,
    iv_licenese_no character varying,
    iv_r_address character varying,
    id_doc character varying,
    bus_doc_license character varying,
    bus_doc_r_status character varying,
    bus_doc_fail_reason integer,
    vat_info_number character varying,
    vat_r_certificate character varying,
    bank_benefi_name character varying,
    bank_name character varying,
    bank_ac_no character varying,
    back_iban character varying,
    swift_code character varying,
    bank_currency integer,
    bank_browse_file character varying,
    warehouse_country integer,
    warehouse_state integer,
    warehouse_city character varying,
    warehouse_address_1 character varying,
    warehouse_address_2 character varying,
    warehouse_display character varying,
    warehouse_phone numeric,
    warehouse_country_code numeric,
    warehouse_phone_vf character varying,
    warehouse_address_line character varying,
    warehouse_pickup_ad character varying,
    created_at timestamp without time zone,
    created_by integer,
    modified_at timestamp without time zone,
    modified_by integer
);
 &   DROP TABLE public.new_seller_details;
       public         heap    postgres    false            �            1259    186483    new_seller_details_id_seq    SEQUENCE     �   CREATE SEQUENCE public.new_seller_details_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 0   DROP SEQUENCE public.new_seller_details_id_seq;
       public          postgres    false    231            �           0    0    new_seller_details_id_seq    SEQUENCE OWNED BY     W   ALTER SEQUENCE public.new_seller_details_id_seq OWNED BY public.new_seller_details.id;
          public          postgres    false    230                       1259    186780    newsletter_management    TABLE     �  CREATE TABLE public.newsletter_management (
    id integer NOT NULL,
    name character varying,
    user_type character varying,
    registered character varying,
    email_id character varying,
    status character varying,
    action character varying,
    created_at timestamp without time zone,
    created_by integer,
    modified_at timestamp without time zone,
    modified_by integer
);
 )   DROP TABLE public.newsletter_management;
       public         heap    postgres    false                       1259    186778    newsletter_management_id_seq    SEQUENCE     �   CREATE SEQUENCE public.newsletter_management_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 3   DROP SEQUENCE public.newsletter_management_id_seq;
       public          postgres    false    286            �           0    0    newsletter_management_id_seq    SEQUENCE OWNED BY     ]   ALTER SEQUENCE public.newsletter_management_id_seq OWNED BY public.newsletter_management.id;
          public          postgres    false    285            "           1259    186802    notification    TABLE     Z  CREATE TABLE public.notification (
    id integer NOT NULL,
    title character varying,
    module character varying,
    execute_on character varying,
    advanced_filter character varying,
    notification_type character varying,
    email_template character varying,
    email_recipient_type character varying,
    email_recipient character varying,
    sms_template character varying,
    sms_recipient_type character varying,
    sms_recipient character varying,
    push_notification_title character varying,
    push_notification character varying,
    push_notification_seller character varying,
    push_notification_seller_name character varying,
    status character varying,
    action character varying,
    created_at timestamp without time zone,
    created_by integer,
    modified_at timestamp without time zone,
    modified_by integer
);
     DROP TABLE public.notification;
       public         heap    postgres    false            !           1259    186800    notification_id_seq    SEQUENCE     �   CREATE SEQUENCE public.notification_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.notification_id_seq;
       public          postgres    false    290            �           0    0    notification_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE public.notification_id_seq OWNED BY public.notification.id;
          public          postgres    false    289                        1259    186791    notification_management    TABLE     8  CREATE TABLE public.notification_management (
    id integer NOT NULL,
    notification_name character varying,
    status character varying,
    action character varying,
    created_at timestamp without time zone,
    created_by integer,
    modified_at timestamp without time zone,
    modified_by integer
);
 +   DROP TABLE public.notification_management;
       public         heap    postgres    false                       1259    186789    notification_management_id_seq    SEQUENCE     �   CREATE SEQUENCE public.notification_management_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 5   DROP SEQUENCE public.notification_management_id_seq;
       public          postgres    false    288            �           0    0    notification_management_id_seq    SEQUENCE OWNED BY     a   ALTER SEQUENCE public.notification_management_id_seq OWNED BY public.notification_management.id;
          public          postgres    false    287            (           1259    186835    offer_management    TABLE     �  CREATE TABLE public.offer_management (
    id integer NOT NULL,
    asking_amount character varying,
    asking_quantity character varying,
    offer_amount character varying,
    offer_quantity character varying,
    remarks character varying,
    user_type character varying,
    offer_date character varying,
    confirmed_date character varying,
    rejected_date character varying,
    rejected_reason character varying,
    offer_validity character varying,
    status character varying,
    action character varying,
    created_at timestamp without time zone,
    created_by integer,
    modified_at timestamp without time zone,
    modified_by integer
);
 $   DROP TABLE public.offer_management;
       public         heap    postgres    false            '           1259    186833    offer_management_id_seq    SEQUENCE     �   CREATE SEQUENCE public.offer_management_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.offer_management_id_seq;
       public          postgres    false    296            �           0    0    offer_management_id_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE public.offer_management_id_seq OWNED BY public.offer_management.id;
          public          postgres    false    295                       1259    186692    order_managemnet    TABLE     �  CREATE TABLE public.order_managemnet (
    id integer NOT NULL,
    order_date character varying,
    order_number character varying,
    order_details character varying,
    order_type character varying,
    order_amount character varying,
    order_status character varying,
    action character varying,
    created_at timestamp without time zone,
    created_by integer,
    modified_at timestamp without time zone,
    modified_by integer
);
 $   DROP TABLE public.order_managemnet;
       public         heap    postgres    false                       1259    186690    order_managemnet_id_seq    SEQUENCE     �   CREATE SEQUENCE public.order_managemnet_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.order_managemnet_id_seq;
       public          postgres    false    270            �           0    0    order_managemnet_id_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE public.order_managemnet_id_seq OWNED BY public.order_managemnet.id;
          public          postgres    false    269                       1259    186703    order_quote    TABLE     <  CREATE TABLE public.order_quote (
    id integer NOT NULL,
    buyer_name character varying,
    buyer_id character varying,
    search_for_product character varying,
    quantity character varying,
    buyer_shipping_address character varying,
    method_of_payment character varying,
    price character varying,
    vat_amount character varying,
    total_price character varying,
    product_type character varying,
    trading_type character varying,
    status character varying,
    seller_code character varying,
    delivery_time character varying,
    discount character varying,
    shipping_charges character varying,
    image character varying,
    brand character varying,
    model character varying,
    shipment_status character varying,
    sku character varying,
    product_id character varying,
    ship_by_date character varying,
    deliver_by_date character varying,
    shipped_by character varying,
    shipped_person character varying,
    shipped_person_contact_no character varying,
    tracking_id character varying,
    shipping_date character varying,
    pickup_date character varying,
    package_type character varying,
    package_dimension character varying,
    package_weight character varying,
    total_shipping_cost character varying,
    seller_note character varying,
    buyer_feedback character varying,
    total_var_amt character varying,
    feedback_buyer character varying,
    action character varying,
    created_at timestamp without time zone,
    created_by integer,
    modified_at timestamp without time zone,
    modified_by integer
);
    DROP TABLE public.order_quote;
       public         heap    postgres    false                       1259    186701    order_quote_id_seq    SEQUENCE     �   CREATE SEQUENCE public.order_quote_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.order_quote_id_seq;
       public          postgres    false    272            �           0    0    order_quote_id_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE public.order_quote_id_seq OWNED BY public.order_quote.id;
          public          postgres    false    271            V           1259    223408    order_status    TABLE       CREATE TABLE public.order_status (
    order_status_id integer NOT NULL,
    order_status_name character varying,
    remarks character varying,
    created_at date,
    created_by integer,
    modified_at date,
    modified_by integer,
    status boolean
);
     DROP TABLE public.order_status;
       public         heap    postgres    false            U           1259    223406     order_status_order_status_id_seq    SEQUENCE     �   CREATE SEQUENCE public.order_status_order_status_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 7   DROP SEQUENCE public.order_status_order_status_id_seq;
       public          postgres    false    342            �           0    0     order_status_order_status_id_seq    SEQUENCE OWNED BY     e   ALTER SEQUENCE public.order_status_order_status_id_seq OWNED BY public.order_status.order_status_id;
          public          postgres    false    341            �            1259    186571 
   parameters    TABLE     s  CREATE TABLE public.parameters (
    id integer NOT NULL,
    brand_name character varying,
    model_name character varying,
    parameter_id character varying,
    parameter_value character varying,
    status character varying,
    created_at timestamp without time zone,
    created_by integer,
    modified_at timestamp without time zone,
    modified_by integer
);
    DROP TABLE public.parameters;
       public         heap    postgres    false            �            1259    186569    parameters_id_seq    SEQUENCE     �   CREATE SEQUENCE public.parameters_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.parameters_id_seq;
       public          postgres    false    248            �           0    0    parameters_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.parameters_id_seq OWNED BY public.parameters.id;
          public          postgres    false    247            &           1259    186824    part_request_unavail    TABLE       CREATE TABLE public.part_request_unavail (
    id integer NOT NULL,
    part_no character varying,
    part_name character varying,
    remarks character varying,
    brand character varying,
    model character varying,
    user_type character varying,
    name character varying,
    requested_date character varying,
    status character varying,
    action character varying,
    created_at timestamp without time zone,
    created_by integer,
    modified_at timestamp without time zone,
    modified_by integer
);
 (   DROP TABLE public.part_request_unavail;
       public         heap    postgres    false            %           1259    186822    part_request_unavail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.part_request_unavail_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 2   DROP SEQUENCE public.part_request_unavail_id_seq;
       public          postgres    false    294            �           0    0    part_request_unavail_id_seq    SEQUENCE OWNED BY     [   ALTER SEQUENCE public.part_request_unavail_id_seq OWNED BY public.part_request_unavail.id;
          public          postgres    false    293                       1259    186626    parts_management    TABLE     �  CREATE TABLE public.parts_management (
    id integer NOT NULL,
    car_id character varying,
    group_id character varying,
    sub_group_id character varying,
    sub_node_id character varying,
    part_number character varying,
    part_name character varying,
    part_erp_id character varying,
    action character varying,
    status character varying,
    created_at timestamp without time zone,
    created_by integer,
    modified_at timestamp without time zone,
    modified_by integer
);
 $   DROP TABLE public.parts_management;
       public         heap    postgres    false                       1259    186624    parts_management_id_seq    SEQUENCE     �   CREATE SEQUENCE public.parts_management_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.parts_management_id_seq;
       public          postgres    false    258            �           0    0    parts_management_id_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE public.parts_management_id_seq OWNED BY public.parts_management.id;
          public          postgres    false    257            X           1259    236452    parts_req_status    TABLE     A  CREATE TABLE public.parts_req_status (
    parts_req_status_id integer NOT NULL,
    parts_req_status_name character varying(50),
    remarks character varying(1000),
    created_at timestamp with time zone,
    created_by integer,
    modified_at timestamp with time zone,
    modified_by integer,
    status boolean
);
 $   DROP TABLE public.parts_req_status;
       public         heap    postgres    false            W           1259    236450 (   parts_req_status_parts_req_status_id_seq    SEQUENCE     �   CREATE SEQUENCE public.parts_req_status_parts_req_status_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ?   DROP SEQUENCE public.parts_req_status_parts_req_status_id_seq;
       public          postgres    false    344            �           0    0 (   parts_req_status_parts_req_status_id_seq    SEQUENCE OWNED BY     u   ALTER SEQUENCE public.parts_req_status_parts_req_status_id_seq OWNED BY public.parts_req_status.parts_req_status_id;
          public          postgres    false    343                       1259    186725    pay_out_schedule_management    TABLE     �  CREATE TABLE public.pay_out_schedule_management (
    id integer NOT NULL,
    pay_out_slot_name character varying,
    no_of_day character varying,
    description character varying,
    action character varying,
    status character varying,
    created_at timestamp without time zone,
    created_by integer,
    modified_at timestamp without time zone,
    modified_by integer
);
 /   DROP TABLE public.pay_out_schedule_management;
       public         heap    postgres    false                       1259    186723 "   pay_out_schedule_management_id_seq    SEQUENCE     �   CREATE SEQUENCE public.pay_out_schedule_management_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 9   DROP SEQUENCE public.pay_out_schedule_management_id_seq;
       public          postgres    false    276            �           0    0 "   pay_out_schedule_management_id_seq    SEQUENCE OWNED BY     i   ALTER SEQUENCE public.pay_out_schedule_management_id_seq OWNED BY public.pay_out_schedule_management.id;
          public          postgres    false    275                       1259    186736    payment_management    TABLE     �  CREATE TABLE public.payment_management (
    id integer NOT NULL,
    seller_name character varying,
    number_of_order character varying,
    total_amount character varying,
    commission_amount character varying,
    seller_amount character varying,
    status character varying,
    action character varying,
    created_at timestamp without time zone,
    created_by integer,
    modified_at timestamp without time zone,
    modified_by integer
);
 &   DROP TABLE public.payment_management;
       public         heap    postgres    false                       1259    186734    payment_management_id_seq    SEQUENCE     �   CREATE SEQUENCE public.payment_management_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 0   DROP SEQUENCE public.payment_management_id_seq;
       public          postgres    false    278            �           0    0    payment_management_id_seq    SEQUENCE OWNED BY     W   ALTER SEQUENCE public.payment_management_id_seq OWNED BY public.payment_management.id;
          public          postgres    false    277            Z           1259    236463    payment_status    TABLE     ;  CREATE TABLE public.payment_status (
    payment_status_id integer NOT NULL,
    payment_status_name character varying(50),
    remarks character varying(1000),
    created_at timestamp with time zone,
    created_by integer,
    modified_at timestamp with time zone,
    modified_by integer,
    status boolean
);
 "   DROP TABLE public.payment_status;
       public         heap    postgres    false            Y           1259    236461 $   payment_status_payment_status_id_seq    SEQUENCE     �   CREATE SEQUENCE public.payment_status_payment_status_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ;   DROP SEQUENCE public.payment_status_payment_status_id_seq;
       public          postgres    false    346            �           0    0 $   payment_status_payment_status_id_seq    SEQUENCE OWNED BY     m   ALTER SEQUENCE public.payment_status_payment_status_id_seq OWNED BY public.payment_status.payment_status_id;
          public          postgres    false    345            �            1259    186364    payout_shed_details    TABLE       CREATE TABLE public.payout_shed_details (
    id integer NOT NULL,
    subscription_name character varying,
    created_at timestamp without time zone,
    created_by integer,
    modified_at timestamp without time zone,
    modified_by integer,
    status boolean
);
 '   DROP TABLE public.payout_shed_details;
       public         heap    postgres    false            �            1259    186362    payout_shed_details_id_seq    SEQUENCE     �   CREATE SEQUENCE public.payout_shed_details_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 1   DROP SEQUENCE public.payout_shed_details_id_seq;
       public          postgres    false    209            �           0    0    payout_shed_details_id_seq    SEQUENCE OWNED BY     Y   ALTER SEQUENCE public.payout_shed_details_id_seq OWNED BY public.payout_shed_details.id;
          public          postgres    false    208            �            1259    186531    product_detail    TABLE     Z  CREATE TABLE public.product_detail (
    id integer NOT NULL,
    product_code integer NOT NULL,
    product_type character varying,
    product_description character varying,
    status character varying,
    created_at timestamp without time zone,
    created_by integer,
    modified_at timestamp without time zone,
    modified_by integer
);
 "   DROP TABLE public.product_detail;
       public         heap    postgres    false                       1259    186659    product_detail_aftermarket    TABLE     �  CREATE TABLE public.product_detail_aftermarket (
    id integer NOT NULL,
    product_id character varying,
    product_erp character varying,
    product_type character varying,
    seller_name character varying,
    aftermarket_no character varying,
    genuine_part_no character varying,
    verify_button character varying,
    barcode_no character varying,
    image character varying,
    weigth character varying,
    dimension_length character varying,
    dimension_width character varying,
    dimension_height character varying,
    trading_type character varying,
    quantity character varying,
    price character varying,
    price_after_sale character varying,
    warehouse_no character varying,
    manufacturer_name character varying,
    product_listing_status character varying,
    action character varying,
    status character varying,
    created_at timestamp without time zone,
    created_by integer,
    modified_at timestamp without time zone,
    modified_by integer
);
 .   DROP TABLE public.product_detail_aftermarket;
       public         heap    postgres    false                       1259    186657 !   product_detail_aftermarket_id_seq    SEQUENCE     �   CREATE SEQUENCE public.product_detail_aftermarket_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 8   DROP SEQUENCE public.product_detail_aftermarket_id_seq;
       public          postgres    false    264            �           0    0 !   product_detail_aftermarket_id_seq    SEQUENCE OWNED BY     g   ALTER SEQUENCE public.product_detail_aftermarket_id_seq OWNED BY public.product_detail_aftermarket.id;
          public          postgres    false    263                       1259    186648    product_detail_genuine    TABLE     �  CREATE TABLE public.product_detail_genuine (
    id integer NOT NULL,
    product_id character varying,
    product_erp character varying,
    product_type character varying,
    seller_name character varying,
    genuine_part_no character varying,
    verify_button character varying,
    image character varying,
    weigth character varying,
    dimension_length character varying,
    dimension_width character varying,
    dimension_height character varying,
    trading_type character varying,
    quantity character varying,
    price character varying,
    price_after_sale character varying,
    warehouse_no character varying,
    manufacturer_name character varying,
    product_listing_status character varying,
    action character varying,
    status character varying,
    created_at timestamp without time zone,
    created_by integer,
    modified_at timestamp without time zone,
    modified_by integer
);
 *   DROP TABLE public.product_detail_genuine;
       public         heap    postgres    false                       1259    186646    product_detail_genuine_id_seq    SEQUENCE     �   CREATE SEQUENCE public.product_detail_genuine_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 4   DROP SEQUENCE public.product_detail_genuine_id_seq;
       public          postgres    false    262            �           0    0    product_detail_genuine_id_seq    SEQUENCE OWNED BY     _   ALTER SEQUENCE public.product_detail_genuine_id_seq OWNED BY public.product_detail_genuine.id;
          public          postgres    false    261            �            1259    186527    product_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.product_detail_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public.product_detail_id_seq;
       public          postgres    false    240            �           0    0    product_detail_id_seq    SEQUENCE OWNED BY     O   ALTER SEQUENCE public.product_detail_id_seq OWNED BY public.product_detail.id;
          public          postgres    false    238            �            1259    186529    product_detail_product_code_seq    SEQUENCE     �   CREATE SEQUENCE public.product_detail_product_code_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 6   DROP SEQUENCE public.product_detail_product_code_seq;
       public          postgres    false    240            �           0    0    product_detail_product_code_seq    SEQUENCE OWNED BY     c   ALTER SEQUENCE public.product_detail_product_code_seq OWNED BY public.product_detail.product_code;
          public          postgres    false    239            
           1259    186670    product_detail_user_part    TABLE     -  CREATE TABLE public.product_detail_user_part (
    id integer NOT NULL,
    product_id character varying,
    product_erp character varying,
    product_type character varying,
    seller_name character varying,
    genuine_part_no character varying,
    verify_button character varying,
    barcode_no character varying,
    number_of_yr_user character varying,
    condition character varying,
    description character varying,
    image character varying,
    weigth character varying,
    dimension_length character varying,
    dimension_width character varying,
    dimension_height character varying,
    trading_type character varying,
    quantity character varying,
    price character varying,
    price_after_sale character varying,
    warehouse_no character varying,
    manufacturer_name character varying,
    product_listing_status character varying,
    action character varying,
    status character varying,
    created_at timestamp without time zone,
    created_by integer,
    modified_at timestamp without time zone,
    modified_by integer
);
 ,   DROP TABLE public.product_detail_user_part;
       public         heap    postgres    false            	           1259    186668    product_detail_user_part_id_seq    SEQUENCE     �   CREATE SEQUENCE public.product_detail_user_part_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 6   DROP SEQUENCE public.product_detail_user_part_id_seq;
       public          postgres    false    266            �           0    0    product_detail_user_part_id_seq    SEQUENCE OWNED BY     c   ALTER SEQUENCE public.product_detail_user_part_id_seq OWNED BY public.product_detail_user_part.id;
          public          postgres    false    265                       1259    186637    product_management    TABLE     �  CREATE TABLE public.product_management (
    id integer NOT NULL,
    seller_name character varying,
    product_type character varying,
    product_name character varying,
    part_number character varying,
    bar_code_number character varying,
    quantity character varying,
    price numeric,
    action character varying,
    status character varying,
    created_at timestamp without time zone,
    created_by integer,
    modified_at timestamp without time zone,
    modified_by integer
);
 &   DROP TABLE public.product_management;
       public         heap    postgres    false                       1259    186635    product_management_id_seq    SEQUENCE     �   CREATE SEQUENCE public.product_management_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 0   DROP SEQUENCE public.product_management_id_seq;
       public          postgres    false    260            �           0    0    product_management_id_seq    SEQUENCE OWNED BY     W   ALTER SEQUENCE public.product_management_id_seq OWNED BY public.product_management.id;
          public          postgres    false    259            �            1259    186430    product_type_details    TABLE       CREATE TABLE public.product_type_details (
    id integer NOT NULL,
    product_type_name character varying,
    created_at timestamp without time zone,
    created_by integer,
    modified_at timestamp without time zone,
    modified_by integer,
    status boolean
);
 (   DROP TABLE public.product_type_details;
       public         heap    postgres    false            �            1259    186428    product_type_details_id_seq    SEQUENCE     �   CREATE SEQUENCE public.product_type_details_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 2   DROP SEQUENCE public.product_type_details_id_seq;
       public          postgres    false    221            �           0    0    product_type_details_id_seq    SEQUENCE OWNED BY     [   ALTER SEQUENCE public.product_type_details_id_seq OWNED BY public.product_type_details.id;
          public          postgres    false    220            \           1259    236474    quote_status    TABLE     5  CREATE TABLE public.quote_status (
    quote_status_id integer NOT NULL,
    quote_status_name character varying(50),
    remarks character varying(1000),
    created_at timestamp with time zone,
    created_by integer,
    modified_at timestamp with time zone,
    modified_by integer,
    status boolean
);
     DROP TABLE public.quote_status;
       public         heap    postgres    false            [           1259    236472     quote_status_quote_status_id_seq    SEQUENCE     �   CREATE SEQUENCE public.quote_status_quote_status_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 7   DROP SEQUENCE public.quote_status_quote_status_id_seq;
       public          postgres    false    348            �           0    0     quote_status_quote_status_id_seq    SEQUENCE OWNED BY     e   ALTER SEQUENCE public.quote_status_quote_status_id_seq OWNED BY public.quote_status.quote_status_id;
          public          postgres    false    347            �            1259    186386    reason_details    TABLE       CREATE TABLE public.reason_details (
    id integer NOT NULL,
    reason_name character varying,
    created_at timestamp without time zone,
    created_by integer,
    modified_at timestamp without time zone,
    modified_by integer,
    status boolean
);
 "   DROP TABLE public.reason_details;
       public         heap    postgres    false            �            1259    186384    reason_details_id_seq    SEQUENCE     �   CREATE SEQUENCE public.reason_details_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public.reason_details_id_seq;
       public          postgres    false    213            �           0    0    reason_details_id_seq    SEQUENCE OWNED BY     O   ALTER SEQUENCE public.reason_details_id_seq OWNED BY public.reason_details.id;
          public          postgres    false    212            ^           1259    236485    registration_status    TABLE     9  CREATE TABLE public.registration_status (
    reg_status_id integer NOT NULL,
    reg_status_name character varying(150),
    remarks character varying(1000),
    created_at timestamp with time zone,
    created_by integer,
    modified_at timestamp with time zone,
    modified_by integer,
    status boolean
);
 '   DROP TABLE public.registration_status;
       public         heap    postgres    false            ]           1259    236483 %   registration_status_reg_status_id_seq    SEQUENCE     �   CREATE SEQUENCE public.registration_status_reg_status_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 <   DROP SEQUENCE public.registration_status_reg_status_id_seq;
       public          postgres    false    350            �           0    0 %   registration_status_reg_status_id_seq    SEQUENCE OWNED BY     o   ALTER SEQUENCE public.registration_status_reg_status_id_seq OWNED BY public.registration_status.reg_status_id;
          public          postgres    false    349            2           1259    186890    revenue_trade_type    TABLE     '  CREATE TABLE public.revenue_trade_type (
    id integer NOT NULL,
    order_brand character varying,
    order_id character varying,
    order_date character varying,
    suborder_id character varying,
    trade_type character varying,
    seller_name character varying,
    total_order_amt character varying,
    total_service_amt character varying,
    status character varying,
    action character varying,
    created_at timestamp without time zone,
    created_by integer,
    modified_at timestamp without time zone,
    modified_by integer
);
 &   DROP TABLE public.revenue_trade_type;
       public         heap    postgres    false            1           1259    186888    revenue_trade_type_id_seq    SEQUENCE     �   CREATE SEQUENCE public.revenue_trade_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 0   DROP SEQUENCE public.revenue_trade_type_id_seq;
       public          postgres    false    306            �           0    0    revenue_trade_type_id_seq    SEQUENCE OWNED BY     W   ALTER SEQUENCE public.revenue_trade_type_id_seq OWNED BY public.revenue_trade_type.id;
          public          postgres    false    305            ,           1259    186857    roles    TABLE     C  CREATE TABLE public.roles (
    id integer NOT NULL,
    role_name character varying,
    status character varying,
    action character varying,
    created_at timestamp without time zone,
    created_by integer,
    modified_at timestamp without time zone,
    modified_by integer,
    role_code character varying(20)
);
    DROP TABLE public.roles;
       public         heap    postgres    false            +           1259    186855    roles_id_seq    SEQUENCE     �   CREATE SEQUENCE public.roles_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.roles_id_seq;
       public          postgres    false    300            �           0    0    roles_id_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE public.roles_id_seq OWNED BY public.roles.id;
          public          postgres    false    299            �            1259    186474    seller_management    TABLE       CREATE TABLE public.seller_management (
    id integer NOT NULL,
    login_id character varying,
    password character varying,
    seller_name character varying,
    email_id character varying,
    trade_type character varying,
    contact_no character varying,
    address character varying,
    status boolean,
    account character varying,
    action character varying,
    created_at timestamp without time zone,
    created_by integer,
    modified_at timestamp without time zone,
    modified_by integer
);
 %   DROP TABLE public.seller_management;
       public         heap    postgres    false            �            1259    186472    seller_management_id_seq    SEQUENCE     �   CREATE SEQUENCE public.seller_management_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 /   DROP SEQUENCE public.seller_management_id_seq;
       public          postgres    false    229            �           0    0    seller_management_id_seq    SEQUENCE OWNED BY     U   ALTER SEQUENCE public.seller_management_id_seq OWNED BY public.seller_management.id;
          public          postgres    false    228            F           1259    195222    services_cms    TABLE     h  CREATE TABLE public.services_cms (
    id integer NOT NULL,
    title character varying(50) NOT NULL,
    content character varying(250) NOT NULL,
    icon character varying(255) NOT NULL,
    status boolean NOT NULL,
    created_at date NOT NULL,
    modified_at date NOT NULL,
    created_by numeric(20,0) NOT NULL,
    modified_by numeric(20,0) NOT NULL
);
     DROP TABLE public.services_cms;
       public         heap    e-parts-admin    false            E           1259    195220    servies_cms_id_seq    SEQUENCE     �   CREATE SEQUENCE public.servies_cms_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.servies_cms_id_seq;
       public          e-parts-admin    false    326            �           0    0    servies_cms_id_seq    SEQUENCE OWNED BY     J   ALTER SEQUENCE public.servies_cms_id_seq OWNED BY public.services_cms.id;
          public          e-parts-admin    false    325            `           1259    236496    shipment_status    TABLE     >  CREATE TABLE public.shipment_status (
    shipment_status_id integer NOT NULL,
    shipment_status_name character varying(50),
    remarks character varying(1000),
    created_at timestamp with time zone,
    created_by integer,
    modified_at timestamp with time zone,
    modified_by integer,
    status boolean
);
 #   DROP TABLE public.shipment_status;
       public         heap    postgres    false            _           1259    236494 "   shipment_status_ship_status_id_seq    SEQUENCE     �   CREATE SEQUENCE public.shipment_status_ship_status_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 9   DROP SEQUENCE public.shipment_status_ship_status_id_seq;
       public          postgres    false    352            �           0    0 "   shipment_status_ship_status_id_seq    SEQUENCE OWNED BY     m   ALTER SEQUENCE public.shipment_status_ship_status_id_seq OWNED BY public.shipment_status.shipment_status_id;
          public          postgres    false    351            f           1259    236586    sms_template    TABLE     a  CREATE TABLE public.sms_template (
    id integer NOT NULL,
    module_id integer,
    title_name character varying(150),
    sms_content character varying(1000),
    action character varying(1000),
    created_at timestamp with time zone,
    created_by integer,
    modified_at timestamp with time zone,
    modified_by integer,
    status boolean
);
     DROP TABLE public.sms_template;
       public         heap    postgres    false            e           1259    236584    sms_template_id_seq    SEQUENCE     �   CREATE SEQUENCE public.sms_template_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.sms_template_id_seq;
       public          postgres    false    358            �           0    0    sms_template_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE public.sms_template_id_seq OWNED BY public.sms_template.id;
          public          postgres    false    357            L           1259    223356    social_media_links_cms    TABLE     0  CREATE TABLE public.social_media_links_cms (
    icon character varying(255) NOT NULL,
    id integer NOT NULL,
    link character varying(500) NOT NULL,
    created_by integer NOT NULL,
    created_at date NOT NULL,
    modified_by integer NOT NULL,
    modified_at date NOT NULL,
    status boolean
);
 *   DROP TABLE public.social_media_links_cms;
       public         heap    e-parts-admin    false            K           1259    223354    social_media_links_id_seq    SEQUENCE     �   CREATE SEQUENCE public.social_media_links_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 0   DROP SEQUENCE public.social_media_links_id_seq;
       public          e-parts-admin    false    332            �           0    0    social_media_links_id_seq    SEQUENCE OWNED BY     [   ALTER SEQUENCE public.social_media_links_id_seq OWNED BY public.social_media_links_cms.id;
          public          e-parts-admin    false    331            �            1259    186419    state_details    TABLE       CREATE TABLE public.state_details (
    id integer NOT NULL,
    country_id integer,
    state_name character varying,
    created_at timestamp without time zone,
    created_by integer,
    modified_at timestamp without time zone,
    modified_by integer,
    status boolean
);
 !   DROP TABLE public.state_details;
       public         heap    postgres    false            �            1259    186417    state_details_id_seq    SEQUENCE     �   CREATE SEQUENCE public.state_details_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.state_details_id_seq;
       public          postgres    false    219            �           0    0    state_details_id_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.state_details_id_seq OWNED BY public.state_details.id;
          public          postgres    false    218            :           1259    186934    static_page    TABLE     �  CREATE TABLE public.static_page (
    id integer NOT NULL,
    static_page_name character varying,
    page_title character varying,
    meta_tag character varying,
    content character varying,
    status character varying,
    action character varying,
    created_at timestamp without time zone,
    created_by integer,
    modified_at timestamp without time zone,
    modified_by integer
);
    DROP TABLE public.static_page;
       public         heap    postgres    false            9           1259    186932    static_page_id_seq    SEQUENCE     �   CREATE SEQUENCE public.static_page_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.static_page_id_seq;
       public          postgres    false    314            �           0    0    static_page_id_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE public.static_page_id_seq OWNED BY public.static_page.id;
          public          postgres    false    313            �            1259    186375    status_details    TABLE     �   CREATE TABLE public.status_details (
    id integer NOT NULL,
    status_name character varying,
    created_at timestamp without time zone,
    created_by integer,
    modified_at timestamp without time zone,
    modified_by integer
);
 "   DROP TABLE public.status_details;
       public         heap    postgres    false            �            1259    186373    status_details_id_seq    SEQUENCE     �   CREATE SEQUENCE public.status_details_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public.status_details_id_seq;
       public          postgres    false    211            �           0    0    status_details_id_seq    SEQUENCE OWNED BY     O   ALTER SEQUENCE public.status_details_id_seq OWNED BY public.status_details.id;
          public          postgres    false    210            �            1259    186604    sub_group_management    TABLE     �  CREATE TABLE public.sub_group_management (
    id integer NOT NULL,
    car_id character varying,
    brand character varying,
    model character varying,
    group_name character varying,
    sub_group_name character varying,
    status character varying,
    created_at timestamp without time zone,
    created_by integer,
    modified_at timestamp without time zone,
    modified_by integer
);
 (   DROP TABLE public.sub_group_management;
       public         heap    postgres    false            �            1259    186602    sub_group_management_id_seq    SEQUENCE     �   CREATE SEQUENCE public.sub_group_management_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 2   DROP SEQUENCE public.sub_group_management_id_seq;
       public          postgres    false    254            �           0    0    sub_group_management_id_seq    SEQUENCE OWNED BY     [   ALTER SEQUENCE public.sub_group_management_id_seq OWNED BY public.sub_group_management.id;
          public          postgres    false    253                        1259    186615    sub_node_management    TABLE     �  CREATE TABLE public.sub_node_management (
    id integer NOT NULL,
    car_id integer,
    brand character varying,
    model character varying,
    sub_node_code character varying,
    sub_node_name character varying,
    status character varying,
    created_at timestamp without time zone,
    created_by integer,
    modified_at timestamp without time zone,
    modified_by integer
);
 '   DROP TABLE public.sub_node_management;
       public         heap    postgres    false            �            1259    186613    sub_node_management_id_seq    SEQUENCE     �   CREATE SEQUENCE public.sub_node_management_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 1   DROP SEQUENCE public.sub_node_management_id_seq;
       public          postgres    false    256                        0    0    sub_node_management_id_seq    SEQUENCE OWNED BY     Y   ALTER SEQUENCE public.sub_node_management_id_seq OWNED BY public.sub_node_management.id;
          public          postgres    false    255            �            1259    186353    subscription_details    TABLE       CREATE TABLE public.subscription_details (
    id integer NOT NULL,
    subscription_name character varying,
    created_at timestamp without time zone,
    created_by integer,
    modified_at timestamp without time zone,
    modified_by integer,
    status boolean
);
 (   DROP TABLE public.subscription_details;
       public         heap    postgres    false            �            1259    186351    subscription_details_id_seq    SEQUENCE     �   CREATE SEQUENCE public.subscription_details_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 2   DROP SEQUENCE public.subscription_details_id_seq;
       public          postgres    false    207                       0    0    subscription_details_id_seq    SEQUENCE OWNED BY     [   ALTER SEQUENCE public.subscription_details_id_seq OWNED BY public.subscription_details.id;
          public          postgres    false    206            *           1259    186846    subscription_management    TABLE     �  CREATE TABLE public.subscription_management (
    id integer NOT NULL,
    subscription_name character varying,
    number_of_users character varying,
    modules_list character varying,
    days character varying,
    price character varying,
    valid_till character varying,
    effective character varying,
    status character varying,
    action character varying,
    created_at timestamp without time zone,
    created_by integer,
    modified_at timestamp without time zone,
    modified_by integer
);
 +   DROP TABLE public.subscription_management;
       public         heap    postgres    false            )           1259    186844    subscription_management_id_seq    SEQUENCE     �   CREATE SEQUENCE public.subscription_management_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 5   DROP SEQUENCE public.subscription_management_id_seq;
       public          postgres    false    298                       0    0    subscription_management_id_seq    SEQUENCE OWNED BY     a   ALTER SEQUENCE public.subscription_management_id_seq OWNED BY public.subscription_management.id;
          public          postgres    false    297            �            1259    186342    super_admin    TABLE       CREATE TABLE public.super_admin (
    id integer NOT NULL,
    login_id character varying,
    password character varying,
    created_at timestamp without time zone,
    created_by integer,
    modified_at timestamp without time zone,
    modified_by integer
);
    DROP TABLE public.super_admin;
       public         heap    postgres    false            �            1259    186340    super_admin_id_seq    SEQUENCE     �   CREATE SEQUENCE public.super_admin_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.super_admin_id_seq;
       public          postgres    false    205                       0    0    super_admin_id_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE public.super_admin_id_seq OWNED BY public.super_admin.id;
          public          postgres    false    204            .           1259    186868    total_order_invoiced    TABLE     �  CREATE TABLE public.total_order_invoiced (
    id integer NOT NULL,
    order_id character varying,
    suborder_id character varying,
    order_date character varying,
    order_status character varying,
    part_no character varying,
    part_name character varying,
    seller_name character varying,
    buyer_name character varying,
    price_of_part character varying,
    discount character varying,
    vat_amount character varying,
    gross_amount character varying,
    status character varying,
    action character varying,
    created_at timestamp without time zone,
    created_by integer,
    modified_at timestamp without time zone,
    modified_by integer
);
 (   DROP TABLE public.total_order_invoiced;
       public         heap    postgres    false            -           1259    186866    total_order_invoiced_id_seq    SEQUENCE     �   CREATE SEQUENCE public.total_order_invoiced_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 2   DROP SEQUENCE public.total_order_invoiced_id_seq;
       public          postgres    false    302                       0    0    total_order_invoiced_id_seq    SEQUENCE OWNED BY     [   ALTER SEQUENCE public.total_order_invoiced_id_seq OWNED BY public.total_order_invoiced.id;
          public          postgres    false    301            0           1259    186879    total_order_refunded    TABLE     �  CREATE TABLE public.total_order_refunded (
    id integer NOT NULL,
    order_id character varying,
    suborder_id character varying,
    order_date character varying,
    order_status character varying,
    part_no character varying,
    part_name character varying,
    seller_name character varying,
    buyer_name character varying,
    price_of_part character varying,
    discount character varying,
    vat_amount character varying,
    gross_amount character varying,
    status character varying,
    action character varying,
    created_at timestamp without time zone,
    created_by integer,
    modified_at timestamp without time zone,
    modified_by integer
);
 (   DROP TABLE public.total_order_refunded;
       public         heap    postgres    false            /           1259    186877    total_order_refunded_id_seq    SEQUENCE     �   CREATE SEQUENCE public.total_order_refunded_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 2   DROP SEQUENCE public.total_order_refunded_id_seq;
       public          postgres    false    304                       0    0    total_order_refunded_id_seq    SEQUENCE OWNED BY     [   ALTER SEQUENCE public.total_order_refunded_id_seq OWNED BY public.total_order_refunded.id;
          public          postgres    false    303            �            1259    186441    trading_type_details    TABLE       CREATE TABLE public.trading_type_details (
    id integer NOT NULL,
    trading_type_name character varying,
    created_at timestamp without time zone,
    created_by integer,
    modified_at timestamp without time zone,
    modified_by integer,
    status boolean
);
 (   DROP TABLE public.trading_type_details;
       public         heap    postgres    false            �            1259    186439    trading_type_details_id_seq    SEQUENCE     �   CREATE SEQUENCE public.trading_type_details_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 2   DROP SEQUENCE public.trading_type_details_id_seq;
       public          postgres    false    223                       0    0    trading_type_details_id_seq    SEQUENCE OWNED BY     [   ALTER SEQUENCE public.trading_type_details_id_seq OWNED BY public.trading_type_details.id;
          public          postgres    false    222            N           1259    223365    useful_links_cms    TABLE     *  CREATE TABLE public.useful_links_cms (
    id integer NOT NULL,
    label character varying(25) NOT NULL,
    link character varying(500) NOT NULL,
    created_by integer NOT NULL,
    created_at date NOT NULL,
    modified_by integer NOT NULL,
    modified_at date NOT NULL,
    status boolean
);
 $   DROP TABLE public.useful_links_cms;
       public         heap    e-parts-admin    false            M           1259    223363    useful_links_cms_id_seq    SEQUENCE     �   CREATE SEQUENCE public.useful_links_cms_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.useful_links_cms_id_seq;
       public          e-parts-admin    false    334                       0    0    useful_links_cms_id_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE public.useful_links_cms_id_seq OWNED BY public.useful_links_cms.id;
          public          e-parts-admin    false    333            d           1259    236521    user_managment    TABLE     �  CREATE TABLE public.user_managment (
    id bigint NOT NULL,
    login_user_type_id integer,
    user_name character varying(150),
    user_email_id character varying(50),
    phone_no character varying(15),
    user_password character varying(250),
    two_way_auth boolean,
    status integer,
    last_login timestamp with time zone,
    created_at timestamp with time zone,
    created_by integer,
    modified_at timestamp with time zone,
    modified_by integer
);
 "   DROP TABLE public.user_managment;
       public         heap    postgres    false            c           1259    236519    user_managment_id_seq    SEQUENCE     ~   CREATE SEQUENCE public.user_managment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public.user_managment_id_seq;
       public          postgres    false    356                       0    0    user_managment_id_seq    SEQUENCE OWNED BY     O   ALTER SEQUENCE public.user_managment_id_seq OWNED BY public.user_managment.id;
          public          postgres    false    355            �            1259    183132    users_login    TABLE     _  CREATE TABLE public.users_login (
    id integer NOT NULL,
    user_id character varying,
    email character varying,
    password character varying,
    username character varying,
    role character varying,
    created_at timestamp without time zone,
    created_by integer,
    modified_at timestamp without time zone,
    modified_by integer
);
    DROP TABLE public.users_login;
       public         heap    postgres    false            �            1259    183130    users_login_id_seq    SEQUENCE     �   CREATE SEQUENCE public.users_login_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.users_login_id_seq;
       public          postgres    false    203            	           0    0    users_login_id_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE public.users_login_id_seq OWNED BY public.users_login.id;
          public          postgres    false    202                       1259    186747    vin_history    TABLE     	  CREATE TABLE public.vin_history (
    id integer NOT NULL,
    vin_number character varying,
    vehicle_identified character varying,
    brand character varying,
    model character varying,
    user_type character varying,
    name character varying,
    region character varying,
    searched_date character varying,
    status character varying,
    action character varying,
    created_at timestamp without time zone,
    created_by integer,
    modified_at timestamp without time zone,
    modified_by integer
);
    DROP TABLE public.vin_history;
       public         heap    postgres    false                       1259    186745    vin_history_id_seq    SEQUENCE     �   CREATE SEQUENCE public.vin_history_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.vin_history_id_seq;
       public          postgres    false    280            
           0    0    vin_history_id_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE public.vin_history_id_seq OWNED BY public.vin_history.id;
          public          postgres    false    279            �           2604    186816    Rating_review id    DEFAULT     x   ALTER TABLE ONLY public."Rating_review" ALTER COLUMN id SET DEFAULT nextval('public."Rating_review_id_seq"'::regclass);
 A   ALTER TABLE public."Rating_review" ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    291    292    292            �           2604    186521    application_user id    DEFAULT     z   ALTER TABLE ONLY public.application_user ALTER COLUMN id SET DEFAULT nextval('public.application_user_id_seq'::regclass);
 B   ALTER TABLE public.application_user ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    237    236    237            �           2604    186926    audit_trail id    DEFAULT     p   ALTER TABLE ONLY public.audit_trail ALTER COLUMN id SET DEFAULT nextval('public.audit_trail_id_seq'::regclass);
 =   ALTER TABLE public.audit_trail ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    311    312    312            �           2604    195197    banner_cms id    DEFAULT     n   ALTER TABLE ONLY public.banner_cms ALTER COLUMN id SET DEFAULT nextval('public.banner_cms_id_seq'::regclass);
 <   ALTER TABLE public.banner_cms ALTER COLUMN id DROP DEFAULT;
       public          e-parts-admin    false    321    322    322            �           2604    186548 	   brands id    DEFAULT     f   ALTER TABLE ONLY public.brands ALTER COLUMN id SET DEFAULT nextval('public.brands_id_seq'::regclass);
 8   ALTER TABLE public.brands ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    241    243    243            �           2604    186549    brands brand_id    DEFAULT     r   ALTER TABLE ONLY public.brands ALTER COLUMN brand_id SET DEFAULT nextval('public.brands_brand_id_seq'::regclass);
 >   ALTER TABLE public.brands ALTER COLUMN brand_id DROP DEFAULT;
       public          postgres    false    242    243    243            �           2604    186455    buyer_management id    DEFAULT     z   ALTER TABLE ONLY public.buyer_management ALTER COLUMN id SET DEFAULT nextval('public.buyer_management_id_seq'::regclass);
 B   ALTER TABLE public.buyer_management ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    224    225    225            �           2604    186772    buyer_saved_quote id    DEFAULT     |   ALTER TABLE ONLY public.buyer_saved_quote ALTER COLUMN id SET DEFAULT nextval('public.buyer_saved_quote_id_seq'::regclass);
 C   ALTER TABLE public.buyer_saved_quote ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    284    283    284            �           2604    211640    buyer_seller_cms id    DEFAULT     z   ALTER TABLE ONLY public.buyer_seller_cms ALTER COLUMN id SET DEFAULT nextval('public.buyer_seller_cms_id_seq'::regclass);
 B   ALTER TABLE public.buyer_seller_cms ALTER COLUMN id DROP DEFAULT;
       public          e-parts-admin    false    330    329    330            �           2604    186717    car_management id    DEFAULT     v   ALTER TABLE ONLY public.car_management ALTER COLUMN id SET DEFAULT nextval('public.car_management_id_seq'::regclass);
 @   ALTER TABLE public.car_management ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    274    273    274            �           2604    186585    cars id    DEFAULT     b   ALTER TABLE ONLY public.cars ALTER COLUMN id SET DEFAULT nextval('public.cars_id_seq'::regclass);
 6   ALTER TABLE public.cars ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    249    250    250            �           2604    186761    category_request_management id    DEFAULT     �   ALTER TABLE ONLY public.category_request_management ALTER COLUMN id SET DEFAULT nextval('public.category_request_management_id_seq'::regclass);
 M   ALTER TABLE public.category_request_management ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    281    282    282            �           2604    223387 )   category_request_status cat_req_status_id    DEFAULT     �   ALTER TABLE ONLY public.category_request_status ALTER COLUMN cat_req_status_id SET DEFAULT nextval('public.category_request_status_cat_req_status_id_seq'::regclass);
 X   ALTER TABLE public.category_request_status ALTER COLUMN cat_req_status_id DROP DEFAULT;
       public          postgres    false    338    337    338            �           2604    186974    city_details id    DEFAULT     r   ALTER TABLE ONLY public.city_details ALTER COLUMN id SET DEFAULT nextval('public.city_details_id_seq'::regclass);
 >   ALTER TABLE public.city_details ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    318    317    318            �           2604    186684    commission_managemnet id    DEFAULT     �   ALTER TABLE ONLY public.commission_managemnet ALTER COLUMN id SET DEFAULT nextval('public.commission_managemnet_id_seq'::regclass);
 G   ALTER TABLE public.commission_managemnet ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    268    267    268            �           2604    236623    configuration_details id    DEFAULT     �   ALTER TABLE ONLY public.configuration_details ALTER COLUMN id SET DEFAULT nextval('public.configuration_details_id_seq'::regclass);
 G   ALTER TABLE public.configuration_details ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    359    360    360            �           2604    186411    country_details id    DEFAULT     x   ALTER TABLE ONLY public.country_details ALTER COLUMN id SET DEFAULT nextval('public.country_details_id_seq'::regclass);
 A   ALTER TABLE public.country_details ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    217    216    217            �           2604    186400    currency_details id    DEFAULT     z   ALTER TABLE ONLY public.currency_details ALTER COLUMN id SET DEFAULT nextval('public.currency_details_id_seq'::regclass);
 B   ALTER TABLE public.currency_details ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    215    214    215            �           2604    186948    email_template id    DEFAULT     r   ALTER TABLE ONLY public.email_template ALTER COLUMN id SET DEFAULT nextval('public.email_temp_id_seq'::regclass);
 @   ALTER TABLE public.email_template ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    316    315    316            �           2604    195208    features_cms id    DEFAULT     r   ALTER TABLE ONLY public.features_cms ALTER COLUMN id SET DEFAULT nextval('public.features_cms_id_seq'::regclass);
 >   ALTER TABLE public.features_cms ALTER COLUMN id DROP DEFAULT;
       public          e-parts-admin    false    323    324    324            �           2604    186596    group_management id    DEFAULT     z   ALTER TABLE ONLY public.group_management ALTER COLUMN id SET DEFAULT nextval('public.group_management_id_seq'::regclass);
 B   ALTER TABLE public.group_management ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    251    252    252            �           2604    186915    help_management id    DEFAULT     x   ALTER TABLE ONLY public.help_management ALTER COLUMN id SET DEFAULT nextval('public.help_management_id_seq'::regclass);
 A   ALTER TABLE public.help_management ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    310    309    310            �           2604    211629    hero_section_cms id    DEFAULT     z   ALTER TABLE ONLY public.hero_section_cms ALTER COLUMN id SET DEFAULT nextval('public.hero_section_cms_id_seq'::regclass);
 B   ALTER TABLE public.hero_section_cms ALTER COLUMN id DROP DEFAULT;
       public          e-parts-admin    false    328    327    328            �           2604    195188    info_cms id    DEFAULT     j   ALTER TABLE ONLY public.info_cms ALTER COLUMN id SET DEFAULT nextval('public.info_cms_id_seq'::regclass);
 :   ALTER TABLE public.info_cms ALTER COLUMN id DROP DEFAULT;
       public          e-parts-admin    false    320    319            �           2604    186510    intermediator id    DEFAULT     t   ALTER TABLE ONLY public.intermediator ALTER COLUMN id SET DEFAULT nextval('public.intermediator_id_seq'::regclass);
 ?   ALTER TABLE public.intermediator ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    235    234    235            �           2604    186499    intermediator_management id    DEFAULT     �   ALTER TABLE ONLY public.intermediator_management ALTER COLUMN id SET DEFAULT nextval('public.intermediator_management_id_seq'::regclass);
 J   ALTER TABLE public.intermediator_management ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    232    233    233            �           2604    186904    inventory_manangement id    DEFAULT     �   ALTER TABLE ONLY public.inventory_manangement ALTER COLUMN id SET DEFAULT nextval('public.inventory_manangement_id_seq'::regclass);
 G   ALTER TABLE public.inventory_manangement ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    308    307    308            �           2604    236516 "   login_user_type login_user_type_id    DEFAULT     �   ALTER TABLE ONLY public.login_user_type ALTER COLUMN login_user_type_id SET DEFAULT nextval('public."Login_user_type_Login_user_type_id_seq"'::regclass);
 Q   ALTER TABLE public.login_user_type ALTER COLUMN login_user_type_id DROP DEFAULT;
       public          postgres    false    353    354    354            �           2604    223400 &   make_offer_status make_offer_status_id    DEFAULT     �   ALTER TABLE ONLY public.make_offer_status ALTER COLUMN make_offer_status_id SET DEFAULT nextval('public.make_offer_status_make_offer_status_id_seq'::regclass);
 U   ALTER TABLE public.make_offer_status ALTER COLUMN make_offer_status_id DROP DEFAULT;
       public          postgres    false    340    339    340            �           2604    236637    mobile_section_cms id    DEFAULT     z   ALTER TABLE ONLY public.mobile_section_cms ALTER COLUMN id SET DEFAULT nextval('public.mobile_section_id_seq'::regclass);
 D   ALTER TABLE public.mobile_section_cms ALTER COLUMN id DROP DEFAULT;
       public          e-parts-admin    false    362    361    362            �           2604    186562 	   models id    DEFAULT     f   ALTER TABLE ONLY public.models ALTER COLUMN id SET DEFAULT nextval('public.models_id_seq'::regclass);
 8   ALTER TABLE public.models ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    246    244    246            �           2604    186563    models brand_name    DEFAULT     v   ALTER TABLE ONLY public.models ALTER COLUMN brand_name SET DEFAULT nextval('public.models_brand_name_seq'::regclass);
 @   ALTER TABLE public.models ALTER COLUMN brand_name DROP DEFAULT;
       public          postgres    false    246    245    246            �           2604    223379    nav_links_cms id    DEFAULT     t   ALTER TABLE ONLY public.nav_links_cms ALTER COLUMN id SET DEFAULT nextval('public.nav_links_cms_id_seq'::regclass);
 ?   ALTER TABLE public.nav_links_cms ALTER COLUMN id DROP DEFAULT;
       public          e-parts-admin    false    335    336    336            �           2604    186466    new_buyer_details id    DEFAULT     |   ALTER TABLE ONLY public.new_buyer_details ALTER COLUMN id SET DEFAULT nextval('public.new_buyer_details_id_seq'::regclass);
 C   ALTER TABLE public.new_buyer_details ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    226    227    227            �           2604    186488    new_seller_details id    DEFAULT     ~   ALTER TABLE ONLY public.new_seller_details ALTER COLUMN id SET DEFAULT nextval('public.new_seller_details_id_seq'::regclass);
 D   ALTER TABLE public.new_seller_details ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    231    230    231            �           2604    186783    newsletter_management id    DEFAULT     �   ALTER TABLE ONLY public.newsletter_management ALTER COLUMN id SET DEFAULT nextval('public.newsletter_management_id_seq'::regclass);
 G   ALTER TABLE public.newsletter_management ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    286    285    286            �           2604    186805    notification id    DEFAULT     r   ALTER TABLE ONLY public.notification ALTER COLUMN id SET DEFAULT nextval('public.notification_id_seq'::regclass);
 >   ALTER TABLE public.notification ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    290    289    290            �           2604    186794    notification_management id    DEFAULT     �   ALTER TABLE ONLY public.notification_management ALTER COLUMN id SET DEFAULT nextval('public.notification_management_id_seq'::regclass);
 I   ALTER TABLE public.notification_management ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    287    288    288            �           2604    186838    offer_management id    DEFAULT     z   ALTER TABLE ONLY public.offer_management ALTER COLUMN id SET DEFAULT nextval('public.offer_management_id_seq'::regclass);
 B   ALTER TABLE public.offer_management ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    296    295    296            �           2604    186695    order_managemnet id    DEFAULT     z   ALTER TABLE ONLY public.order_managemnet ALTER COLUMN id SET DEFAULT nextval('public.order_managemnet_id_seq'::regclass);
 B   ALTER TABLE public.order_managemnet ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    270    269    270            �           2604    186706    order_quote id    DEFAULT     p   ALTER TABLE ONLY public.order_quote ALTER COLUMN id SET DEFAULT nextval('public.order_quote_id_seq'::regclass);
 =   ALTER TABLE public.order_quote ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    272    271    272            �           2604    223411    order_status order_status_id    DEFAULT     �   ALTER TABLE ONLY public.order_status ALTER COLUMN order_status_id SET DEFAULT nextval('public.order_status_order_status_id_seq'::regclass);
 K   ALTER TABLE public.order_status ALTER COLUMN order_status_id DROP DEFAULT;
       public          postgres    false    341    342    342            �           2604    186574    parameters id    DEFAULT     n   ALTER TABLE ONLY public.parameters ALTER COLUMN id SET DEFAULT nextval('public.parameters_id_seq'::regclass);
 <   ALTER TABLE public.parameters ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    247    248    248            �           2604    186827    part_request_unavail id    DEFAULT     �   ALTER TABLE ONLY public.part_request_unavail ALTER COLUMN id SET DEFAULT nextval('public.part_request_unavail_id_seq'::regclass);
 F   ALTER TABLE public.part_request_unavail ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    294    293    294            �           2604    186629    parts_management id    DEFAULT     z   ALTER TABLE ONLY public.parts_management ALTER COLUMN id SET DEFAULT nextval('public.parts_management_id_seq'::regclass);
 B   ALTER TABLE public.parts_management ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    258    257    258            �           2604    236455 $   parts_req_status parts_req_status_id    DEFAULT     �   ALTER TABLE ONLY public.parts_req_status ALTER COLUMN parts_req_status_id SET DEFAULT nextval('public.parts_req_status_parts_req_status_id_seq'::regclass);
 S   ALTER TABLE public.parts_req_status ALTER COLUMN parts_req_status_id DROP DEFAULT;
       public          postgres    false    344    343    344            �           2604    186728    pay_out_schedule_management id    DEFAULT     �   ALTER TABLE ONLY public.pay_out_schedule_management ALTER COLUMN id SET DEFAULT nextval('public.pay_out_schedule_management_id_seq'::regclass);
 M   ALTER TABLE public.pay_out_schedule_management ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    275    276    276            �           2604    186739    payment_management id    DEFAULT     ~   ALTER TABLE ONLY public.payment_management ALTER COLUMN id SET DEFAULT nextval('public.payment_management_id_seq'::regclass);
 D   ALTER TABLE public.payment_management ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    277    278    278            �           2604    236466     payment_status payment_status_id    DEFAULT     �   ALTER TABLE ONLY public.payment_status ALTER COLUMN payment_status_id SET DEFAULT nextval('public.payment_status_payment_status_id_seq'::regclass);
 O   ALTER TABLE public.payment_status ALTER COLUMN payment_status_id DROP DEFAULT;
       public          postgres    false    346    345    346            �           2604    186367    payout_shed_details id    DEFAULT     �   ALTER TABLE ONLY public.payout_shed_details ALTER COLUMN id SET DEFAULT nextval('public.payout_shed_details_id_seq'::regclass);
 E   ALTER TABLE public.payout_shed_details ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    209    208    209            �           2604    186534    product_detail id    DEFAULT     v   ALTER TABLE ONLY public.product_detail ALTER COLUMN id SET DEFAULT nextval('public.product_detail_id_seq'::regclass);
 @   ALTER TABLE public.product_detail ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    240    238    240            �           2604    186535    product_detail product_code    DEFAULT     �   ALTER TABLE ONLY public.product_detail ALTER COLUMN product_code SET DEFAULT nextval('public.product_detail_product_code_seq'::regclass);
 J   ALTER TABLE public.product_detail ALTER COLUMN product_code DROP DEFAULT;
       public          postgres    false    239    240    240            �           2604    186662    product_detail_aftermarket id    DEFAULT     �   ALTER TABLE ONLY public.product_detail_aftermarket ALTER COLUMN id SET DEFAULT nextval('public.product_detail_aftermarket_id_seq'::regclass);
 L   ALTER TABLE public.product_detail_aftermarket ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    264    263    264            �           2604    186651    product_detail_genuine id    DEFAULT     �   ALTER TABLE ONLY public.product_detail_genuine ALTER COLUMN id SET DEFAULT nextval('public.product_detail_genuine_id_seq'::regclass);
 H   ALTER TABLE public.product_detail_genuine ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    261    262    262            �           2604    186673    product_detail_user_part id    DEFAULT     �   ALTER TABLE ONLY public.product_detail_user_part ALTER COLUMN id SET DEFAULT nextval('public.product_detail_user_part_id_seq'::regclass);
 J   ALTER TABLE public.product_detail_user_part ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    266    265    266            �           2604    186640    product_management id    DEFAULT     ~   ALTER TABLE ONLY public.product_management ALTER COLUMN id SET DEFAULT nextval('public.product_management_id_seq'::regclass);
 D   ALTER TABLE public.product_management ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    259    260    260            �           2604    186433    product_type_details id    DEFAULT     �   ALTER TABLE ONLY public.product_type_details ALTER COLUMN id SET DEFAULT nextval('public.product_type_details_id_seq'::regclass);
 F   ALTER TABLE public.product_type_details ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    220    221    221            �           2604    236477    quote_status quote_status_id    DEFAULT     �   ALTER TABLE ONLY public.quote_status ALTER COLUMN quote_status_id SET DEFAULT nextval('public.quote_status_quote_status_id_seq'::regclass);
 K   ALTER TABLE public.quote_status ALTER COLUMN quote_status_id DROP DEFAULT;
       public          postgres    false    348    347    348            �           2604    186389    reason_details id    DEFAULT     v   ALTER TABLE ONLY public.reason_details ALTER COLUMN id SET DEFAULT nextval('public.reason_details_id_seq'::regclass);
 @   ALTER TABLE public.reason_details ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    213    212    213            �           2604    236488 !   registration_status reg_status_id    DEFAULT     �   ALTER TABLE ONLY public.registration_status ALTER COLUMN reg_status_id SET DEFAULT nextval('public.registration_status_reg_status_id_seq'::regclass);
 P   ALTER TABLE public.registration_status ALTER COLUMN reg_status_id DROP DEFAULT;
       public          postgres    false    350    349    350            �           2604    186893    revenue_trade_type id    DEFAULT     ~   ALTER TABLE ONLY public.revenue_trade_type ALTER COLUMN id SET DEFAULT nextval('public.revenue_trade_type_id_seq'::regclass);
 D   ALTER TABLE public.revenue_trade_type ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    306    305    306            �           2604    186860    roles id    DEFAULT     d   ALTER TABLE ONLY public.roles ALTER COLUMN id SET DEFAULT nextval('public.roles_id_seq'::regclass);
 7   ALTER TABLE public.roles ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    299    300    300            �           2604    186477    seller_management id    DEFAULT     |   ALTER TABLE ONLY public.seller_management ALTER COLUMN id SET DEFAULT nextval('public.seller_management_id_seq'::regclass);
 C   ALTER TABLE public.seller_management ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    228    229    229            �           2604    195225    services_cms id    DEFAULT     q   ALTER TABLE ONLY public.services_cms ALTER COLUMN id SET DEFAULT nextval('public.servies_cms_id_seq'::regclass);
 >   ALTER TABLE public.services_cms ALTER COLUMN id DROP DEFAULT;
       public          e-parts-admin    false    325    326    326            �           2604    236499 "   shipment_status shipment_status_id    DEFAULT     �   ALTER TABLE ONLY public.shipment_status ALTER COLUMN shipment_status_id SET DEFAULT nextval('public.shipment_status_ship_status_id_seq'::regclass);
 Q   ALTER TABLE public.shipment_status ALTER COLUMN shipment_status_id DROP DEFAULT;
       public          postgres    false    351    352    352            �           2604    236589    sms_template id    DEFAULT     r   ALTER TABLE ONLY public.sms_template ALTER COLUMN id SET DEFAULT nextval('public.sms_template_id_seq'::regclass);
 >   ALTER TABLE public.sms_template ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    358    357    358            �           2604    223359    social_media_links_cms id    DEFAULT     �   ALTER TABLE ONLY public.social_media_links_cms ALTER COLUMN id SET DEFAULT nextval('public.social_media_links_id_seq'::regclass);
 H   ALTER TABLE public.social_media_links_cms ALTER COLUMN id DROP DEFAULT;
       public          e-parts-admin    false    332    331    332            �           2604    186422    state_details id    DEFAULT     t   ALTER TABLE ONLY public.state_details ALTER COLUMN id SET DEFAULT nextval('public.state_details_id_seq'::regclass);
 ?   ALTER TABLE public.state_details ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    218    219    219            �           2604    186937    static_page id    DEFAULT     p   ALTER TABLE ONLY public.static_page ALTER COLUMN id SET DEFAULT nextval('public.static_page_id_seq'::regclass);
 =   ALTER TABLE public.static_page ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    314    313    314            �           2604    186378    status_details id    DEFAULT     v   ALTER TABLE ONLY public.status_details ALTER COLUMN id SET DEFAULT nextval('public.status_details_id_seq'::regclass);
 @   ALTER TABLE public.status_details ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    211    210    211            �           2604    186607    sub_group_management id    DEFAULT     �   ALTER TABLE ONLY public.sub_group_management ALTER COLUMN id SET DEFAULT nextval('public.sub_group_management_id_seq'::regclass);
 F   ALTER TABLE public.sub_group_management ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    254    253    254            �           2604    186618    sub_node_management id    DEFAULT     �   ALTER TABLE ONLY public.sub_node_management ALTER COLUMN id SET DEFAULT nextval('public.sub_node_management_id_seq'::regclass);
 E   ALTER TABLE public.sub_node_management ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    256    255    256            �           2604    186356    subscription_details id    DEFAULT     �   ALTER TABLE ONLY public.subscription_details ALTER COLUMN id SET DEFAULT nextval('public.subscription_details_id_seq'::regclass);
 F   ALTER TABLE public.subscription_details ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    207    206    207            �           2604    186849    subscription_management id    DEFAULT     �   ALTER TABLE ONLY public.subscription_management ALTER COLUMN id SET DEFAULT nextval('public.subscription_management_id_seq'::regclass);
 I   ALTER TABLE public.subscription_management ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    297    298    298            �           2604    186345    super_admin id    DEFAULT     p   ALTER TABLE ONLY public.super_admin ALTER COLUMN id SET DEFAULT nextval('public.super_admin_id_seq'::regclass);
 =   ALTER TABLE public.super_admin ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    205    204    205            �           2604    186871    total_order_invoiced id    DEFAULT     �   ALTER TABLE ONLY public.total_order_invoiced ALTER COLUMN id SET DEFAULT nextval('public.total_order_invoiced_id_seq'::regclass);
 F   ALTER TABLE public.total_order_invoiced ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    302    301    302            �           2604    186882    total_order_refunded id    DEFAULT     �   ALTER TABLE ONLY public.total_order_refunded ALTER COLUMN id SET DEFAULT nextval('public.total_order_refunded_id_seq'::regclass);
 F   ALTER TABLE public.total_order_refunded ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    304    303    304            �           2604    186444    trading_type_details id    DEFAULT     �   ALTER TABLE ONLY public.trading_type_details ALTER COLUMN id SET DEFAULT nextval('public.trading_type_details_id_seq'::regclass);
 F   ALTER TABLE public.trading_type_details ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    222    223    223            �           2604    223368    useful_links_cms id    DEFAULT     z   ALTER TABLE ONLY public.useful_links_cms ALTER COLUMN id SET DEFAULT nextval('public.useful_links_cms_id_seq'::regclass);
 B   ALTER TABLE public.useful_links_cms ALTER COLUMN id DROP DEFAULT;
       public          e-parts-admin    false    333    334    334            �           2604    236524    user_managment id    DEFAULT     v   ALTER TABLE ONLY public.user_managment ALTER COLUMN id SET DEFAULT nextval('public.user_managment_id_seq'::regclass);
 @   ALTER TABLE public.user_managment ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    356    355    356            �           2604    183135    users_login id    DEFAULT     p   ALTER TABLE ONLY public.users_login ALTER COLUMN id SET DEFAULT nextval('public.users_login_id_seq'::regclass);
 =   ALTER TABLE public.users_login ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    203    202    203            �           2604    186750    vin_history id    DEFAULT     p   ALTER TABLE ONLY public.vin_history ALTER COLUMN id SET DEFAULT nextval('public.vin_history_id_seq'::regclass);
 =   ALTER TABLE public.vin_history ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    279    280    280            l          0    186813    Rating_review 
   TABLE DATA           �   COPY public."Rating_review" (id, seller_name, rate_seller, review_seller, status, action, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    292   I*      5          0    186518    application_user 
   TABLE DATA           �   COPY public.application_user (id, name, phone_no, email_id, user_type, password, designation, status, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    237   f*      �          0    186923    audit_trail 
   TABLE DATA           �   COPY public.audit_trail (id, module_name, field_name, value, user_name, time_stamp, status, action, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    312   �*      �          0    195194 
   banner_cms 
   TABLE DATA           y   COPY public.banner_cms (title, content, image, status, created_at, modified_at, created_by, modified_by, id) FROM stdin;
    public          e-parts-admin    false    322   �*      ;          0    186545    brands 
   TABLE DATA           �   COPY public.brands (id, brand_id, brand_name, brand_api_id, brand_image, status, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    243   ]+      )          0    186452    buyer_management 
   TABLE DATA           �   COPY public.buyer_management (id, login_id, password, buyer_name, email_id, contact_no, address, status, action, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    225   z+      d          0    186769    buyer_saved_quote 
   TABLE DATA           R  COPY public.buyer_saved_quote (id, buyer_id, buyer_name, quote_id, customer_name, email, phone, items, status_of_quote, quantity, price, margin_per, price_with_margin, tax_code, tax_amount, final_price, cusotmer_detail_name, address, car_details_brand, model, status, action, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    284   �+      �          0    211637    buyer_seller_cms 
   TABLE DATA           �   COPY public.buyer_seller_cms (id, buyer_title, buyer_content, buyer_link, buyer_image, seller_title, seller_content, seller_link, seller_image, created_by, modified_at, modified_by, created_at) FROM stdin;
    public          e-parts-admin    false    330   �+      Z          0    186714    car_management 
   TABLE DATA           �   COPY public.car_management (id, buyer_name, car_id, brand, model, vin_no, action, status, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    274   �,      B          0    186582    cars 
   TABLE DATA           �   COPY public.cars (id, car_id, brand_name, model_name, parameters_name, parameters_value, status, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    250   �,      b          0    186758    category_request_management 
   TABLE DATA           �   COPY public.category_request_management (id, category_code, category_name, description, reason_for_reject, status, action, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    282   �,      �          0    223384    category_request_status 
   TABLE DATA           �   COPY public.category_request_status (cat_req_status_id, cat_req_status_name, remarks, created_at, created_by, modified_at, modified_by, status) FROM stdin;
    public          postgres    false    338   �,      �          0    186971    city_details 
   TABLE DATA           }   COPY public.city_details (id, country_id, state_id, city_name, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    318   ;-      T          0    186681    commission_managemnet 
   TABLE DATA           �   COPY public.commission_managemnet (id, product_type, commission_precentage, effective_date, action, status, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    268   �-      �          0    236620    configuration_details 
   TABLE DATA             COPY public.configuration_details (id, config_name, "config_URL", "Config_username", config_password, config_port, config_parameter, "config_MobileNumber", config_mtype, config_message, config_secretkey, status, remarks, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    360   �-      !          0    186408    country_details 
   TABLE DATA           �   COPY public.country_details (id, country_name, created_at, created_by, modified_at, modified_by, status, country_code) FROM stdin;
    public          postgres    false    217   �-                0    186397    currency_details 
   TABLE DATA           w   COPY public.currency_details (id, currency_name, created_at, created_by, modified_at, modified_by, status) FROM stdin;
    public          postgres    false    215   �.      �          0    186945    email_template 
   TABLE DATA           �   COPY public.email_template (id, title, email_type, encoding_type, iso_code, description, subject, email_content, attachment, action, created_at, created_by, modified_at, modified_by, status) FROM stdin;
    public          postgres    false    316   H/      �          0    195205    features_cms 
   TABLE DATA           z   COPY public.features_cms (id, title, content, icon, status, created_at, modified_at, created_by, modified_by) FROM stdin;
    public          e-parts-admin    false    324   e/      D          0    186593    group_management 
   TABLE DATA           �   COPY public.group_management (id, car_id, brand, model, group_code, group_name, status, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    252   L0      ~          0    186912    help_management 
   TABLE DATA           �   COPY public.help_management (id, code, subject_name, status, action, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    310   i0      �          0    211626    hero_section_cms 
   TABLE DATA           �   COPY public.hero_section_cms (title, sub_title, content, link, id, created_by, created_at, modified_by, modified_at) FROM stdin;
    public          e-parts-admin    false    328   �0      �          0    195183    info_cms 
   TABLE DATA           �   COPY public.info_cms (contact, email, created_at, modified_at, created_by, modified_by, id, company_name, description, logo, playstore_link, applestore_link) FROM stdin;
    public          e-parts-admin    false    319   [2      3          0    186507    intermediator 
   TABLE DATA           �   COPY public.intermediator (id, firstname, lastname, user_name, email_id, passwork, role, phone_number, mobile_number, status, image, country, language, address, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    235   �3      1          0    186496    intermediator_management 
   TABLE DATA           �   COPY public.intermediator_management (id, intermdidator_name, email_id, contact_no, role, status, action, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    233   �3      |          0    186901    inventory_manangement 
   TABLE DATA           �   COPY public.inventory_manangement (id, ware_house, seller_name, product, price, onhand, e_part_qnty, buffer_qnty, re_order_level, threshold_limit, sold, balance, bin_loc, status, action, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    308   �3      �          0    236513    login_user_type 
   TABLE DATA           �   COPY public.login_user_type (login_user_type_id, login_user_type_name, status, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    354   �3      �          0    223397    make_offer_status 
   TABLE DATA           �   COPY public.make_offer_status (make_offer_status_id, offer_status_name, remarks, created_at, created_by, modified_at, modified_by, status) FROM stdin;
    public          postgres    false    340   \4      �          0    236634    mobile_section_cms 
   TABLE DATA           �   COPY public.mobile_section_cms (title, id, content, status, created_at, created_by, modified_at, image, modified_by) FROM stdin;
    public          e-parts-admin    false    362   �4      >          0    186559    models 
   TABLE DATA           �   COPY public.models (id, brand_name, model_id, model_name, model_api_id, model_image, status, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    246   L5      �          0    223376    nav_links_cms 
   TABLE DATA           r   COPY public.nav_links_cms (id, label, link, created_by, created_at, modified_by, modified_at, status) FROM stdin;
    public          e-parts-admin    false    336   i5      +          0    186463    new_buyer_details 
   TABLE DATA           ^  COPY public.new_buyer_details (id, login_id, password, business_name, business_com_logo, business_subscrip_plan, business_status, billing_country, billing_state, billing_city, b_address1, b_address2, b_displayname, b_phonenumber, b_country_code, shipping_country, shipping_state, shipping_city, s_address1, s_address2, s_displayname, s_phonenumber, s_country_code, s_phoneno_verified, pickup_address, payment_info, product_type, trading_type, iv_locate, iv_licenese, iv_national, iv_exp_date, iv_country_issue, iv_f_name, iv_m_name, iv_l_name, iv_dob, iv_r_bussiness_name, iv_licenese_no, iv_r_address, id_doc, bus_doc_license, bus_doc_r_status, bus_doc_fail_reason, vat_info_number, vat_r_certificate, bank_benefi_name, bank_name, bank_ac_no, back_iban, swift_code, bank_currency, bank_browse_file, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    227   �5      /          0    186485    new_seller_details 
   TABLE DATA           W  COPY public.new_seller_details (id, login_id, password, b_multi_branch, business_name, business_com_logo, business_subscrip_plan, payout_shed, business_status, billing_country, billing_state, billing_city, b_address1, b_address2, b_displayname, b_phonenumber, b_country_code, shipping_country, shipping_state, shipping_city, s_address1, s_address2, s_displayname, s_phonenumber, s_country_code, s_phoneno_verified, pickup_address, payment_info, product_type, trading_type, iv_locate, iv_licenese, iv_national, iv_exp_date, iv_country_issue, iv_f_name, iv_m_name, iv_l_name, iv_dob, iv_r_bussiness_name, iv_licenese_no, iv_r_address, id_doc, bus_doc_license, bus_doc_r_status, bus_doc_fail_reason, vat_info_number, vat_r_certificate, bank_benefi_name, bank_name, bank_ac_no, back_iban, swift_code, bank_currency, bank_browse_file, warehouse_country, warehouse_state, warehouse_city, warehouse_address_1, warehouse_address_2, warehouse_display, warehouse_phone, warehouse_country_code, warehouse_phone_vf, warehouse_address_line, warehouse_pickup_ad, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    231   6      f          0    186780    newsletter_management 
   TABLE DATA           �   COPY public.newsletter_management (id, name, user_type, registered, email_id, status, action, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    286   #6      j          0    186802    notification 
   TABLE DATA           x  COPY public.notification (id, title, module, execute_on, advanced_filter, notification_type, email_template, email_recipient_type, email_recipient, sms_template, sms_recipient_type, sms_recipient, push_notification_title, push_notification, push_notification_seller, push_notification_seller_name, status, action, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    290   @6      h          0    186791    notification_management 
   TABLE DATA           �   COPY public.notification_management (id, notification_name, status, action, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    288   ]6      p          0    186835    offer_management 
   TABLE DATA             COPY public.offer_management (id, asking_amount, asking_quantity, offer_amount, offer_quantity, remarks, user_type, offer_date, confirmed_date, rejected_date, rejected_reason, offer_validity, status, action, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    296   z6      V          0    186692    order_managemnet 
   TABLE DATA           �   COPY public.order_managemnet (id, order_date, order_number, order_details, order_type, order_amount, order_status, action, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    270   �6      X          0    186703    order_quote 
   TABLE DATA           v  COPY public.order_quote (id, buyer_name, buyer_id, search_for_product, quantity, buyer_shipping_address, method_of_payment, price, vat_amount, total_price, product_type, trading_type, status, seller_code, delivery_time, discount, shipping_charges, image, brand, model, shipment_status, sku, product_id, ship_by_date, deliver_by_date, shipped_by, shipped_person, shipped_person_contact_no, tracking_id, shipping_date, pickup_date, package_type, package_dimension, package_weight, total_shipping_cost, seller_note, buyer_feedback, total_var_amt, feedback_buyer, action, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    272   �6      �          0    223408    order_status 
   TABLE DATA           �   COPY public.order_status (order_status_id, order_status_name, remarks, created_at, created_by, modified_at, modified_by, status) FROM stdin;
    public          postgres    false    342   �6      @          0    186571 
   parameters 
   TABLE DATA           �   COPY public.parameters (id, brand_name, model_name, parameter_id, parameter_value, status, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    248   7      n          0    186824    part_request_unavail 
   TABLE DATA           �   COPY public.part_request_unavail (id, part_no, part_name, remarks, brand, model, user_type, name, requested_date, status, action, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    294   :7      J          0    186626    parts_management 
   TABLE DATA           �   COPY public.parts_management (id, car_id, group_id, sub_group_id, sub_node_id, part_number, part_name, part_erp_id, action, status, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    258   W7      �          0    236452    parts_req_status 
   TABLE DATA           �   COPY public.parts_req_status (parts_req_status_id, parts_req_status_name, remarks, created_at, created_by, modified_at, modified_by, status) FROM stdin;
    public          postgres    false    344   t7      \          0    186725    pay_out_schedule_management 
   TABLE DATA           �   COPY public.pay_out_schedule_management (id, pay_out_slot_name, no_of_day, description, action, status, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    276   �7      ^          0    186736    payment_management 
   TABLE DATA           �   COPY public.payment_management (id, seller_name, number_of_order, total_amount, commission_amount, seller_amount, status, action, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    278   �7      �          0    236463    payment_status 
   TABLE DATA           �   COPY public.payment_status (payment_status_id, payment_status_name, remarks, created_at, created_by, modified_at, modified_by, status) FROM stdin;
    public          postgres    false    346   8                0    186364    payout_shed_details 
   TABLE DATA           ~   COPY public.payout_shed_details (id, subscription_name, created_at, created_by, modified_at, modified_by, status) FROM stdin;
    public          postgres    false    209   b8      8          0    186531    product_detail 
   TABLE DATA           �   COPY public.product_detail (id, product_code, product_type, product_description, status, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    240   8      P          0    186659    product_detail_aftermarket 
   TABLE DATA           �  COPY public.product_detail_aftermarket (id, product_id, product_erp, product_type, seller_name, aftermarket_no, genuine_part_no, verify_button, barcode_no, image, weigth, dimension_length, dimension_width, dimension_height, trading_type, quantity, price, price_after_sale, warehouse_no, manufacturer_name, product_listing_status, action, status, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    264   �8      N          0    186648    product_detail_genuine 
   TABLE DATA           x  COPY public.product_detail_genuine (id, product_id, product_erp, product_type, seller_name, genuine_part_no, verify_button, image, weigth, dimension_length, dimension_width, dimension_height, trading_type, quantity, price, price_after_sale, warehouse_no, manufacturer_name, product_listing_status, action, status, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    262   �8      R          0    186670    product_detail_user_part 
   TABLE DATA           �  COPY public.product_detail_user_part (id, product_id, product_erp, product_type, seller_name, genuine_part_no, verify_button, barcode_no, number_of_yr_user, condition, description, image, weigth, dimension_length, dimension_width, dimension_height, trading_type, quantity, price, price_after_sale, warehouse_no, manufacturer_name, product_listing_status, action, status, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    266   �8      L          0    186637    product_management 
   TABLE DATA           �   COPY public.product_management (id, seller_name, product_type, product_name, part_number, bar_code_number, quantity, price, action, status, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    260   �8      %          0    186430    product_type_details 
   TABLE DATA              COPY public.product_type_details (id, product_type_name, created_at, created_by, modified_at, modified_by, status) FROM stdin;
    public          postgres    false    221   9      �          0    236474    quote_status 
   TABLE DATA           �   COPY public.quote_status (quote_status_id, quote_status_name, remarks, created_at, created_by, modified_at, modified_by, status) FROM stdin;
    public          postgres    false    348   e9                0    186386    reason_details 
   TABLE DATA           s   COPY public.reason_details (id, reason_name, created_at, created_by, modified_at, modified_by, status) FROM stdin;
    public          postgres    false    213   �9      �          0    236485    registration_status 
   TABLE DATA           �   COPY public.registration_status (reg_status_id, reg_status_name, remarks, created_at, created_by, modified_at, modified_by, status) FROM stdin;
    public          postgres    false    350   .:      z          0    186890    revenue_trade_type 
   TABLE DATA           �   COPY public.revenue_trade_type (id, order_brand, order_id, order_date, suborder_id, trade_type, seller_name, total_order_amt, total_service_amt, status, action, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    306   s:      t          0    186857    roles 
   TABLE DATA           {   COPY public.roles (id, role_name, status, action, created_at, created_by, modified_at, modified_by, role_code) FROM stdin;
    public          postgres    false    300   �:      -          0    186474    seller_management 
   TABLE DATA           �   COPY public.seller_management (id, login_id, password, seller_name, email_id, trade_type, contact_no, address, status, account, action, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    229   (;      �          0    195222    services_cms 
   TABLE DATA           z   COPY public.services_cms (id, title, content, icon, status, created_at, modified_at, created_by, modified_by) FROM stdin;
    public          e-parts-admin    false    326   E;      �          0    236496    shipment_status 
   TABLE DATA           �   COPY public.shipment_status (shipment_status_id, shipment_status_name, remarks, created_at, created_by, modified_at, modified_by, status) FROM stdin;
    public          postgres    false    352   q<      �          0    236586    sms_template 
   TABLE DATA           �   COPY public.sms_template (id, module_id, title_name, sms_content, action, created_at, created_by, modified_at, modified_by, status) FROM stdin;
    public          postgres    false    358   �<      �          0    223356    social_media_links_cms 
   TABLE DATA           z   COPY public.social_media_links_cms (icon, id, link, created_by, created_at, modified_by, modified_at, status) FROM stdin;
    public          e-parts-admin    false    332   _=      #          0    186419    state_details 
   TABLE DATA           }   COPY public.state_details (id, country_id, state_name, created_at, created_by, modified_at, modified_by, status) FROM stdin;
    public          postgres    false    219   4>      �          0    186934    static_page 
   TABLE DATA           �   COPY public.static_page (id, static_page_name, page_title, meta_tag, content, status, action, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    314   �>                0    186375    status_details 
   TABLE DATA           k   COPY public.status_details (id, status_name, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    211   �>      F          0    186604    sub_group_management 
   TABLE DATA           �   COPY public.sub_group_management (id, car_id, brand, model, group_name, sub_group_name, status, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    254   ?      H          0    186615    sub_node_management 
   TABLE DATA           �   COPY public.sub_node_management (id, car_id, brand, model, sub_node_code, sub_node_name, status, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    256   (?                0    186353    subscription_details 
   TABLE DATA              COPY public.subscription_details (id, subscription_name, created_at, created_by, modified_at, modified_by, status) FROM stdin;
    public          postgres    false    207   E?      r          0    186846    subscription_management 
   TABLE DATA           �   COPY public.subscription_management (id, subscription_name, number_of_users, modules_list, days, price, valid_till, effective, status, action, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    298   �?                0    186342    super_admin 
   TABLE DATA           o   COPY public.super_admin (id, login_id, password, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    205   �?      v          0    186868    total_order_invoiced 
   TABLE DATA             COPY public.total_order_invoiced (id, order_id, suborder_id, order_date, order_status, part_no, part_name, seller_name, buyer_name, price_of_part, discount, vat_amount, gross_amount, status, action, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    302   �?      x          0    186879    total_order_refunded 
   TABLE DATA             COPY public.total_order_refunded (id, order_id, suborder_id, order_date, order_status, part_no, part_name, seller_name, buyer_name, price_of_part, discount, vat_amount, gross_amount, status, action, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    304   �?      '          0    186441    trading_type_details 
   TABLE DATA              COPY public.trading_type_details (id, trading_type_name, created_at, created_by, modified_at, modified_by, status) FROM stdin;
    public          postgres    false    223   �?      �          0    223365    useful_links_cms 
   TABLE DATA           u   COPY public.useful_links_cms (id, label, link, created_by, created_at, modified_by, modified_at, status) FROM stdin;
    public          e-parts-admin    false    334   6@      �          0    236521    user_managment 
   TABLE DATA           �   COPY public.user_managment (id, login_user_type_id, user_name, user_email_id, phone_no, user_password, two_way_auth, status, last_login, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    356   �@                0    183132    users_login 
   TABLE DATA           �   COPY public.users_login (id, user_id, email, password, username, role, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    203   A      `          0    186747    vin_history 
   TABLE DATA           �   COPY public.vin_history (id, vin_number, vehicle_identified, brand, model, user_type, name, region, searched_date, status, action, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    280   rA                 0    0 &   Login_user_type_Login_user_type_id_seq    SEQUENCE SET     V   SELECT pg_catalog.setval('public."Login_user_type_Login_user_type_id_seq"', 7, true);
          public          postgres    false    353                       0    0    Rating_review_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public."Rating_review_id_seq"', 1, false);
          public          postgres    false    291                       0    0    application_user_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.application_user_id_seq', 1, false);
          public          postgres    false    236                       0    0    audit_trail_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.audit_trail_id_seq', 1, false);
          public          postgres    false    311                       0    0    banner_cms_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.banner_cms_id_seq', 4, true);
          public          e-parts-admin    false    321                       0    0    brands_brand_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.brands_brand_id_seq', 1, false);
          public          postgres    false    242                       0    0    brands_id_seq    SEQUENCE SET     <   SELECT pg_catalog.setval('public.brands_id_seq', 1, false);
          public          postgres    false    241                       0    0    buyer_management_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.buyer_management_id_seq', 1, false);
          public          postgres    false    224                       0    0    buyer_saved_quote_id_seq    SEQUENCE SET     G   SELECT pg_catalog.setval('public.buyer_saved_quote_id_seq', 1, false);
          public          postgres    false    283                       0    0    buyer_seller_cms_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.buyer_seller_cms_id_seq', 1, false);
          public          e-parts-admin    false    329                       0    0    car_management_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.car_management_id_seq', 1, false);
          public          postgres    false    273                       0    0    cars_id_seq    SEQUENCE SET     :   SELECT pg_catalog.setval('public.cars_id_seq', 1, false);
          public          postgres    false    249                       0    0 "   category_request_management_id_seq    SEQUENCE SET     Q   SELECT pg_catalog.setval('public.category_request_management_id_seq', 1, false);
          public          postgres    false    281                       0    0 -   category_request_status_cat_req_status_id_seq    SEQUENCE SET     [   SELECT pg_catalog.setval('public.category_request_status_cat_req_status_id_seq', 2, true);
          public          postgres    false    337                       0    0    city_details_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.city_details_id_seq', 5, true);
          public          postgres    false    317                       0    0    commission_managemnet_id_seq    SEQUENCE SET     K   SELECT pg_catalog.setval('public.commission_managemnet_id_seq', 1, false);
          public          postgres    false    267                       0    0    configuration_details_id_seq    SEQUENCE SET     K   SELECT pg_catalog.setval('public.configuration_details_id_seq', 1, false);
          public          postgres    false    359                       0    0    country_details_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.country_details_id_seq', 38, true);
          public          postgres    false    216                       0    0    currency_details_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.currency_details_id_seq', 7, true);
          public          postgres    false    214                       0    0    email_temp_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public.email_temp_id_seq', 1, false);
          public          postgres    false    315                       0    0    features_cms_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.features_cms_id_seq', 9, true);
          public          e-parts-admin    false    323                        0    0    group_management_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.group_management_id_seq', 1, false);
          public          postgres    false    251            !           0    0    help_management_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.help_management_id_seq', 1, false);
          public          postgres    false    309            "           0    0    hero_section_cms_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.hero_section_cms_id_seq', 1, true);
          public          e-parts-admin    false    327            #           0    0    info_cms_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.info_cms_id_seq', 1, true);
          public          e-parts-admin    false    320            $           0    0    intermediator_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.intermediator_id_seq', 1, false);
          public          postgres    false    234            %           0    0    intermediator_management_id_seq    SEQUENCE SET     N   SELECT pg_catalog.setval('public.intermediator_management_id_seq', 1, false);
          public          postgres    false    232            &           0    0    inventory_manangement_id_seq    SEQUENCE SET     K   SELECT pg_catalog.setval('public.inventory_manangement_id_seq', 1, false);
          public          postgres    false    307            '           0    0 *   make_offer_status_make_offer_status_id_seq    SEQUENCE SET     X   SELECT pg_catalog.setval('public.make_offer_status_make_offer_status_id_seq', 2, true);
          public          postgres    false    339            (           0    0    mobile_section_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.mobile_section_id_seq', 3, true);
          public          e-parts-admin    false    361            )           0    0    models_brand_name_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.models_brand_name_seq', 1, false);
          public          postgres    false    245            *           0    0    models_id_seq    SEQUENCE SET     <   SELECT pg_catalog.setval('public.models_id_seq', 1, false);
          public          postgres    false    244            +           0    0    nav_links_cms_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.nav_links_cms_id_seq', 8, true);
          public          e-parts-admin    false    335            ,           0    0    new_buyer_details_id_seq    SEQUENCE SET     G   SELECT pg_catalog.setval('public.new_buyer_details_id_seq', 1, false);
          public          postgres    false    226            -           0    0    new_seller_details_id_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('public.new_seller_details_id_seq', 1, false);
          public          postgres    false    230            .           0    0    newsletter_management_id_seq    SEQUENCE SET     K   SELECT pg_catalog.setval('public.newsletter_management_id_seq', 1, false);
          public          postgres    false    285            /           0    0    notification_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.notification_id_seq', 1, false);
          public          postgres    false    289            0           0    0    notification_management_id_seq    SEQUENCE SET     M   SELECT pg_catalog.setval('public.notification_management_id_seq', 1, false);
          public          postgres    false    287            1           0    0    offer_management_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.offer_management_id_seq', 1, false);
          public          postgres    false    295            2           0    0    order_managemnet_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.order_managemnet_id_seq', 1, false);
          public          postgres    false    269            3           0    0    order_quote_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.order_quote_id_seq', 1, false);
          public          postgres    false    271            4           0    0     order_status_order_status_id_seq    SEQUENCE SET     N   SELECT pg_catalog.setval('public.order_status_order_status_id_seq', 2, true);
          public          postgres    false    341            5           0    0    parameters_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public.parameters_id_seq', 1, false);
          public          postgres    false    247            6           0    0    part_request_unavail_id_seq    SEQUENCE SET     J   SELECT pg_catalog.setval('public.part_request_unavail_id_seq', 1, false);
          public          postgres    false    293            7           0    0    parts_management_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.parts_management_id_seq', 1, false);
          public          postgres    false    257            8           0    0 (   parts_req_status_parts_req_status_id_seq    SEQUENCE SET     V   SELECT pg_catalog.setval('public.parts_req_status_parts_req_status_id_seq', 2, true);
          public          postgres    false    343            9           0    0 "   pay_out_schedule_management_id_seq    SEQUENCE SET     Q   SELECT pg_catalog.setval('public.pay_out_schedule_management_id_seq', 1, false);
          public          postgres    false    275            :           0    0    payment_management_id_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('public.payment_management_id_seq', 1, false);
          public          postgres    false    277            ;           0    0 $   payment_status_payment_status_id_seq    SEQUENCE SET     R   SELECT pg_catalog.setval('public.payment_status_payment_status_id_seq', 1, true);
          public          postgres    false    345            <           0    0    payout_shed_details_id_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('public.payout_shed_details_id_seq', 5, true);
          public          postgres    false    208            =           0    0 !   product_detail_aftermarket_id_seq    SEQUENCE SET     P   SELECT pg_catalog.setval('public.product_detail_aftermarket_id_seq', 1, false);
          public          postgres    false    263            >           0    0    product_detail_genuine_id_seq    SEQUENCE SET     L   SELECT pg_catalog.setval('public.product_detail_genuine_id_seq', 1, false);
          public          postgres    false    261            ?           0    0    product_detail_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.product_detail_id_seq', 1, false);
          public          postgres    false    238            @           0    0    product_detail_product_code_seq    SEQUENCE SET     N   SELECT pg_catalog.setval('public.product_detail_product_code_seq', 1, false);
          public          postgres    false    239            A           0    0    product_detail_user_part_id_seq    SEQUENCE SET     N   SELECT pg_catalog.setval('public.product_detail_user_part_id_seq', 1, false);
          public          postgres    false    265            B           0    0    product_management_id_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('public.product_management_id_seq', 1, false);
          public          postgres    false    259            C           0    0    product_type_details_id_seq    SEQUENCE SET     I   SELECT pg_catalog.setval('public.product_type_details_id_seq', 4, true);
          public          postgres    false    220            D           0    0     quote_status_quote_status_id_seq    SEQUENCE SET     N   SELECT pg_catalog.setval('public.quote_status_quote_status_id_seq', 1, true);
          public          postgres    false    347            E           0    0    reason_details_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.reason_details_id_seq', 5, true);
          public          postgres    false    212            F           0    0 %   registration_status_reg_status_id_seq    SEQUENCE SET     S   SELECT pg_catalog.setval('public.registration_status_reg_status_id_seq', 3, true);
          public          postgres    false    349            G           0    0    revenue_trade_type_id_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('public.revenue_trade_type_id_seq', 1, false);
          public          postgres    false    305            H           0    0    roles_id_seq    SEQUENCE SET     :   SELECT pg_catalog.setval('public.roles_id_seq', 6, true);
          public          postgres    false    299            I           0    0    seller_management_id_seq    SEQUENCE SET     G   SELECT pg_catalog.setval('public.seller_management_id_seq', 1, false);
          public          postgres    false    228            J           0    0    servies_cms_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.servies_cms_id_seq', 10, true);
          public          e-parts-admin    false    325            K           0    0 "   shipment_status_ship_status_id_seq    SEQUENCE SET     P   SELECT pg_catalog.setval('public.shipment_status_ship_status_id_seq', 2, true);
          public          postgres    false    351            L           0    0    sms_template_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.sms_template_id_seq', 2, true);
          public          postgres    false    357            M           0    0    social_media_links_id_seq    SEQUENCE SET     G   SELECT pg_catalog.setval('public.social_media_links_id_seq', 8, true);
          public          e-parts-admin    false    331            N           0    0    state_details_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.state_details_id_seq', 9, true);
          public          postgres    false    218            O           0    0    static_page_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.static_page_id_seq', 1, false);
          public          postgres    false    313            P           0    0    status_details_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.status_details_id_seq', 1, false);
          public          postgres    false    210            Q           0    0    sub_group_management_id_seq    SEQUENCE SET     J   SELECT pg_catalog.setval('public.sub_group_management_id_seq', 1, false);
          public          postgres    false    253            R           0    0    sub_node_management_id_seq    SEQUENCE SET     I   SELECT pg_catalog.setval('public.sub_node_management_id_seq', 1, false);
          public          postgres    false    255            S           0    0    subscription_details_id_seq    SEQUENCE SET     I   SELECT pg_catalog.setval('public.subscription_details_id_seq', 3, true);
          public          postgres    false    206            T           0    0    subscription_management_id_seq    SEQUENCE SET     M   SELECT pg_catalog.setval('public.subscription_management_id_seq', 1, false);
          public          postgres    false    297            U           0    0    super_admin_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.super_admin_id_seq', 1, false);
          public          postgres    false    204            V           0    0    total_order_invoiced_id_seq    SEQUENCE SET     J   SELECT pg_catalog.setval('public.total_order_invoiced_id_seq', 1, false);
          public          postgres    false    301            W           0    0    total_order_refunded_id_seq    SEQUENCE SET     J   SELECT pg_catalog.setval('public.total_order_refunded_id_seq', 1, false);
          public          postgres    false    303            X           0    0    trading_type_details_id_seq    SEQUENCE SET     I   SELECT pg_catalog.setval('public.trading_type_details_id_seq', 2, true);
          public          postgres    false    222            Y           0    0    useful_links_cms_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.useful_links_cms_id_seq', 5, true);
          public          e-parts-admin    false    333            Z           0    0    user_managment_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.user_managment_id_seq', 2, true);
          public          postgres    false    355            [           0    0    users_login_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public.users_login_id_seq', 1, true);
          public          postgres    false    202            \           0    0    vin_history_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.vin_history_id_seq', 1, false);
          public          postgres    false    279            �           2606    236518 $   login_user_type Login_user_type_pkey 
   CONSTRAINT     t   ALTER TABLE ONLY public.login_user_type
    ADD CONSTRAINT "Login_user_type_pkey" PRIMARY KEY (login_user_type_id);
 P   ALTER TABLE ONLY public.login_user_type DROP CONSTRAINT "Login_user_type_pkey";
       public            postgres    false    354            N           2606    186821     Rating_review Rating_review_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY public."Rating_review"
    ADD CONSTRAINT "Rating_review_pkey" PRIMARY KEY (id);
 N   ALTER TABLE ONLY public."Rating_review" DROP CONSTRAINT "Rating_review_pkey";
       public            postgres    false    292                       2606    186526 &   application_user application_user_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.application_user
    ADD CONSTRAINT application_user_pkey PRIMARY KEY (id);
 P   ALTER TABLE ONLY public.application_user DROP CONSTRAINT application_user_pkey;
       public            postgres    false    237            b           2606    186931    audit_trail audit_trail_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.audit_trail
    ADD CONSTRAINT audit_trail_pkey PRIMARY KEY (id);
 F   ALTER TABLE ONLY public.audit_trail DROP CONSTRAINT audit_trail_pkey;
       public            postgres    false    312            j           2606    195202    banner_cms banner_cms_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.banner_cms
    ADD CONSTRAINT banner_cms_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY public.banner_cms DROP CONSTRAINT banner_cms_pkey;
       public            e-parts-admin    false    322                       2606    186554    brands brands_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY public.brands
    ADD CONSTRAINT brands_pkey PRIMARY KEY (id);
 <   ALTER TABLE ONLY public.brands DROP CONSTRAINT brands_pkey;
       public            postgres    false    243                       2606    186460 &   buyer_management buyer_management_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.buyer_management
    ADD CONSTRAINT buyer_management_pkey PRIMARY KEY (id);
 P   ALTER TABLE ONLY public.buyer_management DROP CONSTRAINT buyer_management_pkey;
       public            postgres    false    225            F           2606    186777 (   buyer_saved_quote buyer_saved_quote_pkey 
   CONSTRAINT     f   ALTER TABLE ONLY public.buyer_saved_quote
    ADD CONSTRAINT buyer_saved_quote_pkey PRIMARY KEY (id);
 R   ALTER TABLE ONLY public.buyer_saved_quote DROP CONSTRAINT buyer_saved_quote_pkey;
       public            postgres    false    284            r           2606    211645 &   buyer_seller_cms buyer_seller_cms_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.buyer_seller_cms
    ADD CONSTRAINT buyer_seller_cms_pkey PRIMARY KEY (id);
 P   ALTER TABLE ONLY public.buyer_seller_cms DROP CONSTRAINT buyer_seller_cms_pkey;
       public            e-parts-admin    false    330            <           2606    186722 "   car_management car_management_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY public.car_management
    ADD CONSTRAINT car_management_pkey PRIMARY KEY (id);
 L   ALTER TABLE ONLY public.car_management DROP CONSTRAINT car_management_pkey;
       public            postgres    false    274            $           2606    186590    cars cars_pkey 
   CONSTRAINT     L   ALTER TABLE ONLY public.cars
    ADD CONSTRAINT cars_pkey PRIMARY KEY (id);
 8   ALTER TABLE ONLY public.cars DROP CONSTRAINT cars_pkey;
       public            postgres    false    250            D           2606    186766 <   category_request_management category_request_management_pkey 
   CONSTRAINT     z   ALTER TABLE ONLY public.category_request_management
    ADD CONSTRAINT category_request_management_pkey PRIMARY KEY (id);
 f   ALTER TABLE ONLY public.category_request_management DROP CONSTRAINT category_request_management_pkey;
       public            postgres    false    282            x           2606    223392 4   category_request_status category_request_status_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.category_request_status
    ADD CONSTRAINT category_request_status_pkey PRIMARY KEY (cat_req_status_id);
 ^   ALTER TABLE ONLY public.category_request_status DROP CONSTRAINT category_request_status_pkey;
       public            postgres    false    338            h           2606    186979    city_details city_details_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.city_details
    ADD CONSTRAINT city_details_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY public.city_details DROP CONSTRAINT city_details_pkey;
       public            postgres    false    318            6           2606    186689 0   commission_managemnet commission_managemnet_pkey 
   CONSTRAINT     n   ALTER TABLE ONLY public.commission_managemnet
    ADD CONSTRAINT commission_managemnet_pkey PRIMARY KEY (id);
 Z   ALTER TABLE ONLY public.commission_managemnet DROP CONSTRAINT commission_managemnet_pkey;
       public            postgres    false    268            �           2606    236628 0   configuration_details configuration_details_pkey 
   CONSTRAINT     n   ALTER TABLE ONLY public.configuration_details
    ADD CONSTRAINT configuration_details_pkey PRIMARY KEY (id);
 Z   ALTER TABLE ONLY public.configuration_details DROP CONSTRAINT configuration_details_pkey;
       public            postgres    false    360                       2606    186416 $   country_details country_details_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY public.country_details
    ADD CONSTRAINT country_details_pkey PRIMARY KEY (id);
 N   ALTER TABLE ONLY public.country_details DROP CONSTRAINT country_details_pkey;
       public            postgres    false    217                       2606    186405 &   currency_details currency_details_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.currency_details
    ADD CONSTRAINT currency_details_pkey PRIMARY KEY (id);
 P   ALTER TABLE ONLY public.currency_details DROP CONSTRAINT currency_details_pkey;
       public            postgres    false    215            f           2606    186953    email_template email_temp_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.email_template
    ADD CONSTRAINT email_temp_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY public.email_template DROP CONSTRAINT email_temp_pkey;
       public            postgres    false    316            l           2606    195213    features_cms features_cms_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.features_cms
    ADD CONSTRAINT features_cms_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY public.features_cms DROP CONSTRAINT features_cms_pkey;
       public            e-parts-admin    false    324            &           2606    186601 &   group_management group_management_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.group_management
    ADD CONSTRAINT group_management_pkey PRIMARY KEY (id);
 P   ALTER TABLE ONLY public.group_management DROP CONSTRAINT group_management_pkey;
       public            postgres    false    252            `           2606    186920 $   help_management help_management_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY public.help_management
    ADD CONSTRAINT help_management_pkey PRIMARY KEY (id);
 N   ALTER TABLE ONLY public.help_management DROP CONSTRAINT help_management_pkey;
       public            postgres    false    310            p           2606    211634 &   hero_section_cms hero_section_cms_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.hero_section_cms
    ADD CONSTRAINT hero_section_cms_pkey PRIMARY KEY (id);
 P   ALTER TABLE ONLY public.hero_section_cms DROP CONSTRAINT hero_section_cms_pkey;
       public            e-parts-admin    false    328                       2606    186504 6   intermediator_management intermediator_management_pkey 
   CONSTRAINT     t   ALTER TABLE ONLY public.intermediator_management
    ADD CONSTRAINT intermediator_management_pkey PRIMARY KEY (id);
 `   ALTER TABLE ONLY public.intermediator_management DROP CONSTRAINT intermediator_management_pkey;
       public            postgres    false    233                       2606    186515     intermediator intermediator_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public.intermediator
    ADD CONSTRAINT intermediator_pkey PRIMARY KEY (id);
 J   ALTER TABLE ONLY public.intermediator DROP CONSTRAINT intermediator_pkey;
       public            postgres    false    235            ^           2606    186909 0   inventory_manangement inventory_manangement_pkey 
   CONSTRAINT     n   ALTER TABLE ONLY public.inventory_manangement
    ADD CONSTRAINT inventory_manangement_pkey PRIMARY KEY (id);
 Z   ALTER TABLE ONLY public.inventory_manangement DROP CONSTRAINT inventory_manangement_pkey;
       public            postgres    false    308            z           2606    223405 (   make_offer_status make_offer_status_pkey 
   CONSTRAINT     x   ALTER TABLE ONLY public.make_offer_status
    ADD CONSTRAINT make_offer_status_pkey PRIMARY KEY (make_offer_status_id);
 R   ALTER TABLE ONLY public.make_offer_status DROP CONSTRAINT make_offer_status_pkey;
       public            postgres    false    340            �           2606    236642 &   mobile_section_cms mobile_section_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.mobile_section_cms
    ADD CONSTRAINT mobile_section_pkey PRIMARY KEY (id);
 P   ALTER TABLE ONLY public.mobile_section_cms DROP CONSTRAINT mobile_section_pkey;
       public            e-parts-admin    false    362                        2606    186568    models models_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY public.models
    ADD CONSTRAINT models_pkey PRIMARY KEY (id);
 <   ALTER TABLE ONLY public.models DROP CONSTRAINT models_pkey;
       public            postgres    false    246            v           2606    223381     nav_links_cms nav_links_cms_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public.nav_links_cms
    ADD CONSTRAINT nav_links_cms_pkey PRIMARY KEY (id);
 J   ALTER TABLE ONLY public.nav_links_cms DROP CONSTRAINT nav_links_cms_pkey;
       public            e-parts-admin    false    336                       2606    186471 (   new_buyer_details new_buyer_details_pkey 
   CONSTRAINT     f   ALTER TABLE ONLY public.new_buyer_details
    ADD CONSTRAINT new_buyer_details_pkey PRIMARY KEY (id);
 R   ALTER TABLE ONLY public.new_buyer_details DROP CONSTRAINT new_buyer_details_pkey;
       public            postgres    false    227                       2606    186493 *   new_seller_details new_seller_details_pkey 
   CONSTRAINT     h   ALTER TABLE ONLY public.new_seller_details
    ADD CONSTRAINT new_seller_details_pkey PRIMARY KEY (id);
 T   ALTER TABLE ONLY public.new_seller_details DROP CONSTRAINT new_seller_details_pkey;
       public            postgres    false    231            H           2606    186788 0   newsletter_management newsletter_management_pkey 
   CONSTRAINT     n   ALTER TABLE ONLY public.newsletter_management
    ADD CONSTRAINT newsletter_management_pkey PRIMARY KEY (id);
 Z   ALTER TABLE ONLY public.newsletter_management DROP CONSTRAINT newsletter_management_pkey;
       public            postgres    false    286            J           2606    186799 4   notification_management notification_management_pkey 
   CONSTRAINT     r   ALTER TABLE ONLY public.notification_management
    ADD CONSTRAINT notification_management_pkey PRIMARY KEY (id);
 ^   ALTER TABLE ONLY public.notification_management DROP CONSTRAINT notification_management_pkey;
       public            postgres    false    288            L           2606    186810    notification notification_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.notification
    ADD CONSTRAINT notification_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY public.notification DROP CONSTRAINT notification_pkey;
       public            postgres    false    290            R           2606    186843 &   offer_management offer_management_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.offer_management
    ADD CONSTRAINT offer_management_pkey PRIMARY KEY (id);
 P   ALTER TABLE ONLY public.offer_management DROP CONSTRAINT offer_management_pkey;
       public            postgres    false    296            8           2606    186700 &   order_managemnet order_managemnet_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.order_managemnet
    ADD CONSTRAINT order_managemnet_pkey PRIMARY KEY (id);
 P   ALTER TABLE ONLY public.order_managemnet DROP CONSTRAINT order_managemnet_pkey;
       public            postgres    false    270            :           2606    186711    order_quote order_quote_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.order_quote
    ADD CONSTRAINT order_quote_pkey PRIMARY KEY (id);
 F   ALTER TABLE ONLY public.order_quote DROP CONSTRAINT order_quote_pkey;
       public            postgres    false    272            |           2606    223416    order_status order_status_pkey 
   CONSTRAINT     i   ALTER TABLE ONLY public.order_status
    ADD CONSTRAINT order_status_pkey PRIMARY KEY (order_status_id);
 H   ALTER TABLE ONLY public.order_status DROP CONSTRAINT order_status_pkey;
       public            postgres    false    342            "           2606    186579    parameters parameters_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.parameters
    ADD CONSTRAINT parameters_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY public.parameters DROP CONSTRAINT parameters_pkey;
       public            postgres    false    248            P           2606    186832 .   part_request_unavail part_request_unavail_pkey 
   CONSTRAINT     l   ALTER TABLE ONLY public.part_request_unavail
    ADD CONSTRAINT part_request_unavail_pkey PRIMARY KEY (id);
 X   ALTER TABLE ONLY public.part_request_unavail DROP CONSTRAINT part_request_unavail_pkey;
       public            postgres    false    294            ,           2606    186634 &   parts_management parts_management_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.parts_management
    ADD CONSTRAINT parts_management_pkey PRIMARY KEY (id);
 P   ALTER TABLE ONLY public.parts_management DROP CONSTRAINT parts_management_pkey;
       public            postgres    false    258            ~           2606    236460 &   parts_req_status parts_req_status_pkey 
   CONSTRAINT     u   ALTER TABLE ONLY public.parts_req_status
    ADD CONSTRAINT parts_req_status_pkey PRIMARY KEY (parts_req_status_id);
 P   ALTER TABLE ONLY public.parts_req_status DROP CONSTRAINT parts_req_status_pkey;
       public            postgres    false    344            >           2606    186733 <   pay_out_schedule_management pay_out_schedule_management_pkey 
   CONSTRAINT     z   ALTER TABLE ONLY public.pay_out_schedule_management
    ADD CONSTRAINT pay_out_schedule_management_pkey PRIMARY KEY (id);
 f   ALTER TABLE ONLY public.pay_out_schedule_management DROP CONSTRAINT pay_out_schedule_management_pkey;
       public            postgres    false    276            @           2606    186744 *   payment_management payment_management_pkey 
   CONSTRAINT     h   ALTER TABLE ONLY public.payment_management
    ADD CONSTRAINT payment_management_pkey PRIMARY KEY (id);
 T   ALTER TABLE ONLY public.payment_management DROP CONSTRAINT payment_management_pkey;
       public            postgres    false    278            �           2606    236471 "   payment_status payment_status_pkey 
   CONSTRAINT     o   ALTER TABLE ONLY public.payment_status
    ADD CONSTRAINT payment_status_pkey PRIMARY KEY (payment_status_id);
 L   ALTER TABLE ONLY public.payment_status DROP CONSTRAINT payment_status_pkey;
       public            postgres    false    346            �           2606    186372 ,   payout_shed_details payout_shed_details_pkey 
   CONSTRAINT     j   ALTER TABLE ONLY public.payout_shed_details
    ADD CONSTRAINT payout_shed_details_pkey PRIMARY KEY (id);
 V   ALTER TABLE ONLY public.payout_shed_details DROP CONSTRAINT payout_shed_details_pkey;
       public            postgres    false    209            2           2606    186667 :   product_detail_aftermarket product_detail_aftermarket_pkey 
   CONSTRAINT     x   ALTER TABLE ONLY public.product_detail_aftermarket
    ADD CONSTRAINT product_detail_aftermarket_pkey PRIMARY KEY (id);
 d   ALTER TABLE ONLY public.product_detail_aftermarket DROP CONSTRAINT product_detail_aftermarket_pkey;
       public            postgres    false    264            0           2606    186656 2   product_detail_genuine product_detail_genuine_pkey 
   CONSTRAINT     p   ALTER TABLE ONLY public.product_detail_genuine
    ADD CONSTRAINT product_detail_genuine_pkey PRIMARY KEY (id);
 \   ALTER TABLE ONLY public.product_detail_genuine DROP CONSTRAINT product_detail_genuine_pkey;
       public            postgres    false    262                       2606    186540 "   product_detail product_detail_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY public.product_detail
    ADD CONSTRAINT product_detail_pkey PRIMARY KEY (id);
 L   ALTER TABLE ONLY public.product_detail DROP CONSTRAINT product_detail_pkey;
       public            postgres    false    240            4           2606    186678 6   product_detail_user_part product_detail_user_part_pkey 
   CONSTRAINT     t   ALTER TABLE ONLY public.product_detail_user_part
    ADD CONSTRAINT product_detail_user_part_pkey PRIMARY KEY (id);
 `   ALTER TABLE ONLY public.product_detail_user_part DROP CONSTRAINT product_detail_user_part_pkey;
       public            postgres    false    266            .           2606    186645 *   product_management product_management_pkey 
   CONSTRAINT     h   ALTER TABLE ONLY public.product_management
    ADD CONSTRAINT product_management_pkey PRIMARY KEY (id);
 T   ALTER TABLE ONLY public.product_management DROP CONSTRAINT product_management_pkey;
       public            postgres    false    260            
           2606    186438 .   product_type_details product_type_details_pkey 
   CONSTRAINT     l   ALTER TABLE ONLY public.product_type_details
    ADD CONSTRAINT product_type_details_pkey PRIMARY KEY (id);
 X   ALTER TABLE ONLY public.product_type_details DROP CONSTRAINT product_type_details_pkey;
       public            postgres    false    221            �           2606    236482    quote_status quote_status_pkey 
   CONSTRAINT     i   ALTER TABLE ONLY public.quote_status
    ADD CONSTRAINT quote_status_pkey PRIMARY KEY (quote_status_id);
 H   ALTER TABLE ONLY public.quote_status DROP CONSTRAINT quote_status_pkey;
       public            postgres    false    348                       2606    186394 "   reason_details reason_details_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY public.reason_details
    ADD CONSTRAINT reason_details_pkey PRIMARY KEY (id);
 L   ALTER TABLE ONLY public.reason_details DROP CONSTRAINT reason_details_pkey;
       public            postgres    false    213            �           2606    236493 ,   registration_status registration_status_pkey 
   CONSTRAINT     u   ALTER TABLE ONLY public.registration_status
    ADD CONSTRAINT registration_status_pkey PRIMARY KEY (reg_status_id);
 V   ALTER TABLE ONLY public.registration_status DROP CONSTRAINT registration_status_pkey;
       public            postgres    false    350            \           2606    186898 *   revenue_trade_type revenue_trade_type_pkey 
   CONSTRAINT     h   ALTER TABLE ONLY public.revenue_trade_type
    ADD CONSTRAINT revenue_trade_type_pkey PRIMARY KEY (id);
 T   ALTER TABLE ONLY public.revenue_trade_type DROP CONSTRAINT revenue_trade_type_pkey;
       public            postgres    false    306            V           2606    186865    roles roles_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public.roles
    ADD CONSTRAINT roles_pkey PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.roles DROP CONSTRAINT roles_pkey;
       public            postgres    false    300                       2606    186482 (   seller_management seller_management_pkey 
   CONSTRAINT     f   ALTER TABLE ONLY public.seller_management
    ADD CONSTRAINT seller_management_pkey PRIMARY KEY (id);
 R   ALTER TABLE ONLY public.seller_management DROP CONSTRAINT seller_management_pkey;
       public            postgres    false    229            n           2606    195230    services_cms services_cms_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.services_cms
    ADD CONSTRAINT services_cms_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY public.services_cms DROP CONSTRAINT services_cms_pkey;
       public            e-parts-admin    false    326            �           2606    236504 $   shipment_status shipment_status_pkey 
   CONSTRAINT     r   ALTER TABLE ONLY public.shipment_status
    ADD CONSTRAINT shipment_status_pkey PRIMARY KEY (shipment_status_id);
 N   ALTER TABLE ONLY public.shipment_status DROP CONSTRAINT shipment_status_pkey;
       public            postgres    false    352            �           2606    236594    sms_template sms_template_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.sms_template
    ADD CONSTRAINT sms_template_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY public.sms_template DROP CONSTRAINT sms_template_pkey;
       public            postgres    false    358                       2606    186427     state_details state_details_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public.state_details
    ADD CONSTRAINT state_details_pkey PRIMARY KEY (id);
 J   ALTER TABLE ONLY public.state_details DROP CONSTRAINT state_details_pkey;
       public            postgres    false    219            d           2606    186942    static_page static_page_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.static_page
    ADD CONSTRAINT static_page_pkey PRIMARY KEY (id);
 F   ALTER TABLE ONLY public.static_page DROP CONSTRAINT static_page_pkey;
       public            postgres    false    314                        2606    186383 "   status_details status_details_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY public.status_details
    ADD CONSTRAINT status_details_pkey PRIMARY KEY (id);
 L   ALTER TABLE ONLY public.status_details DROP CONSTRAINT status_details_pkey;
       public            postgres    false    211            (           2606    186612 .   sub_group_management sub_group_management_pkey 
   CONSTRAINT     l   ALTER TABLE ONLY public.sub_group_management
    ADD CONSTRAINT sub_group_management_pkey PRIMARY KEY (id);
 X   ALTER TABLE ONLY public.sub_group_management DROP CONSTRAINT sub_group_management_pkey;
       public            postgres    false    254            *           2606    186623 ,   sub_node_management sub_node_management_pkey 
   CONSTRAINT     j   ALTER TABLE ONLY public.sub_node_management
    ADD CONSTRAINT sub_node_management_pkey PRIMARY KEY (id);
 V   ALTER TABLE ONLY public.sub_node_management DROP CONSTRAINT sub_node_management_pkey;
       public            postgres    false    256            �           2606    186361 .   subscription_details subscription_details_pkey 
   CONSTRAINT     l   ALTER TABLE ONLY public.subscription_details
    ADD CONSTRAINT subscription_details_pkey PRIMARY KEY (id);
 X   ALTER TABLE ONLY public.subscription_details DROP CONSTRAINT subscription_details_pkey;
       public            postgres    false    207            T           2606    186854 4   subscription_management subscription_management_pkey 
   CONSTRAINT     r   ALTER TABLE ONLY public.subscription_management
    ADD CONSTRAINT subscription_management_pkey PRIMARY KEY (id);
 ^   ALTER TABLE ONLY public.subscription_management DROP CONSTRAINT subscription_management_pkey;
       public            postgres    false    298            �           2606    186350    super_admin super_admin_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.super_admin
    ADD CONSTRAINT super_admin_pkey PRIMARY KEY (id);
 F   ALTER TABLE ONLY public.super_admin DROP CONSTRAINT super_admin_pkey;
       public            postgres    false    205            X           2606    186876 .   total_order_invoiced total_order_invoiced_pkey 
   CONSTRAINT     l   ALTER TABLE ONLY public.total_order_invoiced
    ADD CONSTRAINT total_order_invoiced_pkey PRIMARY KEY (id);
 X   ALTER TABLE ONLY public.total_order_invoiced DROP CONSTRAINT total_order_invoiced_pkey;
       public            postgres    false    302            Z           2606    186887 .   total_order_refunded total_order_refunded_pkey 
   CONSTRAINT     l   ALTER TABLE ONLY public.total_order_refunded
    ADD CONSTRAINT total_order_refunded_pkey PRIMARY KEY (id);
 X   ALTER TABLE ONLY public.total_order_refunded DROP CONSTRAINT total_order_refunded_pkey;
       public            postgres    false    304                       2606    186449 .   trading_type_details trading_type_details_pkey 
   CONSTRAINT     l   ALTER TABLE ONLY public.trading_type_details
    ADD CONSTRAINT trading_type_details_pkey PRIMARY KEY (id);
 X   ALTER TABLE ONLY public.trading_type_details DROP CONSTRAINT trading_type_details_pkey;
       public            postgres    false    223            t           2606    223373 &   useful_links_cms useful_links_cms_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.useful_links_cms
    ADD CONSTRAINT useful_links_cms_pkey PRIMARY KEY (id);
 P   ALTER TABLE ONLY public.useful_links_cms DROP CONSTRAINT useful_links_cms_pkey;
       public            e-parts-admin    false    334            �           2606    236526 "   user_managment user_managment_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY public.user_managment
    ADD CONSTRAINT user_managment_pkey PRIMARY KEY (id);
 L   ALTER TABLE ONLY public.user_managment DROP CONSTRAINT user_managment_pkey;
       public            postgres    false    356            �           2606    183140    users_login users_login_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.users_login
    ADD CONSTRAINT users_login_pkey PRIMARY KEY (id);
 F   ALTER TABLE ONLY public.users_login DROP CONSTRAINT users_login_pkey;
       public            postgres    false    203            B           2606    186755    vin_history vin_history_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.vin_history
    ADD CONSTRAINT vin_history_pkey PRIMARY KEY (id);
 F   ALTER TABLE ONLY public.vin_history DROP CONSTRAINT vin_history_pkey;
       public            postgres    false    280            �           2606    236595 )   sms_template sms_template_created_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.sms_template
    ADD CONSTRAINT sms_template_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.user_managment(id);
 S   ALTER TABLE ONLY public.sms_template DROP CONSTRAINT sms_template_created_by_fkey;
       public          postgres    false    356    358    3466            �           2606    236600 *   sms_template sms_template_modified_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.sms_template
    ADD CONSTRAINT sms_template_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.user_managment(id);
 T   ALTER TABLE ONLY public.sms_template DROP CONSTRAINT sms_template_modified_by_fkey;
       public          postgres    false    3466    358    356            �           2606    236527 5   user_managment user_managment_login_user_type_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.user_managment
    ADD CONSTRAINT user_managment_login_user_type_id_fkey FOREIGN KEY (login_user_type_id) REFERENCES public.login_user_type(login_user_type_id);
 _   ALTER TABLE ONLY public.user_managment DROP CONSTRAINT user_managment_login_user_type_id_fkey;
       public          postgres    false    354    3464    356            l      x������ � �      5      x������ � �      �      x������ � �      �   �   x�M��
�@@���뮦YA݂���r�g\ܱ��.Ż��8��pq���s��8�34��/���!��DIZ�Ҁ�#�I�)�8gI�6������u�p�Y��8�$��Ep�|�>�ľ�ݝzϸ͗E���u]�������ɳ m�I�ܸڸ�_�ez��4}J�E      ;      x������ � �      )      x������ � �      d      x������ � �      �   �   x���J�@�����0{Hh�D�]D�x!eݎہ=����ۻ�BA�~���'�~�|� ����T�aF4���9�0C��a�|D�0-3�3M5yh)18�G�A%�'�KJS�s��+3!�LF�_�\ؠ����������v�r*oޖO�O�͒S�A��S��ɢ�o�ʶn��Fn�n��=����q���o��F��+YԢ��h+�~���w�++��9��      Z      x������ � �      B      x������ � �      b      x������ � �      �   .   x�3�v!##C]3]#NCTN	��s]�(��+F��� �D      �   6   x�3�42�4�t���7�4202�50�54P04�2��25�305��#����� ��      T      x������ � �      �      x������ � �      !   �   x�u�KN1D��)r�����$;4�A�@B� �̂�Ӄ�К�������O/oB�������9g؈/T�f���PZI��;�
���{��
�떅$7�fEd��H��YPi��\�X��5;r�QT�6��'k oD���5r�I�6$���$7�1���t0T�|T�"���Q�QI��,<HG5(ţ�M��PP3ԣb��Yx�Ձ��Y?+1��ޑ�Q�L�X���Y㌯�k���         s   x�m�=�0@��>E.�?q�z������P�?��P����N�(�T�ʣ��[J�x�l%>E-͓�&Yn��UM���t*��8�/�Wp|=�2*[�.N����o�e"�      �      x������ � �      �   �   x����J1�u�+�T�;ώd@Ľ� H���)ȋNF�ƍ��w�]��Pe�kY1q���f�̡�K,덤Q琰?�Pr�б_VG���c�>�_���{=K�x.���R@%RP'��
�����OJ���"�m�r�8o���>X��֙�Z�ҍ�^���a�5��Yۍ�����T�k�l������3��n{�����꿩o�0_�1�      D      x������ � �      ~      x������ � �      �   �  x�uR�n�0='_A��vim'�ts�X ���6m�)A����G)�f_D����Qgqx����G`+`f�����:4��I�K���C���%z��=a����`���x8U�,+���2���Ȅ�%�4��d�(9��/ʵpG�ah)c��|V~Q���}�b��:�Շ/U�O�N�6]ȯt������9ܲ:�Uq��:�A��h�瀝���Ux����� �:(�����!�-��vQs��̡	:�R��.�g�{/�@0��-Ȭ,VUn:B7�ύWm|X����؛I��SŰZ�r��aL�Q�xB���|t��<f3&�ɌY3J�FQ��G����Y��� ��Jgի7�{�J�]����sK��tCl=q��g��(=��Lﮩۨ���&��_�r]�b�~tXt��T�����:=�����~���r�      �   3  x�5�MO�0�����4��֦-�l�n7.!1��|�q?�l�����<v�v�W�p�EL!��ﵷ�\��-Z�6+ѭ��V�(��y|~xy{-�a��A�ݓ����,ho���E^��.�b�4��(*r#��܌��@J�z�6k 9E�tr�����s4���� ����ٜ�N�1_�]�!Q�X4����g�����SҀ?8)b��$c�U~fQG�N�3�B~(�(6[�y���?��b�6um��f�!�NQ�@��qu�k}$����w_d���6};����X7Ə�
n,Ţ��ʲ�t���      3      x������ � �      1      x������ � �      |      x������ � �      �   W   x�3�tt����,�4202�50�5��4D�ps:�F�)Vi����C�RSNO�� _WO�W�J͠����Uh����=... z70�      �   "   x�3�v!##C]3]#NCTN	W� �|�      �   �   x�E�M�0��u9�\ �C0jb\�xSa�I������%�`��_�k����̪V�M@2q�f�?�	2 ���P���dI�Q~���Y��R��q�4��:e74jIG����q�Y��1F�D��\���9>h�{�V(������M�{<?�)xƓݹ�1�k̾n��*�^��VE�˳G�      >      x������ � �      �   p   x�3����M��K,�LO,����� �9��u�t��Q9%\F��I��%
�Ŝ�xse&g�Pg�霟W��L��f�~���
j
��)�����O)M.!�.F��� ��7�      +      x������ � �      /      x������ � �      f      x������ � �      j      x������ � �      h      x������ � �      p      x������ � �      V      x������ � �      X      x������ � �      �   <   x�3�H�K��K�J�M,�.Vp�/R��)�$��s����p�rJ�b���� �8      @      x������ � �      n      x������ � �      J      x������ � �      �   S   x�u�;
�PD�ڬ�^^�I�7�k�u����]86m�����l�}A	���7���?��R������%�Ү"r��9      \      x������ � �      ^      x������ � �      �   A   x�3�vTpu�ҜFF���f
�V�&V��z���V�����FFz����=... ��            x������ � �      8      x������ � �      P      x������ � �      N      x������ � �      R      x������ � �      L      x������ � �      %   E   x�3�v��q5�4202�50�5�P04�24�20�3�0��X��*Z�Z�eJ�b���� ؛�      �   E   x�3�vTpu�ҜFF���f
�V��V&�z�f��V������V&zƆ�p��=... ���         d   x�m�1
�0D�:9�Ȳ;����NA���Eҙ��������%�:V�5�pւ'�CU�T��ܑM�?'3�����s���w�����/�1>_-$�      �   5   x�3�v!##C]s]3C+#s+=KsmS+cNC��%\1z\\\ QI}      z      x������ � �      t   �   x�3�ttw��LK�)N�9��u,t�̬ḼLM���8qK8�r�r�&�%��q���r;�5�+Z��Y���!�)��2˘�3D�����f�o��+�6#=sCl�e]@��qqq ��0      -      x������ � �      �     x����J�0 �s�sT�?i�l� �����dvw ?�I��7�aaqWϕ@2�0�H��)�耦�80��"%Pӕ���#�i�A�(j�{@K9��@Z��)��d�,>������0}�Fpj�(Ko���CJӶil��BL۾m�FM�h+��7�#i�/��9�Sn�;����Lt�7������֓���k;V��b�)�+X��"����~"V"X?t|�+������+��rcx4�9�D�S(�D��?9�3y��� �uY���#�      �   5   x�3�v!##C]s]3C+S+=Kc3mS+cNC��%\1z\\\ OBp      �   �   x���=�0E�_�viyI�6�D�\\b�D������":q�ý�s$H8r������㶷!E8�@��5i���ڨ�\�-LE��O����r�=�?����!�:�42>��6���1;�1^�}g����(i�*i^���I�J!�ڗ?      �   �   x���M�� �5\����JU/R)�P�X�'*�\�i���K��z�<����ɂ�S.'�9g� �!70��,���!��#�k�^�*����E��i���9׽��b)��.q"=9��.���F��P����7Z����9���(���������|���Tkm#��+�_��a8��l�'q����~(��RJ�1�7      #   �   x�u�1�0��99E/P�ϱ��'�#���E���J���I�$�n�e�>��3��؃{H��zz�6�' ���}�5�T��g2p��a�(�4����;<V���`m��n�m�6 ����*k�gZ@E���5�)��D�9�      �      x������ � �            x������ � �      F      x������ � �      H      x������ � �         .   x�3�v�4202�50�5�T00�26�20�31�4�-Q����� f9      r      x������ � �            x������ � �      v      x������ � �      x      x������ � �      '   /   x�3�v�4202�50�5�T00�26�22�332�4�#S����� ~w      �   ~   x�3�tJM��MUHTp*�L-��())(���///�K��O�I�K������4�4202�50�52B�s�p!�	N��!�cts�9CR�r�����R2K2���4Ʉ3�(�,1�R! ?'3��Lcb���� 6OF      �   J   x�3�4�pRpt����LL���sH-H,*)�K���47�4�4445��40722����,�4��CF\1z\\\ ��         D   x�3�4�LL���sH-H,*)�K�υ
r��EA"qN������������*����� ���      `      x������ � �     
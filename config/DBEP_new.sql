PGDMP                         y            Epartslatest    12.2    12.2 �   <           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            =           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            >           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            ?           1262    211652    Epartslatest    DATABASE     �   CREATE DATABASE "Epartslatest" WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'English_United States.1252' LC_CTYPE = 'English_United States.1252';
    DROP DATABASE "Epartslatest";
                e-parts-admin    false            �            1259    221561    Login_user_type    TABLE     z   CREATE TABLE public."Login_user_type" (
    "Login_user_type_id" integer,
    "Login_user_type_name" character varying
);
 %   DROP TABLE public."Login_user_type";
       public         heap    postgres    false            M           1259    222226    Rating_review    TABLE     #  CREATE TABLE public."Rating_review" (
    id integer NOT NULL,
    seller_id bigint,
    rate_seller numeric,
    review_seller character varying,
    status integer,
    action character varying,
    created_at date,
    created_by integer,
    modified_at date,
    modified_by integer
);
 #   DROP TABLE public."Rating_review";
       public         heap    postgres    false            L           1259    222224    Rating_review_id_seq    SEQUENCE     �   CREATE SEQUENCE public."Rating_review_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public."Rating_review_id_seq";
       public          postgres    false    333            @           0    0    Rating_review_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE public."Rating_review_id_seq" OWNED BY public."Rating_review".id;
          public          postgres    false    332                       1259    221838    VAT_details    TABLE     /  CREATE TABLE public."VAT_details" (
    id integer NOT NULL,
    vat_percentage integer,
    effective_date date,
    tax_code_id integer,
    pos_id integer,
    juristriction_id integer,
    status integer,
    created_at date,
    created_by integer,
    modified_at date,
    modified_by integer
);
 !   DROP TABLE public."VAT_details";
       public         heap    postgres    false                       1259    221836    VAT_details_id_seq    SEQUENCE     �   CREATE SEQUENCE public."VAT_details_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public."VAT_details_id_seq";
       public          postgres    false    262            A           0    0    VAT_details_id_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public."VAT_details_id_seq" OWNED BY public."VAT_details".id;
          public          postgres    false    261            �            1259    221541    application_user    TABLE     �  CREATE TABLE public.application_user (
    application_user_id integer NOT NULL,
    user_id bigint,
    portal_user_id bigint,
    user_name character varying,
    user_email_id character varying,
    phone_no character varying,
    designation character varying,
    email_verification_status boolean,
    phoneno_verified boolean,
    status integer,
    created_at date,
    created_by integer,
    modified_at date,
    modified_by integer
);
 $   DROP TABLE public.application_user;
       public         heap    postgres    false            �            1259    221539 (   application_user_application_user_id_seq    SEQUENCE     �   CREATE SEQUENCE public.application_user_application_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ?   DROP SEQUENCE public.application_user_application_user_id_seq;
       public          postgres    false    207            B           0    0 (   application_user_application_user_id_seq    SEQUENCE OWNED BY     u   ALTER SEQUENCE public.application_user_application_user_id_seq OWNED BY public.application_user.application_user_id;
          public          postgres    false    206            W           1259    222281    audit_trail    TABLE     �  CREATE TABLE public.audit_trail (
    id integer NOT NULL,
    module_id integer,
    field_name character varying,
    old_value character varying,
    new_value character varying,
    user_id character varying,
    time_stamp date,
    status integer,
    audit_action character varying,
    created_at date,
    created_by integer,
    modified_at date,
    modified_by integer
);
    DROP TABLE public.audit_trail;
       public         heap    postgres    false            V           1259    222279    audit_trail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.audit_trail_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.audit_trail_id_seq;
       public          postgres    false    343            C           0    0    audit_trail_id_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE public.audit_trail_id_seq OWNED BY public.audit_trail.id;
          public          postgres    false    342                       1259    221951    brands    TABLE     l  CREATE TABLE public.brands (
    brand_id integer NOT NULL,
    brand_code character varying,
    brand_name character varying,
    erp_id integer,
    last_integrated_date date,
    brand_api_id character varying,
    brand_image character varying,
    status integer,
    created_at date,
    created_by integer,
    modified_at date,
    modified_by integer
);
    DROP TABLE public.brands;
       public         heap    postgres    false                       1259    221949    brands_brand_id_seq    SEQUENCE     �   CREATE SEQUENCE public.brands_brand_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.brands_brand_id_seq;
       public          postgres    false    283            D           0    0    brands_brand_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE public.brands_brand_id_seq OWNED BY public.brands.brand_id;
          public          postgres    false    282            �            1259    221673    bulk_upload_details    TABLE     4  CREATE TABLE public.bulk_upload_details (
    id integer NOT NULL,
    file_name character varying,
    "file_URL" character varying,
    product_type_id integer,
    status integer,
    remarks character varying,
    created_at date,
    created_by integer,
    modified_at date,
    modified_by integer
);
 '   DROP TABLE public.bulk_upload_details;
       public         heap    postgres    false            �            1259    221671    bulk_upload_details_id_seq    SEQUENCE     �   CREATE SEQUENCE public.bulk_upload_details_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 1   DROP SEQUENCE public.bulk_upload_details_id_seq;
       public          postgres    false    232            E           0    0    bulk_upload_details_id_seq    SEQUENCE OWNED BY     Y   ALTER SEQUENCE public.bulk_upload_details_id_seq OWNED BY public.bulk_upload_details.id;
          public          postgres    false    231            E           1259    222182    buyer_customer_details    TABLE     �  CREATE TABLE public.buyer_customer_details (
    buyer_customer_id integer NOT NULL,
    buyer_id integer,
    customer_name character varying,
    email character varying,
    phone character varying,
    address character varying,
    car_details_brand integer,
    model_id integer,
    status integer,
    action character varying,
    created_at date,
    created_by integer,
    modified_at date,
    modified_by integer
);
 *   DROP TABLE public.buyer_customer_details;
       public         heap    postgres    false            D           1259    222180 ,   buyer_customer_details_buyer_customer_id_seq    SEQUENCE     �   CREATE SEQUENCE public.buyer_customer_details_buyer_customer_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 C   DROP SEQUENCE public.buyer_customer_details_buyer_customer_id_seq;
       public          postgres    false    325            F           0    0 ,   buyer_customer_details_buyer_customer_id_seq    SEQUENCE OWNED BY     }   ALTER SEQUENCE public.buyer_customer_details_buyer_customer_id_seq OWNED BY public.buyer_customer_details.buyer_customer_id;
          public          postgres    false    324            A           1259    222160    buyer_saved_quote    TABLE     ^  CREATE TABLE public.buyer_saved_quote (
    buyer_quote_id integer NOT NULL,
    buyer_id bigint,
    cart_item_id integer,
    quote_no character varying,
    tax_code integer,
    buyer_customer_id integer,
    status_of_quote integer,
    quantity numeric,
    price numeric,
    margin_per numeric,
    price_with_margin numeric,
    customer_detail_name character varying,
    address character varying,
    car_details_brand integer,
    model_id integer,
    status integer,
    action character varying,
    created_at date,
    created_by integer,
    modified_at date,
    modified_by integer
);
 %   DROP TABLE public.buyer_saved_quote;
       public         heap    postgres    false            @           1259    222158 $   buyer_saved_quote_buyer_quote_id_seq    SEQUENCE     �   CREATE SEQUENCE public.buyer_saved_quote_buyer_quote_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ;   DROP SEQUENCE public.buyer_saved_quote_buyer_quote_id_seq;
       public          postgres    false    321            G           0    0 $   buyer_saved_quote_buyer_quote_id_seq    SEQUENCE OWNED BY     m   ALTER SEQUENCE public.buyer_saved_quote_buyer_quote_id_seq OWNED BY public.buyer_saved_quote.buyer_quote_id;
          public          postgres    false    320            C           1259    222171    buyer_saved_quote_service_items    TABLE     ^  CREATE TABLE public.buyer_saved_quote_service_items (
    id integer NOT NULL,
    buyer_quote_id integer,
    service_item character varying,
    quantity numeric,
    price numeric,
    tax_code integer,
    status integer,
    action character varying,
    created_at date,
    created_by integer,
    modified_at date,
    modified_by integer
);
 3   DROP TABLE public.buyer_saved_quote_service_items;
       public         heap    postgres    false            B           1259    222169 &   buyer_saved_quote_service_items_id_seq    SEQUENCE     �   CREATE SEQUENCE public.buyer_saved_quote_service_items_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 =   DROP SEQUENCE public.buyer_saved_quote_service_items_id_seq;
       public          postgres    false    323            H           0    0 &   buyer_saved_quote_service_items_id_seq    SEQUENCE OWNED BY     q   ALTER SEQUENCE public.buyer_saved_quote_service_items_id_seq OWNED BY public.buyer_saved_quote_service_items.id;
          public          postgres    false    322            7           1259    222105    car_management    TABLE     �  CREATE TABLE public.car_management (
    id integer NOT NULL,
    buyer_id bigint,
    buyer_customer_id integer,
    car_info character varying,
    brand_info character varying,
    model_info character varying,
    vin_no character varying,
    action character varying,
    status integer,
    created_at date,
    created_by integer,
    modified_at date,
    modified_by integer
);
 "   DROP TABLE public.car_management;
       public         heap    postgres    false            6           1259    222103    car_management_id_seq    SEQUENCE     �   CREATE SEQUENCE public.car_management_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public.car_management_id_seq;
       public          postgres    false    311            I           0    0    car_management_id_seq    SEQUENCE OWNED BY     O   ALTER SEQUENCE public.car_management_id_seq OWNED BY public.car_management.id;
          public          postgres    false    310            !           1259    221984    cars    TABLE     l  CREATE TABLE public.cars (
    car_id integer NOT NULL,
    car_name character varying,
    brand_id integer,
    model_id integer,
    parameters_id integer,
    parameters_value character varying,
    erp_id integer,
    last_integrated_date date,
    status integer,
    created_at date,
    created_by integer,
    modified_at date,
    modified_by integer
);
    DROP TABLE public.cars;
       public         heap    postgres    false                        1259    221982    cars_car_id_seq    SEQUENCE     �   CREATE SEQUENCE public.cars_car_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.cars_car_id_seq;
       public          postgres    false    289            J           0    0    cars_car_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE public.cars_car_id_seq OWNED BY public.cars.car_id;
          public          postgres    false    288            G           1259    222193    cart_details    TABLE     �  CREATE TABLE public.cart_details (
    cart_item_id integer NOT NULL,
    buyer_id bigint,
    product_id numeric,
    price numeric,
    quantity numeric,
    tax_code integer,
    tax_amount numeric,
    final_price numeric,
    checkout_status character varying,
    action character varying,
    created_at date,
    created_by integer,
    modified_at date,
    modified_by integer
);
     DROP TABLE public.cart_details;
       public         heap    postgres    false            F           1259    222191    cart_details_cart_item_id_seq    SEQUENCE     �   CREATE SEQUENCE public.cart_details_cart_item_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 4   DROP SEQUENCE public.cart_details_cart_item_id_seq;
       public          postgres    false    327            K           0    0    cart_details_cart_item_id_seq    SEQUENCE OWNED BY     _   ALTER SEQUENCE public.cart_details_cart_item_id_seq OWNED BY public.cart_details.cart_item_id;
          public          postgres    false    326            ?           1259    222149    category_request_management    TABLE     �  CREATE TABLE public.category_request_management (
    id integer NOT NULL,
    category_code character varying,
    category_name character varying,
    description character varying,
    cat_req_status_id integer,
    reason_for_reject integer,
    status integer,
    action character varying,
    created_at date,
    created_by integer,
    modified_at date,
    modified_by integer
);
 /   DROP TABLE public.category_request_management;
       public         heap    postgres    false            >           1259    222147 "   category_request_management_id_seq    SEQUENCE     �   CREATE SEQUENCE public.category_request_management_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 9   DROP SEQUENCE public.category_request_management_id_seq;
       public          postgres    false    319            L           0    0 "   category_request_management_id_seq    SEQUENCE OWNED BY     i   ALTER SEQUENCE public.category_request_management_id_seq OWNED BY public.category_request_management.id;
          public          postgres    false    318            �            1259    221772    category_request_status    TABLE     �   CREATE TABLE public.category_request_status (
    cat_req_status_id integer NOT NULL,
    cat_req_status_name character varying,
    remarks character varying,
    created_at date,
    created_by integer,
    modified_at date,
    modified_by integer
);
 +   DROP TABLE public.category_request_status;
       public         heap    postgres    false            �            1259    221770 -   category_request_status_cat_req_status_id_seq    SEQUENCE     �   CREATE SEQUENCE public.category_request_status_cat_req_status_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 D   DROP SEQUENCE public.category_request_status_cat_req_status_id_seq;
       public          postgres    false    250            M           0    0 -   category_request_status_cat_req_status_id_seq    SEQUENCE OWNED BY        ALTER SEQUENCE public.category_request_status_cat_req_status_id_seq OWNED BY public.category_request_status.cat_req_status_id;
          public          postgres    false    249            -           1259    222050    commission_management    TABLE     K  CREATE TABLE public.commission_management (
    id integer NOT NULL,
    product_type_id integer,
    trading_type_id integer,
    commission_precentage numeric,
    effective_date date,
    action character varying,
    status integer,
    created_at date,
    created_by integer,
    modified_at date,
    modified_by integer
);
 )   DROP TABLE public.commission_management;
       public         heap    postgres    false            ,           1259    222048    commission_management_id_seq    SEQUENCE     �   CREATE SEQUENCE public.commission_management_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 3   DROP SEQUENCE public.commission_management_id_seq;
       public          postgres    false    301            N           0    0    commission_management_id_seq    SEQUENCE OWNED BY     ]   ALTER SEQUENCE public.commission_management_id_seq OWNED BY public.commission_management.id;
          public          postgres    false    300            �            1259    221662    configuration_details    TABLE     /  CREATE TABLE public.configuration_details (
    id integer NOT NULL,
    config_name character varying,
    "config_URL" character varying,
    "Config_username" character varying,
    config_password character varying,
    config_port integer,
    config_parameter character varying,
    "config_MobileNumber" integer,
    config_mtype integer,
    config_message integer,
    config_secretkey character varying,
    status integer,
    remarks character varying,
    created_at date,
    created_by integer,
    modified_at date,
    modified_by integer
);
 )   DROP TABLE public.configuration_details;
       public         heap    postgres    false            �            1259    221660    configuration_details_id_seq    SEQUENCE     �   CREATE SEQUENCE public.configuration_details_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 3   DROP SEQUENCE public.configuration_details_id_seq;
       public          postgres    false    230            O           0    0    configuration_details_id_seq    SEQUENCE OWNED BY     ]   ALTER SEQUENCE public.configuration_details_id_seq OWNED BY public.configuration_details.id;
          public          postgres    false    229                       1259    221816    country_details    TABLE       CREATE TABLE public.country_details (
    country_id integer NOT NULL,
    country_name character varying,
    country_code character varying,
    status integer,
    created_at date,
    created_by integer,
    modified_at date,
    modified_by integer
);
 #   DROP TABLE public.country_details;
       public         heap    postgres    false                       1259    221814    country_details_country_id_seq    SEQUENCE     �   CREATE SEQUENCE public.country_details_country_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 5   DROP SEQUENCE public.country_details_country_id_seq;
       public          postgres    false    258            P           0    0    country_details_country_id_seq    SEQUENCE OWNED BY     a   ALTER SEQUENCE public.country_details_country_id_seq OWNED BY public.country_details.country_id;
          public          postgres    false    257                        1259    221805    currency_details    TABLE     
  CREATE TABLE public.currency_details (
    currency_id integer NOT NULL,
    currency_name character varying,
    currency_shortcode character varying,
    status integer,
    created_at date,
    created_by integer,
    modified_at date,
    modified_by integer
);
 $   DROP TABLE public.currency_details;
       public         heap    postgres    false            �            1259    221803     currency_details_currency_id_seq    SEQUENCE     �   CREATE SEQUENCE public.currency_details_currency_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 7   DROP SEQUENCE public.currency_details_currency_id_seq;
       public          postgres    false    256            Q           0    0     currency_details_currency_id_seq    SEQUENCE OWNED BY     e   ALTER SEQUENCE public.currency_details_currency_id_seq OWNED BY public.currency_details.currency_id;
          public          postgres    false    255            �            1259    221610 	   dashboard    TABLE     �   CREATE TABLE public.dashboard (
    dashboard_id integer NOT NULL,
    dashboard_name character varying,
    "Login_user_type_id" integer,
    status integer,
    created_at date,
    created_by integer,
    modified_at date,
    modified_by integer
);
    DROP TABLE public.dashboard;
       public         heap    postgres    false            �            1259    221608    dashboard_dashboard_id_seq    SEQUENCE     �   CREATE SEQUENCE public.dashboard_dashboard_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 1   DROP SEQUENCE public.dashboard_dashboard_id_seq;
       public          postgres    false    220            R           0    0    dashboard_dashboard_id_seq    SEQUENCE OWNED BY     Y   ALTER SEQUENCE public.dashboard_dashboard_id_seq OWNED BY public.dashboard.dashboard_id;
          public          postgres    false    219            [           1259    222303    email_template    TABLE     �  CREATE TABLE public.email_template (
    id integer NOT NULL,
    title character varying,
    email_type character varying,
    encoding_type character varying,
    iso_code character varying,
    description character varying,
    subject character varying,
    email_content character varying,
    attachment character varying,
    status integer,
    action character varying,
    created_at date,
    created_by integer,
    modified_at date,
    modified_by integer
);
 "   DROP TABLE public.email_template;
       public         heap    postgres    false            Z           1259    222301    email_template_id_seq    SEQUENCE     �   CREATE SEQUENCE public.email_template_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public.email_template_id_seq;
       public          postgres    false    347            S           0    0    email_template_id_seq    SEQUENCE OWNED BY     O   ALTER SEQUENCE public.email_template_id_seq OWNED BY public.email_template.id;
          public          postgres    false    346            #           1259    221995    group_management    TABLE     C  CREATE TABLE public.group_management (
    group_id integer NOT NULL,
    car_id integer,
    erp_id integer,
    last_integrated_date date,
    group_code character varying,
    group_name character varying,
    status integer,
    created_at date,
    created_by integer,
    modified_at date,
    modified_by integer
);
 $   DROP TABLE public.group_management;
       public         heap    postgres    false            "           1259    221993    group_management_group_id_seq    SEQUENCE     �   CREATE SEQUENCE public.group_management_group_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 4   DROP SEQUENCE public.group_management_group_id_seq;
       public          postgres    false    291            T           0    0    group_management_group_id_seq    SEQUENCE OWNED BY     _   ALTER SEQUENCE public.group_management_group_id_seq OWNED BY public.group_management.group_id;
          public          postgres    false    290            U           1259    222270    help_management    TABLE     y  CREATE TABLE public.help_management (
    id integer NOT NULL,
    help_management_code character varying,
    subject_name character varying,
    description character varying,
    module_id integer,
    keywords character varying,
    status integer,
    action character varying,
    created_at date,
    created_by integer,
    modified_at date,
    modified_by integer
);
 #   DROP TABLE public.help_management;
       public         heap    postgres    false            T           1259    222268    help_management_id_seq    SEQUENCE     �   CREATE SEQUENCE public.help_management_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.help_management_id_seq;
       public          postgres    false    341            U           0    0    help_management_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE public.help_management_id_seq OWNED BY public.help_management.id;
          public          postgres    false    340            �            1259    221530    intermediator    TABLE     E  CREATE TABLE public.intermediator (
    id integer NOT NULL,
    user_id bigint,
    intermediator_code character varying,
    firstname character varying,
    lastname character varying,
    user_name character varying,
    role_id integer,
    email_id character varying,
    phone_number character varying,
    mobile_number character varying,
    status integer,
    image character varying,
    country integer,
    language_spoken character varying,
    address character varying,
    created_at date,
    created_by integer,
    modified_at date,
    modified_by integer
);
 !   DROP TABLE public.intermediator;
       public         heap    postgres    false            �            1259    221528    intermediator_id_seq    SEQUENCE     �   CREATE SEQUENCE public.intermediator_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.intermediator_id_seq;
       public          postgres    false    205            V           0    0    intermediator_id_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.intermediator_id_seq OWNED BY public.intermediator.id;
          public          postgres    false    204            S           1259    222259    inventory_manangement    TABLE     w  CREATE TABLE public.inventory_manangement (
    id integer NOT NULL,
    warehouse_id integer,
    seller_id bigint,
    product_id integer,
    brand_info character varying,
    model_info character varying,
    car_info character varying,
    price numeric,
    price_after_sale numeric,
    onhand numeric,
    e_part_qnty numeric,
    buffer_qnty numeric,
    re_order_level numeric,
    threshold_limit numeric,
    sold numeric,
    balance numeric,
    bin_loc character varying,
    status integer,
    action character varying,
    created_at date,
    created_by integer,
    modified_at date,
    modified_by integer
);
 )   DROP TABLE public.inventory_manangement;
       public         heap    postgres    false            R           1259    222257    inventory_manangement_id_seq    SEQUENCE     �   CREATE SEQUENCE public.inventory_manangement_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 3   DROP SEQUENCE public.inventory_manangement_id_seq;
       public          postgres    false    339            W           0    0    inventory_manangement_id_seq    SEQUENCE OWNED BY     ]   ALTER SEQUENCE public.inventory_manangement_id_seq OWNED BY public.inventory_manangement.id;
          public          postgres    false    338                       1259    221868    jurisdiction    TABLE     &  CREATE TABLE public.jurisdiction (
    jurisdiction_id integer NOT NULL,
    name character varying,
    code character varying,
    erp_id integer,
    last_integrated_date date,
    status integer,
    created_at date,
    created_by integer,
    modified_at date,
    modified_by integer
);
     DROP TABLE public.jurisdiction;
       public         heap    postgres    false                       1259    221866     jurisdiction_jurisdiction_id_seq    SEQUENCE     �   CREATE SEQUENCE public.jurisdiction_jurisdiction_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 7   DROP SEQUENCE public.jurisdiction_jurisdiction_id_seq;
       public          postgres    false    268            X           0    0     jurisdiction_jurisdiction_id_seq    SEQUENCE OWNED BY     e   ALTER SEQUENCE public.jurisdiction_jurisdiction_id_seq OWNED BY public.jurisdiction.jurisdiction_id;
          public          postgres    false    267            �            1259    221761    make_offer_status    TABLE     �   CREATE TABLE public.make_offer_status (
    make_offer_status_id integer NOT NULL,
    offer_status_name character varying,
    remarks character varying,
    created_at date,
    created_by integer,
    modified_at date,
    modified_by integer
);
 %   DROP TABLE public.make_offer_status;
       public         heap    postgres    false            �            1259    221759 *   make_offer_status_make_offer_status_id_seq    SEQUENCE     �   CREATE SEQUENCE public.make_offer_status_make_offer_status_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 A   DROP SEQUENCE public.make_offer_status_make_offer_status_id_seq;
       public          postgres    false    248            Y           0    0 *   make_offer_status_make_offer_status_id_seq    SEQUENCE OWNED BY     y   ALTER SEQUENCE public.make_offer_status_make_offer_status_id_seq OWNED BY public.make_offer_status.make_offer_status_id;
          public          postgres    false    247                       1259    221962    models    TABLE     �  CREATE TABLE public.models (
    model_id integer NOT NULL,
    brand_id integer,
    erp_id integer,
    last_integrated_date date,
    model_code character varying,
    model_name character varying,
    model_api_id character varying,
    model_image character varying,
    status integer,
    created_at date,
    created_by integer,
    modified_at date,
    modified_by integer
);
    DROP TABLE public.models;
       public         heap    postgres    false                       1259    221960    models_model_id_seq    SEQUENCE     �   CREATE SEQUENCE public.models_model_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.models_model_id_seq;
       public          postgres    false    285            Z           0    0    models_model_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE public.models_model_id_seq OWNED BY public.models.model_id;
          public          postgres    false    284            �            1259    221599    module    TABLE       CREATE TABLE public.module (
    module_id integer NOT NULL,
    module_type_id integer,
    module_name character varying,
    module_abbr character varying,
    status integer,
    created_at date,
    created_by integer,
    modified_at date,
    modified_by integer
);
    DROP TABLE public.module;
       public         heap    postgres    false            �            1259    221597    module_module_id_seq    SEQUENCE     �   CREATE SEQUENCE public.module_module_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.module_module_id_seq;
       public          postgres    false    218            [           0    0    module_module_id_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.module_module_id_seq OWNED BY public.module.module_id;
          public          postgres    false    217            �            1259    221588    module_type    TABLE     �   CREATE TABLE public.module_type (
    module_type_id integer NOT NULL,
    module_type_name character varying,
    status integer,
    created_at date,
    created_by integer,
    modified_at date,
    modified_by integer
);
    DROP TABLE public.module_type;
       public         heap    postgres    false            �            1259    221586    module_type_module_type_id_seq    SEQUENCE     �   CREATE SEQUENCE public.module_type_module_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 5   DROP SEQUENCE public.module_type_module_type_id_seq;
       public          postgres    false    216            \           0    0    module_type_module_type_id_seq    SEQUENCE OWNED BY     a   ALTER SEQUENCE public.module_type_module_type_id_seq OWNED BY public.module_type.module_type_id;
          public          postgres    false    215                       1259    221912    new_buyer_details    TABLE     	  CREATE TABLE public.new_buyer_details (
    id bigint NOT NULL,
    user_id bigint,
    buyer_code character varying,
    email_verification_status boolean,
    business_name character varying,
    business_com_logo character varying,
    email_id character varying,
    subscription_id integer,
    subscription_effective_date date,
    business_status boolean,
    billing_country integer,
    billing_state integer,
    billing_city character varying,
    b_address1 character varying,
    b_address2 character varying,
    b_displayname character varying,
    b_phonenumber numeric,
    b_country_code character varying,
    jurisdiction_id integer,
    pos_id integer,
    shipping_country integer,
    shipping_state integer,
    shipping_city character varying,
    s_address1 character varying,
    s_address2 character varying,
    s_displayname character varying,
    s_phonenumber numeric,
    s_country_code character varying,
    phoneno_verified boolean,
    drop_address character varying,
    googlemap_link character varying,
    payment_type_info json,
    product_type_id integer,
    trading_type_id integer,
    iv_location integer,
    iv_trade_licenese character varying,
    iv_res_national character varying,
    iv_exp_date date,
    iv_country_issue integer,
    iv_f_name character varying,
    iv_m_name character varying,
    iv_l_name character varying,
    iv_dob date,
    iv_r_bussiness_name character varying,
    iv_licenese_no character varying,
    iv_r_address character varying,
    id_doc_front character varying,
    id_doc_back character varying,
    bus_doc_license character varying,
    registration_status integer,
    cancel_reason integer,
    vat_registration_number character varying,
    tax_registration_exp_date date,
    vat_reg_certificate_certificate character varying,
    bank_benefi_name character varying,
    bank_name character varying,
    bank_ac_no character varying,
    bank_iban character varying,
    bank_branch_name character varying,
    swift_code character varying,
    bank_currency integer,
    bank_browse_file character varying,
    erp_id integer,
    last_integrated_date date,
    company_buyer_representative boolean,
    t_and_c_acknowledge boolean,
    created_at date,
    created_by integer,
    modified_at date,
    modified_by integer
);
 %   DROP TABLE public.new_buyer_details;
       public         heap    postgres    false                       1259    221910    new_buyer_details_id_seq    SEQUENCE     �   CREATE SEQUENCE public.new_buyer_details_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 /   DROP SEQUENCE public.new_buyer_details_id_seq;
       public          postgres    false    276            ]           0    0    new_buyer_details_id_seq    SEQUENCE OWNED BY     U   ALTER SEQUENCE public.new_buyer_details_id_seq OWNED BY public.new_buyer_details.id;
          public          postgres    false    275                       1259    221923    new_seller_details    TABLE     4	  CREATE TABLE public.new_seller_details (
    id bigint NOT NULL,
    user_id bigint,
    seller_code character varying,
    email_verification_status boolean,
    email_id character varying,
    b_multi_branch boolean,
    business_name character varying,
    business_com_logo character varying,
    subscription_id integer,
    subscription_effective_date date,
    payout_shed_id integer,
    business_status boolean,
    billing_country integer,
    billing_state integer,
    billing_city character varying,
    b_address1 character varying,
    b_address2 character varying,
    jurisdiction_id integer,
    pos_id integer,
    b_displayname character varying,
    b_phonenumber numeric,
    b_country_code character varying,
    shipping_country integer,
    shipping_state integer,
    shipping_city character varying,
    s_address1 character varying,
    s_address2 character varying,
    s_displayname character varying,
    s_phonenumber numeric,
    s_country_code numeric,
    phoneno_verified boolean,
    drop_address character varying,
    googlemap_link character varying,
    payment_type_info json,
    product_type_id integer,
    trading_type_id integer,
    iv_location integer,
    iv_licenese character varying,
    iv_national_id character varying,
    iv_exp_date date,
    iv_country_issue integer,
    iv_f_name character varying,
    iv_m_name character varying,
    iv_l_name character varying,
    iv_dob date,
    iv_r_bussiness_name character varying,
    iv_licenese_no character varying,
    iv_r_address character varying,
    id_doc_front character varying,
    id_doc_back character varying,
    bus_doc_license character varying,
    registration_status integer,
    fail_reason integer,
    tax_registration_number character varying,
    tax_registration_exp_date date,
    vat_reg_certificate character varying,
    bank_benefi_name character varying,
    bank_name character varying,
    bank_ac_no character varying,
    bank_iban character varying,
    bank_branch_name character varying,
    swift_code character varying,
    bank_currency integer,
    bank_browse_file character varying,
    company_seller_representative boolean,
    t_and_c_acknowledge boolean,
    erp_id integer,
    last_integrated_date date,
    created_at date,
    created_by integer,
    modified_at date,
    modified_by integer
);
 &   DROP TABLE public.new_seller_details;
       public         heap    postgres    false                       1259    221921    new_seller_details_id_seq    SEQUENCE     �   CREATE SEQUENCE public.new_seller_details_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 0   DROP SEQUENCE public.new_seller_details_id_seq;
       public          postgres    false    278            ^           0    0    new_seller_details_id_seq    SEQUENCE OWNED BY     W   ALTER SEQUENCE public.new_seller_details_id_seq OWNED BY public.new_seller_details.id;
          public          postgres    false    277            I           1259    222204    newsletter_management    TABLE     U  CREATE TABLE public.newsletter_management (
    id integer NOT NULL,
    "Title" character varying,
    user_id character varying,
    registered character varying,
    email_id character varying,
    status integer,
    action character varying,
    created_at date,
    created_by integer,
    modified_at date,
    modified_by integer
);
 )   DROP TABLE public.newsletter_management;
       public         heap    postgres    false            H           1259    222202    newsletter_management_id_seq    SEQUENCE     �   CREATE SEQUENCE public.newsletter_management_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 3   DROP SEQUENCE public.newsletter_management_id_seq;
       public          postgres    false    329            _           0    0    newsletter_management_id_seq    SEQUENCE OWNED BY     ]   ALTER SEQUENCE public.newsletter_management_id_seq OWNED BY public.newsletter_management.id;
          public          postgres    false    328            K           1259    222215    notification    TABLE       CREATE TABLE public.notification (
    id integer NOT NULL,
    title character varying,
    advance_filter character varying,
    module_id integer,
    execute_on character varying,
    notification_type character varying,
    email_template integer,
    email_recipient_type character varying,
    email_recipient character varying,
    sms_template integer,
    sms_recipient_type character varying,
    sms_recipient character varying,
    push_notification_title character varying,
    push_notification character varying,
    push_notification_seller character varying,
    push_notification_seller_name character varying,
    status integer,
    action character varying,
    created_at date,
    created_by integer,
    modified_at date,
    modified_by integer
);
     DROP TABLE public.notification;
       public         heap    postgres    false            J           1259    222213    notification_id_seq    SEQUENCE     �   CREATE SEQUENCE public.notification_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.notification_id_seq;
       public          postgres    false    331            `           0    0    notification_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE public.notification_id_seq OWNED BY public.notification.id;
          public          postgres    false    330            Q           1259    222248    offer_management    TABLE     �  CREATE TABLE public.offer_management (
    id integer NOT NULL,
    buyer_id bigint,
    seller_id bigint,
    product_id integer,
    part_no character varying,
    asking_amount numeric,
    asking_quantity numeric,
    seller_offer_amount numeric,
    seller_offer_quantity numeric,
    remarks character varying,
    approved_by integer,
    offer_date date,
    approved_date date,
    seller_confirmed_date date,
    rejected_date date,
    rejected_reason character varying,
    offer_validity date,
    make_offer_status_id integer,
    status integer,
    action character varying,
    created_at date,
    created_by integer,
    modified_at date,
    modified_by integer
);
 $   DROP TABLE public.offer_management;
       public         heap    postgres    false            P           1259    222246    offer_management_id_seq    SEQUENCE     �   CREATE SEQUENCE public.offer_management_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.offer_management_id_seq;
       public          postgres    false    337            a           0    0    offer_management_id_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE public.offer_management_id_seq OWNED BY public.offer_management.id;
          public          postgres    false    336            /           1259    222061    order_management    TABLE     �  CREATE TABLE public.order_management (
    order_id bigint NOT NULL,
    order_code character varying,
    buyer_id integer,
    user_id integer,
    method_of_payment integer,
    order_erp_id integer,
    order_date date,
    order_type integer,
    order_amount numeric,
    order_status_id integer,
    total_quantity numeric,
    buyer_shipping_address character varying,
    googlemap_link character varying,
    total_vat_amount numeric,
    total_price numeric,
    last_delivery_time date,
    total_discount numeric,
    total_shipping_charges numeric,
    ship_status_id integer,
    buyer_feedback character varying,
    action character varying,
    created_at date,
    created_by integer,
    modified_at date,
    modified_by integer
);
 $   DROP TABLE public.order_management;
       public         heap    postgres    false            .           1259    222059    order_management_order_id_seq    SEQUENCE     �   CREATE SEQUENCE public.order_management_order_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 4   DROP SEQUENCE public.order_management_order_id_seq;
       public          postgres    false    303            b           0    0    order_management_order_id_seq    SEQUENCE OWNED BY     _   ALTER SEQUENCE public.order_management_order_id_seq OWNED BY public.order_management.order_id;
          public          postgres    false    302            3           1259    222083    order_quote_Manage    TABLE     n  CREATE TABLE public."order_quote_Manage" (
    order_quote_id integer NOT NULL,
    order_quote_code character varying,
    buyer_id bigint,
    user_id integer,
    method_of_payment integer,
    order_quote_erp_id integer,
    order_quote_date date,
    total_quantity numeric,
    order_type integer,
    order_amount numeric,
    order_status_id integer,
    total_vat_amount numeric,
    total_price numeric,
    buyer_shipping_address character varying,
    googlemap_link character varying,
    action character varying,
    created_at date,
    created_by integer,
    modified_at date,
    modified_by integer
);
 (   DROP TABLE public."order_quote_Manage";
       public         heap    postgres    false            2           1259    222081 %   order_quote_Manage_order_quote_id_seq    SEQUENCE     �   CREATE SEQUENCE public."order_quote_Manage_order_quote_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 >   DROP SEQUENCE public."order_quote_Manage_order_quote_id_seq";
       public          postgres    false    307            c           0    0 %   order_quote_Manage_order_quote_id_seq    SEQUENCE OWNED BY     s   ALTER SEQUENCE public."order_quote_Manage_order_quote_id_seq" OWNED BY public."order_quote_Manage".order_quote_id;
          public          postgres    false    306            �            1259    221717    order_status    TABLE     �   CREATE TABLE public.order_status (
    order_status_id integer NOT NULL,
    order_status_name character varying,
    remarks character varying,
    created_at date,
    created_by integer,
    modified_at date,
    modified_by integer
);
     DROP TABLE public.order_status;
       public         heap    postgres    false            �            1259    221715     order_status_order_status_id_seq    SEQUENCE     �   CREATE SEQUENCE public.order_status_order_status_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 7   DROP SEQUENCE public.order_status_order_status_id_seq;
       public          postgres    false    240            d           0    0     order_status_order_status_id_seq    SEQUENCE OWNED BY     e   ALTER SEQUENCE public.order_status_order_status_id_seq OWNED BY public.order_status.order_status_id;
          public          postgres    false    239            �            1259    221695    order_type_details    TABLE     �   CREATE TABLE public.order_type_details (
    order_type_id integer NOT NULL,
    order_type_name character varying,
    created_at date,
    created_by integer,
    modified_at date,
    modified_by integer
);
 &   DROP TABLE public.order_type_details;
       public         heap    postgres    false            �            1259    221693 $   order_type_details_order_type_id_seq    SEQUENCE     �   CREATE SEQUENCE public.order_type_details_order_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ;   DROP SEQUENCE public.order_type_details_order_type_id_seq;
       public          postgres    false    236            e           0    0 $   order_type_details_order_type_id_seq    SEQUENCE OWNED BY     m   ALTER SEQUENCE public.order_type_details_order_type_id_seq OWNED BY public.order_type_details.order_type_id;
          public          postgres    false    235                       1259    221973 
   parameters    TABLE     y  CREATE TABLE public.parameters (
    id integer NOT NULL,
    brand_id integer,
    model_id json,
    parameter_id character varying,
    parameter_name character varying,
    parameter_value character varying,
    erp_id integer,
    last_integrated_date date,
    status integer,
    created_at date,
    created_by integer,
    modified_at date,
    modified_by integer
);
    DROP TABLE public.parameters;
       public         heap    postgres    false                       1259    221971    parameters_id_seq    SEQUENCE     �   CREATE SEQUENCE public.parameters_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.parameters_id_seq;
       public          postgres    false    287            f           0    0    parameters_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.parameters_id_seq OWNED BY public.parameters.id;
          public          postgres    false    286            O           1259    222237    part_request_unavail    TABLE     *  CREATE TABLE public.part_request_unavail (
    id integer NOT NULL,
    part_no character varying,
    part_name character varying,
    remarks character varying,
    user_id integer,
    user_type_info character varying,
    user_name character varying,
    requested_date date,
    intermediator_remarks character varying,
    parts_req_status_id character varying,
    parts_request_type character varying,
    status integer,
    action character varying,
    created_at date,
    created_by integer,
    modified_at date,
    modified_by integer
);
 (   DROP TABLE public.part_request_unavail;
       public         heap    postgres    false            N           1259    222235    part_request_unavail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.part_request_unavail_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 2   DROP SEQUENCE public.part_request_unavail_id_seq;
       public          postgres    false    335            g           0    0    part_request_unavail_id_seq    SEQUENCE OWNED BY     [   ALTER SEQUENCE public.part_request_unavail_id_seq OWNED BY public.part_request_unavail.id;
          public          postgres    false    334            )           1259    222028    parts_management    TABLE     �  CREATE TABLE public.parts_management (
    id integer NOT NULL,
    car_id integer,
    group_id integer,
    sub_group_id integer,
    sub_node_id integer,
    part_number character varying,
    part_name character varying,
    part_erp_id integer,
    last_integrated_date date,
    action character varying,
    status integer,
    created_at date,
    created_by integer,
    modified_at date,
    modified_by integer
);
 $   DROP TABLE public.parts_management;
       public         heap    postgres    false            (           1259    222026    parts_management_id_seq    SEQUENCE     �   CREATE SEQUENCE public.parts_management_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.parts_management_id_seq;
       public          postgres    false    297            h           0    0    parts_management_id_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE public.parts_management_id_seq OWNED BY public.parts_management.id;
          public          postgres    false    296            �            1259    221739    parts_req_status    TABLE     �   CREATE TABLE public.parts_req_status (
    parts_req_status_id integer NOT NULL,
    parts_req_status_name character varying,
    remarks character varying,
    created_at date,
    created_by integer,
    modified_at date,
    modified_by integer
);
 $   DROP TABLE public.parts_req_status;
       public         heap    postgres    false            �            1259    221737 (   parts_req_status_parts_req_status_id_seq    SEQUENCE     �   CREATE SEQUENCE public.parts_req_status_parts_req_status_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ?   DROP SEQUENCE public.parts_req_status_parts_req_status_id_seq;
       public          postgres    false    244            i           0    0 (   parts_req_status_parts_req_status_id_seq    SEQUENCE OWNED BY     u   ALTER SEQUENCE public.parts_req_status_parts_req_status_id_seq OWNED BY public.parts_req_status.parts_req_status_id;
          public          postgres    false    243            9           1259    222116    pay_out_schedule_management    TABLE     �  CREATE TABLE public.pay_out_schedule_management (
    id integer NOT NULL,
    pay_out_slot_name character varying,
    payout_shed_id integer,
    no_of_day numeric,
    order_id bigint,
    sub_order_id bigint,
    description character varying,
    action character varying,
    status integer,
    created_at date,
    created_by integer,
    modified_at date,
    modified_by integer
);
 /   DROP TABLE public.pay_out_schedule_management;
       public         heap    postgres    false            8           1259    222114 "   pay_out_schedule_management_id_seq    SEQUENCE     �   CREATE SEQUENCE public.pay_out_schedule_management_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 9   DROP SEQUENCE public.pay_out_schedule_management_id_seq;
       public          postgres    false    313            j           0    0 "   pay_out_schedule_management_id_seq    SEQUENCE OWNED BY     i   ALTER SEQUENCE public.pay_out_schedule_management_id_seq OWNED BY public.pay_out_schedule_management.id;
          public          postgres    false    312            ;           1259    222127    payment_management    TABLE     �  CREATE TABLE public.payment_management (
    id integer NOT NULL,
    seller_id bigint,
    order_id bigint,
    sub_order_id bigint,
    number_of_order integer,
    total_amount numeric,
    commission_amount numeric,
    seller_amount numeric,
    payment_status_id integer,
    status integer,
    action character varying,
    created_at date,
    created_by integer,
    modified_at date,
    modified_by integer
);
 &   DROP TABLE public.payment_management;
       public         heap    postgres    false            :           1259    222125    payment_management_id_seq    SEQUENCE     �   CREATE SEQUENCE public.payment_management_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 0   DROP SEQUENCE public.payment_management_id_seq;
       public          postgres    false    315            k           0    0    payment_management_id_seq    SEQUENCE OWNED BY     W   ALTER SEQUENCE public.payment_management_id_seq OWNED BY public.payment_management.id;
          public          postgres    false    314            �            1259    221783    payment_status    TABLE     �   CREATE TABLE public.payment_status (
    payment_status_id integer NOT NULL,
    payment_status_name character varying,
    remarks character varying,
    created_at date,
    created_by integer,
    modified_at date,
    modified_by integer
);
 "   DROP TABLE public.payment_status;
       public         heap    postgres    false            �            1259    221781 $   payment_status_payment_status_id_seq    SEQUENCE     �   CREATE SEQUENCE public.payment_status_payment_status_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ;   DROP SEQUENCE public.payment_status_payment_status_id_seq;
       public          postgres    false    252            l           0    0 $   payment_status_payment_status_id_seq    SEQUENCE OWNED BY     m   ALTER SEQUENCE public.payment_status_payment_status_id_seq OWNED BY public.payment_status.payment_status_id;
          public          postgres    false    251                       1259    221901    payment_type_details    TABLE     �   CREATE TABLE public.payment_type_details (
    payment_type_id integer NOT NULL,
    payment_type_name character varying,
    status integer,
    created_at date,
    created_by integer,
    modified_at date,
    modified_by integer
);
 (   DROP TABLE public.payment_type_details;
       public         heap    postgres    false                       1259    221899 (   payment_type_details_payment_type_id_seq    SEQUENCE     �   CREATE SEQUENCE public.payment_type_details_payment_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ?   DROP SEQUENCE public.payment_type_details_payment_type_id_seq;
       public          postgres    false    274            m           0    0 (   payment_type_details_payment_type_id_seq    SEQUENCE OWNED BY     u   ALTER SEQUENCE public.payment_type_details_payment_type_id_seq OWNED BY public.payment_type_details.payment_type_id;
          public          postgres    false    273            �            1259    221654    payout_shed_details    TABLE     �   CREATE TABLE public.payout_shed_details (
    payout_shed_id integer NOT NULL,
    subscription_detail_id integer,
    status integer,
    created_at date,
    created_by integer,
    modified_at date,
    modified_by integer
);
 '   DROP TABLE public.payout_shed_details;
       public         heap    postgres    false            �            1259    221652 &   payout_shed_details_payout_shed_id_seq    SEQUENCE     �   CREATE SEQUENCE public.payout_shed_details_payout_shed_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 =   DROP SEQUENCE public.payout_shed_details_payout_shed_id_seq;
       public          postgres    false    228            n           0    0 &   payout_shed_details_payout_shed_id_seq    SEQUENCE OWNED BY     q   ALTER SEQUENCE public.payout_shed_details_payout_shed_id_seq OWNED BY public.payout_shed_details.payout_shed_id;
          public          postgres    false    227            
           1259    221857    place_of_supply    TABLE     (  CREATE TABLE public.place_of_supply (
    pos_id integer NOT NULL,
    tax_name character varying,
    tax_code character varying,
    erp_id integer,
    last_integrated_date date,
    status integer,
    created_at date,
    created_by integer,
    modified_at date,
    modified_by integer
);
 #   DROP TABLE public.place_of_supply;
       public         heap    postgres    false            	           1259    221855    place_of_supply_pos_id_seq    SEQUENCE     �   CREATE SEQUENCE public.place_of_supply_pos_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 1   DROP SEQUENCE public.place_of_supply_pos_id_seq;
       public          postgres    false    266            o           0    0    place_of_supply_pos_id_seq    SEQUENCE OWNED BY     Y   ALTER SEQUENCE public.place_of_supply_pos_id_seq OWNED BY public.place_of_supply.pos_id;
          public          postgres    false    265            +           1259    222039    product_details    TABLE     �  CREATE TABLE public.product_details (
    product_id bigint NOT NULL,
    product_code character varying,
    product_erp_id integer,
    last_integrated_date date,
    product_type integer,
    app_product_type integer,
    brand_info character varying,
    model_info character varying,
    car_info character varying,
    seller_id bigint,
    genuine_part_no character varying,
    verify_button character varying,
    number_of_yr_used numeric,
    image character varying,
    image1 character varying,
    image2 character varying,
    image3 character varying,
    image4 character varying,
    image5 character varying,
    image6 character varying,
    image7 character varying,
    image8 character varying,
    image9 character varying,
    image10 character varying,
    weeight character varying,
    dimension_length numeric,
    dimension_width numeric,
    dimension_height numeric,
    condition character varying,
    description character varying,
    trading_type integer,
    manufacturer_name character varying,
    product_listing_status character varying,
    barcode_no numeric,
    aftermarket_no numeric,
    make_offer boolean,
    status integer,
    created_at date,
    created_by integer,
    modified_at date,
    modified_by integer
);
 #   DROP TABLE public.product_details;
       public         heap    postgres    false            *           1259    222037    product_details_product_id_seq    SEQUENCE     �   CREATE SEQUENCE public.product_details_product_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 5   DROP SEQUENCE public.product_details_product_id_seq;
       public          postgres    false    299            p           0    0    product_details_product_id_seq    SEQUENCE OWNED BY     a   ALTER SEQUENCE public.product_details_product_id_seq OWNED BY public.product_details.product_id;
          public          postgres    false    298                       1259    221879    product_type_details    TABLE       CREATE TABLE public.product_type_details (
    product_type_id integer NOT NULL,
    product_type_name character varying,
    product_desc character varying,
    status integer,
    created_at date,
    created_by integer,
    modified_at date,
    modified_by integer
);
 (   DROP TABLE public.product_type_details;
       public         heap    postgres    false                       1259    221877 (   product_type_details_product_type_id_seq    SEQUENCE     �   CREATE SEQUENCE public.product_type_details_product_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ?   DROP SEQUENCE public.product_type_details_product_type_id_seq;
       public          postgres    false    270            q           0    0 (   product_type_details_product_type_id_seq    SEQUENCE OWNED BY     u   ALTER SEQUENCE public.product_type_details_product_type_id_seq OWNED BY public.product_type_details.product_type_id;
          public          postgres    false    269            �            1259    221750    quote_status    TABLE     �   CREATE TABLE public.quote_status (
    quote_status_id integer NOT NULL,
    quote_status_name character varying,
    remarks character varying,
    created_at date,
    created_by integer,
    modified_at date,
    modified_by integer
);
     DROP TABLE public.quote_status;
       public         heap    postgres    false            �            1259    221748     quote_status_quote_status_id_seq    SEQUENCE     �   CREATE SEQUENCE public.quote_status_quote_status_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 7   DROP SEQUENCE public.quote_status_quote_status_id_seq;
       public          postgres    false    246            r           0    0     quote_status_quote_status_id_seq    SEQUENCE OWNED BY     e   ALTER SEQUENCE public.quote_status_quote_status_id_seq OWNED BY public.quote_status.quote_status_id;
          public          postgres    false    245            �            1259    221794    reason_details    TABLE     �   CREATE TABLE public.reason_details (
    reason_id integer NOT NULL,
    reason_name character varying,
    status integer,
    created_at date,
    created_by integer,
    modified_at date,
    modified_by integer
);
 "   DROP TABLE public.reason_details;
       public         heap    postgres    false            �            1259    221792    reason_details_reason_id_seq    SEQUENCE     �   CREATE SEQUENCE public.reason_details_reason_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 3   DROP SEQUENCE public.reason_details_reason_id_seq;
       public          postgres    false    254            s           0    0    reason_details_reason_id_seq    SEQUENCE OWNED BY     ]   ALTER SEQUENCE public.reason_details_reason_id_seq OWNED BY public.reason_details.reason_id;
          public          postgres    false    253            �            1259    221728    registration_status    TABLE     �   CREATE TABLE public.registration_status (
    reg_status_id integer NOT NULL,
    reg_status_name character varying,
    remarks character varying,
    created_at date,
    created_by integer,
    modified_at date,
    modified_by integer
);
 '   DROP TABLE public.registration_status;
       public         heap    postgres    false            �            1259    221726 %   registration_status_reg_status_id_seq    SEQUENCE     �   CREATE SEQUENCE public.registration_status_reg_status_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 <   DROP SEQUENCE public.registration_status_reg_status_id_seq;
       public          postgres    false    242            t           0    0 %   registration_status_reg_status_id_seq    SEQUENCE OWNED BY     o   ALTER SEQUENCE public.registration_status_reg_status_id_seq OWNED BY public.registration_status.reg_status_id;
          public          postgres    false    241            �            1259    221621    reports    TABLE     �   CREATE TABLE public.reports (
    report_id integer NOT NULL,
    report_name character varying,
    "Login_user_type_id" integer,
    status integer,
    created_at date,
    created_by integer,
    modified_at date,
    modified_by integer
);
    DROP TABLE public.reports;
       public         heap    postgres    false            �            1259    221619    reports_report_id_seq    SEQUENCE     �   CREATE SEQUENCE public.reports_report_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public.reports_report_id_seq;
       public          postgres    false    222            u           0    0    reports_report_id_seq    SEQUENCE OWNED BY     O   ALTER SEQUENCE public.reports_report_id_seq OWNED BY public.reports.report_id;
          public          postgres    false    221            �            1259    221580    role_permission    TABLE     �  CREATE TABLE public.role_permission (
    role_permission_id integer NOT NULL,
    module_id integer,
    dashboard_id integer,
    report_id integer,
    role_id integer,
    add_access boolean,
    delete_access boolean,
    read_access boolean,
    modify_access boolean,
    export_access boolean,
    print_access boolean,
    created_at date,
    created_by integer,
    modified_at date,
    modified_by integer
);
 #   DROP TABLE public.role_permission;
       public         heap    postgres    false            �            1259    221578 &   role_permission_role_permission_id_seq    SEQUENCE     �   CREATE SEQUENCE public.role_permission_role_permission_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 =   DROP SEQUENCE public.role_permission_role_permission_id_seq;
       public          postgres    false    214            v           0    0 &   role_permission_role_permission_id_seq    SEQUENCE OWNED BY     q   ALTER SEQUENCE public.role_permission_role_permission_id_seq OWNED BY public.role_permission.role_permission_id;
          public          postgres    false    213            �            1259    221569    roles    TABLE       CREATE TABLE public.roles (
    role_id integer NOT NULL,
    role_code character varying,
    role_name character varying,
    status integer,
    action character varying,
    created_at date,
    created_by integer,
    modified_at date,
    modified_by integer
);
    DROP TABLE public.roles;
       public         heap    postgres    false            �            1259    221567    roles_role_id_seq    SEQUENCE     �   CREATE SEQUENCE public.roles_role_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.roles_role_id_seq;
       public          postgres    false    212            w           0    0    roles_role_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.roles_role_id_seq OWNED BY public.roles.role_id;
          public          postgres    false    211                       1259    221932    seller_branch    TABLE     �  CREATE TABLE public.seller_branch (
    "Seller_branch_id" integer,
    seller_id bigint,
    seller_branch_name character varying,
    branch_country integer,
    branch_state integer,
    branch_city character varying,
    branch_address_1 character varying,
    branch_address_2 character varying,
    branch_phone numeric,
    phone_country_code character varying,
    branch_drop_address character varying,
    branch_googlemap_link character varying
);
 !   DROP TABLE public.seller_branch;
       public         heap    postgres    false                       1259    221940    seller_warehouse    TABLE       CREATE TABLE public.seller_warehouse (
    warehouse_id integer NOT NULL,
    "Seller_branch_id" integer,
    seller_id bigint,
    warehouse_name character varying,
    warehouse_phone numeric,
    warehouse_country_code character varying,
    warehouse_address character varying
);
 $   DROP TABLE public.seller_warehouse;
       public         heap    postgres    false                       1259    221938 !   seller_warehouse_warehouse_id_seq    SEQUENCE     �   CREATE SEQUENCE public.seller_warehouse_warehouse_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 8   DROP SEQUENCE public.seller_warehouse_warehouse_id_seq;
       public          postgres    false    281            x           0    0 !   seller_warehouse_warehouse_id_seq    SEQUENCE OWNED BY     g   ALTER SEQUENCE public.seller_warehouse_warehouse_id_seq OWNED BY public.seller_warehouse.warehouse_id;
          public          postgres    false    280            �            1259    221706    shipment_status    TABLE     �   CREATE TABLE public.shipment_status (
    ship_status_id integer NOT NULL,
    ship_status_name character varying,
    remarks character varying,
    created_at date,
    created_by integer,
    modified_at date,
    modified_by integer
);
 #   DROP TABLE public.shipment_status;
       public         heap    postgres    false            �            1259    221704 "   shipment_status_ship_status_id_seq    SEQUENCE     �   CREATE SEQUENCE public.shipment_status_ship_status_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 9   DROP SEQUENCE public.shipment_status_ship_status_id_seq;
       public          postgres    false    238            y           0    0 "   shipment_status_ship_status_id_seq    SEQUENCE OWNED BY     i   ALTER SEQUENCE public.shipment_status_ship_status_id_seq OWNED BY public.shipment_status.ship_status_id;
          public          postgres    false    237            ]           1259    222314    sms_template    TABLE     (  CREATE TABLE public.sms_template (
    id integer NOT NULL,
    module_id integer,
    title_name character varying,
    sms_content character varying,
    status integer,
    action character varying,
    created_at date,
    created_by integer,
    modified_at date,
    modified_by integer
);
     DROP TABLE public.sms_template;
       public         heap    postgres    false            \           1259    222312    sms_template_id_seq    SEQUENCE     �   CREATE SEQUENCE public.sms_template_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.sms_template_id_seq;
       public          postgres    false    349            z           0    0    sms_template_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE public.sms_template_id_seq OWNED BY public.sms_template.id;
          public          postgres    false    348                       1259    221827    state_details    TABLE     �   CREATE TABLE public.state_details (
    id integer NOT NULL,
    country_id integer,
    state_name character varying,
    status integer,
    created_at date,
    created_by integer,
    modified_at date,
    modified_by integer
);
 !   DROP TABLE public.state_details;
       public         heap    postgres    false                       1259    221825    state_details_id_seq    SEQUENCE     �   CREATE SEQUENCE public.state_details_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.state_details_id_seq;
       public          postgres    false    260            {           0    0    state_details_id_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.state_details_id_seq OWNED BY public.state_details.id;
          public          postgres    false    259            Y           1259    222292    static_page    TABLE     T  CREATE TABLE public.static_page (
    id integer NOT NULL,
    static_page_name character varying,
    page_title character varying,
    meta_tag character varying,
    content character varying,
    status integer,
    action character varying,
    created_at date,
    created_by integer,
    modified_at date,
    modified_by integer
);
    DROP TABLE public.static_page;
       public         heap    postgres    false            X           1259    222290    static_page_id_seq    SEQUENCE     �   CREATE SEQUENCE public.static_page_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.static_page_id_seq;
       public          postgres    false    345            |           0    0    static_page_id_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE public.static_page_id_seq OWNED BY public.static_page.id;
          public          postgres    false    344            �            1259    221684    status_details    TABLE     �   CREATE TABLE public.status_details (
    status_id integer NOT NULL,
    status_name character varying,
    created_at date,
    created_by integer,
    modified_at date,
    modified_by integer
);
 "   DROP TABLE public.status_details;
       public         heap    postgres    false            �            1259    221682    status_details_status_id_seq    SEQUENCE     �   CREATE SEQUENCE public.status_details_status_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 3   DROP SEQUENCE public.status_details_status_id_seq;
       public          postgres    false    234            }           0    0    status_details_status_id_seq    SEQUENCE OWNED BY     ]   ALTER SEQUENCE public.status_details_status_id_seq OWNED BY public.status_details.status_id;
          public          postgres    false    233            %           1259    222006    sub_group_management    TABLE     /  CREATE TABLE public.sub_group_management (
    sub_group_id integer NOT NULL,
    erp_id integer,
    last_integrated_date date,
    group_id integer,
    sub_group_name character varying,
    status integer,
    created_at date,
    created_by integer,
    modified_at date,
    modified_by integer
);
 (   DROP TABLE public.sub_group_management;
       public         heap    postgres    false            $           1259    222004 %   sub_group_management_sub_group_id_seq    SEQUENCE     �   CREATE SEQUENCE public.sub_group_management_sub_group_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 <   DROP SEQUENCE public.sub_group_management_sub_group_id_seq;
       public          postgres    false    293            ~           0    0 %   sub_group_management_sub_group_id_seq    SEQUENCE OWNED BY     o   ALTER SEQUENCE public.sub_group_management_sub_group_id_seq OWNED BY public.sub_group_management.sub_group_id;
          public          postgres    false    292            '           1259    222017    sub_node_management    TABLE     U  CREATE TABLE public.sub_node_management (
    sub_node_id integer NOT NULL,
    sub_group_id integer,
    sub_node_code character varying,
    sub_node_name character varying,
    erp_id integer,
    last_integrated_date date,
    status integer,
    created_at date,
    created_by integer,
    modified_at date,
    modified_by integer
);
 '   DROP TABLE public.sub_node_management;
       public         heap    postgres    false            &           1259    222015 #   sub_node_management_sub_node_id_seq    SEQUENCE     �   CREATE SEQUENCE public.sub_node_management_sub_node_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 :   DROP SEQUENCE public.sub_node_management_sub_node_id_seq;
       public          postgres    false    295                       0    0 #   sub_node_management_sub_node_id_seq    SEQUENCE OWNED BY     k   ALTER SEQUENCE public.sub_node_management_sub_node_id_seq OWNED BY public.sub_node_management.sub_node_id;
          public          postgres    false    294            1           1259    222072    sub_order_details    TABLE     �  CREATE TABLE public.sub_order_details (
    id bigint NOT NULL,
    order_id bigint,
    quantity numeric,
    price numeric,
    vat_amount numeric,
    total_price numeric,
    product_type_id integer,
    trading_type_id integer,
    status integer,
    seller_id integer,
    delivery_time character varying,
    discount numeric,
    shipping_charges numeric,
    brand_id integer,
    ship_status_id integer,
    order_status_id integer,
    sku character varying,
    product_id integer,
    brand_info character varying,
    model_info character varying,
    car_info character varying,
    ship_by_date date,
    deliver_by_date date,
    shipped_by character varying,
    shipped_person character varying,
    shipped_person_contact_no character varying,
    tracking_id character varying,
    shipping_date date,
    pickup_date date,
    package_type character varying,
    package_dimension character varying,
    package_weight numeric,
    total_shipping_cost numeric,
    seller_note character varying,
    buyer_feedback character varying,
    total_vat_amt numeric,
    created_at date,
    created_by integer,
    modified_at date,
    modified_by integer
);
 %   DROP TABLE public.sub_order_details;
       public         heap    postgres    false            0           1259    222070    sub_order_details_id_seq    SEQUENCE     �   CREATE SEQUENCE public.sub_order_details_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 /   DROP SEQUENCE public.sub_order_details_id_seq;
       public          postgres    false    305            �           0    0    sub_order_details_id_seq    SEQUENCE OWNED BY     U   ALTER SEQUENCE public.sub_order_details_id_seq OWNED BY public.sub_order_details.id;
          public          postgres    false    304            5           1259    222094    sub_order_quote_details    TABLE     �  CREATE TABLE public.sub_order_quote_details (
    id integer NOT NULL,
    order_quote_id integer,
    seller_id integer,
    product_id integer,
    brand_info character varying,
    model_info character varying,
    car_info character varying,
    quantity numeric,
    product_price numeric,
    product_vat_amount numeric,
    total_vat_amount numeric,
    total_price numeric,
    image character varying,
    product_type_id integer,
    trading_type_id integer,
    delivery_time character varying,
    discount numeric,
    shipping_charges numeric,
    ship_status_id integer,
    order_status_id integer,
    sku character varying,
    ship_by_date date,
    deliver_by_date date,
    shipped_by character varying,
    shipped_person character varying,
    shipped_person_contact_no character varying,
    tracking_id character varying,
    shipping_date date,
    pickup_date date,
    package_type character varying,
    package_dimension character varying,
    package_weight numeric,
    total_shipping_cost numeric,
    seller_note character varying,
    action character varying,
    status integer,
    created_at date,
    created_by integer,
    modified_at date,
    modified_by integer
);
 +   DROP TABLE public.sub_order_quote_details;
       public         heap    postgres    false            4           1259    222092    sub_order_quote_details_id_seq    SEQUENCE     �   CREATE SEQUENCE public.sub_order_quote_details_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 5   DROP SEQUENCE public.sub_order_quote_details_id_seq;
       public          postgres    false    309            �           0    0    sub_order_quote_details_id_seq    SEQUENCE OWNED BY     a   ALTER SEQUENCE public.sub_order_quote_details_id_seq OWNED BY public.sub_order_quote_details.id;
          public          postgres    false    308            �            1259    221632    subscription_detail    TABLE     i  CREATE TABLE public.subscription_detail (
    subscription_detail_id integer NOT NULL,
    subscription_plan_id integer,
    "Login_user_type_id" integer,
    user_id integer,
    valid_till date,
    effective date,
    status integer,
    action character varying,
    created_at date,
    created_by integer,
    modified_at date,
    modified_by integer
);
 '   DROP TABLE public.subscription_detail;
       public         heap    postgres    false            �            1259    221630 .   subscription_detail_subscription_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.subscription_detail_subscription_detail_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 E   DROP SEQUENCE public.subscription_detail_subscription_detail_id_seq;
       public          postgres    false    224            �           0    0 .   subscription_detail_subscription_detail_id_seq    SEQUENCE OWNED BY     �   ALTER SEQUENCE public.subscription_detail_subscription_detail_id_seq OWNED BY public.subscription_detail.subscription_detail_id;
          public          postgres    false    223            �            1259    221643    subscription_plan_details    TABLE     �  CREATE TABLE public.subscription_plan_details (
    subscription_plan_id integer NOT NULL,
    "Login_user_type_id" integer,
    subscription_name character varying,
    number_of_users integer,
    modules_list character varying,
    days character varying,
    price numeric,
    valid_till date,
    effective date,
    status integer,
    action character varying,
    created_at date,
    created_by integer,
    modified_at date,
    modified_by integer
);
 -   DROP TABLE public.subscription_plan_details;
       public         heap    postgres    false            �            1259    221641 2   subscription_plan_details_subscription_plan_id_seq    SEQUENCE     �   CREATE SEQUENCE public.subscription_plan_details_subscription_plan_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 I   DROP SEQUENCE public.subscription_plan_details_subscription_plan_id_seq;
       public          postgres    false    226            �           0    0 2   subscription_plan_details_subscription_plan_id_seq    SEQUENCE OWNED BY     �   ALTER SEQUENCE public.subscription_plan_details_subscription_plan_id_seq OWNED BY public.subscription_plan_details.subscription_plan_id;
          public          postgres    false    225            �            1259    221519    super_admin    TABLE     �   CREATE TABLE public.super_admin (
    id integer NOT NULL,
    user_id character varying,
    password character varying,
    created_at date,
    created_by integer,
    modified_at date,
    modified_by integer
);
    DROP TABLE public.super_admin;
       public         heap    postgres    false            �            1259    221517    super_admin_id_seq    SEQUENCE     �   CREATE SEQUENCE public.super_admin_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.super_admin_id_seq;
       public          postgres    false    203            �           0    0    super_admin_id_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE public.super_admin_id_seq OWNED BY public.super_admin.id;
          public          postgres    false    202                       1259    221846    tax_code_details    TABLE     �   CREATE TABLE public.tax_code_details (
    tax_code_id integer NOT NULL,
    tax_code_name character varying,
    status integer,
    created_at date,
    created_by integer,
    modified_at date,
    modified_by integer
);
 $   DROP TABLE public.tax_code_details;
       public         heap    postgres    false                       1259    221844     tax_code_details_tax_code_id_seq    SEQUENCE     �   CREATE SEQUENCE public.tax_code_details_tax_code_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 7   DROP SEQUENCE public.tax_code_details_tax_code_id_seq;
       public          postgres    false    264            �           0    0     tax_code_details_tax_code_id_seq    SEQUENCE OWNED BY     e   ALTER SEQUENCE public.tax_code_details_tax_code_id_seq OWNED BY public.tax_code_details.tax_code_id;
          public          postgres    false    263                       1259    221890    trading_type_details    TABLE     �   CREATE TABLE public.trading_type_details (
    trading_type_id integer NOT NULL,
    trading_type_name character varying,
    status integer,
    created_at date,
    created_by integer,
    modified_at date,
    modified_by integer
);
 (   DROP TABLE public.trading_type_details;
       public         heap    postgres    false                       1259    221888 (   trading_type_details_trading_type_id_seq    SEQUENCE     �   CREATE SEQUENCE public.trading_type_details_trading_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ?   DROP SEQUENCE public.trading_type_details_trading_type_id_seq;
       public          postgres    false    272            �           0    0 (   trading_type_details_trading_type_id_seq    SEQUENCE OWNED BY     u   ALTER SEQUENCE public.trading_type_details_trading_type_id_seq OWNED BY public.trading_type_details.trading_type_id;
          public          postgres    false    271            �            1259    221552    user_managment    TABLE     �  CREATE TABLE public.user_managment (
    id bigint NOT NULL,
    "Login_user_type_id" integer,
    user_name character varying,
    user_email_id character varying,
    phone_no character varying,
    user_password character varying,
    two_way_auth boolean,
    status integer,
    last_login date,
    created_at date,
    created_by integer,
    modified_at date,
    modified_by integer
);
 "   DROP TABLE public.user_managment;
       public         heap    postgres    false            �            1259    221550    user_managment_id_seq    SEQUENCE     ~   CREATE SEQUENCE public.user_managment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public.user_managment_id_seq;
       public          postgres    false    209            �           0    0    user_managment_id_seq    SEQUENCE OWNED BY     O   ALTER SEQUENCE public.user_managment_id_seq OWNED BY public.user_managment.id;
          public          postgres    false    208            =           1259    222138    vin_history    TABLE     �  CREATE TABLE public.vin_history (
    id integer NOT NULL,
    vin_number character varying,
    vehicle_identified character varying,
    brand_id integer,
    model_id integer,
    user_type_info character varying,
    user_name character varying,
    user_id integer,
    region character varying,
    searched_date date,
    status integer,
    action character varying,
    created_at date,
    created_by integer,
    modified_at date,
    modified_by integer
);
    DROP TABLE public.vin_history;
       public         heap    postgres    false            <           1259    222136    vin_history_id_seq    SEQUENCE     �   CREATE SEQUENCE public.vin_history_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.vin_history_id_seq;
       public          postgres    false    317            �           0    0    vin_history_id_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE public.vin_history_id_seq OWNED BY public.vin_history.id;
          public          postgres    false    316            �           2604    222229    Rating_review id    DEFAULT     x   ALTER TABLE ONLY public."Rating_review" ALTER COLUMN id SET DEFAULT nextval('public."Rating_review_id_seq"'::regclass);
 A   ALTER TABLE public."Rating_review" ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    333    332    333            �           2604    221841    VAT_details id    DEFAULT     t   ALTER TABLE ONLY public."VAT_details" ALTER COLUMN id SET DEFAULT nextval('public."VAT_details_id_seq"'::regclass);
 ?   ALTER TABLE public."VAT_details" ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    262    261    262            �           2604    221544 $   application_user application_user_id    DEFAULT     �   ALTER TABLE ONLY public.application_user ALTER COLUMN application_user_id SET DEFAULT nextval('public.application_user_application_user_id_seq'::regclass);
 S   ALTER TABLE public.application_user ALTER COLUMN application_user_id DROP DEFAULT;
       public          postgres    false    206    207    207            �           2604    222284    audit_trail id    DEFAULT     p   ALTER TABLE ONLY public.audit_trail ALTER COLUMN id SET DEFAULT nextval('public.audit_trail_id_seq'::regclass);
 =   ALTER TABLE public.audit_trail ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    342    343    343            �           2604    221954    brands brand_id    DEFAULT     r   ALTER TABLE ONLY public.brands ALTER COLUMN brand_id SET DEFAULT nextval('public.brands_brand_id_seq'::regclass);
 >   ALTER TABLE public.brands ALTER COLUMN brand_id DROP DEFAULT;
       public          postgres    false    283    282    283            �           2604    221676    bulk_upload_details id    DEFAULT     �   ALTER TABLE ONLY public.bulk_upload_details ALTER COLUMN id SET DEFAULT nextval('public.bulk_upload_details_id_seq'::regclass);
 E   ALTER TABLE public.bulk_upload_details ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    232    231    232            �           2604    222185 (   buyer_customer_details buyer_customer_id    DEFAULT     �   ALTER TABLE ONLY public.buyer_customer_details ALTER COLUMN buyer_customer_id SET DEFAULT nextval('public.buyer_customer_details_buyer_customer_id_seq'::regclass);
 W   ALTER TABLE public.buyer_customer_details ALTER COLUMN buyer_customer_id DROP DEFAULT;
       public          postgres    false    325    324    325            �           2604    222163     buyer_saved_quote buyer_quote_id    DEFAULT     �   ALTER TABLE ONLY public.buyer_saved_quote ALTER COLUMN buyer_quote_id SET DEFAULT nextval('public.buyer_saved_quote_buyer_quote_id_seq'::regclass);
 O   ALTER TABLE public.buyer_saved_quote ALTER COLUMN buyer_quote_id DROP DEFAULT;
       public          postgres    false    320    321    321            �           2604    222174 "   buyer_saved_quote_service_items id    DEFAULT     �   ALTER TABLE ONLY public.buyer_saved_quote_service_items ALTER COLUMN id SET DEFAULT nextval('public.buyer_saved_quote_service_items_id_seq'::regclass);
 Q   ALTER TABLE public.buyer_saved_quote_service_items ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    322    323    323            �           2604    222108    car_management id    DEFAULT     v   ALTER TABLE ONLY public.car_management ALTER COLUMN id SET DEFAULT nextval('public.car_management_id_seq'::regclass);
 @   ALTER TABLE public.car_management ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    310    311    311            �           2604    221987    cars car_id    DEFAULT     j   ALTER TABLE ONLY public.cars ALTER COLUMN car_id SET DEFAULT nextval('public.cars_car_id_seq'::regclass);
 :   ALTER TABLE public.cars ALTER COLUMN car_id DROP DEFAULT;
       public          postgres    false    289    288    289            �           2604    222196    cart_details cart_item_id    DEFAULT     �   ALTER TABLE ONLY public.cart_details ALTER COLUMN cart_item_id SET DEFAULT nextval('public.cart_details_cart_item_id_seq'::regclass);
 H   ALTER TABLE public.cart_details ALTER COLUMN cart_item_id DROP DEFAULT;
       public          postgres    false    326    327    327            �           2604    222152    category_request_management id    DEFAULT     �   ALTER TABLE ONLY public.category_request_management ALTER COLUMN id SET DEFAULT nextval('public.category_request_management_id_seq'::regclass);
 M   ALTER TABLE public.category_request_management ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    318    319    319            �           2604    221775 )   category_request_status cat_req_status_id    DEFAULT     �   ALTER TABLE ONLY public.category_request_status ALTER COLUMN cat_req_status_id SET DEFAULT nextval('public.category_request_status_cat_req_status_id_seq'::regclass);
 X   ALTER TABLE public.category_request_status ALTER COLUMN cat_req_status_id DROP DEFAULT;
       public          postgres    false    249    250    250            �           2604    222053    commission_management id    DEFAULT     �   ALTER TABLE ONLY public.commission_management ALTER COLUMN id SET DEFAULT nextval('public.commission_management_id_seq'::regclass);
 G   ALTER TABLE public.commission_management ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    301    300    301            �           2604    221665    configuration_details id    DEFAULT     �   ALTER TABLE ONLY public.configuration_details ALTER COLUMN id SET DEFAULT nextval('public.configuration_details_id_seq'::regclass);
 G   ALTER TABLE public.configuration_details ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    229    230    230            �           2604    221819    country_details country_id    DEFAULT     �   ALTER TABLE ONLY public.country_details ALTER COLUMN country_id SET DEFAULT nextval('public.country_details_country_id_seq'::regclass);
 I   ALTER TABLE public.country_details ALTER COLUMN country_id DROP DEFAULT;
       public          postgres    false    258    257    258            �           2604    221808    currency_details currency_id    DEFAULT     �   ALTER TABLE ONLY public.currency_details ALTER COLUMN currency_id SET DEFAULT nextval('public.currency_details_currency_id_seq'::regclass);
 K   ALTER TABLE public.currency_details ALTER COLUMN currency_id DROP DEFAULT;
       public          postgres    false    256    255    256            �           2604    221613    dashboard dashboard_id    DEFAULT     �   ALTER TABLE ONLY public.dashboard ALTER COLUMN dashboard_id SET DEFAULT nextval('public.dashboard_dashboard_id_seq'::regclass);
 E   ALTER TABLE public.dashboard ALTER COLUMN dashboard_id DROP DEFAULT;
       public          postgres    false    219    220    220            �           2604    222306    email_template id    DEFAULT     v   ALTER TABLE ONLY public.email_template ALTER COLUMN id SET DEFAULT nextval('public.email_template_id_seq'::regclass);
 @   ALTER TABLE public.email_template ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    346    347    347            �           2604    221998    group_management group_id    DEFAULT     �   ALTER TABLE ONLY public.group_management ALTER COLUMN group_id SET DEFAULT nextval('public.group_management_group_id_seq'::regclass);
 H   ALTER TABLE public.group_management ALTER COLUMN group_id DROP DEFAULT;
       public          postgres    false    290    291    291            �           2604    222273    help_management id    DEFAULT     x   ALTER TABLE ONLY public.help_management ALTER COLUMN id SET DEFAULT nextval('public.help_management_id_seq'::regclass);
 A   ALTER TABLE public.help_management ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    340    341    341            �           2604    221533    intermediator id    DEFAULT     t   ALTER TABLE ONLY public.intermediator ALTER COLUMN id SET DEFAULT nextval('public.intermediator_id_seq'::regclass);
 ?   ALTER TABLE public.intermediator ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    205    204    205            �           2604    222262    inventory_manangement id    DEFAULT     �   ALTER TABLE ONLY public.inventory_manangement ALTER COLUMN id SET DEFAULT nextval('public.inventory_manangement_id_seq'::regclass);
 G   ALTER TABLE public.inventory_manangement ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    338    339    339            �           2604    221871    jurisdiction jurisdiction_id    DEFAULT     �   ALTER TABLE ONLY public.jurisdiction ALTER COLUMN jurisdiction_id SET DEFAULT nextval('public.jurisdiction_jurisdiction_id_seq'::regclass);
 K   ALTER TABLE public.jurisdiction ALTER COLUMN jurisdiction_id DROP DEFAULT;
       public          postgres    false    268    267    268            �           2604    221764 &   make_offer_status make_offer_status_id    DEFAULT     �   ALTER TABLE ONLY public.make_offer_status ALTER COLUMN make_offer_status_id SET DEFAULT nextval('public.make_offer_status_make_offer_status_id_seq'::regclass);
 U   ALTER TABLE public.make_offer_status ALTER COLUMN make_offer_status_id DROP DEFAULT;
       public          postgres    false    248    247    248            �           2604    221965    models model_id    DEFAULT     r   ALTER TABLE ONLY public.models ALTER COLUMN model_id SET DEFAULT nextval('public.models_model_id_seq'::regclass);
 >   ALTER TABLE public.models ALTER COLUMN model_id DROP DEFAULT;
       public          postgres    false    284    285    285            �           2604    221602    module module_id    DEFAULT     t   ALTER TABLE ONLY public.module ALTER COLUMN module_id SET DEFAULT nextval('public.module_module_id_seq'::regclass);
 ?   ALTER TABLE public.module ALTER COLUMN module_id DROP DEFAULT;
       public          postgres    false    217    218    218            �           2604    221591    module_type module_type_id    DEFAULT     �   ALTER TABLE ONLY public.module_type ALTER COLUMN module_type_id SET DEFAULT nextval('public.module_type_module_type_id_seq'::regclass);
 I   ALTER TABLE public.module_type ALTER COLUMN module_type_id DROP DEFAULT;
       public          postgres    false    215    216    216            �           2604    221915    new_buyer_details id    DEFAULT     |   ALTER TABLE ONLY public.new_buyer_details ALTER COLUMN id SET DEFAULT nextval('public.new_buyer_details_id_seq'::regclass);
 C   ALTER TABLE public.new_buyer_details ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    276    275    276            �           2604    221926    new_seller_details id    DEFAULT     ~   ALTER TABLE ONLY public.new_seller_details ALTER COLUMN id SET DEFAULT nextval('public.new_seller_details_id_seq'::regclass);
 D   ALTER TABLE public.new_seller_details ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    278    277    278            �           2604    222207    newsletter_management id    DEFAULT     �   ALTER TABLE ONLY public.newsletter_management ALTER COLUMN id SET DEFAULT nextval('public.newsletter_management_id_seq'::regclass);
 G   ALTER TABLE public.newsletter_management ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    328    329    329            �           2604    222218    notification id    DEFAULT     r   ALTER TABLE ONLY public.notification ALTER COLUMN id SET DEFAULT nextval('public.notification_id_seq'::regclass);
 >   ALTER TABLE public.notification ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    330    331    331            �           2604    222251    offer_management id    DEFAULT     z   ALTER TABLE ONLY public.offer_management ALTER COLUMN id SET DEFAULT nextval('public.offer_management_id_seq'::regclass);
 B   ALTER TABLE public.offer_management ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    336    337    337            �           2604    222064    order_management order_id    DEFAULT     �   ALTER TABLE ONLY public.order_management ALTER COLUMN order_id SET DEFAULT nextval('public.order_management_order_id_seq'::regclass);
 H   ALTER TABLE public.order_management ALTER COLUMN order_id DROP DEFAULT;
       public          postgres    false    302    303    303            �           2604    222086 !   order_quote_Manage order_quote_id    DEFAULT     �   ALTER TABLE ONLY public."order_quote_Manage" ALTER COLUMN order_quote_id SET DEFAULT nextval('public."order_quote_Manage_order_quote_id_seq"'::regclass);
 R   ALTER TABLE public."order_quote_Manage" ALTER COLUMN order_quote_id DROP DEFAULT;
       public          postgres    false    306    307    307            �           2604    221720    order_status order_status_id    DEFAULT     �   ALTER TABLE ONLY public.order_status ALTER COLUMN order_status_id SET DEFAULT nextval('public.order_status_order_status_id_seq'::regclass);
 K   ALTER TABLE public.order_status ALTER COLUMN order_status_id DROP DEFAULT;
       public          postgres    false    240    239    240            �           2604    221698     order_type_details order_type_id    DEFAULT     �   ALTER TABLE ONLY public.order_type_details ALTER COLUMN order_type_id SET DEFAULT nextval('public.order_type_details_order_type_id_seq'::regclass);
 O   ALTER TABLE public.order_type_details ALTER COLUMN order_type_id DROP DEFAULT;
       public          postgres    false    235    236    236            �           2604    221976    parameters id    DEFAULT     n   ALTER TABLE ONLY public.parameters ALTER COLUMN id SET DEFAULT nextval('public.parameters_id_seq'::regclass);
 <   ALTER TABLE public.parameters ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    286    287    287            �           2604    222240    part_request_unavail id    DEFAULT     �   ALTER TABLE ONLY public.part_request_unavail ALTER COLUMN id SET DEFAULT nextval('public.part_request_unavail_id_seq'::regclass);
 F   ALTER TABLE public.part_request_unavail ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    334    335    335            �           2604    222031    parts_management id    DEFAULT     z   ALTER TABLE ONLY public.parts_management ALTER COLUMN id SET DEFAULT nextval('public.parts_management_id_seq'::regclass);
 B   ALTER TABLE public.parts_management ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    296    297    297            �           2604    221742 $   parts_req_status parts_req_status_id    DEFAULT     �   ALTER TABLE ONLY public.parts_req_status ALTER COLUMN parts_req_status_id SET DEFAULT nextval('public.parts_req_status_parts_req_status_id_seq'::regclass);
 S   ALTER TABLE public.parts_req_status ALTER COLUMN parts_req_status_id DROP DEFAULT;
       public          postgres    false    244    243    244            �           2604    222119    pay_out_schedule_management id    DEFAULT     �   ALTER TABLE ONLY public.pay_out_schedule_management ALTER COLUMN id SET DEFAULT nextval('public.pay_out_schedule_management_id_seq'::regclass);
 M   ALTER TABLE public.pay_out_schedule_management ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    312    313    313            �           2604    222130    payment_management id    DEFAULT     ~   ALTER TABLE ONLY public.payment_management ALTER COLUMN id SET DEFAULT nextval('public.payment_management_id_seq'::regclass);
 D   ALTER TABLE public.payment_management ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    314    315    315            �           2604    221786     payment_status payment_status_id    DEFAULT     �   ALTER TABLE ONLY public.payment_status ALTER COLUMN payment_status_id SET DEFAULT nextval('public.payment_status_payment_status_id_seq'::regclass);
 O   ALTER TABLE public.payment_status ALTER COLUMN payment_status_id DROP DEFAULT;
       public          postgres    false    251    252    252            �           2604    221904 $   payment_type_details payment_type_id    DEFAULT     �   ALTER TABLE ONLY public.payment_type_details ALTER COLUMN payment_type_id SET DEFAULT nextval('public.payment_type_details_payment_type_id_seq'::regclass);
 S   ALTER TABLE public.payment_type_details ALTER COLUMN payment_type_id DROP DEFAULT;
       public          postgres    false    273    274    274            �           2604    221657 "   payout_shed_details payout_shed_id    DEFAULT     �   ALTER TABLE ONLY public.payout_shed_details ALTER COLUMN payout_shed_id SET DEFAULT nextval('public.payout_shed_details_payout_shed_id_seq'::regclass);
 Q   ALTER TABLE public.payout_shed_details ALTER COLUMN payout_shed_id DROP DEFAULT;
       public          postgres    false    227    228    228            �           2604    221860    place_of_supply pos_id    DEFAULT     �   ALTER TABLE ONLY public.place_of_supply ALTER COLUMN pos_id SET DEFAULT nextval('public.place_of_supply_pos_id_seq'::regclass);
 E   ALTER TABLE public.place_of_supply ALTER COLUMN pos_id DROP DEFAULT;
       public          postgres    false    266    265    266            �           2604    222042    product_details product_id    DEFAULT     �   ALTER TABLE ONLY public.product_details ALTER COLUMN product_id SET DEFAULT nextval('public.product_details_product_id_seq'::regclass);
 I   ALTER TABLE public.product_details ALTER COLUMN product_id DROP DEFAULT;
       public          postgres    false    299    298    299            �           2604    221882 $   product_type_details product_type_id    DEFAULT     �   ALTER TABLE ONLY public.product_type_details ALTER COLUMN product_type_id SET DEFAULT nextval('public.product_type_details_product_type_id_seq'::regclass);
 S   ALTER TABLE public.product_type_details ALTER COLUMN product_type_id DROP DEFAULT;
       public          postgres    false    270    269    270            �           2604    221753    quote_status quote_status_id    DEFAULT     �   ALTER TABLE ONLY public.quote_status ALTER COLUMN quote_status_id SET DEFAULT nextval('public.quote_status_quote_status_id_seq'::regclass);
 K   ALTER TABLE public.quote_status ALTER COLUMN quote_status_id DROP DEFAULT;
       public          postgres    false    245    246    246            �           2604    221797    reason_details reason_id    DEFAULT     �   ALTER TABLE ONLY public.reason_details ALTER COLUMN reason_id SET DEFAULT nextval('public.reason_details_reason_id_seq'::regclass);
 G   ALTER TABLE public.reason_details ALTER COLUMN reason_id DROP DEFAULT;
       public          postgres    false    254    253    254            �           2604    221731 !   registration_status reg_status_id    DEFAULT     �   ALTER TABLE ONLY public.registration_status ALTER COLUMN reg_status_id SET DEFAULT nextval('public.registration_status_reg_status_id_seq'::regclass);
 P   ALTER TABLE public.registration_status ALTER COLUMN reg_status_id DROP DEFAULT;
       public          postgres    false    241    242    242            �           2604    221624    reports report_id    DEFAULT     v   ALTER TABLE ONLY public.reports ALTER COLUMN report_id SET DEFAULT nextval('public.reports_report_id_seq'::regclass);
 @   ALTER TABLE public.reports ALTER COLUMN report_id DROP DEFAULT;
       public          postgres    false    221    222    222            �           2604    221583 "   role_permission role_permission_id    DEFAULT     �   ALTER TABLE ONLY public.role_permission ALTER COLUMN role_permission_id SET DEFAULT nextval('public.role_permission_role_permission_id_seq'::regclass);
 Q   ALTER TABLE public.role_permission ALTER COLUMN role_permission_id DROP DEFAULT;
       public          postgres    false    214    213    214            �           2604    221572    roles role_id    DEFAULT     n   ALTER TABLE ONLY public.roles ALTER COLUMN role_id SET DEFAULT nextval('public.roles_role_id_seq'::regclass);
 <   ALTER TABLE public.roles ALTER COLUMN role_id DROP DEFAULT;
       public          postgres    false    212    211    212            �           2604    221943    seller_warehouse warehouse_id    DEFAULT     �   ALTER TABLE ONLY public.seller_warehouse ALTER COLUMN warehouse_id SET DEFAULT nextval('public.seller_warehouse_warehouse_id_seq'::regclass);
 L   ALTER TABLE public.seller_warehouse ALTER COLUMN warehouse_id DROP DEFAULT;
       public          postgres    false    280    281    281            �           2604    221709    shipment_status ship_status_id    DEFAULT     �   ALTER TABLE ONLY public.shipment_status ALTER COLUMN ship_status_id SET DEFAULT nextval('public.shipment_status_ship_status_id_seq'::regclass);
 M   ALTER TABLE public.shipment_status ALTER COLUMN ship_status_id DROP DEFAULT;
       public          postgres    false    237    238    238            �           2604    222317    sms_template id    DEFAULT     r   ALTER TABLE ONLY public.sms_template ALTER COLUMN id SET DEFAULT nextval('public.sms_template_id_seq'::regclass);
 >   ALTER TABLE public.sms_template ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    349    348    349            �           2604    221830    state_details id    DEFAULT     t   ALTER TABLE ONLY public.state_details ALTER COLUMN id SET DEFAULT nextval('public.state_details_id_seq'::regclass);
 ?   ALTER TABLE public.state_details ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    259    260    260            �           2604    222295    static_page id    DEFAULT     p   ALTER TABLE ONLY public.static_page ALTER COLUMN id SET DEFAULT nextval('public.static_page_id_seq'::regclass);
 =   ALTER TABLE public.static_page ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    344    345    345            �           2604    221687    status_details status_id    DEFAULT     �   ALTER TABLE ONLY public.status_details ALTER COLUMN status_id SET DEFAULT nextval('public.status_details_status_id_seq'::regclass);
 G   ALTER TABLE public.status_details ALTER COLUMN status_id DROP DEFAULT;
       public          postgres    false    234    233    234            �           2604    222009 !   sub_group_management sub_group_id    DEFAULT     �   ALTER TABLE ONLY public.sub_group_management ALTER COLUMN sub_group_id SET DEFAULT nextval('public.sub_group_management_sub_group_id_seq'::regclass);
 P   ALTER TABLE public.sub_group_management ALTER COLUMN sub_group_id DROP DEFAULT;
       public          postgres    false    293    292    293            �           2604    222020    sub_node_management sub_node_id    DEFAULT     �   ALTER TABLE ONLY public.sub_node_management ALTER COLUMN sub_node_id SET DEFAULT nextval('public.sub_node_management_sub_node_id_seq'::regclass);
 N   ALTER TABLE public.sub_node_management ALTER COLUMN sub_node_id DROP DEFAULT;
       public          postgres    false    294    295    295            �           2604    222075    sub_order_details id    DEFAULT     |   ALTER TABLE ONLY public.sub_order_details ALTER COLUMN id SET DEFAULT nextval('public.sub_order_details_id_seq'::regclass);
 C   ALTER TABLE public.sub_order_details ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    304    305    305            �           2604    222097    sub_order_quote_details id    DEFAULT     �   ALTER TABLE ONLY public.sub_order_quote_details ALTER COLUMN id SET DEFAULT nextval('public.sub_order_quote_details_id_seq'::regclass);
 I   ALTER TABLE public.sub_order_quote_details ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    308    309    309            �           2604    221635 *   subscription_detail subscription_detail_id    DEFAULT     �   ALTER TABLE ONLY public.subscription_detail ALTER COLUMN subscription_detail_id SET DEFAULT nextval('public.subscription_detail_subscription_detail_id_seq'::regclass);
 Y   ALTER TABLE public.subscription_detail ALTER COLUMN subscription_detail_id DROP DEFAULT;
       public          postgres    false    224    223    224            �           2604    221646 .   subscription_plan_details subscription_plan_id    DEFAULT     �   ALTER TABLE ONLY public.subscription_plan_details ALTER COLUMN subscription_plan_id SET DEFAULT nextval('public.subscription_plan_details_subscription_plan_id_seq'::regclass);
 ]   ALTER TABLE public.subscription_plan_details ALTER COLUMN subscription_plan_id DROP DEFAULT;
       public          postgres    false    225    226    226                       2604    221522    super_admin id    DEFAULT     p   ALTER TABLE ONLY public.super_admin ALTER COLUMN id SET DEFAULT nextval('public.super_admin_id_seq'::regclass);
 =   ALTER TABLE public.super_admin ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    202    203    203            �           2604    221849    tax_code_details tax_code_id    DEFAULT     �   ALTER TABLE ONLY public.tax_code_details ALTER COLUMN tax_code_id SET DEFAULT nextval('public.tax_code_details_tax_code_id_seq'::regclass);
 K   ALTER TABLE public.tax_code_details ALTER COLUMN tax_code_id DROP DEFAULT;
       public          postgres    false    264    263    264            �           2604    221893 $   trading_type_details trading_type_id    DEFAULT     �   ALTER TABLE ONLY public.trading_type_details ALTER COLUMN trading_type_id SET DEFAULT nextval('public.trading_type_details_trading_type_id_seq'::regclass);
 S   ALTER TABLE public.trading_type_details ALTER COLUMN trading_type_id DROP DEFAULT;
       public          postgres    false    272    271    272            �           2604    221555    user_managment id    DEFAULT     v   ALTER TABLE ONLY public.user_managment ALTER COLUMN id SET DEFAULT nextval('public.user_managment_id_seq'::regclass);
 @   ALTER TABLE public.user_managment ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    208    209    209            �           2604    222141    vin_history id    DEFAULT     p   ALTER TABLE ONLY public.vin_history ALTER COLUMN id SET DEFAULT nextval('public.vin_history_id_seq'::regclass);
 =   ALTER TABLE public.vin_history ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    316    317    317            �          0    221561    Login_user_type 
   TABLE DATA           Y   COPY public."Login_user_type" ("Login_user_type_id", "Login_user_type_name") FROM stdin;
    public          postgres    false    210   �b      )          0    222226    Rating_review 
   TABLE DATA           �   COPY public."Rating_review" (id, seller_id, rate_seller, review_seller, status, action, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    333   c      �          0    221838    VAT_details 
   TABLE DATA           �   COPY public."VAT_details" (id, vat_percentage, effective_date, tax_code_id, pos_id, juristriction_id, status, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    262   3c      �          0    221541    application_user 
   TABLE DATA           �   COPY public.application_user (application_user_id, user_id, portal_user_id, user_name, user_email_id, phone_no, designation, email_verification_status, phoneno_verified, status, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    207   Pc      3          0    222281    audit_trail 
   TABLE DATA           �   COPY public.audit_trail (id, module_id, field_name, old_value, new_value, user_id, time_stamp, status, audit_action, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    343   mc      �          0    221951    brands 
   TABLE DATA           �   COPY public.brands (brand_id, brand_code, brand_name, erp_id, last_integrated_date, brand_api_id, brand_image, status, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    283   �c      �          0    221673    bulk_upload_details 
   TABLE DATA           �   COPY public.bulk_upload_details (id, file_name, "file_URL", product_type_id, status, remarks, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    232   �c      !          0    222182    buyer_customer_details 
   TABLE DATA           �   COPY public.buyer_customer_details (buyer_customer_id, buyer_id, customer_name, email, phone, address, car_details_brand, model_id, status, action, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    325   �c                0    222160    buyer_saved_quote 
   TABLE DATA           9  COPY public.buyer_saved_quote (buyer_quote_id, buyer_id, cart_item_id, quote_no, tax_code, buyer_customer_id, status_of_quote, quantity, price, margin_per, price_with_margin, customer_detail_name, address, car_details_brand, model_id, status, action, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    321   �c                0    222171    buyer_saved_quote_service_items 
   TABLE DATA           �   COPY public.buyer_saved_quote_service_items (id, buyer_quote_id, service_item, quantity, price, tax_code, status, action, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    323   �c                0    222105    car_management 
   TABLE DATA           �   COPY public.car_management (id, buyer_id, buyer_customer_id, car_info, brand_info, model_info, vin_no, action, status, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    311   d      �          0    221984    cars 
   TABLE DATA           �   COPY public.cars (car_id, car_name, brand_id, model_id, parameters_id, parameters_value, erp_id, last_integrated_date, status, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    289   8d      #          0    222193    cart_details 
   TABLE DATA           �   COPY public.cart_details (cart_item_id, buyer_id, product_id, price, quantity, tax_code, tax_amount, final_price, checkout_status, action, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    327   Ud                0    222149    category_request_management 
   TABLE DATA           �   COPY public.category_request_management (id, category_code, category_name, description, cat_req_status_id, reason_for_reject, status, action, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    319   rd      �          0    221772    category_request_status 
   TABLE DATA           �   COPY public.category_request_status (cat_req_status_id, cat_req_status_name, remarks, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    250   �d      	          0    222050    commission_management 
   TABLE DATA           �   COPY public.commission_management (id, product_type_id, trading_type_id, commission_precentage, effective_date, action, status, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    301   �d      �          0    221662    configuration_details 
   TABLE DATA             COPY public.configuration_details (id, config_name, "config_URL", "Config_username", config_password, config_port, config_parameter, "config_MobileNumber", config_mtype, config_message, config_secretkey, status, remarks, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    230   �d      �          0    221816    country_details 
   TABLE DATA           �   COPY public.country_details (country_id, country_name, country_code, status, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    258   �d      �          0    221805    currency_details 
   TABLE DATA           �   COPY public.currency_details (currency_id, currency_name, currency_shortcode, status, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    256   e      �          0    221610 	   dashboard 
   TABLE DATA           �   COPY public.dashboard (dashboard_id, dashboard_name, "Login_user_type_id", status, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    220    e      7          0    222303    email_template 
   TABLE DATA           �   COPY public.email_template (id, title, email_type, encoding_type, iso_code, description, subject, email_content, attachment, status, action, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    347   =e      �          0    221995    group_management 
   TABLE DATA           �   COPY public.group_management (group_id, car_id, erp_id, last_integrated_date, group_code, group_name, status, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    291   Ze      1          0    222270    help_management 
   TABLE DATA           �   COPY public.help_management (id, help_management_code, subject_name, description, module_id, keywords, status, action, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    341   we      �          0    221530    intermediator 
   TABLE DATA           �   COPY public.intermediator (id, user_id, intermediator_code, firstname, lastname, user_name, role_id, email_id, phone_number, mobile_number, status, image, country, language_spoken, address, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    205   �e      /          0    222259    inventory_manangement 
   TABLE DATA           0  COPY public.inventory_manangement (id, warehouse_id, seller_id, product_id, brand_info, model_info, car_info, price, price_after_sale, onhand, e_part_qnty, buffer_qnty, re_order_level, threshold_limit, sold, balance, bin_loc, status, action, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    339   �e      �          0    221868    jurisdiction 
   TABLE DATA           �   COPY public.jurisdiction (jurisdiction_id, name, code, erp_id, last_integrated_date, status, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    268   �e      �          0    221761    make_offer_status 
   TABLE DATA           �   COPY public.make_offer_status (make_offer_status_id, offer_status_name, remarks, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    248   �e      �          0    221962    models 
   TABLE DATA           �   COPY public.models (model_id, brand_id, erp_id, last_integrated_date, model_code, model_name, model_api_id, model_image, status, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    285   f      �          0    221599    module 
   TABLE DATA           �   COPY public.module (module_id, module_type_id, module_name, module_abbr, status, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    218   %f      �          0    221588    module_type 
   TABLE DATA           �   COPY public.module_type (module_type_id, module_type_name, status, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    216   Bf      �          0    221912    new_buyer_details 
   TABLE DATA           z  COPY public.new_buyer_details (id, user_id, buyer_code, email_verification_status, business_name, business_com_logo, email_id, subscription_id, subscription_effective_date, business_status, billing_country, billing_state, billing_city, b_address1, b_address2, b_displayname, b_phonenumber, b_country_code, jurisdiction_id, pos_id, shipping_country, shipping_state, shipping_city, s_address1, s_address2, s_displayname, s_phonenumber, s_country_code, phoneno_verified, drop_address, googlemap_link, payment_type_info, product_type_id, trading_type_id, iv_location, iv_trade_licenese, iv_res_national, iv_exp_date, iv_country_issue, iv_f_name, iv_m_name, iv_l_name, iv_dob, iv_r_bussiness_name, iv_licenese_no, iv_r_address, id_doc_front, id_doc_back, bus_doc_license, registration_status, cancel_reason, vat_registration_number, tax_registration_exp_date, vat_reg_certificate_certificate, bank_benefi_name, bank_name, bank_ac_no, bank_iban, bank_branch_name, swift_code, bank_currency, bank_browse_file, erp_id, last_integrated_date, company_buyer_representative, t_and_c_acknowledge, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    276   _f      �          0    221923    new_seller_details 
   TABLE DATA           �  COPY public.new_seller_details (id, user_id, seller_code, email_verification_status, email_id, b_multi_branch, business_name, business_com_logo, subscription_id, subscription_effective_date, payout_shed_id, business_status, billing_country, billing_state, billing_city, b_address1, b_address2, jurisdiction_id, pos_id, b_displayname, b_phonenumber, b_country_code, shipping_country, shipping_state, shipping_city, s_address1, s_address2, s_displayname, s_phonenumber, s_country_code, phoneno_verified, drop_address, googlemap_link, payment_type_info, product_type_id, trading_type_id, iv_location, iv_licenese, iv_national_id, iv_exp_date, iv_country_issue, iv_f_name, iv_m_name, iv_l_name, iv_dob, iv_r_bussiness_name, iv_licenese_no, iv_r_address, id_doc_front, id_doc_back, bus_doc_license, registration_status, fail_reason, tax_registration_number, tax_registration_exp_date, vat_reg_certificate, bank_benefi_name, bank_name, bank_ac_no, bank_iban, bank_branch_name, swift_code, bank_currency, bank_browse_file, company_seller_representative, t_and_c_acknowledge, erp_id, last_integrated_date, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    278   |f      %          0    222204    newsletter_management 
   TABLE DATA           �   COPY public.newsletter_management (id, "Title", user_id, registered, email_id, status, action, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    329   �f      '          0    222215    notification 
   TABLE DATA           z  COPY public.notification (id, title, advance_filter, module_id, execute_on, notification_type, email_template, email_recipient_type, email_recipient, sms_template, sms_recipient_type, sms_recipient, push_notification_title, push_notification, push_notification_seller, push_notification_seller_name, status, action, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    331   �f      -          0    222248    offer_management 
   TABLE DATA           t  COPY public.offer_management (id, buyer_id, seller_id, product_id, part_no, asking_amount, asking_quantity, seller_offer_amount, seller_offer_quantity, remarks, approved_by, offer_date, approved_date, seller_confirmed_date, rejected_date, rejected_reason, offer_validity, make_offer_status_id, status, action, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    337   �f                0    222061    order_management 
   TABLE DATA           �  COPY public.order_management (order_id, order_code, buyer_id, user_id, method_of_payment, order_erp_id, order_date, order_type, order_amount, order_status_id, total_quantity, buyer_shipping_address, googlemap_link, total_vat_amount, total_price, last_delivery_time, total_discount, total_shipping_charges, ship_status_id, buyer_feedback, action, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    303   �f                0    222083    order_quote_Manage 
   TABLE DATA           X  COPY public."order_quote_Manage" (order_quote_id, order_quote_code, buyer_id, user_id, method_of_payment, order_quote_erp_id, order_quote_date, total_quantity, order_type, order_amount, order_status_id, total_vat_amount, total_price, buyer_shipping_address, googlemap_link, action, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    307   g      �          0    221717    order_status 
   TABLE DATA           �   COPY public.order_status (order_status_id, order_status_name, remarks, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    240   *g      �          0    221695    order_type_details 
   TABLE DATA           ~   COPY public.order_type_details (order_type_id, order_type_name, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    236   Gg      �          0    221973 
   parameters 
   TABLE DATA           �   COPY public.parameters (id, brand_id, model_id, parameter_id, parameter_name, parameter_value, erp_id, last_integrated_date, status, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    287   dg      +          0    222237    part_request_unavail 
   TABLE DATA             COPY public.part_request_unavail (id, part_no, part_name, remarks, user_id, user_type_info, user_name, requested_date, intermediator_remarks, parts_req_status_id, parts_request_type, status, action, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    335   �g                0    222028    parts_management 
   TABLE DATA           �   COPY public.parts_management (id, car_id, group_id, sub_group_id, sub_node_id, part_number, part_name, part_erp_id, last_integrated_date, action, status, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    297   �g      �          0    221739    parts_req_status 
   TABLE DATA           �   COPY public.parts_req_status (parts_req_status_id, parts_req_status_name, remarks, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    244   �g                0    222116    pay_out_schedule_management 
   TABLE DATA           �   COPY public.pay_out_schedule_management (id, pay_out_slot_name, payout_shed_id, no_of_day, order_id, sub_order_id, description, action, status, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    313   �g                0    222127    payment_management 
   TABLE DATA           �   COPY public.payment_management (id, seller_id, order_id, sub_order_id, number_of_order, total_amount, commission_amount, seller_amount, payment_status_id, status, action, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    315   �g      �          0    221783    payment_status 
   TABLE DATA           �   COPY public.payment_status (payment_status_id, payment_status_name, remarks, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    252   h      �          0    221901    payment_type_details 
   TABLE DATA           �   COPY public.payment_type_details (payment_type_id, payment_type_name, status, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    274   /h      �          0    221654    payout_shed_details 
   TABLE DATA           �   COPY public.payout_shed_details (payout_shed_id, subscription_detail_id, status, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    228   Lh      �          0    221857    place_of_supply 
   TABLE DATA           �   COPY public.place_of_supply (pos_id, tax_name, tax_code, erp_id, last_integrated_date, status, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    266   ih                0    222039    product_details 
   TABLE DATA           2  COPY public.product_details (product_id, product_code, product_erp_id, last_integrated_date, product_type, app_product_type, brand_info, model_info, car_info, seller_id, genuine_part_no, verify_button, number_of_yr_used, image, image1, image2, image3, image4, image5, image6, image7, image8, image9, image10, weeight, dimension_length, dimension_width, dimension_height, condition, description, trading_type, manufacturer_name, product_listing_status, barcode_no, aftermarket_no, make_offer, status, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    299   �h      �          0    221879    product_type_details 
   TABLE DATA           �   COPY public.product_type_details (product_type_id, product_type_name, product_desc, status, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    270   �h      �          0    221750    quote_status 
   TABLE DATA           �   COPY public.quote_status (quote_status_id, quote_status_name, remarks, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    246   �h      �          0    221794    reason_details 
   TABLE DATA           z   COPY public.reason_details (reason_id, reason_name, status, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    254   �h      �          0    221728    registration_status 
   TABLE DATA           �   COPY public.registration_status (reg_status_id, reg_status_name, remarks, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    242   �h      �          0    221621    reports 
   TABLE DATA           �   COPY public.reports (report_id, report_name, "Login_user_type_id", status, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    222   i      �          0    221580    role_permission 
   TABLE DATA           �   COPY public.role_permission (role_permission_id, module_id, dashboard_id, report_id, role_id, add_access, delete_access, read_access, modify_access, export_access, print_access, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    214   4i      �          0    221569    roles 
   TABLE DATA           �   COPY public.roles (role_id, role_code, role_name, status, action, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    212   Qi      �          0    221932    seller_branch 
   TABLE DATA           �   COPY public.seller_branch ("Seller_branch_id", seller_id, seller_branch_name, branch_country, branch_state, branch_city, branch_address_1, branch_address_2, branch_phone, phone_country_code, branch_drop_address, branch_googlemap_link) FROM stdin;
    public          postgres    false    279   ni      �          0    221940    seller_warehouse 
   TABLE DATA           �   COPY public.seller_warehouse (warehouse_id, "Seller_branch_id", seller_id, warehouse_name, warehouse_phone, warehouse_country_code, warehouse_address) FROM stdin;
    public          postgres    false    281   �i      �          0    221706    shipment_status 
   TABLE DATA           �   COPY public.shipment_status (ship_status_id, ship_status_name, remarks, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    238   �i      9          0    222314    sms_template 
   TABLE DATA           �   COPY public.sms_template (id, module_id, title_name, sms_content, status, action, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    349   �i      �          0    221827    state_details 
   TABLE DATA           }   COPY public.state_details (id, country_id, state_name, status, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    260   �i      5          0    222292    static_page 
   TABLE DATA           �   COPY public.static_page (id, static_page_name, page_title, meta_tag, content, status, action, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    345   �i      �          0    221684    status_details 
   TABLE DATA           r   COPY public.status_details (status_id, status_name, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    234   j                0    222006    sub_group_management 
   TABLE DATA           �   COPY public.sub_group_management (sub_group_id, erp_id, last_integrated_date, group_id, sub_group_name, status, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    293   9j                0    222017    sub_node_management 
   TABLE DATA           �   COPY public.sub_node_management (sub_node_id, sub_group_id, sub_node_code, sub_node_name, erp_id, last_integrated_date, status, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    295   Vj                0    222072    sub_order_details 
   TABLE DATA           D  COPY public.sub_order_details (id, order_id, quantity, price, vat_amount, total_price, product_type_id, trading_type_id, status, seller_id, delivery_time, discount, shipping_charges, brand_id, ship_status_id, order_status_id, sku, product_id, brand_info, model_info, car_info, ship_by_date, deliver_by_date, shipped_by, shipped_person, shipped_person_contact_no, tracking_id, shipping_date, pickup_date, package_type, package_dimension, package_weight, total_shipping_cost, seller_note, buyer_feedback, total_vat_amt, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    305   sj                0    222094    sub_order_quote_details 
   TABLE DATA           X  COPY public.sub_order_quote_details (id, order_quote_id, seller_id, product_id, brand_info, model_info, car_info, quantity, product_price, product_vat_amount, total_vat_amount, total_price, image, product_type_id, trading_type_id, delivery_time, discount, shipping_charges, ship_status_id, order_status_id, sku, ship_by_date, deliver_by_date, shipped_by, shipped_person, shipped_person_contact_no, tracking_id, shipping_date, pickup_date, package_type, package_dimension, package_weight, total_shipping_cost, seller_note, action, status, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    309   �j      �          0    221632    subscription_detail 
   TABLE DATA           �   COPY public.subscription_detail (subscription_detail_id, subscription_plan_id, "Login_user_type_id", user_id, valid_till, effective, status, action, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    224   �j      �          0    221643    subscription_plan_details 
   TABLE DATA           �   COPY public.subscription_plan_details (subscription_plan_id, "Login_user_type_id", subscription_name, number_of_users, modules_list, days, price, valid_till, effective, status, action, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    226   �j      �          0    221519    super_admin 
   TABLE DATA           n   COPY public.super_admin (id, user_id, password, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    203   �j      �          0    221846    tax_code_details 
   TABLE DATA           �   COPY public.tax_code_details (tax_code_id, tax_code_name, status, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    264   k      �          0    221890    trading_type_details 
   TABLE DATA           �   COPY public.trading_type_details (trading_type_id, trading_type_name, status, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    272   !k      �          0    221552    user_managment 
   TABLE DATA           �   COPY public.user_managment (id, "Login_user_type_id", user_name, user_email_id, phone_no, user_password, two_way_auth, status, last_login, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    209   >k                0    222138    vin_history 
   TABLE DATA           �   COPY public.vin_history (id, vin_number, vehicle_identified, brand_id, model_id, user_type_info, user_name, user_id, region, searched_date, status, action, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    317   [k      �           0    0    Rating_review_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public."Rating_review_id_seq"', 1, false);
          public          postgres    false    332            �           0    0    VAT_details_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public."VAT_details_id_seq"', 1, false);
          public          postgres    false    261            �           0    0 (   application_user_application_user_id_seq    SEQUENCE SET     W   SELECT pg_catalog.setval('public.application_user_application_user_id_seq', 1, false);
          public          postgres    false    206            �           0    0    audit_trail_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.audit_trail_id_seq', 1, false);
          public          postgres    false    342            �           0    0    brands_brand_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.brands_brand_id_seq', 1, false);
          public          postgres    false    282            �           0    0    bulk_upload_details_id_seq    SEQUENCE SET     I   SELECT pg_catalog.setval('public.bulk_upload_details_id_seq', 1, false);
          public          postgres    false    231            �           0    0 ,   buyer_customer_details_buyer_customer_id_seq    SEQUENCE SET     [   SELECT pg_catalog.setval('public.buyer_customer_details_buyer_customer_id_seq', 1, false);
          public          postgres    false    324            �           0    0 $   buyer_saved_quote_buyer_quote_id_seq    SEQUENCE SET     S   SELECT pg_catalog.setval('public.buyer_saved_quote_buyer_quote_id_seq', 1, false);
          public          postgres    false    320            �           0    0 &   buyer_saved_quote_service_items_id_seq    SEQUENCE SET     U   SELECT pg_catalog.setval('public.buyer_saved_quote_service_items_id_seq', 1, false);
          public          postgres    false    322            �           0    0    car_management_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.car_management_id_seq', 1, false);
          public          postgres    false    310            �           0    0    cars_car_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.cars_car_id_seq', 1, false);
          public          postgres    false    288            �           0    0    cart_details_cart_item_id_seq    SEQUENCE SET     L   SELECT pg_catalog.setval('public.cart_details_cart_item_id_seq', 1, false);
          public          postgres    false    326            �           0    0 "   category_request_management_id_seq    SEQUENCE SET     Q   SELECT pg_catalog.setval('public.category_request_management_id_seq', 1, false);
          public          postgres    false    318            �           0    0 -   category_request_status_cat_req_status_id_seq    SEQUENCE SET     \   SELECT pg_catalog.setval('public.category_request_status_cat_req_status_id_seq', 1, false);
          public          postgres    false    249            �           0    0    commission_management_id_seq    SEQUENCE SET     K   SELECT pg_catalog.setval('public.commission_management_id_seq', 1, false);
          public          postgres    false    300            �           0    0    configuration_details_id_seq    SEQUENCE SET     K   SELECT pg_catalog.setval('public.configuration_details_id_seq', 1, false);
          public          postgres    false    229            �           0    0    country_details_country_id_seq    SEQUENCE SET     M   SELECT pg_catalog.setval('public.country_details_country_id_seq', 1, false);
          public          postgres    false    257            �           0    0     currency_details_currency_id_seq    SEQUENCE SET     O   SELECT pg_catalog.setval('public.currency_details_currency_id_seq', 1, false);
          public          postgres    false    255            �           0    0    dashboard_dashboard_id_seq    SEQUENCE SET     I   SELECT pg_catalog.setval('public.dashboard_dashboard_id_seq', 1, false);
          public          postgres    false    219            �           0    0    email_template_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.email_template_id_seq', 1, false);
          public          postgres    false    346            �           0    0    group_management_group_id_seq    SEQUENCE SET     L   SELECT pg_catalog.setval('public.group_management_group_id_seq', 1, false);
          public          postgres    false    290            �           0    0    help_management_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.help_management_id_seq', 1, false);
          public          postgres    false    340            �           0    0    intermediator_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.intermediator_id_seq', 1, false);
          public          postgres    false    204            �           0    0    inventory_manangement_id_seq    SEQUENCE SET     K   SELECT pg_catalog.setval('public.inventory_manangement_id_seq', 1, false);
          public          postgres    false    338            �           0    0     jurisdiction_jurisdiction_id_seq    SEQUENCE SET     O   SELECT pg_catalog.setval('public.jurisdiction_jurisdiction_id_seq', 1, false);
          public          postgres    false    267            �           0    0 *   make_offer_status_make_offer_status_id_seq    SEQUENCE SET     Y   SELECT pg_catalog.setval('public.make_offer_status_make_offer_status_id_seq', 1, false);
          public          postgres    false    247            �           0    0    models_model_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.models_model_id_seq', 1, false);
          public          postgres    false    284            �           0    0    module_module_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.module_module_id_seq', 1, false);
          public          postgres    false    217            �           0    0    module_type_module_type_id_seq    SEQUENCE SET     M   SELECT pg_catalog.setval('public.module_type_module_type_id_seq', 1, false);
          public          postgres    false    215            �           0    0    new_buyer_details_id_seq    SEQUENCE SET     G   SELECT pg_catalog.setval('public.new_buyer_details_id_seq', 1, false);
          public          postgres    false    275            �           0    0    new_seller_details_id_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('public.new_seller_details_id_seq', 1, false);
          public          postgres    false    277            �           0    0    newsletter_management_id_seq    SEQUENCE SET     K   SELECT pg_catalog.setval('public.newsletter_management_id_seq', 1, false);
          public          postgres    false    328            �           0    0    notification_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.notification_id_seq', 1, false);
          public          postgres    false    330            �           0    0    offer_management_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.offer_management_id_seq', 1, false);
          public          postgres    false    336            �           0    0    order_management_order_id_seq    SEQUENCE SET     L   SELECT pg_catalog.setval('public.order_management_order_id_seq', 1, false);
          public          postgres    false    302            �           0    0 %   order_quote_Manage_order_quote_id_seq    SEQUENCE SET     V   SELECT pg_catalog.setval('public."order_quote_Manage_order_quote_id_seq"', 1, false);
          public          postgres    false    306            �           0    0     order_status_order_status_id_seq    SEQUENCE SET     O   SELECT pg_catalog.setval('public.order_status_order_status_id_seq', 1, false);
          public          postgres    false    239            �           0    0 $   order_type_details_order_type_id_seq    SEQUENCE SET     S   SELECT pg_catalog.setval('public.order_type_details_order_type_id_seq', 1, false);
          public          postgres    false    235            �           0    0    parameters_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public.parameters_id_seq', 1, false);
          public          postgres    false    286            �           0    0    part_request_unavail_id_seq    SEQUENCE SET     J   SELECT pg_catalog.setval('public.part_request_unavail_id_seq', 1, false);
          public          postgres    false    334            �           0    0    parts_management_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.parts_management_id_seq', 1, false);
          public          postgres    false    296            �           0    0 (   parts_req_status_parts_req_status_id_seq    SEQUENCE SET     W   SELECT pg_catalog.setval('public.parts_req_status_parts_req_status_id_seq', 1, false);
          public          postgres    false    243            �           0    0 "   pay_out_schedule_management_id_seq    SEQUENCE SET     Q   SELECT pg_catalog.setval('public.pay_out_schedule_management_id_seq', 1, false);
          public          postgres    false    312            �           0    0    payment_management_id_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('public.payment_management_id_seq', 1, false);
          public          postgres    false    314            �           0    0 $   payment_status_payment_status_id_seq    SEQUENCE SET     S   SELECT pg_catalog.setval('public.payment_status_payment_status_id_seq', 1, false);
          public          postgres    false    251            �           0    0 (   payment_type_details_payment_type_id_seq    SEQUENCE SET     W   SELECT pg_catalog.setval('public.payment_type_details_payment_type_id_seq', 1, false);
          public          postgres    false    273            �           0    0 &   payout_shed_details_payout_shed_id_seq    SEQUENCE SET     U   SELECT pg_catalog.setval('public.payout_shed_details_payout_shed_id_seq', 1, false);
          public          postgres    false    227            �           0    0    place_of_supply_pos_id_seq    SEQUENCE SET     I   SELECT pg_catalog.setval('public.place_of_supply_pos_id_seq', 1, false);
          public          postgres    false    265            �           0    0    product_details_product_id_seq    SEQUENCE SET     M   SELECT pg_catalog.setval('public.product_details_product_id_seq', 1, false);
          public          postgres    false    298            �           0    0 (   product_type_details_product_type_id_seq    SEQUENCE SET     W   SELECT pg_catalog.setval('public.product_type_details_product_type_id_seq', 1, false);
          public          postgres    false    269            �           0    0     quote_status_quote_status_id_seq    SEQUENCE SET     O   SELECT pg_catalog.setval('public.quote_status_quote_status_id_seq', 1, false);
          public          postgres    false    245            �           0    0    reason_details_reason_id_seq    SEQUENCE SET     K   SELECT pg_catalog.setval('public.reason_details_reason_id_seq', 1, false);
          public          postgres    false    253            �           0    0 %   registration_status_reg_status_id_seq    SEQUENCE SET     T   SELECT pg_catalog.setval('public.registration_status_reg_status_id_seq', 1, false);
          public          postgres    false    241            �           0    0    reports_report_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.reports_report_id_seq', 1, false);
          public          postgres    false    221            �           0    0 &   role_permission_role_permission_id_seq    SEQUENCE SET     U   SELECT pg_catalog.setval('public.role_permission_role_permission_id_seq', 1, false);
          public          postgres    false    213            �           0    0    roles_role_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public.roles_role_id_seq', 1, false);
          public          postgres    false    211            �           0    0 !   seller_warehouse_warehouse_id_seq    SEQUENCE SET     P   SELECT pg_catalog.setval('public.seller_warehouse_warehouse_id_seq', 1, false);
          public          postgres    false    280            �           0    0 "   shipment_status_ship_status_id_seq    SEQUENCE SET     Q   SELECT pg_catalog.setval('public.shipment_status_ship_status_id_seq', 1, false);
          public          postgres    false    237            �           0    0    sms_template_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.sms_template_id_seq', 1, false);
          public          postgres    false    348            �           0    0    state_details_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.state_details_id_seq', 1, false);
          public          postgres    false    259            �           0    0    static_page_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.static_page_id_seq', 1, false);
          public          postgres    false    344            �           0    0    status_details_status_id_seq    SEQUENCE SET     K   SELECT pg_catalog.setval('public.status_details_status_id_seq', 1, false);
          public          postgres    false    233            �           0    0 %   sub_group_management_sub_group_id_seq    SEQUENCE SET     T   SELECT pg_catalog.setval('public.sub_group_management_sub_group_id_seq', 1, false);
          public          postgres    false    292            �           0    0 #   sub_node_management_sub_node_id_seq    SEQUENCE SET     R   SELECT pg_catalog.setval('public.sub_node_management_sub_node_id_seq', 1, false);
          public          postgres    false    294            �           0    0    sub_order_details_id_seq    SEQUENCE SET     G   SELECT pg_catalog.setval('public.sub_order_details_id_seq', 1, false);
          public          postgres    false    304            �           0    0    sub_order_quote_details_id_seq    SEQUENCE SET     M   SELECT pg_catalog.setval('public.sub_order_quote_details_id_seq', 1, false);
          public          postgres    false    308            �           0    0 .   subscription_detail_subscription_detail_id_seq    SEQUENCE SET     ]   SELECT pg_catalog.setval('public.subscription_detail_subscription_detail_id_seq', 1, false);
          public          postgres    false    223            �           0    0 2   subscription_plan_details_subscription_plan_id_seq    SEQUENCE SET     a   SELECT pg_catalog.setval('public.subscription_plan_details_subscription_plan_id_seq', 1, false);
          public          postgres    false    225            �           0    0    super_admin_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.super_admin_id_seq', 1, false);
          public          postgres    false    202            �           0    0     tax_code_details_tax_code_id_seq    SEQUENCE SET     O   SELECT pg_catalog.setval('public.tax_code_details_tax_code_id_seq', 1, false);
          public          postgres    false    263            �           0    0 (   trading_type_details_trading_type_id_seq    SEQUENCE SET     W   SELECT pg_catalog.setval('public.trading_type_details_trading_type_id_seq', 1, false);
          public          postgres    false    271            �           0    0    user_managment_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.user_managment_id_seq', 1, false);
          public          postgres    false    208            �           0    0    vin_history_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.vin_history_id_seq', 1, false);
          public          postgres    false    316            I           2606    222234     Rating_review Rating_review_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY public."Rating_review"
    ADD CONSTRAINT "Rating_review_pkey" PRIMARY KEY (id);
 N   ALTER TABLE ONLY public."Rating_review" DROP CONSTRAINT "Rating_review_pkey";
       public            postgres    false    333                       2606    221843    VAT_details VAT_details_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public."VAT_details"
    ADD CONSTRAINT "VAT_details_pkey" PRIMARY KEY (id);
 J   ALTER TABLE ONLY public."VAT_details" DROP CONSTRAINT "VAT_details_pkey";
       public            postgres    false    262            �           2606    221549 &   application_user application_user_pkey 
   CONSTRAINT     u   ALTER TABLE ONLY public.application_user
    ADD CONSTRAINT application_user_pkey PRIMARY KEY (application_user_id);
 P   ALTER TABLE ONLY public.application_user DROP CONSTRAINT application_user_pkey;
       public            postgres    false    207            S           2606    222289    audit_trail audit_trail_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.audit_trail
    ADD CONSTRAINT audit_trail_pkey PRIMARY KEY (id);
 F   ALTER TABLE ONLY public.audit_trail DROP CONSTRAINT audit_trail_pkey;
       public            postgres    false    343                       2606    221959    brands brands_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY public.brands
    ADD CONSTRAINT brands_pkey PRIMARY KEY (brand_id);
 <   ALTER TABLE ONLY public.brands DROP CONSTRAINT brands_pkey;
       public            postgres    false    283            �           2606    221681 ,   bulk_upload_details bulk_upload_details_pkey 
   CONSTRAINT     j   ALTER TABLE ONLY public.bulk_upload_details
    ADD CONSTRAINT bulk_upload_details_pkey PRIMARY KEY (id);
 V   ALTER TABLE ONLY public.bulk_upload_details DROP CONSTRAINT bulk_upload_details_pkey;
       public            postgres    false    232            A           2606    222190 2   buyer_customer_details buyer_customer_details_pkey 
   CONSTRAINT        ALTER TABLE ONLY public.buyer_customer_details
    ADD CONSTRAINT buyer_customer_details_pkey PRIMARY KEY (buyer_customer_id);
 \   ALTER TABLE ONLY public.buyer_customer_details DROP CONSTRAINT buyer_customer_details_pkey;
       public            postgres    false    325            =           2606    222168 (   buyer_saved_quote buyer_saved_quote_pkey 
   CONSTRAINT     r   ALTER TABLE ONLY public.buyer_saved_quote
    ADD CONSTRAINT buyer_saved_quote_pkey PRIMARY KEY (buyer_quote_id);
 R   ALTER TABLE ONLY public.buyer_saved_quote DROP CONSTRAINT buyer_saved_quote_pkey;
       public            postgres    false    321            ?           2606    222179 D   buyer_saved_quote_service_items buyer_saved_quote_service_items_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.buyer_saved_quote_service_items
    ADD CONSTRAINT buyer_saved_quote_service_items_pkey PRIMARY KEY (id);
 n   ALTER TABLE ONLY public.buyer_saved_quote_service_items DROP CONSTRAINT buyer_saved_quote_service_items_pkey;
       public            postgres    false    323            3           2606    222113 "   car_management car_management_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY public.car_management
    ADD CONSTRAINT car_management_pkey PRIMARY KEY (id);
 L   ALTER TABLE ONLY public.car_management DROP CONSTRAINT car_management_pkey;
       public            postgres    false    311                       2606    221992    cars cars_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY public.cars
    ADD CONSTRAINT cars_pkey PRIMARY KEY (car_id);
 8   ALTER TABLE ONLY public.cars DROP CONSTRAINT cars_pkey;
       public            postgres    false    289            C           2606    222201    cart_details cart_details_pkey 
   CONSTRAINT     f   ALTER TABLE ONLY public.cart_details
    ADD CONSTRAINT cart_details_pkey PRIMARY KEY (cart_item_id);
 H   ALTER TABLE ONLY public.cart_details DROP CONSTRAINT cart_details_pkey;
       public            postgres    false    327            ;           2606    222157 <   category_request_management category_request_management_pkey 
   CONSTRAINT     z   ALTER TABLE ONLY public.category_request_management
    ADD CONSTRAINT category_request_management_pkey PRIMARY KEY (id);
 f   ALTER TABLE ONLY public.category_request_management DROP CONSTRAINT category_request_management_pkey;
       public            postgres    false    319            �           2606    221780 4   category_request_status category_request_status_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.category_request_status
    ADD CONSTRAINT category_request_status_pkey PRIMARY KEY (cat_req_status_id);
 ^   ALTER TABLE ONLY public.category_request_status DROP CONSTRAINT category_request_status_pkey;
       public            postgres    false    250            )           2606    222058 0   commission_management commission_management_pkey 
   CONSTRAINT     n   ALTER TABLE ONLY public.commission_management
    ADD CONSTRAINT commission_management_pkey PRIMARY KEY (id);
 Z   ALTER TABLE ONLY public.commission_management DROP CONSTRAINT commission_management_pkey;
       public            postgres    false    301            �           2606    221670 0   configuration_details configuration_details_pkey 
   CONSTRAINT     n   ALTER TABLE ONLY public.configuration_details
    ADD CONSTRAINT configuration_details_pkey PRIMARY KEY (id);
 Z   ALTER TABLE ONLY public.configuration_details DROP CONSTRAINT configuration_details_pkey;
       public            postgres    false    230            �           2606    221824 $   country_details country_details_pkey 
   CONSTRAINT     j   ALTER TABLE ONLY public.country_details
    ADD CONSTRAINT country_details_pkey PRIMARY KEY (country_id);
 N   ALTER TABLE ONLY public.country_details DROP CONSTRAINT country_details_pkey;
       public            postgres    false    258            �           2606    221813 &   currency_details currency_details_pkey 
   CONSTRAINT     m   ALTER TABLE ONLY public.currency_details
    ADD CONSTRAINT currency_details_pkey PRIMARY KEY (currency_id);
 P   ALTER TABLE ONLY public.currency_details DROP CONSTRAINT currency_details_pkey;
       public            postgres    false    256            �           2606    221618    dashboard dashboard_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY public.dashboard
    ADD CONSTRAINT dashboard_pkey PRIMARY KEY (dashboard_id);
 B   ALTER TABLE ONLY public.dashboard DROP CONSTRAINT dashboard_pkey;
       public            postgres    false    220            W           2606    222311 "   email_template email_template_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY public.email_template
    ADD CONSTRAINT email_template_pkey PRIMARY KEY (id);
 L   ALTER TABLE ONLY public.email_template DROP CONSTRAINT email_template_pkey;
       public            postgres    false    347                       2606    222003 &   group_management group_management_pkey 
   CONSTRAINT     j   ALTER TABLE ONLY public.group_management
    ADD CONSTRAINT group_management_pkey PRIMARY KEY (group_id);
 P   ALTER TABLE ONLY public.group_management DROP CONSTRAINT group_management_pkey;
       public            postgres    false    291            Q           2606    222278 $   help_management help_management_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY public.help_management
    ADD CONSTRAINT help_management_pkey PRIMARY KEY (id);
 N   ALTER TABLE ONLY public.help_management DROP CONSTRAINT help_management_pkey;
       public            postgres    false    341            �           2606    221538     intermediator intermediator_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public.intermediator
    ADD CONSTRAINT intermediator_pkey PRIMARY KEY (id);
 J   ALTER TABLE ONLY public.intermediator DROP CONSTRAINT intermediator_pkey;
       public            postgres    false    205            O           2606    222267 0   inventory_manangement inventory_manangement_pkey 
   CONSTRAINT     n   ALTER TABLE ONLY public.inventory_manangement
    ADD CONSTRAINT inventory_manangement_pkey PRIMARY KEY (id);
 Z   ALTER TABLE ONLY public.inventory_manangement DROP CONSTRAINT inventory_manangement_pkey;
       public            postgres    false    339            	           2606    221876    jurisdiction jurisdiction_pkey 
   CONSTRAINT     i   ALTER TABLE ONLY public.jurisdiction
    ADD CONSTRAINT jurisdiction_pkey PRIMARY KEY (jurisdiction_id);
 H   ALTER TABLE ONLY public.jurisdiction DROP CONSTRAINT jurisdiction_pkey;
       public            postgres    false    268            �           2606    221769 (   make_offer_status make_offer_status_pkey 
   CONSTRAINT     x   ALTER TABLE ONLY public.make_offer_status
    ADD CONSTRAINT make_offer_status_pkey PRIMARY KEY (make_offer_status_id);
 R   ALTER TABLE ONLY public.make_offer_status DROP CONSTRAINT make_offer_status_pkey;
       public            postgres    false    248                       2606    221970    models models_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY public.models
    ADD CONSTRAINT models_pkey PRIMARY KEY (model_id);
 <   ALTER TABLE ONLY public.models DROP CONSTRAINT models_pkey;
       public            postgres    false    285            �           2606    221607    module module_pkey 
   CONSTRAINT     W   ALTER TABLE ONLY public.module
    ADD CONSTRAINT module_pkey PRIMARY KEY (module_id);
 <   ALTER TABLE ONLY public.module DROP CONSTRAINT module_pkey;
       public            postgres    false    218            �           2606    221596    module_type module_type_pkey 
   CONSTRAINT     f   ALTER TABLE ONLY public.module_type
    ADD CONSTRAINT module_type_pkey PRIMARY KEY (module_type_id);
 F   ALTER TABLE ONLY public.module_type DROP CONSTRAINT module_type_pkey;
       public            postgres    false    216                       2606    221920 (   new_buyer_details new_buyer_details_pkey 
   CONSTRAINT     f   ALTER TABLE ONLY public.new_buyer_details
    ADD CONSTRAINT new_buyer_details_pkey PRIMARY KEY (id);
 R   ALTER TABLE ONLY public.new_buyer_details DROP CONSTRAINT new_buyer_details_pkey;
       public            postgres    false    276                       2606    221931 *   new_seller_details new_seller_details_pkey 
   CONSTRAINT     h   ALTER TABLE ONLY public.new_seller_details
    ADD CONSTRAINT new_seller_details_pkey PRIMARY KEY (id);
 T   ALTER TABLE ONLY public.new_seller_details DROP CONSTRAINT new_seller_details_pkey;
       public            postgres    false    278            E           2606    222212 0   newsletter_management newsletter_management_pkey 
   CONSTRAINT     n   ALTER TABLE ONLY public.newsletter_management
    ADD CONSTRAINT newsletter_management_pkey PRIMARY KEY (id);
 Z   ALTER TABLE ONLY public.newsletter_management DROP CONSTRAINT newsletter_management_pkey;
       public            postgres    false    329            G           2606    222223    notification notification_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.notification
    ADD CONSTRAINT notification_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY public.notification DROP CONSTRAINT notification_pkey;
       public            postgres    false    331            M           2606    222256 &   offer_management offer_management_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.offer_management
    ADD CONSTRAINT offer_management_pkey PRIMARY KEY (id);
 P   ALTER TABLE ONLY public.offer_management DROP CONSTRAINT offer_management_pkey;
       public            postgres    false    337            +           2606    222069 &   order_management order_management_pkey 
   CONSTRAINT     j   ALTER TABLE ONLY public.order_management
    ADD CONSTRAINT order_management_pkey PRIMARY KEY (order_id);
 P   ALTER TABLE ONLY public.order_management DROP CONSTRAINT order_management_pkey;
       public            postgres    false    303            /           2606    222091 *   order_quote_Manage order_quote_Manage_pkey 
   CONSTRAINT     x   ALTER TABLE ONLY public."order_quote_Manage"
    ADD CONSTRAINT "order_quote_Manage_pkey" PRIMARY KEY (order_quote_id);
 X   ALTER TABLE ONLY public."order_quote_Manage" DROP CONSTRAINT "order_quote_Manage_pkey";
       public            postgres    false    307            �           2606    221725    order_status order_status_pkey 
   CONSTRAINT     i   ALTER TABLE ONLY public.order_status
    ADD CONSTRAINT order_status_pkey PRIMARY KEY (order_status_id);
 H   ALTER TABLE ONLY public.order_status DROP CONSTRAINT order_status_pkey;
       public            postgres    false    240            �           2606    221703 *   order_type_details order_type_details_pkey 
   CONSTRAINT     s   ALTER TABLE ONLY public.order_type_details
    ADD CONSTRAINT order_type_details_pkey PRIMARY KEY (order_type_id);
 T   ALTER TABLE ONLY public.order_type_details DROP CONSTRAINT order_type_details_pkey;
       public            postgres    false    236                       2606    221981    parameters parameters_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.parameters
    ADD CONSTRAINT parameters_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY public.parameters DROP CONSTRAINT parameters_pkey;
       public            postgres    false    287            K           2606    222245 .   part_request_unavail part_request_unavail_pkey 
   CONSTRAINT     l   ALTER TABLE ONLY public.part_request_unavail
    ADD CONSTRAINT part_request_unavail_pkey PRIMARY KEY (id);
 X   ALTER TABLE ONLY public.part_request_unavail DROP CONSTRAINT part_request_unavail_pkey;
       public            postgres    false    335            %           2606    222036 &   parts_management parts_management_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.parts_management
    ADD CONSTRAINT parts_management_pkey PRIMARY KEY (id);
 P   ALTER TABLE ONLY public.parts_management DROP CONSTRAINT parts_management_pkey;
       public            postgres    false    297            �           2606    221747 &   parts_req_status parts_req_status_pkey 
   CONSTRAINT     u   ALTER TABLE ONLY public.parts_req_status
    ADD CONSTRAINT parts_req_status_pkey PRIMARY KEY (parts_req_status_id);
 P   ALTER TABLE ONLY public.parts_req_status DROP CONSTRAINT parts_req_status_pkey;
       public            postgres    false    244            5           2606    222124 <   pay_out_schedule_management pay_out_schedule_management_pkey 
   CONSTRAINT     z   ALTER TABLE ONLY public.pay_out_schedule_management
    ADD CONSTRAINT pay_out_schedule_management_pkey PRIMARY KEY (id);
 f   ALTER TABLE ONLY public.pay_out_schedule_management DROP CONSTRAINT pay_out_schedule_management_pkey;
       public            postgres    false    313            7           2606    222135 *   payment_management payment_management_pkey 
   CONSTRAINT     h   ALTER TABLE ONLY public.payment_management
    ADD CONSTRAINT payment_management_pkey PRIMARY KEY (id);
 T   ALTER TABLE ONLY public.payment_management DROP CONSTRAINT payment_management_pkey;
       public            postgres    false    315            �           2606    221791 "   payment_status payment_status_pkey 
   CONSTRAINT     o   ALTER TABLE ONLY public.payment_status
    ADD CONSTRAINT payment_status_pkey PRIMARY KEY (payment_status_id);
 L   ALTER TABLE ONLY public.payment_status DROP CONSTRAINT payment_status_pkey;
       public            postgres    false    252                       2606    221909 .   payment_type_details payment_type_details_pkey 
   CONSTRAINT     y   ALTER TABLE ONLY public.payment_type_details
    ADD CONSTRAINT payment_type_details_pkey PRIMARY KEY (payment_type_id);
 X   ALTER TABLE ONLY public.payment_type_details DROP CONSTRAINT payment_type_details_pkey;
       public            postgres    false    274            �           2606    221659 ,   payout_shed_details payout_shed_details_pkey 
   CONSTRAINT     v   ALTER TABLE ONLY public.payout_shed_details
    ADD CONSTRAINT payout_shed_details_pkey PRIMARY KEY (payout_shed_id);
 V   ALTER TABLE ONLY public.payout_shed_details DROP CONSTRAINT payout_shed_details_pkey;
       public            postgres    false    228                       2606    221865 $   place_of_supply place_of_supply_pkey 
   CONSTRAINT     f   ALTER TABLE ONLY public.place_of_supply
    ADD CONSTRAINT place_of_supply_pkey PRIMARY KEY (pos_id);
 N   ALTER TABLE ONLY public.place_of_supply DROP CONSTRAINT place_of_supply_pkey;
       public            postgres    false    266            '           2606    222047 $   product_details product_details_pkey 
   CONSTRAINT     j   ALTER TABLE ONLY public.product_details
    ADD CONSTRAINT product_details_pkey PRIMARY KEY (product_id);
 N   ALTER TABLE ONLY public.product_details DROP CONSTRAINT product_details_pkey;
       public            postgres    false    299                       2606    221887 .   product_type_details product_type_details_pkey 
   CONSTRAINT     y   ALTER TABLE ONLY public.product_type_details
    ADD CONSTRAINT product_type_details_pkey PRIMARY KEY (product_type_id);
 X   ALTER TABLE ONLY public.product_type_details DROP CONSTRAINT product_type_details_pkey;
       public            postgres    false    270            �           2606    221758    quote_status quote_status_pkey 
   CONSTRAINT     i   ALTER TABLE ONLY public.quote_status
    ADD CONSTRAINT quote_status_pkey PRIMARY KEY (quote_status_id);
 H   ALTER TABLE ONLY public.quote_status DROP CONSTRAINT quote_status_pkey;
       public            postgres    false    246            �           2606    221802 "   reason_details reason_details_pkey 
   CONSTRAINT     g   ALTER TABLE ONLY public.reason_details
    ADD CONSTRAINT reason_details_pkey PRIMARY KEY (reason_id);
 L   ALTER TABLE ONLY public.reason_details DROP CONSTRAINT reason_details_pkey;
       public            postgres    false    254            �           2606    221736 ,   registration_status registration_status_pkey 
   CONSTRAINT     u   ALTER TABLE ONLY public.registration_status
    ADD CONSTRAINT registration_status_pkey PRIMARY KEY (reg_status_id);
 V   ALTER TABLE ONLY public.registration_status DROP CONSTRAINT registration_status_pkey;
       public            postgres    false    242            �           2606    221629    reports reports_pkey 
   CONSTRAINT     Y   ALTER TABLE ONLY public.reports
    ADD CONSTRAINT reports_pkey PRIMARY KEY (report_id);
 >   ALTER TABLE ONLY public.reports DROP CONSTRAINT reports_pkey;
       public            postgres    false    222            �           2606    221585 $   role_permission role_permission_pkey 
   CONSTRAINT     r   ALTER TABLE ONLY public.role_permission
    ADD CONSTRAINT role_permission_pkey PRIMARY KEY (role_permission_id);
 N   ALTER TABLE ONLY public.role_permission DROP CONSTRAINT role_permission_pkey;
       public            postgres    false    214            �           2606    221577    roles roles_pkey 
   CONSTRAINT     S   ALTER TABLE ONLY public.roles
    ADD CONSTRAINT roles_pkey PRIMARY KEY (role_id);
 :   ALTER TABLE ONLY public.roles DROP CONSTRAINT roles_pkey;
       public            postgres    false    212                       2606    221948 &   seller_warehouse seller_warehouse_pkey 
   CONSTRAINT     n   ALTER TABLE ONLY public.seller_warehouse
    ADD CONSTRAINT seller_warehouse_pkey PRIMARY KEY (warehouse_id);
 P   ALTER TABLE ONLY public.seller_warehouse DROP CONSTRAINT seller_warehouse_pkey;
       public            postgres    false    281            �           2606    221714 $   shipment_status shipment_status_pkey 
   CONSTRAINT     n   ALTER TABLE ONLY public.shipment_status
    ADD CONSTRAINT shipment_status_pkey PRIMARY KEY (ship_status_id);
 N   ALTER TABLE ONLY public.shipment_status DROP CONSTRAINT shipment_status_pkey;
       public            postgres    false    238            Y           2606    222322    sms_template sms_template_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.sms_template
    ADD CONSTRAINT sms_template_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY public.sms_template DROP CONSTRAINT sms_template_pkey;
       public            postgres    false    349                       2606    221835     state_details state_details_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public.state_details
    ADD CONSTRAINT state_details_pkey PRIMARY KEY (id);
 J   ALTER TABLE ONLY public.state_details DROP CONSTRAINT state_details_pkey;
       public            postgres    false    260            U           2606    222300    static_page static_page_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.static_page
    ADD CONSTRAINT static_page_pkey PRIMARY KEY (id);
 F   ALTER TABLE ONLY public.static_page DROP CONSTRAINT static_page_pkey;
       public            postgres    false    345            �           2606    221692 "   status_details status_details_pkey 
   CONSTRAINT     g   ALTER TABLE ONLY public.status_details
    ADD CONSTRAINT status_details_pkey PRIMARY KEY (status_id);
 L   ALTER TABLE ONLY public.status_details DROP CONSTRAINT status_details_pkey;
       public            postgres    false    234            !           2606    222014 .   sub_group_management sub_group_management_pkey 
   CONSTRAINT     v   ALTER TABLE ONLY public.sub_group_management
    ADD CONSTRAINT sub_group_management_pkey PRIMARY KEY (sub_group_id);
 X   ALTER TABLE ONLY public.sub_group_management DROP CONSTRAINT sub_group_management_pkey;
       public            postgres    false    293            #           2606    222025 ,   sub_node_management sub_node_management_pkey 
   CONSTRAINT     s   ALTER TABLE ONLY public.sub_node_management
    ADD CONSTRAINT sub_node_management_pkey PRIMARY KEY (sub_node_id);
 V   ALTER TABLE ONLY public.sub_node_management DROP CONSTRAINT sub_node_management_pkey;
       public            postgres    false    295            -           2606    222080 (   sub_order_details sub_order_details_pkey 
   CONSTRAINT     f   ALTER TABLE ONLY public.sub_order_details
    ADD CONSTRAINT sub_order_details_pkey PRIMARY KEY (id);
 R   ALTER TABLE ONLY public.sub_order_details DROP CONSTRAINT sub_order_details_pkey;
       public            postgres    false    305            1           2606    222102 4   sub_order_quote_details sub_order_quote_details_pkey 
   CONSTRAINT     r   ALTER TABLE ONLY public.sub_order_quote_details
    ADD CONSTRAINT sub_order_quote_details_pkey PRIMARY KEY (id);
 ^   ALTER TABLE ONLY public.sub_order_quote_details DROP CONSTRAINT sub_order_quote_details_pkey;
       public            postgres    false    309            �           2606    221640 ,   subscription_detail subscription_detail_pkey 
   CONSTRAINT     ~   ALTER TABLE ONLY public.subscription_detail
    ADD CONSTRAINT subscription_detail_pkey PRIMARY KEY (subscription_detail_id);
 V   ALTER TABLE ONLY public.subscription_detail DROP CONSTRAINT subscription_detail_pkey;
       public            postgres    false    224            �           2606    221651 8   subscription_plan_details subscription_plan_details_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.subscription_plan_details
    ADD CONSTRAINT subscription_plan_details_pkey PRIMARY KEY (subscription_plan_id);
 b   ALTER TABLE ONLY public.subscription_plan_details DROP CONSTRAINT subscription_plan_details_pkey;
       public            postgres    false    226            �           2606    221527    super_admin super_admin_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.super_admin
    ADD CONSTRAINT super_admin_pkey PRIMARY KEY (id);
 F   ALTER TABLE ONLY public.super_admin DROP CONSTRAINT super_admin_pkey;
       public            postgres    false    203                       2606    221854 &   tax_code_details tax_code_details_pkey 
   CONSTRAINT     m   ALTER TABLE ONLY public.tax_code_details
    ADD CONSTRAINT tax_code_details_pkey PRIMARY KEY (tax_code_id);
 P   ALTER TABLE ONLY public.tax_code_details DROP CONSTRAINT tax_code_details_pkey;
       public            postgres    false    264                       2606    221898 .   trading_type_details trading_type_details_pkey 
   CONSTRAINT     y   ALTER TABLE ONLY public.trading_type_details
    ADD CONSTRAINT trading_type_details_pkey PRIMARY KEY (trading_type_id);
 X   ALTER TABLE ONLY public.trading_type_details DROP CONSTRAINT trading_type_details_pkey;
       public            postgres    false    272            �           2606    221560 "   user_managment user_managment_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY public.user_managment
    ADD CONSTRAINT user_managment_pkey PRIMARY KEY (id);
 L   ALTER TABLE ONLY public.user_managment DROP CONSTRAINT user_managment_pkey;
       public            postgres    false    209            9           2606    222146    vin_history vin_history_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.vin_history
    ADD CONSTRAINT vin_history_pkey PRIMARY KEY (id);
 F   ALTER TABLE ONLY public.vin_history DROP CONSTRAINT vin_history_pkey;
       public            postgres    false    317            
           2606    222518 '   Rating_review Rating_review_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public."Rating_review"
    ADD CONSTRAINT "Rating_review_status_fkey" FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 U   ALTER TABLE ONLY public."Rating_review" DROP CONSTRAINT "Rating_review_status_fkey";
       public          postgres    false    333    3303    234                       2606    222383 #   VAT_details VAT_details_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public."VAT_details"
    ADD CONSTRAINT "VAT_details_status_fkey" FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 Q   ALTER TABLE ONLY public."VAT_details" DROP CONSTRAINT "VAT_details_status_fkey";
       public          postgres    false    234    262    3303            ]           2606    222408 -   application_user application_user_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.application_user
    ADD CONSTRAINT application_user_status_fkey FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 W   ALTER TABLE ONLY public.application_user DROP CONSTRAINT application_user_status_fkey;
       public          postgres    false    3303    207    234                       2606    223093 '   audit_trail audit_trail_created_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.audit_trail
    ADD CONSTRAINT audit_trail_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.user_managment(id);
 Q   ALTER TABLE ONLY public.audit_trail DROP CONSTRAINT audit_trail_created_by_fkey;
       public          postgres    false    3279    343    209                       2606    223098 (   audit_trail audit_trail_modified_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.audit_trail
    ADD CONSTRAINT audit_trail_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.user_managment(id);
 R   ALTER TABLE ONLY public.audit_trail DROP CONSTRAINT audit_trail_modified_by_fkey;
       public          postgres    false    3279    343    209                       2606    223288 &   audit_trail audit_trail_module_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.audit_trail
    ADD CONSTRAINT audit_trail_module_id_fkey FOREIGN KEY (module_id) REFERENCES public.module(module_id);
 P   ALTER TABLE ONLY public.audit_trail DROP CONSTRAINT audit_trail_module_id_fkey;
       public          postgres    false    3287    218    343                       2606    222548 #   audit_trail audit_trail_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.audit_trail
    ADD CONSTRAINT audit_trail_status_fkey FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 M   ALTER TABLE ONLY public.audit_trail DROP CONSTRAINT audit_trail_status_fkey;
       public          postgres    false    234    343    3303            �           2606    222698    brands brands_created_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.brands
    ADD CONSTRAINT brands_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.user_managment(id);
 G   ALTER TABLE ONLY public.brands DROP CONSTRAINT brands_created_by_fkey;
       public          postgres    false    209    283    3279            �           2606    222703    brands brands_modified_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.brands
    ADD CONSTRAINT brands_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.user_managment(id);
 H   ALTER TABLE ONLY public.brands DROP CONSTRAINT brands_modified_by_fkey;
       public          postgres    false    3279    283    209            �           2606    222413    brands brands_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.brands
    ADD CONSTRAINT brands_status_fkey FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 C   ALTER TABLE ONLY public.brands DROP CONSTRAINT brands_status_fkey;
       public          postgres    false    283    3303    234            q           2606    222578 <   bulk_upload_details bulk_upload_details_product_type_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.bulk_upload_details
    ADD CONSTRAINT bulk_upload_details_product_type_id_fkey FOREIGN KEY (product_type_id) REFERENCES public.product_type_details(product_type_id);
 f   ALTER TABLE ONLY public.bulk_upload_details DROP CONSTRAINT bulk_upload_details_product_type_id_fkey;
       public          postgres    false    270    232    3339            p           2606    222573 3   bulk_upload_details bulk_upload_details_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.bulk_upload_details
    ADD CONSTRAINT bulk_upload_details_status_fkey FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 ]   ALTER TABLE ONLY public.bulk_upload_details DROP CONSTRAINT bulk_upload_details_status_fkey;
       public          postgres    false    234    232    3303                       2606    222503 ;   buyer_customer_details buyer_customer_details_buyer_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.buyer_customer_details
    ADD CONSTRAINT buyer_customer_details_buyer_id_fkey FOREIGN KEY (buyer_id) REFERENCES public.new_buyer_details(id);
 e   ALTER TABLE ONLY public.buyer_customer_details DROP CONSTRAINT buyer_customer_details_buyer_id_fkey;
       public          postgres    false    3345    325    276                       2606    222498 9   buyer_customer_details buyer_customer_details_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.buyer_customer_details
    ADD CONSTRAINT buyer_customer_details_status_fkey FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 c   ALTER TABLE ONLY public.buyer_customer_details DROP CONSTRAINT buyer_customer_details_status_fkey;
       public          postgres    false    3303    234    325            �           2606    223208 1   buyer_saved_quote buyer_saved_quote_buyer_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.buyer_saved_quote
    ADD CONSTRAINT buyer_saved_quote_buyer_id_fkey FOREIGN KEY (buyer_id) REFERENCES public.application_user(application_user_id);
 [   ALTER TABLE ONLY public.buyer_saved_quote DROP CONSTRAINT buyer_saved_quote_buyer_id_fkey;
       public          postgres    false    321    207    3277            �           2606    223218 :   buyer_saved_quote buyer_saved_quote_car_details_brand_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.buyer_saved_quote
    ADD CONSTRAINT buyer_saved_quote_car_details_brand_fkey FOREIGN KEY (car_details_brand) REFERENCES public.brands(brand_id);
 d   ALTER TABLE ONLY public.buyer_saved_quote DROP CONSTRAINT buyer_saved_quote_car_details_brand_fkey;
       public          postgres    false    321    283    3351            �           2606    223223 5   buyer_saved_quote buyer_saved_quote_cart_item_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.buyer_saved_quote
    ADD CONSTRAINT buyer_saved_quote_cart_item_id_fkey FOREIGN KEY (cart_item_id) REFERENCES public.cart_details(cart_item_id);
 _   ALTER TABLE ONLY public.buyer_saved_quote DROP CONSTRAINT buyer_saved_quote_cart_item_id_fkey;
       public          postgres    false    321    327    3395            �           2606    223213 1   buyer_saved_quote buyer_saved_quote_model_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.buyer_saved_quote
    ADD CONSTRAINT buyer_saved_quote_model_id_fkey FOREIGN KEY (model_id) REFERENCES public.models(model_id);
 [   ALTER TABLE ONLY public.buyer_saved_quote DROP CONSTRAINT buyer_saved_quote_model_id_fkey;
       public          postgres    false    321    285    3353                        2606    223233 S   buyer_saved_quote_service_items buyer_saved_quote_service_items_buyer_quote_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.buyer_saved_quote_service_items
    ADD CONSTRAINT buyer_saved_quote_service_items_buyer_quote_id_fkey FOREIGN KEY (buyer_quote_id) REFERENCES public.buyer_saved_quote(buyer_quote_id);
 }   ALTER TABLE ONLY public.buyer_saved_quote_service_items DROP CONSTRAINT buyer_saved_quote_service_items_buyer_quote_id_fkey;
       public          postgres    false    3389    321    323            �           2606    222493 K   buyer_saved_quote_service_items buyer_saved_quote_service_items_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.buyer_saved_quote_service_items
    ADD CONSTRAINT buyer_saved_quote_service_items_status_fkey FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 u   ALTER TABLE ONLY public.buyer_saved_quote_service_items DROP CONSTRAINT buyer_saved_quote_service_items_status_fkey;
       public          postgres    false    323    234    3303            �           2606    222488 /   buyer_saved_quote buyer_saved_quote_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.buyer_saved_quote
    ADD CONSTRAINT buyer_saved_quote_status_fkey FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 Y   ALTER TABLE ONLY public.buyer_saved_quote DROP CONSTRAINT buyer_saved_quote_status_fkey;
       public          postgres    false    234    321    3303            �           2606    223228 8   buyer_saved_quote buyer_saved_quote_status_of_quote_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.buyer_saved_quote
    ADD CONSTRAINT buyer_saved_quote_status_of_quote_fkey FOREIGN KEY (status_of_quote) REFERENCES public.quote_status(quote_status_id);
 b   ALTER TABLE ONLY public.buyer_saved_quote DROP CONSTRAINT buyer_saved_quote_status_of_quote_fkey;
       public          postgres    false    321    3315    246            �           2606    222723 4   car_management car_management_buyer_customer_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.car_management
    ADD CONSTRAINT car_management_buyer_customer_id_fkey FOREIGN KEY (buyer_customer_id) REFERENCES public.buyer_customer_details(buyer_customer_id);
 ^   ALTER TABLE ONLY public.car_management DROP CONSTRAINT car_management_buyer_customer_id_fkey;
       public          postgres    false    325    311    3393            �           2606    222718 +   car_management car_management_buyer_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.car_management
    ADD CONSTRAINT car_management_buyer_id_fkey FOREIGN KEY (buyer_id) REFERENCES public.new_buyer_details(id);
 U   ALTER TABLE ONLY public.car_management DROP CONSTRAINT car_management_buyer_id_fkey;
       public          postgres    false    276    311    3345            �           2606    222708 -   car_management car_management_created_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.car_management
    ADD CONSTRAINT car_management_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.user_managment(id);
 W   ALTER TABLE ONLY public.car_management DROP CONSTRAINT car_management_created_by_fkey;
       public          postgres    false    311    209    3279            �           2606    222713 .   car_management car_management_modified_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.car_management
    ADD CONSTRAINT car_management_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.user_managment(id);
 X   ALTER TABLE ONLY public.car_management DROP CONSTRAINT car_management_modified_by_fkey;
       public          postgres    false    3279    311    209            �           2606    222463 )   car_management car_management_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.car_management
    ADD CONSTRAINT car_management_status_fkey FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 S   ALTER TABLE ONLY public.car_management DROP CONSTRAINT car_management_status_fkey;
       public          postgres    false    311    234    3303            �           2606    222958    cars cars_brand_id_fkey    FK CONSTRAINT     ~   ALTER TABLE ONLY public.cars
    ADD CONSTRAINT cars_brand_id_fkey FOREIGN KEY (brand_id) REFERENCES public.brands(brand_id);
 A   ALTER TABLE ONLY public.cars DROP CONSTRAINT cars_brand_id_fkey;
       public          postgres    false    289    3351    283            �           2606    222953    cars cars_created_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.cars
    ADD CONSTRAINT cars_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.user_managment(id);
 C   ALTER TABLE ONLY public.cars DROP CONSTRAINT cars_created_by_fkey;
       public          postgres    false    289    3279    209            �           2606    222968    cars cars_model_id_fkey    FK CONSTRAINT     ~   ALTER TABLE ONLY public.cars
    ADD CONSTRAINT cars_model_id_fkey FOREIGN KEY (model_id) REFERENCES public.models(model_id);
 A   ALTER TABLE ONLY public.cars DROP CONSTRAINT cars_model_id_fkey;
       public          postgres    false    289    3353    285            �           2606    222948    cars cars_modified_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.cars
    ADD CONSTRAINT cars_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.user_managment(id);
 D   ALTER TABLE ONLY public.cars DROP CONSTRAINT cars_modified_by_fkey;
       public          postgres    false    289    209    3279            �           2606    222963    cars cars_parameters_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.cars
    ADD CONSTRAINT cars_parameters_id_fkey FOREIGN KEY (parameters_id) REFERENCES public.parameters(id);
 F   ALTER TABLE ONLY public.cars DROP CONSTRAINT cars_parameters_id_fkey;
       public          postgres    false    3355    287    289            �           2606    222428    cars cars_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.cars
    ADD CONSTRAINT cars_status_fkey FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 ?   ALTER TABLE ONLY public.cars DROP CONSTRAINT cars_status_fkey;
       public          postgres    false    289    3303    234            �           2606    223048 N   category_request_management category_request_management_cat_req_status_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.category_request_management
    ADD CONSTRAINT category_request_management_cat_req_status_id_fkey FOREIGN KEY (cat_req_status_id) REFERENCES public.category_request_status(cat_req_status_id);
 x   ALTER TABLE ONLY public.category_request_management DROP CONSTRAINT category_request_management_cat_req_status_id_fkey;
       public          postgres    false    250    3319    319            �           2606    223038 G   category_request_management category_request_management_created_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.category_request_management
    ADD CONSTRAINT category_request_management_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.super_admin(id);
 q   ALTER TABLE ONLY public.category_request_management DROP CONSTRAINT category_request_management_created_by_fkey;
       public          postgres    false    319    203    3273            �           2606    223033 H   category_request_management category_request_management_modified_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.category_request_management
    ADD CONSTRAINT category_request_management_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.super_admin(id);
 r   ALTER TABLE ONLY public.category_request_management DROP CONSTRAINT category_request_management_modified_by_fkey;
       public          postgres    false    319    203    3273            �           2606    223043 N   category_request_management category_request_management_reason_for_reject_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.category_request_management
    ADD CONSTRAINT category_request_management_reason_for_reject_fkey FOREIGN KEY (reason_for_reject) REFERENCES public.reason_details(reason_id);
 x   ALTER TABLE ONLY public.category_request_management DROP CONSTRAINT category_request_management_reason_for_reject_fkey;
       public          postgres    false    3323    254    319            �           2606    222483 C   category_request_management category_request_management_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.category_request_management
    ADD CONSTRAINT category_request_management_status_fkey FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 m   ALTER TABLE ONLY public.category_request_management DROP CONSTRAINT category_request_management_status_fkey;
       public          postgres    false    319    3303    234            �           2606    223013 ;   commission_management commission_management_created_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.commission_management
    ADD CONSTRAINT commission_management_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.user_managment(id);
 e   ALTER TABLE ONLY public.commission_management DROP CONSTRAINT commission_management_created_by_fkey;
       public          postgres    false    301    209    3279            �           2606    223018 <   commission_management commission_management_modified_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.commission_management
    ADD CONSTRAINT commission_management_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.user_managment(id);
 f   ALTER TABLE ONLY public.commission_management DROP CONSTRAINT commission_management_modified_by_fkey;
       public          postgres    false    3279    301    209            �           2606    223008 @   commission_management commission_management_product_type_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.commission_management
    ADD CONSTRAINT commission_management_product_type_id_fkey FOREIGN KEY (product_type_id) REFERENCES public.product_type_details(product_type_id);
 j   ALTER TABLE ONLY public.commission_management DROP CONSTRAINT commission_management_product_type_id_fkey;
       public          postgres    false    270    3339    301            �           2606    222458 7   commission_management commission_management_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.commission_management
    ADD CONSTRAINT commission_management_status_fkey FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 a   ALTER TABLE ONLY public.commission_management DROP CONSTRAINT commission_management_status_fkey;
       public          postgres    false    301    3303    234            o           2606    222358 7   configuration_details configuration_details_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.configuration_details
    ADD CONSTRAINT configuration_details_status_fkey FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 a   ALTER TABLE ONLY public.configuration_details DROP CONSTRAINT configuration_details_status_fkey;
       public          postgres    false    3303    234    230            y           2606    222618 /   country_details country_details_created_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.country_details
    ADD CONSTRAINT country_details_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.user_managment(id);
 Y   ALTER TABLE ONLY public.country_details DROP CONSTRAINT country_details_created_by_fkey;
       public          postgres    false    209    258    3279            z           2606    222623 0   country_details country_details_modified_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.country_details
    ADD CONSTRAINT country_details_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.user_managment(id);
 Z   ALTER TABLE ONLY public.country_details DROP CONSTRAINT country_details_modified_by_fkey;
       public          postgres    false    209    258    3279            x           2606    222373 +   country_details country_details_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.country_details
    ADD CONSTRAINT country_details_status_fkey FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 U   ALTER TABLE ONLY public.country_details DROP CONSTRAINT country_details_status_fkey;
       public          postgres    false    3303    234    258            v           2606    222608 1   currency_details currency_details_created_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.currency_details
    ADD CONSTRAINT currency_details_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.user_managment(id);
 [   ALTER TABLE ONLY public.currency_details DROP CONSTRAINT currency_details_created_by_fkey;
       public          postgres    false    209    256    3279            w           2606    222613 2   currency_details currency_details_modified_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.currency_details
    ADD CONSTRAINT currency_details_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.user_managment(id);
 \   ALTER TABLE ONLY public.currency_details DROP CONSTRAINT currency_details_modified_by_fkey;
       public          postgres    false    209    256    3279            u           2606    222368 -   currency_details currency_details_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.currency_details
    ADD CONSTRAINT currency_details_status_fkey FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 W   ALTER TABLE ONLY public.currency_details DROP CONSTRAINT currency_details_status_fkey;
       public          postgres    false    234    3303    256            "           2606    223083 -   email_template email_template_created_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.email_template
    ADD CONSTRAINT email_template_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.super_admin(id);
 W   ALTER TABLE ONLY public.email_template DROP CONSTRAINT email_template_created_by_fkey;
       public          postgres    false    3273    203    347            #           2606    223088 .   email_template email_template_modified_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.email_template
    ADD CONSTRAINT email_template_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.super_admin(id);
 X   ALTER TABLE ONLY public.email_template DROP CONSTRAINT email_template_modified_by_fkey;
       public          postgres    false    347    203    3273            !           2606    222558 )   email_template email_template_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.email_template
    ADD CONSTRAINT email_template_status_fkey FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 S   ALTER TABLE ONLY public.email_template DROP CONSTRAINT email_template_status_fkey;
       public          postgres    false    347    234    3303            �           2606    223058 -   group_management group_management_car_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.group_management
    ADD CONSTRAINT group_management_car_id_fkey FOREIGN KEY (car_id) REFERENCES public.cars(car_id);
 W   ALTER TABLE ONLY public.group_management DROP CONSTRAINT group_management_car_id_fkey;
       public          postgres    false    291    3357    289            �           2606    222728 1   group_management group_management_created_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.group_management
    ADD CONSTRAINT group_management_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.user_managment(id);
 [   ALTER TABLE ONLY public.group_management DROP CONSTRAINT group_management_created_by_fkey;
       public          postgres    false    209    291    3279            �           2606    222733 2   group_management group_management_modified_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.group_management
    ADD CONSTRAINT group_management_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.user_managment(id);
 \   ALTER TABLE ONLY public.group_management DROP CONSTRAINT group_management_modified_by_fkey;
       public          postgres    false    209    291    3279            �           2606    222433 -   group_management group_management_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.group_management
    ADD CONSTRAINT group_management_status_fkey FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 W   ALTER TABLE ONLY public.group_management DROP CONSTRAINT group_management_status_fkey;
       public          postgres    false    291    234    3303                       2606    222913 /   help_management help_management_created_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.help_management
    ADD CONSTRAINT help_management_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.user_managment(id);
 Y   ALTER TABLE ONLY public.help_management DROP CONSTRAINT help_management_created_by_fkey;
       public          postgres    false    209    341    3279                       2606    222918 0   help_management help_management_modified_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.help_management
    ADD CONSTRAINT help_management_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.user_managment(id);
 Z   ALTER TABLE ONLY public.help_management DROP CONSTRAINT help_management_modified_by_fkey;
       public          postgres    false    3279    209    341                       2606    223278 .   help_management help_management_module_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.help_management
    ADD CONSTRAINT help_management_module_id_fkey FOREIGN KEY (module_id) REFERENCES public.module(module_id);
 X   ALTER TABLE ONLY public.help_management DROP CONSTRAINT help_management_module_id_fkey;
       public          postgres    false    341    3287    218                       2606    222543 +   help_management help_management_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.help_management
    ADD CONSTRAINT help_management_status_fkey FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 U   ALTER TABLE ONLY public.help_management DROP CONSTRAINT help_management_status_fkey;
       public          postgres    false    234    341    3303            [           2606    222898 (   intermediator intermediator_country_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.intermediator
    ADD CONSTRAINT intermediator_country_fkey FOREIGN KEY (country) REFERENCES public.country_details(country_id);
 R   ALTER TABLE ONLY public.intermediator DROP CONSTRAINT intermediator_country_fkey;
       public          postgres    false    3327    205    258            Z           2606    222403 '   intermediator intermediator_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.intermediator
    ADD CONSTRAINT intermediator_status_fkey FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 Q   ALTER TABLE ONLY public.intermediator DROP CONSTRAINT intermediator_status_fkey;
       public          postgres    false    3303    205    234            \           2606    223348 (   intermediator intermediator_user_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.intermediator
    ADD CONSTRAINT intermediator_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.user_managment(id);
 R   ALTER TABLE ONLY public.intermediator DROP CONSTRAINT intermediator_user_id_fkey;
       public          postgres    false    3279    209    205                       2606    222923 ;   inventory_manangement inventory_manangement_created_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.inventory_manangement
    ADD CONSTRAINT inventory_manangement_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.user_managment(id);
 e   ALTER TABLE ONLY public.inventory_manangement DROP CONSTRAINT inventory_manangement_created_by_fkey;
       public          postgres    false    339    209    3279                       2606    222928 <   inventory_manangement inventory_manangement_modified_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.inventory_manangement
    ADD CONSTRAINT inventory_manangement_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.user_managment(id);
 f   ALTER TABLE ONLY public.inventory_manangement DROP CONSTRAINT inventory_manangement_modified_by_fkey;
       public          postgres    false    339    209    3279                       2606    222943 :   inventory_manangement inventory_manangement_seller_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.inventory_manangement
    ADD CONSTRAINT inventory_manangement_seller_id_fkey FOREIGN KEY (seller_id) REFERENCES public.new_seller_details(id);
 d   ALTER TABLE ONLY public.inventory_manangement DROP CONSTRAINT inventory_manangement_seller_id_fkey;
       public          postgres    false    3347    278    339                       2606    222538 7   inventory_manangement inventory_manangement_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.inventory_manangement
    ADD CONSTRAINT inventory_manangement_status_fkey FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 a   ALTER TABLE ONLY public.inventory_manangement DROP CONSTRAINT inventory_manangement_status_fkey;
       public          postgres    false    234    339    3303                       2606    222593 =   inventory_manangement inventory_manangement_warehouse_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.inventory_manangement
    ADD CONSTRAINT inventory_manangement_warehouse_id_fkey FOREIGN KEY (warehouse_id) REFERENCES public.seller_warehouse(warehouse_id);
 g   ALTER TABLE ONLY public.inventory_manangement DROP CONSTRAINT inventory_manangement_warehouse_id_fkey;
       public          postgres    false    339    281    3349            �           2606    222678    models models_created_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.models
    ADD CONSTRAINT models_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.user_managment(id);
 G   ALTER TABLE ONLY public.models DROP CONSTRAINT models_created_by_fkey;
       public          postgres    false    3279    285    209            �           2606    222683    models models_modified_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.models
    ADD CONSTRAINT models_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.user_managment(id);
 H   ALTER TABLE ONLY public.models DROP CONSTRAINT models_modified_by_fkey;
       public          postgres    false    209    285    3279            �           2606    222418    models models_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.models
    ADD CONSTRAINT models_status_fkey FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 C   ALTER TABLE ONLY public.models DROP CONSTRAINT models_status_fkey;
       public          postgres    false    3303    285    234            c           2606    222568    module module_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.module
    ADD CONSTRAINT module_status_fkey FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 C   ALTER TABLE ONLY public.module DROP CONSTRAINT module_status_fkey;
       public          postgres    false    234    218    3303            �           2606    222773 6   new_buyer_details new_buyer_details_bank_currency_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.new_buyer_details
    ADD CONSTRAINT new_buyer_details_bank_currency_fkey FOREIGN KEY (bank_currency) REFERENCES public.currency_details(currency_id);
 `   ALTER TABLE ONLY public.new_buyer_details DROP CONSTRAINT new_buyer_details_bank_currency_fkey;
       public          postgres    false    3325    256    276            �           2606    222818 8   new_buyer_details new_buyer_details_billing_country_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.new_buyer_details
    ADD CONSTRAINT new_buyer_details_billing_country_fkey FOREIGN KEY (billing_country) REFERENCES public.country_details(country_id);
 b   ALTER TABLE ONLY public.new_buyer_details DROP CONSTRAINT new_buyer_details_billing_country_fkey;
       public          postgres    false    258    276    3327            �           2606    222813 6   new_buyer_details new_buyer_details_billing_state_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.new_buyer_details
    ADD CONSTRAINT new_buyer_details_billing_state_fkey FOREIGN KEY (billing_state) REFERENCES public.state_details(id);
 `   ALTER TABLE ONLY public.new_buyer_details DROP CONSTRAINT new_buyer_details_billing_state_fkey;
       public          postgres    false    276    3329    260            �           2606    222778 6   new_buyer_details new_buyer_details_cancel_reason_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.new_buyer_details
    ADD CONSTRAINT new_buyer_details_cancel_reason_fkey FOREIGN KEY (cancel_reason) REFERENCES public.reason_details(reason_id);
 `   ALTER TABLE ONLY public.new_buyer_details DROP CONSTRAINT new_buyer_details_cancel_reason_fkey;
       public          postgres    false    276    254    3323            �           2606    222768 3   new_buyer_details new_buyer_details_created_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.new_buyer_details
    ADD CONSTRAINT new_buyer_details_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.user_managment(id);
 ]   ALTER TABLE ONLY public.new_buyer_details DROP CONSTRAINT new_buyer_details_created_by_fkey;
       public          postgres    false    3279    276    209            �           2606    222783 9   new_buyer_details new_buyer_details_iv_country_issue_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.new_buyer_details
    ADD CONSTRAINT new_buyer_details_iv_country_issue_fkey FOREIGN KEY (iv_country_issue) REFERENCES public.country_details(country_id);
 c   ALTER TABLE ONLY public.new_buyer_details DROP CONSTRAINT new_buyer_details_iv_country_issue_fkey;
       public          postgres    false    258    276    3327            �           2606    222788 4   new_buyer_details new_buyer_details_iv_location_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.new_buyer_details
    ADD CONSTRAINT new_buyer_details_iv_location_fkey FOREIGN KEY (iv_location) REFERENCES public.country_details(country_id);
 ^   ALTER TABLE ONLY public.new_buyer_details DROP CONSTRAINT new_buyer_details_iv_location_fkey;
       public          postgres    false    3327    276    258            �           2606    222763 4   new_buyer_details new_buyer_details_modified_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.new_buyer_details
    ADD CONSTRAINT new_buyer_details_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.user_managment(id);
 ^   ALTER TABLE ONLY public.new_buyer_details DROP CONSTRAINT new_buyer_details_modified_by_fkey;
       public          postgres    false    3279    276    209            �           2606    222798 8   new_buyer_details new_buyer_details_product_type_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.new_buyer_details
    ADD CONSTRAINT new_buyer_details_product_type_id_fkey FOREIGN KEY (product_type_id) REFERENCES public.product_type_details(product_type_id);
 b   ALTER TABLE ONLY public.new_buyer_details DROP CONSTRAINT new_buyer_details_product_type_id_fkey;
       public          postgres    false    270    276    3339            �           2606    222803 9   new_buyer_details new_buyer_details_shipping_country_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.new_buyer_details
    ADD CONSTRAINT new_buyer_details_shipping_country_fkey FOREIGN KEY (shipping_country) REFERENCES public.country_details(country_id);
 c   ALTER TABLE ONLY public.new_buyer_details DROP CONSTRAINT new_buyer_details_shipping_country_fkey;
       public          postgres    false    258    3327    276            �           2606    222808 7   new_buyer_details new_buyer_details_shipping_state_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.new_buyer_details
    ADD CONSTRAINT new_buyer_details_shipping_state_fkey FOREIGN KEY (shipping_state) REFERENCES public.state_details(id);
 a   ALTER TABLE ONLY public.new_buyer_details DROP CONSTRAINT new_buyer_details_shipping_state_fkey;
       public          postgres    false    3329    260    276            �           2606    222823 8   new_buyer_details new_buyer_details_subscription_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.new_buyer_details
    ADD CONSTRAINT new_buyer_details_subscription_id_fkey FOREIGN KEY (subscription_id) REFERENCES public.subscription_plan_details(subscription_plan_id);
 b   ALTER TABLE ONLY public.new_buyer_details DROP CONSTRAINT new_buyer_details_subscription_id_fkey;
       public          postgres    false    3295    226    276            �           2606    222793 8   new_buyer_details new_buyer_details_trading_type_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.new_buyer_details
    ADD CONSTRAINT new_buyer_details_trading_type_id_fkey FOREIGN KEY (trading_type_id) REFERENCES public.trading_type_details(trading_type_id);
 b   ALTER TABLE ONLY public.new_buyer_details DROP CONSTRAINT new_buyer_details_trading_type_id_fkey;
       public          postgres    false    276    272    3341            �           2606    223343 0   new_buyer_details new_buyer_details_user_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.new_buyer_details
    ADD CONSTRAINT new_buyer_details_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.user_managment(id);
 Z   ALTER TABLE ONLY public.new_buyer_details DROP CONSTRAINT new_buyer_details_user_id_fkey;
       public          postgres    false    276    3279    209            �           2606    222883 8   new_seller_details new_seller_details_bank_currency_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.new_seller_details
    ADD CONSTRAINT new_seller_details_bank_currency_fkey FOREIGN KEY (bank_currency) REFERENCES public.currency_details(currency_id);
 b   ALTER TABLE ONLY public.new_seller_details DROP CONSTRAINT new_seller_details_bank_currency_fkey;
       public          postgres    false    256    278    3325            �           2606    222838 :   new_seller_details new_seller_details_billing_country_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.new_seller_details
    ADD CONSTRAINT new_seller_details_billing_country_fkey FOREIGN KEY (billing_country) REFERENCES public.country_details(country_id);
 d   ALTER TABLE ONLY public.new_seller_details DROP CONSTRAINT new_seller_details_billing_country_fkey;
       public          postgres    false    258    278    3327            �           2606    222843 8   new_seller_details new_seller_details_billing_state_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.new_seller_details
    ADD CONSTRAINT new_seller_details_billing_state_fkey FOREIGN KEY (billing_state) REFERENCES public.state_details(id);
 b   ALTER TABLE ONLY public.new_seller_details DROP CONSTRAINT new_seller_details_billing_state_fkey;
       public          postgres    false    260    278    3329            �           2606    222878 6   new_seller_details new_seller_details_fail_reason_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.new_seller_details
    ADD CONSTRAINT new_seller_details_fail_reason_fkey FOREIGN KEY (fail_reason) REFERENCES public.reason_details(reason_id);
 `   ALTER TABLE ONLY public.new_seller_details DROP CONSTRAINT new_seller_details_fail_reason_fkey;
       public          postgres    false    278    3323    254            �           2606    222873 ;   new_seller_details new_seller_details_iv_country_issue_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.new_seller_details
    ADD CONSTRAINT new_seller_details_iv_country_issue_fkey FOREIGN KEY (iv_country_issue) REFERENCES public.country_details(country_id);
 e   ALTER TABLE ONLY public.new_seller_details DROP CONSTRAINT new_seller_details_iv_country_issue_fkey;
       public          postgres    false    3327    258    278            �           2606    222868 6   new_seller_details new_seller_details_iv_location_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.new_seller_details
    ADD CONSTRAINT new_seller_details_iv_location_fkey FOREIGN KEY (iv_location) REFERENCES public.country_details(country_id);
 `   ALTER TABLE ONLY public.new_seller_details DROP CONSTRAINT new_seller_details_iv_location_fkey;
       public          postgres    false    278    3327    258            �           2606    222833 9   new_seller_details new_seller_details_payout_shed_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.new_seller_details
    ADD CONSTRAINT new_seller_details_payout_shed_id_fkey FOREIGN KEY (payout_shed_id) REFERENCES public.payout_shed_details(payout_shed_id);
 c   ALTER TABLE ONLY public.new_seller_details DROP CONSTRAINT new_seller_details_payout_shed_id_fkey;
       public          postgres    false    278    228    3297            �           2606    222858 :   new_seller_details new_seller_details_product_type_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.new_seller_details
    ADD CONSTRAINT new_seller_details_product_type_id_fkey FOREIGN KEY (product_type_id) REFERENCES public.product_type_details(product_type_id);
 d   ALTER TABLE ONLY public.new_seller_details DROP CONSTRAINT new_seller_details_product_type_id_fkey;
       public          postgres    false    3339    278    270            �           2606    222848 ;   new_seller_details new_seller_details_shipping_country_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.new_seller_details
    ADD CONSTRAINT new_seller_details_shipping_country_fkey FOREIGN KEY (shipping_country) REFERENCES public.country_details(country_id);
 e   ALTER TABLE ONLY public.new_seller_details DROP CONSTRAINT new_seller_details_shipping_country_fkey;
       public          postgres    false    278    3327    258            �           2606    222853 9   new_seller_details new_seller_details_shipping_state_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.new_seller_details
    ADD CONSTRAINT new_seller_details_shipping_state_fkey FOREIGN KEY (shipping_state) REFERENCES public.state_details(id);
 c   ALTER TABLE ONLY public.new_seller_details DROP CONSTRAINT new_seller_details_shipping_state_fkey;
       public          postgres    false    3329    278    260            �           2606    222828 :   new_seller_details new_seller_details_subscription_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.new_seller_details
    ADD CONSTRAINT new_seller_details_subscription_id_fkey FOREIGN KEY (subscription_id) REFERENCES public.subscription_plan_details(subscription_plan_id);
 d   ALTER TABLE ONLY public.new_seller_details DROP CONSTRAINT new_seller_details_subscription_id_fkey;
       public          postgres    false    226    278    3295            �           2606    222863 :   new_seller_details new_seller_details_trading_type_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.new_seller_details
    ADD CONSTRAINT new_seller_details_trading_type_id_fkey FOREIGN KEY (trading_type_id) REFERENCES public.trading_type_details(trading_type_id);
 d   ALTER TABLE ONLY public.new_seller_details DROP CONSTRAINT new_seller_details_trading_type_id_fkey;
       public          postgres    false    278    3341    272            �           2606    223338 2   new_seller_details new_seller_details_user_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.new_seller_details
    ADD CONSTRAINT new_seller_details_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.user_managment(id);
 \   ALTER TABLE ONLY public.new_seller_details DROP CONSTRAINT new_seller_details_user_id_fkey;
       public          postgres    false    278    209    3279                       2606    223023 ;   newsletter_management newsletter_management_created_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.newsletter_management
    ADD CONSTRAINT newsletter_management_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.user_managment(id);
 e   ALTER TABLE ONLY public.newsletter_management DROP CONSTRAINT newsletter_management_created_by_fkey;
       public          postgres    false    329    3279    209                       2606    223028 <   newsletter_management newsletter_management_modified_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.newsletter_management
    ADD CONSTRAINT newsletter_management_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.user_managment(id);
 f   ALTER TABLE ONLY public.newsletter_management DROP CONSTRAINT newsletter_management_modified_by_fkey;
       public          postgres    false    329    209    3279                       2606    222508 7   newsletter_management newsletter_management_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.newsletter_management
    ADD CONSTRAINT newsletter_management_status_fkey FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 a   ALTER TABLE ONLY public.newsletter_management DROP CONSTRAINT newsletter_management_status_fkey;
       public          postgres    false    329    234    3303                       2606    223268 -   notification notification_email_template_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.notification
    ADD CONSTRAINT notification_email_template_fkey FOREIGN KEY (email_template) REFERENCES public.email_template(id);
 W   ALTER TABLE ONLY public.notification DROP CONSTRAINT notification_email_template_fkey;
       public          postgres    false    3415    347    331                       2606    223263 (   notification notification_module_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.notification
    ADD CONSTRAINT notification_module_id_fkey FOREIGN KEY (module_id) REFERENCES public.module(module_id);
 R   ALTER TABLE ONLY public.notification DROP CONSTRAINT notification_module_id_fkey;
       public          postgres    false    3287    218    331            	           2606    223273 +   notification notification_sms_template_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.notification
    ADD CONSTRAINT notification_sms_template_fkey FOREIGN KEY (sms_template) REFERENCES public.sms_template(id);
 U   ALTER TABLE ONLY public.notification DROP CONSTRAINT notification_sms_template_fkey;
       public          postgres    false    349    331    3417                       2606    222513 %   notification notification_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.notification
    ADD CONSTRAINT notification_status_fkey FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 O   ALTER TABLE ONLY public.notification DROP CONSTRAINT notification_status_fkey;
       public          postgres    false    3303    234    331                       2606    223328 /   offer_management offer_management_buyer_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.offer_management
    ADD CONSTRAINT offer_management_buyer_id_fkey FOREIGN KEY (buyer_id) REFERENCES public.new_buyer_details(id);
 Y   ALTER TABLE ONLY public.offer_management DROP CONSTRAINT offer_management_buyer_id_fkey;
       public          postgres    false    3345    337    276                       2606    222933 1   offer_management offer_management_created_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.offer_management
    ADD CONSTRAINT offer_management_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.user_managment(id);
 [   ALTER TABLE ONLY public.offer_management DROP CONSTRAINT offer_management_created_by_fkey;
       public          postgres    false    337    209    3279                       2606    222938 2   offer_management offer_management_modified_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.offer_management
    ADD CONSTRAINT offer_management_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.user_managment(id);
 \   ALTER TABLE ONLY public.offer_management DROP CONSTRAINT offer_management_modified_by_fkey;
       public          postgres    false    3279    209    337                       2606    223333 0   offer_management offer_management_seller_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.offer_management
    ADD CONSTRAINT offer_management_seller_id_fkey FOREIGN KEY (seller_id) REFERENCES public.new_seller_details(id);
 Z   ALTER TABLE ONLY public.offer_management DROP CONSTRAINT offer_management_seller_id_fkey;
       public          postgres    false    278    3347    337                       2606    222528 -   offer_management offer_management_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.offer_management
    ADD CONSTRAINT offer_management_status_fkey FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 W   ALTER TABLE ONLY public.offer_management DROP CONSTRAINT offer_management_status_fkey;
       public          postgres    false    337    234    3303            �           2606    223308 /   order_management order_management_buyer_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.order_management
    ADD CONSTRAINT order_management_buyer_id_fkey FOREIGN KEY (buyer_id) REFERENCES public.new_buyer_details(id);
 Y   ALTER TABLE ONLY public.order_management DROP CONSTRAINT order_management_buyer_id_fkey;
       public          postgres    false    3345    303    276            �           2606    223108 1   order_management order_management_created_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.order_management
    ADD CONSTRAINT order_management_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.user_managment(id);
 [   ALTER TABLE ONLY public.order_management DROP CONSTRAINT order_management_created_by_fkey;
       public          postgres    false    303    3279    209            �           2606    223323 8   order_management order_management_method_of_payment_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.order_management
    ADD CONSTRAINT order_management_method_of_payment_fkey FOREIGN KEY (method_of_payment) REFERENCES public.payment_type_details(payment_type_id);
 b   ALTER TABLE ONLY public.order_management DROP CONSTRAINT order_management_method_of_payment_fkey;
       public          postgres    false    303    274    3343            �           2606    223103 2   order_management order_management_modified_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.order_management
    ADD CONSTRAINT order_management_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.user_managment(id);
 \   ALTER TABLE ONLY public.order_management DROP CONSTRAINT order_management_modified_by_fkey;
       public          postgres    false    303    209    3279            �           2606    223113 6   order_management order_management_order_status_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.order_management
    ADD CONSTRAINT order_management_order_status_id_fkey FOREIGN KEY (order_status_id) REFERENCES public.order_status(order_status_id);
 `   ALTER TABLE ONLY public.order_management DROP CONSTRAINT order_management_order_status_id_fkey;
       public          postgres    false    303    3309    240            �           2606    223313 1   order_management order_management_order_type_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.order_management
    ADD CONSTRAINT order_management_order_type_fkey FOREIGN KEY (order_type) REFERENCES public.order_type_details(order_type_id);
 [   ALTER TABLE ONLY public.order_management DROP CONSTRAINT order_management_order_type_fkey;
       public          postgres    false    303    236    3305            �           2606    223118 5   order_management order_management_ship_status_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.order_management
    ADD CONSTRAINT order_management_ship_status_id_fkey FOREIGN KEY (ship_status_id) REFERENCES public.shipment_status(ship_status_id);
 _   ALTER TABLE ONLY public.order_management DROP CONSTRAINT order_management_ship_status_id_fkey;
       public          postgres    false    303    3307    238            �           2606    223298 .   order_management order_management_user_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.order_management
    ADD CONSTRAINT order_management_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.new_seller_details(id);
 X   ALTER TABLE ONLY public.order_management DROP CONSTRAINT order_management_user_id_fkey;
       public          postgres    false    3347    303    278            �           2606    223163 3   order_quote_Manage order_quote_Manage_buyer_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public."order_quote_Manage"
    ADD CONSTRAINT "order_quote_Manage_buyer_id_fkey" FOREIGN KEY (buyer_id) REFERENCES public.new_buyer_details(id);
 a   ALTER TABLE ONLY public."order_quote_Manage" DROP CONSTRAINT "order_quote_Manage_buyer_id_fkey";
       public          postgres    false    307    276    3345            �           2606    223238 4   order_quote_Manage order_quote_Manage_buyer_id_fkey1    FK CONSTRAINT     �   ALTER TABLE ONLY public."order_quote_Manage"
    ADD CONSTRAINT "order_quote_Manage_buyer_id_fkey1" FOREIGN KEY (buyer_id) REFERENCES public.application_user(application_user_id);
 b   ALTER TABLE ONLY public."order_quote_Manage" DROP CONSTRAINT "order_quote_Manage_buyer_id_fkey1";
       public          postgres    false    3277    207    307            �           2606    223243 <   order_quote_Manage order_quote_Manage_method_of_payment_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public."order_quote_Manage"
    ADD CONSTRAINT "order_quote_Manage_method_of_payment_fkey" FOREIGN KEY (method_of_payment) REFERENCES public.payment_type_details(payment_type_id);
 j   ALTER TABLE ONLY public."order_quote_Manage" DROP CONSTRAINT "order_quote_Manage_method_of_payment_fkey";
       public          postgres    false    274    307    3343            �           2606    223158 :   order_quote_Manage order_quote_Manage_order_status_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public."order_quote_Manage"
    ADD CONSTRAINT "order_quote_Manage_order_status_id_fkey" FOREIGN KEY (order_status_id) REFERENCES public.order_status(order_status_id);
 h   ALTER TABLE ONLY public."order_quote_Manage" DROP CONSTRAINT "order_quote_Manage_order_status_id_fkey";
       public          postgres    false    307    240    3309            �           2606    223318 5   order_quote_Manage order_quote_Manage_order_type_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public."order_quote_Manage"
    ADD CONSTRAINT "order_quote_Manage_order_type_fkey" FOREIGN KEY (order_type) REFERENCES public.order_type_details(order_type_id);
 c   ALTER TABLE ONLY public."order_quote_Manage" DROP CONSTRAINT "order_quote_Manage_order_type_fkey";
       public          postgres    false    307    3305    236            �           2606    223168 2   order_quote_Manage order_quote_Manage_user_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public."order_quote_Manage"
    ADD CONSTRAINT "order_quote_Manage_user_id_fkey" FOREIGN KEY (user_id) REFERENCES public.user_managment(id);
 `   ALTER TABLE ONLY public."order_quote_Manage" DROP CONSTRAINT "order_quote_Manage_user_id_fkey";
       public          postgres    false    307    209    3279            �           2606    222638 %   parameters parameters_created_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.parameters
    ADD CONSTRAINT parameters_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.user_managment(id);
 O   ALTER TABLE ONLY public.parameters DROP CONSTRAINT parameters_created_by_fkey;
       public          postgres    false    3279    287    209            �           2606    222643 &   parameters parameters_modified_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.parameters
    ADD CONSTRAINT parameters_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.user_managment(id);
 P   ALTER TABLE ONLY public.parameters DROP CONSTRAINT parameters_modified_by_fkey;
       public          postgres    false    209    287    3279            �           2606    222423 !   parameters parameters_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.parameters
    ADD CONSTRAINT parameters_status_fkey FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 K   ALTER TABLE ONLY public.parameters DROP CONSTRAINT parameters_status_fkey;
       public          postgres    false    287    3303    234                       2606    222523 5   part_request_unavail part_request_unavail_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.part_request_unavail
    ADD CONSTRAINT part_request_unavail_status_fkey FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 _   ALTER TABLE ONLY public.part_request_unavail DROP CONSTRAINT part_request_unavail_status_fkey;
       public          postgres    false    335    3303    234            �           2606    222688 1   parts_management parts_management_created_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.parts_management
    ADD CONSTRAINT parts_management_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.user_managment(id);
 [   ALTER TABLE ONLY public.parts_management DROP CONSTRAINT parts_management_created_by_fkey;
       public          postgres    false    209    297    3279            �           2606    222693 2   parts_management parts_management_modified_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.parts_management
    ADD CONSTRAINT parts_management_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.user_managment(id);
 \   ALTER TABLE ONLY public.parts_management DROP CONSTRAINT parts_management_modified_by_fkey;
       public          postgres    false    209    297    3279            �           2606    222448 -   parts_management parts_management_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.parts_management
    ADD CONSTRAINT parts_management_status_fkey FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 W   ALTER TABLE ONLY public.parts_management DROP CONSTRAINT parts_management_status_fkey;
       public          postgres    false    297    234    3303            �           2606    222903 G   pay_out_schedule_management pay_out_schedule_management_created_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.pay_out_schedule_management
    ADD CONSTRAINT pay_out_schedule_management_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.user_managment(id);
 q   ALTER TABLE ONLY public.pay_out_schedule_management DROP CONSTRAINT pay_out_schedule_management_created_by_fkey;
       public          postgres    false    209    313    3279            �           2606    222908 H   pay_out_schedule_management pay_out_schedule_management_modified_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.pay_out_schedule_management
    ADD CONSTRAINT pay_out_schedule_management_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.user_managment(id);
 r   ALTER TABLE ONLY public.pay_out_schedule_management DROP CONSTRAINT pay_out_schedule_management_modified_by_fkey;
       public          postgres    false    209    313    3279            �           2606    223198 E   pay_out_schedule_management pay_out_schedule_management_order_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.pay_out_schedule_management
    ADD CONSTRAINT pay_out_schedule_management_order_id_fkey FOREIGN KEY (order_id) REFERENCES public.order_management(order_id);
 o   ALTER TABLE ONLY public.pay_out_schedule_management DROP CONSTRAINT pay_out_schedule_management_order_id_fkey;
       public          postgres    false    313    303    3371            �           2606    222468 C   pay_out_schedule_management pay_out_schedule_management_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.pay_out_schedule_management
    ADD CONSTRAINT pay_out_schedule_management_status_fkey FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 m   ALTER TABLE ONLY public.pay_out_schedule_management DROP CONSTRAINT pay_out_schedule_management_status_fkey;
       public          postgres    false    3303    234    313            �           2606    223203 I   pay_out_schedule_management pay_out_schedule_management_sub_order_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.pay_out_schedule_management
    ADD CONSTRAINT pay_out_schedule_management_sub_order_id_fkey FOREIGN KEY (sub_order_id) REFERENCES public.sub_order_details(id);
 s   ALTER TABLE ONLY public.pay_out_schedule_management DROP CONSTRAINT pay_out_schedule_management_sub_order_id_fkey;
       public          postgres    false    313    305    3373            �           2606    222978 5   payment_management payment_management_created_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.payment_management
    ADD CONSTRAINT payment_management_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.user_managment(id);
 _   ALTER TABLE ONLY public.payment_management DROP CONSTRAINT payment_management_created_by_fkey;
       public          postgres    false    315    3279    209            �           2606    222983 6   payment_management payment_management_modified_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.payment_management
    ADD CONSTRAINT payment_management_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.user_managment(id);
 `   ALTER TABLE ONLY public.payment_management DROP CONSTRAINT payment_management_modified_by_fkey;
       public          postgres    false    3279    209    315            �           2606    222993 3   payment_management payment_management_order_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.payment_management
    ADD CONSTRAINT payment_management_order_id_fkey FOREIGN KEY (order_id) REFERENCES public.order_management(order_id);
 ]   ALTER TABLE ONLY public.payment_management DROP CONSTRAINT payment_management_order_id_fkey;
       public          postgres    false    3371    303    315            �           2606    223003 <   payment_management payment_management_payment_status_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.payment_management
    ADD CONSTRAINT payment_management_payment_status_id_fkey FOREIGN KEY (payment_status_id) REFERENCES public.payment_status(payment_status_id);
 f   ALTER TABLE ONLY public.payment_management DROP CONSTRAINT payment_management_payment_status_id_fkey;
       public          postgres    false    3321    252    315            �           2606    222988 4   payment_management payment_management_seller_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.payment_management
    ADD CONSTRAINT payment_management_seller_id_fkey FOREIGN KEY (seller_id) REFERENCES public.new_seller_details(id);
 ^   ALTER TABLE ONLY public.payment_management DROP CONSTRAINT payment_management_seller_id_fkey;
       public          postgres    false    3347    278    315            �           2606    222473 1   payment_management payment_management_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.payment_management
    ADD CONSTRAINT payment_management_status_fkey FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 [   ALTER TABLE ONLY public.payment_management DROP CONSTRAINT payment_management_status_fkey;
       public          postgres    false    234    315    3303            �           2606    222998 7   payment_management payment_management_sub_order_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.payment_management
    ADD CONSTRAINT payment_management_sub_order_id_fkey FOREIGN KEY (sub_order_id) REFERENCES public.sub_order_details(id);
 a   ALTER TABLE ONLY public.payment_management DROP CONSTRAINT payment_management_sub_order_id_fkey;
       public          postgres    false    3373    305    315            �           2606    222398 5   payment_type_details payment_type_details_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.payment_type_details
    ADD CONSTRAINT payment_type_details_status_fkey FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 _   ALTER TABLE ONLY public.payment_type_details DROP CONSTRAINT payment_type_details_status_fkey;
       public          postgres    false    274    3303    234            k           2606    222583 7   payout_shed_details payout_shed_details_created_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.payout_shed_details
    ADD CONSTRAINT payout_shed_details_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.user_managment(id);
 a   ALTER TABLE ONLY public.payout_shed_details DROP CONSTRAINT payout_shed_details_created_by_fkey;
       public          postgres    false    209    228    3279            l           2606    222588 8   payout_shed_details payout_shed_details_modified_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.payout_shed_details
    ADD CONSTRAINT payout_shed_details_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.user_managment(id);
 b   ALTER TABLE ONLY public.payout_shed_details DROP CONSTRAINT payout_shed_details_modified_by_fkey;
       public          postgres    false    209    228    3279            n           2606    223193 ;   payout_shed_details payout_shed_details_payout_shed_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.payout_shed_details
    ADD CONSTRAINT payout_shed_details_payout_shed_id_fkey FOREIGN KEY (payout_shed_id) REFERENCES public.pay_out_schedule_management(id);
 e   ALTER TABLE ONLY public.payout_shed_details DROP CONSTRAINT payout_shed_details_payout_shed_id_fkey;
       public          postgres    false    228    313    3381            j           2606    222353 3   payout_shed_details payout_shed_details_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.payout_shed_details
    ADD CONSTRAINT payout_shed_details_status_fkey FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 ]   ALTER TABLE ONLY public.payout_shed_details DROP CONSTRAINT payout_shed_details_status_fkey;
       public          postgres    false    234    3303    228            m           2606    222973 C   payout_shed_details payout_shed_details_subscription_detail_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.payout_shed_details
    ADD CONSTRAINT payout_shed_details_subscription_detail_id_fkey FOREIGN KEY (subscription_detail_id) REFERENCES public.subscription_plan_details(subscription_plan_id);
 m   ALTER TABLE ONLY public.payout_shed_details DROP CONSTRAINT payout_shed_details_subscription_detail_id_fkey;
       public          postgres    false    3295    226    228            �           2606    222453 +   product_details product_details_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.product_details
    ADD CONSTRAINT product_details_status_fkey FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 U   ALTER TABLE ONLY public.product_details DROP CONSTRAINT product_details_status_fkey;
       public          postgres    false    3303    234    299            �           2606    222658 9   product_type_details product_type_details_created_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.product_type_details
    ADD CONSTRAINT product_type_details_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.user_managment(id);
 c   ALTER TABLE ONLY public.product_type_details DROP CONSTRAINT product_type_details_created_by_fkey;
       public          postgres    false    209    270    3279            �           2606    222663 :   product_type_details product_type_details_modified_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.product_type_details
    ADD CONSTRAINT product_type_details_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.user_managment(id);
 d   ALTER TABLE ONLY public.product_type_details DROP CONSTRAINT product_type_details_modified_by_fkey;
       public          postgres    false    270    3279    209            �           2606    222388 5   product_type_details product_type_details_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.product_type_details
    ADD CONSTRAINT product_type_details_status_fkey FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 _   ALTER TABLE ONLY public.product_type_details DROP CONSTRAINT product_type_details_status_fkey;
       public          postgres    false    234    3303    270            s           2606    222598 -   reason_details reason_details_created_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.reason_details
    ADD CONSTRAINT reason_details_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.user_managment(id);
 W   ALTER TABLE ONLY public.reason_details DROP CONSTRAINT reason_details_created_by_fkey;
       public          postgres    false    254    209    3279            t           2606    222603 .   reason_details reason_details_modified_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.reason_details
    ADD CONSTRAINT reason_details_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.user_managment(id);
 X   ALTER TABLE ONLY public.reason_details DROP CONSTRAINT reason_details_modified_by_fkey;
       public          postgres    false    3279    254    209            r           2606    222363 )   reason_details reason_details_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.reason_details
    ADD CONSTRAINT reason_details_status_fkey FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 S   ALTER TABLE ONLY public.reason_details DROP CONSTRAINT reason_details_status_fkey;
       public          postgres    false    254    3303    234            a           2606    223258 .   role_permission role_permission_module_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.role_permission
    ADD CONSTRAINT role_permission_module_id_fkey FOREIGN KEY (module_id) REFERENCES public.module(module_id);
 X   ALTER TABLE ONLY public.role_permission DROP CONSTRAINT role_permission_module_id_fkey;
       public          postgres    false    3287    214    218            b           2606    223293 ,   role_permission role_permission_role_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.role_permission
    ADD CONSTRAINT role_permission_role_id_fkey FOREIGN KEY (role_id) REFERENCES public.roles(role_id);
 V   ALTER TABLE ONLY public.role_permission DROP CONSTRAINT role_permission_role_id_fkey;
       public          postgres    false    214    212    3281            _           2606    222738    roles roles_created_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.roles
    ADD CONSTRAINT roles_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.user_managment(id);
 E   ALTER TABLE ONLY public.roles DROP CONSTRAINT roles_created_by_fkey;
       public          postgres    false    209    212    3279            `           2606    222743    roles roles_modified_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.roles
    ADD CONSTRAINT roles_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.user_managment(id);
 F   ALTER TABLE ONLY public.roles DROP CONSTRAINT roles_modified_by_fkey;
       public          postgres    false    212    209    3279            ^           2606    222533    roles roles_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.roles
    ADD CONSTRAINT roles_status_fkey FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 A   ALTER TABLE ONLY public.roles DROP CONSTRAINT roles_status_fkey;
       public          postgres    false    234    212    3303            �           2606    222888 /   seller_branch seller_branch_branch_country_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.seller_branch
    ADD CONSTRAINT seller_branch_branch_country_fkey FOREIGN KEY (branch_country) REFERENCES public.country_details(country_id);
 Y   ALTER TABLE ONLY public.seller_branch DROP CONSTRAINT seller_branch_branch_country_fkey;
       public          postgres    false    258    3327    279            �           2606    222893 -   seller_branch seller_branch_branch_state_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.seller_branch
    ADD CONSTRAINT seller_branch_branch_state_fkey FOREIGN KEY (branch_state) REFERENCES public.state_details(id);
 W   ALTER TABLE ONLY public.seller_branch DROP CONSTRAINT seller_branch_branch_state_fkey;
       public          postgres    false    279    260    3329            %           2606    223073 )   sms_template sms_template_created_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.sms_template
    ADD CONSTRAINT sms_template_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.user_managment(id);
 S   ALTER TABLE ONLY public.sms_template DROP CONSTRAINT sms_template_created_by_fkey;
       public          postgres    false    349    209    3279            &           2606    223078 *   sms_template sms_template_modified_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.sms_template
    ADD CONSTRAINT sms_template_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.user_managment(id);
 T   ALTER TABLE ONLY public.sms_template DROP CONSTRAINT sms_template_modified_by_fkey;
       public          postgres    false    349    3279    209            '           2606    223283 (   sms_template sms_template_module_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.sms_template
    ADD CONSTRAINT sms_template_module_id_fkey FOREIGN KEY (module_id) REFERENCES public.module(module_id);
 R   ALTER TABLE ONLY public.sms_template DROP CONSTRAINT sms_template_module_id_fkey;
       public          postgres    false    218    349    3287            $           2606    222563 %   sms_template sms_template_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.sms_template
    ADD CONSTRAINT sms_template_status_fkey FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 O   ALTER TABLE ONLY public.sms_template DROP CONSTRAINT sms_template_status_fkey;
       public          postgres    false    3303    349    234            ~           2606    222758 +   state_details state_details_country_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.state_details
    ADD CONSTRAINT state_details_country_id_fkey FOREIGN KEY (country_id) REFERENCES public.country_details(country_id);
 U   ALTER TABLE ONLY public.state_details DROP CONSTRAINT state_details_country_id_fkey;
       public          postgres    false    258    260    3327            |           2606    222648 +   state_details state_details_created_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.state_details
    ADD CONSTRAINT state_details_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.user_managment(id);
 U   ALTER TABLE ONLY public.state_details DROP CONSTRAINT state_details_created_by_fkey;
       public          postgres    false    209    260    3279            }           2606    222653 ,   state_details state_details_modified_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.state_details
    ADD CONSTRAINT state_details_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.user_managment(id);
 V   ALTER TABLE ONLY public.state_details DROP CONSTRAINT state_details_modified_by_fkey;
       public          postgres    false    209    260    3279            {           2606    222378 '   state_details state_details_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.state_details
    ADD CONSTRAINT state_details_status_fkey FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 Q   ALTER TABLE ONLY public.state_details DROP CONSTRAINT state_details_status_fkey;
       public          postgres    false    3303    234    260                       2606    222748 '   static_page static_page_created_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.static_page
    ADD CONSTRAINT static_page_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.user_managment(id);
 Q   ALTER TABLE ONLY public.static_page DROP CONSTRAINT static_page_created_by_fkey;
       public          postgres    false    345    209    3279                        2606    222753 (   static_page static_page_modified_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.static_page
    ADD CONSTRAINT static_page_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.user_managment(id);
 R   ALTER TABLE ONLY public.static_page DROP CONSTRAINT static_page_modified_by_fkey;
       public          postgres    false    3279    345    209                       2606    222553 #   static_page static_page_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.static_page
    ADD CONSTRAINT static_page_status_fkey FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 M   ALTER TABLE ONLY public.static_page DROP CONSTRAINT static_page_status_fkey;
       public          postgres    false    3303    345    234            �           2606    222628 9   sub_group_management sub_group_management_created_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.sub_group_management
    ADD CONSTRAINT sub_group_management_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.user_managment(id);
 c   ALTER TABLE ONLY public.sub_group_management DROP CONSTRAINT sub_group_management_created_by_fkey;
       public          postgres    false    3279    293    209            �           2606    223053 7   sub_group_management sub_group_management_group_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.sub_group_management
    ADD CONSTRAINT sub_group_management_group_id_fkey FOREIGN KEY (group_id) REFERENCES public.group_management(group_id);
 a   ALTER TABLE ONLY public.sub_group_management DROP CONSTRAINT sub_group_management_group_id_fkey;
       public          postgres    false    291    3359    293            �           2606    222633 :   sub_group_management sub_group_management_modified_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.sub_group_management
    ADD CONSTRAINT sub_group_management_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.user_managment(id);
 d   ALTER TABLE ONLY public.sub_group_management DROP CONSTRAINT sub_group_management_modified_by_fkey;
       public          postgres    false    293    209    3279            �           2606    222438 5   sub_group_management sub_group_management_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.sub_group_management
    ADD CONSTRAINT sub_group_management_status_fkey FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 _   ALTER TABLE ONLY public.sub_group_management DROP CONSTRAINT sub_group_management_status_fkey;
       public          postgres    false    234    293    3303            �           2606    223063 7   sub_node_management sub_node_management_created_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.sub_node_management
    ADD CONSTRAINT sub_node_management_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.user_managment(id);
 a   ALTER TABLE ONLY public.sub_node_management DROP CONSTRAINT sub_node_management_created_by_fkey;
       public          postgres    false    295    3279    209            �           2606    223068 8   sub_node_management sub_node_management_modified_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.sub_node_management
    ADD CONSTRAINT sub_node_management_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.user_managment(id);
 b   ALTER TABLE ONLY public.sub_node_management DROP CONSTRAINT sub_node_management_modified_by_fkey;
       public          postgres    false    3279    209    295            �           2606    222443 3   sub_node_management sub_node_management_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.sub_node_management
    ADD CONSTRAINT sub_node_management_status_fkey FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 ]   ALTER TABLE ONLY public.sub_node_management DROP CONSTRAINT sub_node_management_status_fkey;
       public          postgres    false    3303    234    295            �           2606    223148 1   sub_order_details sub_order_details_brand_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.sub_order_details
    ADD CONSTRAINT sub_order_details_brand_id_fkey FOREIGN KEY (brand_id) REFERENCES public.brands(brand_id);
 [   ALTER TABLE ONLY public.sub_order_details DROP CONSTRAINT sub_order_details_brand_id_fkey;
       public          postgres    false    305    283    3351            �           2606    223303 1   sub_order_details sub_order_details_order_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.sub_order_details
    ADD CONSTRAINT sub_order_details_order_id_fkey FOREIGN KEY (order_id) REFERENCES public.order_management(order_id);
 [   ALTER TABLE ONLY public.sub_order_details DROP CONSTRAINT sub_order_details_order_id_fkey;
       public          postgres    false    3371    303    305            �           2606    223123 8   sub_order_details sub_order_details_order_status_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.sub_order_details
    ADD CONSTRAINT sub_order_details_order_status_id_fkey FOREIGN KEY (order_status_id) REFERENCES public.order_status(order_status_id);
 b   ALTER TABLE ONLY public.sub_order_details DROP CONSTRAINT sub_order_details_order_status_id_fkey;
       public          postgres    false    3309    240    305            �           2606    223153 3   sub_order_details sub_order_details_product_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.sub_order_details
    ADD CONSTRAINT sub_order_details_product_id_fkey FOREIGN KEY (product_id) REFERENCES public.product_details(product_id);
 ]   ALTER TABLE ONLY public.sub_order_details DROP CONSTRAINT sub_order_details_product_id_fkey;
       public          postgres    false    305    299    3367            �           2606    223128 8   sub_order_details sub_order_details_product_type_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.sub_order_details
    ADD CONSTRAINT sub_order_details_product_type_id_fkey FOREIGN KEY (product_type_id) REFERENCES public.product_type_details(product_type_id);
 b   ALTER TABLE ONLY public.sub_order_details DROP CONSTRAINT sub_order_details_product_type_id_fkey;
       public          postgres    false    305    3339    270            �           2606    223143 2   sub_order_details sub_order_details_seller_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.sub_order_details
    ADD CONSTRAINT sub_order_details_seller_id_fkey FOREIGN KEY (seller_id) REFERENCES public.new_seller_details(id);
 \   ALTER TABLE ONLY public.sub_order_details DROP CONSTRAINT sub_order_details_seller_id_fkey;
       public          postgres    false    305    278    3347            �           2606    223138 7   sub_order_details sub_order_details_ship_status_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.sub_order_details
    ADD CONSTRAINT sub_order_details_ship_status_id_fkey FOREIGN KEY (ship_status_id) REFERENCES public.shipment_status(ship_status_id);
 a   ALTER TABLE ONLY public.sub_order_details DROP CONSTRAINT sub_order_details_ship_status_id_fkey;
       public          postgres    false    238    3307    305            �           2606    223133 8   sub_order_details sub_order_details_trading_type_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.sub_order_details
    ADD CONSTRAINT sub_order_details_trading_type_id_fkey FOREIGN KEY (trading_type_id) REFERENCES public.trading_type_details(trading_type_id);
 b   ALTER TABLE ONLY public.sub_order_details DROP CONSTRAINT sub_order_details_trading_type_id_fkey;
       public          postgres    false    272    3341    305            �           2606    223178 D   sub_order_quote_details sub_order_quote_details_order_status_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.sub_order_quote_details
    ADD CONSTRAINT sub_order_quote_details_order_status_id_fkey FOREIGN KEY (order_status_id) REFERENCES public.order_status(order_status_id);
 n   ALTER TABLE ONLY public.sub_order_quote_details DROP CONSTRAINT sub_order_quote_details_order_status_id_fkey;
       public          postgres    false    309    240    3309            �           2606    223188 ?   sub_order_quote_details sub_order_quote_details_product_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.sub_order_quote_details
    ADD CONSTRAINT sub_order_quote_details_product_id_fkey FOREIGN KEY (product_id) REFERENCES public.product_details(product_id);
 i   ALTER TABLE ONLY public.sub_order_quote_details DROP CONSTRAINT sub_order_quote_details_product_id_fkey;
       public          postgres    false    309    299    3367            �           2606    223248 D   sub_order_quote_details sub_order_quote_details_product_type_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.sub_order_quote_details
    ADD CONSTRAINT sub_order_quote_details_product_type_id_fkey FOREIGN KEY (product_type_id) REFERENCES public.product_type_details(product_type_id);
 n   ALTER TABLE ONLY public.sub_order_quote_details DROP CONSTRAINT sub_order_quote_details_product_type_id_fkey;
       public          postgres    false    309    3339    270            �           2606    223173 >   sub_order_quote_details sub_order_quote_details_seller_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.sub_order_quote_details
    ADD CONSTRAINT sub_order_quote_details_seller_id_fkey FOREIGN KEY (seller_id) REFERENCES public.new_seller_details(id);
 h   ALTER TABLE ONLY public.sub_order_quote_details DROP CONSTRAINT sub_order_quote_details_seller_id_fkey;
       public          postgres    false    309    278    3347            �           2606    223183 C   sub_order_quote_details sub_order_quote_details_ship_status_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.sub_order_quote_details
    ADD CONSTRAINT sub_order_quote_details_ship_status_id_fkey FOREIGN KEY (ship_status_id) REFERENCES public.shipment_status(ship_status_id);
 m   ALTER TABLE ONLY public.sub_order_quote_details DROP CONSTRAINT sub_order_quote_details_ship_status_id_fkey;
       public          postgres    false    309    238    3307            �           2606    223253 D   sub_order_quote_details sub_order_quote_details_trading_type_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.sub_order_quote_details
    ADD CONSTRAINT sub_order_quote_details_trading_type_id_fkey FOREIGN KEY (trading_type_id) REFERENCES public.trading_type_details(trading_type_id);
 n   ALTER TABLE ONLY public.sub_order_quote_details DROP CONSTRAINT sub_order_quote_details_trading_type_id_fkey;
       public          postgres    false    3341    309    272            d           2606    222323 7   subscription_detail subscription_detail_created_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.subscription_detail
    ADD CONSTRAINT subscription_detail_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.user_managment(id);
 a   ALTER TABLE ONLY public.subscription_detail DROP CONSTRAINT subscription_detail_created_by_fkey;
       public          postgres    false    224    3279    209            e           2606    222333 8   subscription_detail subscription_detail_modified_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.subscription_detail
    ADD CONSTRAINT subscription_detail_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.user_managment(id);
 b   ALTER TABLE ONLY public.subscription_detail DROP CONSTRAINT subscription_detail_modified_by_fkey;
       public          postgres    false    3279    224    209            g           2606    222348 C   subscription_detail subscription_detail_subscription_detail_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.subscription_detail
    ADD CONSTRAINT subscription_detail_subscription_detail_id_fkey FOREIGN KEY (subscription_detail_id) REFERENCES public.subscription_plan_details(subscription_plan_id);
 m   ALTER TABLE ONLY public.subscription_detail DROP CONSTRAINT subscription_detail_subscription_detail_id_fkey;
       public          postgres    false    3295    224    226            f           2606    222343 4   subscription_detail subscription_detail_user_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.subscription_detail
    ADD CONSTRAINT subscription_detail_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.user_managment(id);
 ^   ALTER TABLE ONLY public.subscription_detail DROP CONSTRAINT subscription_detail_user_id_fkey;
       public          postgres    false    3279    209    224            h           2606    222328 C   subscription_plan_details subscription_plan_details_created_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.subscription_plan_details
    ADD CONSTRAINT subscription_plan_details_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.user_managment(id);
 m   ALTER TABLE ONLY public.subscription_plan_details DROP CONSTRAINT subscription_plan_details_created_by_fkey;
       public          postgres    false    226    3279    209            i           2606    222338 D   subscription_plan_details subscription_plan_details_modified_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.subscription_plan_details
    ADD CONSTRAINT subscription_plan_details_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.user_managment(id);
 n   ALTER TABLE ONLY public.subscription_plan_details DROP CONSTRAINT subscription_plan_details_modified_by_fkey;
       public          postgres    false    209    226    3279            �           2606    222668 9   trading_type_details trading_type_details_created_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.trading_type_details
    ADD CONSTRAINT trading_type_details_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.user_managment(id);
 c   ALTER TABLE ONLY public.trading_type_details DROP CONSTRAINT trading_type_details_created_by_fkey;
       public          postgres    false    272    209    3279            �           2606    222673 :   trading_type_details trading_type_details_modified_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.trading_type_details
    ADD CONSTRAINT trading_type_details_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.user_managment(id);
 d   ALTER TABLE ONLY public.trading_type_details DROP CONSTRAINT trading_type_details_modified_by_fkey;
       public          postgres    false    272    209    3279            �           2606    222393 5   trading_type_details trading_type_details_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.trading_type_details
    ADD CONSTRAINT trading_type_details_status_fkey FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 _   ALTER TABLE ONLY public.trading_type_details DROP CONSTRAINT trading_type_details_status_fkey;
       public          postgres    false    234    272    3303            �           2606    222478 #   vin_history vin_history_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.vin_history
    ADD CONSTRAINT vin_history_status_fkey FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 M   ALTER TABLE ONLY public.vin_history DROP CONSTRAINT vin_history_status_fkey;
       public          postgres    false    3303    234    317            �      x������ � �      )      x������ � �      �      x������ � �      �      x������ � �      3      x������ � �      �      x������ � �      �      x������ � �      !      x������ � �            x������ � �            x������ � �            x������ � �      �      x������ � �      #      x������ � �            x������ � �      �      x������ � �      	      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      7      x������ � �      �      x������ � �      1      x������ � �      �      x������ � �      /      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      %      x������ � �      '      x������ � �      -      x������ � �            x������ � �            x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      +      x������ � �            x������ � �      �      x������ � �            x������ � �            x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �            x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      9      x������ � �      �      x������ � �      5      x������ � �      �      x������ � �            x������ � �            x������ � �            x������ � �            x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �            x������ � �     
PGDMP                         y            EpartsLatest    12.2    12.2 ;   �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            �           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            �           1262    223428    EpartsLatest    DATABASE     �   CREATE DATABASE "EpartsLatest" WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'English_United States.1252' LC_CTYPE = 'English_United States.1252';
    DROP DATABASE "EpartsLatest";
                e-parts-admin    false            �            1259    231431    Login_user_type    TABLE     �   CREATE TABLE public."Login_user_type" (
    "Login_user_type_id" integer NOT NULL,
    "Login_user_type_name" character varying(25)
);
 %   DROP TABLE public."Login_user_type";
       public         heap    postgres    false            �            1259    231429 &   Login_user_type_Login_user_type_id_seq    SEQUENCE     �   CREATE SEQUENCE public."Login_user_type_Login_user_type_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ?   DROP SEQUENCE public."Login_user_type_Login_user_type_id_seq";
       public          postgres    false    213            �           0    0 &   Login_user_type_Login_user_type_id_seq    SEQUENCE OWNED BY     w   ALTER SEQUENCE public."Login_user_type_Login_user_type_id_seq" OWNED BY public."Login_user_type"."Login_user_type_id";
          public          postgres    false    212            `           1259    232187    Rating_review    TABLE     �  CREATE TABLE public."Rating_review" (
    id integer NOT NULL,
    seller_id bigint NOT NULL,
    rate_seller numeric(1,0),
    review_seller character varying(1000),
    status integer,
    action character varying(150),
    created_at timestamp with time zone NOT NULL,
    created_by integer NOT NULL,
    modified_at timestamp with time zone NOT NULL,
    modified_by integer NOT NULL
);
 #   DROP TABLE public."Rating_review";
       public         heap    postgres    false            _           1259    232185    Rating_review_id_seq    SEQUENCE     �   CREATE SEQUENCE public."Rating_review_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public."Rating_review_id_seq";
       public          postgres    false    352            �           0    0    Rating_review_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE public."Rating_review_id_seq" OWNED BY public."Rating_review".id;
          public          postgres    false    351                       1259    231799    VAT_details    TABLE     �  CREATE TABLE public."VAT_details" (
    id integer NOT NULL,
    vat_percentage integer,
    effective_date timestamp with time zone,
    tax_code_id integer,
    pos_id integer,
    juristriction_id integer,
    status integer NOT NULL,
    created_at timestamp with time zone NOT NULL,
    created_by integer NOT NULL,
    modified_at timestamp with time zone NOT NULL,
    modified_by integer NOT NULL
);
 !   DROP TABLE public."VAT_details";
       public         heap    postgres    false                       1259    231797    VAT_details_id_seq    SEQUENCE     �   CREATE SEQUENCE public."VAT_details_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public."VAT_details_id_seq";
       public          postgres    false    281            �           0    0    VAT_details_id_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public."VAT_details_id_seq" OWNED BY public."VAT_details".id;
          public          postgres    false    280            �            1259    231401    application_user    TABLE     '  CREATE TABLE public.application_user (
    application_user_id integer NOT NULL,
    user_id bigint,
    portal_user_id bigint,
    user_name character varying(150),
    user_email_id character varying(50),
    phone_no character varying(15),
    designation character varying(100),
    email_verification_status boolean,
    phoneno_verified boolean,
    status integer NOT NULL,
    created_at timestamp with time zone NOT NULL,
    created_by integer NOT NULL,
    modified_at timestamp with time zone NOT NULL,
    modified_by integer NOT NULL
);
 $   DROP TABLE public.application_user;
       public         heap    postgres    false            �            1259    231399 (   application_user_application_user_id_seq    SEQUENCE     �   CREATE SEQUENCE public.application_user_application_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ?   DROP SEQUENCE public.application_user_application_user_id_seq;
       public          postgres    false    207            �           0    0 (   application_user_application_user_id_seq    SEQUENCE OWNED BY     u   ALTER SEQUENCE public.application_user_application_user_id_seq OWNED BY public.application_user.application_user_id;
          public          postgres    false    206            �            1259    231412    application_user_permissions    TABLE     �  CREATE TABLE public.application_user_permissions (
    id integer NOT NULL,
    application_user_id integer,
    module_id integer,
    dashboard_id integer,
    report_id integer,
    add_access boolean,
    delete_access boolean,
    read_access boolean,
    modify_access boolean,
    export_access boolean,
    print_access boolean,
    created_at timestamp with time zone,
    created_by integer,
    modified_at timestamp with time zone,
    modified_by integer
);
 0   DROP TABLE public.application_user_permissions;
       public         heap    postgres    false            �            1259    231410 #   application_user_permissions_id_seq    SEQUENCE     �   CREATE SEQUENCE public.application_user_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 :   DROP SEQUENCE public.application_user_permissions_id_seq;
       public          postgres    false    209            �           0    0 #   application_user_permissions_id_seq    SEQUENCE OWNED BY     k   ALTER SEQUENCE public.application_user_permissions_id_seq OWNED BY public.application_user_permissions.id;
          public          postgres    false    208            j           1259    232242    audit_trail    TABLE     �  CREATE TABLE public.audit_trail (
    id integer NOT NULL,
    module_id integer,
    field_name character varying(150),
    old_value character varying(150),
    new_value character varying(150),
    user_id character varying(10),
    time_stamp timestamp with time zone,
    status integer,
    audit_action character varying(1000),
    created_at timestamp with time zone,
    created_by integer,
    modified_at timestamp with time zone,
    modified_by integer
);
    DROP TABLE public.audit_trail;
       public         heap    postgres    false            i           1259    232240    audit_trail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.audit_trail_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.audit_trail_id_seq;
       public          postgres    false    362            �           0    0    audit_trail_id_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE public.audit_trail_id_seq OWNED BY public.audit_trail.id;
          public          postgres    false    361            '           1259    231873    brands    TABLE     �  CREATE TABLE public.brands (
    brand_id integer NOT NULL,
    brand_code character varying(50),
    brand_name character varying(150),
    erp_id integer,
    last_integrated_date timestamp with time zone,
    brand_api_id character varying(255),
    brand_image character varying(255),
    status integer,
    created_at timestamp with time zone,
    created_by integer,
    modified_at timestamp with time zone,
    modified_by integer
);
    DROP TABLE public.brands;
       public         heap    postgres    false            &           1259    231871    brands_brand_id_seq    SEQUENCE     �   CREATE SEQUENCE public.brands_brand_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.brands_brand_id_seq;
       public          postgres    false    295            �           0    0    brands_brand_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE public.brands_brand_id_seq OWNED BY public.brands.brand_id;
          public          postgres    false    294            �            1259    231634    bulk_upload_details    TABLE     k  CREATE TABLE public.bulk_upload_details (
    id integer NOT NULL,
    file_name character varying(50),
    "file_URL" character varying(255),
    product_type_id integer,
    status integer,
    remarks character varying(1000),
    created_at timestamp with time zone,
    created_by integer,
    modified_at timestamp with time zone,
    modified_by integer
);
 '   DROP TABLE public.bulk_upload_details;
       public         heap    postgres    false            �            1259    231632    bulk_upload_details_id_seq    SEQUENCE     �   CREATE SEQUENCE public.bulk_upload_details_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 1   DROP SEQUENCE public.bulk_upload_details_id_seq;
       public          postgres    false    251            �           0    0    bulk_upload_details_id_seq    SEQUENCE OWNED BY     Y   ALTER SEQUENCE public.bulk_upload_details_id_seq OWNED BY public.bulk_upload_details.id;
          public          postgres    false    250            X           1259    232143    buyer_customer_details    TABLE     �  CREATE TABLE public.buyer_customer_details (
    buyer_customer_id integer NOT NULL,
    buyer_id integer,
    customer_name character varying(150),
    email character varying(50),
    phone character varying(15),
    address character varying(1000),
    car_details_brand integer,
    model_id integer,
    status integer,
    action character varying(1000),
    created_at timestamp with time zone,
    created_by integer,
    modified_at timestamp with time zone,
    modified_by integer
);
 *   DROP TABLE public.buyer_customer_details;
       public         heap    postgres    false            W           1259    232141 ,   buyer_customer_details_buyer_customer_id_seq    SEQUENCE     �   CREATE SEQUENCE public.buyer_customer_details_buyer_customer_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 C   DROP SEQUENCE public.buyer_customer_details_buyer_customer_id_seq;
       public          postgres    false    344            �           0    0 ,   buyer_customer_details_buyer_customer_id_seq    SEQUENCE OWNED BY     }   ALTER SEQUENCE public.buyer_customer_details_buyer_customer_id_seq OWNED BY public.buyer_customer_details.buyer_customer_id;
          public          postgres    false    343            T           1259    232121    buyer_saved_quote    TABLE     �  CREATE TABLE public.buyer_saved_quote (
    buyer_quote_id integer NOT NULL,
    buyer_id bigint,
    cart_item_id integer,
    quote_no character varying(50),
    tax_code integer,
    buyer_customer_id integer,
    status_of_quote integer,
    quantity numeric(6,0),
    price numeric(10,0),
    margin_per numeric(10,0),
    price_with_margin numeric(10,0),
    customer_detail_name character varying(50),
    address character varying(1000),
    car_details_brand integer,
    model_id integer,
    status integer,
    action character varying(1000),
    created_at timestamp with time zone,
    created_by integer,
    modified_at timestamp with time zone,
    modified_by integer
);
 %   DROP TABLE public.buyer_saved_quote;
       public         heap    postgres    false            S           1259    232119 $   buyer_saved_quote_buyer_quote_id_seq    SEQUENCE     �   CREATE SEQUENCE public.buyer_saved_quote_buyer_quote_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ;   DROP SEQUENCE public.buyer_saved_quote_buyer_quote_id_seq;
       public          postgres    false    340            �           0    0 $   buyer_saved_quote_buyer_quote_id_seq    SEQUENCE OWNED BY     m   ALTER SEQUENCE public.buyer_saved_quote_buyer_quote_id_seq OWNED BY public.buyer_saved_quote.buyer_quote_id;
          public          postgres    false    339            V           1259    232132    buyer_saved_quote_service_items    TABLE     �  CREATE TABLE public.buyer_saved_quote_service_items (
    id integer NOT NULL,
    buyer_quote_id integer,
    service_item character varying(150),
    quantity numeric(6,0),
    price numeric(10,0),
    tax_code integer,
    status integer,
    action character varying(1000),
    created_at timestamp with time zone,
    created_by integer,
    modified_at timestamp with time zone,
    modified_by integer
);
 3   DROP TABLE public.buyer_saved_quote_service_items;
       public         heap    postgres    false            U           1259    232130 &   buyer_saved_quote_service_items_id_seq    SEQUENCE     �   CREATE SEQUENCE public.buyer_saved_quote_service_items_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 =   DROP SEQUENCE public.buyer_saved_quote_service_items_id_seq;
       public          postgres    false    342            �           0    0 &   buyer_saved_quote_service_items_id_seq    SEQUENCE OWNED BY     q   ALTER SEQUENCE public.buyer_saved_quote_service_items_id_seq OWNED BY public.buyer_saved_quote_service_items.id;
          public          postgres    false    341            J           1259    232066    car_management    TABLE     �  CREATE TABLE public.car_management (
    id integer NOT NULL,
    buyer_id bigint,
    buyer_customer_id integer,
    car_info character varying(1000),
    brand_info character varying(1000),
    model_info character varying(1000),
    vin_no character varying(50),
    action character varying(1000),
    status integer,
    created_at timestamp with time zone,
    created_by integer,
    modified_at timestamp with time zone,
    modified_by integer
);
 "   DROP TABLE public.car_management;
       public         heap    postgres    false            I           1259    232064    car_management_id_seq    SEQUENCE     �   CREATE SEQUENCE public.car_management_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public.car_management_id_seq;
       public          postgres    false    330            �           0    0    car_management_id_seq    SEQUENCE OWNED BY     O   ALTER SEQUENCE public.car_management_id_seq OWNED BY public.car_management.id;
          public          postgres    false    329            -           1259    231906    cars    TABLE     �  CREATE TABLE public.cars (
    car_id integer NOT NULL,
    car_name character varying(150),
    brand_id integer,
    model_id integer,
    parameters_id integer,
    parameters_value character varying(1000),
    erp_id integer,
    last_integrated_date timestamp with time zone,
    status integer,
    created_at timestamp with time zone,
    created_by integer,
    modified_at timestamp with time zone,
    modified_by integer
);
    DROP TABLE public.cars;
       public         heap    postgres    false            ,           1259    231904    cars_car_id_seq    SEQUENCE     �   CREATE SEQUENCE public.cars_car_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.cars_car_id_seq;
       public          postgres    false    301            �           0    0    cars_car_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE public.cars_car_id_seq OWNED BY public.cars.car_id;
          public          postgres    false    300            Z           1259    232154    cart_details    TABLE     �  CREATE TABLE public.cart_details (
    cart_item_id integer NOT NULL,
    buyer_id bigint,
    product_id numeric(10,0),
    price numeric(10,0),
    quantity numeric(6,0),
    tax_code integer,
    tax_amount numeric(10,0),
    final_price numeric(10,0),
    checkout_status character varying(50),
    action character varying(1000),
    created_at timestamp with time zone,
    created_by integer,
    modified_at timestamp with time zone,
    modified_by integer
);
     DROP TABLE public.cart_details;
       public         heap    postgres    false            Y           1259    232152    cart_details_cart_item_id_seq    SEQUENCE     �   CREATE SEQUENCE public.cart_details_cart_item_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 4   DROP SEQUENCE public.cart_details_cart_item_id_seq;
       public          postgres    false    346            �           0    0    cart_details_cart_item_id_seq    SEQUENCE OWNED BY     _   ALTER SEQUENCE public.cart_details_cart_item_id_seq OWNED BY public.cart_details.cart_item_id;
          public          postgres    false    345            R           1259    232110    category_request_management    TABLE     �  CREATE TABLE public.category_request_management (
    id integer NOT NULL,
    category_code character varying(50),
    category_name character varying(150),
    description character varying(1000),
    cat_req_status_id integer,
    reason_for_reject integer,
    status integer,
    action character varying(1000),
    created_at timestamp with time zone,
    created_by integer,
    modified_at timestamp with time zone,
    modified_by integer
);
 /   DROP TABLE public.category_request_management;
       public         heap    postgres    false            Q           1259    232108 "   category_request_management_id_seq    SEQUENCE     �   CREATE SEQUENCE public.category_request_management_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 9   DROP SEQUENCE public.category_request_management_id_seq;
       public          postgres    false    338            �           0    0 "   category_request_management_id_seq    SEQUENCE OWNED BY     i   ALTER SEQUENCE public.category_request_management_id_seq OWNED BY public.category_request_management.id;
          public          postgres    false    337                       1259    231733    category_request_status    TABLE     0  CREATE TABLE public.category_request_status (
    cat_req_status_id integer NOT NULL,
    cat_req_status_name character varying(50),
    remarks character varying(1000),
    created_at timestamp with time zone,
    created_by integer,
    modified_at timestamp with time zone,
    modified_by integer
);
 +   DROP TABLE public.category_request_status;
       public         heap    postgres    false                       1259    231731 -   category_request_status_cat_req_status_id_seq    SEQUENCE     �   CREATE SEQUENCE public.category_request_status_cat_req_status_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 D   DROP SEQUENCE public.category_request_status_cat_req_status_id_seq;
       public          postgres    false    269            �           0    0 -   category_request_status_cat_req_status_id_seq    SEQUENCE OWNED BY        ALTER SEQUENCE public.category_request_status_cat_req_status_id_seq OWNED BY public.category_request_status.cat_req_status_id;
          public          postgres    false    268            �            1259    231505    cms_company_info    TABLE     �  CREATE TABLE public.cms_company_info (
    id integer NOT NULL,
    company_name character varying(150),
    company_logo character varying(255),
    company_description character varying(1000),
    mobile_number character varying(15),
    company_email character varying(50),
    status integer,
    created_at timestamp with time zone,
    created_by integer,
    modified_at timestamp with time zone,
    modified_by integer
);
 $   DROP TABLE public.cms_company_info;
       public         heap    postgres    false            �            1259    231503    cms_company_info_id_seq    SEQUENCE     �   CREATE SEQUENCE public.cms_company_info_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.cms_company_info_id_seq;
       public          postgres    false    227            �           0    0    cms_company_info_id_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE public.cms_company_info_id_seq OWNED BY public.cms_company_info.id;
          public          postgres    false    226            �            1259    231582    cms_footer_links    TABLE     )  CREATE TABLE public.cms_footer_links (
    id integer NOT NULL,
    link_name character varying(50),
    redirect_link character varying(255),
    status integer,
    created_at timestamp with time zone,
    created_by integer,
    modified_at timestamp with time zone,
    modified_by integer
);
 $   DROP TABLE public.cms_footer_links;
       public         heap    postgres    false            �            1259    231580    cms_footer_links_id_seq    SEQUENCE     �   CREATE SEQUENCE public.cms_footer_links_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.cms_footer_links_id_seq;
       public          postgres    false    241            �           0    0    cms_footer_links_id_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE public.cms_footer_links_id_seq OWNED BY public.cms_footer_links.id;
          public          postgres    false    240            �            1259    231560    cms_homepage_about_section    TABLE     z  CREATE TABLE public.cms_homepage_about_section (
    id integer NOT NULL,
    title character varying(50),
    sub_title character varying(150),
    content character varying(1000),
    redirect_link character varying(255),
    status integer,
    created_at timestamp with time zone,
    created_by integer,
    modified_at timestamp with time zone,
    modified_by integer
);
 .   DROP TABLE public.cms_homepage_about_section;
       public         heap    postgres    false            �            1259    231558 !   cms_homepage_about_section_id_seq    SEQUENCE     �   CREATE SEQUENCE public.cms_homepage_about_section_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 8   DROP SEQUENCE public.cms_homepage_about_section_id_seq;
       public          postgres    false    237            �           0    0 !   cms_homepage_about_section_id_seq    SEQUENCE OWNED BY     g   ALTER SEQUENCE public.cms_homepage_about_section_id_seq OWNED BY public.cms_homepage_about_section.id;
          public          postgres    false    236            �            1259    231538    cms_homepage_banner_section    TABLE     N  CREATE TABLE public.cms_homepage_banner_section (
    id integer NOT NULL,
    title character varying(150),
    content character varying(1000),
    image character varying(255),
    status integer,
    created_at timestamp with time zone,
    created_by integer,
    modified_at timestamp with time zone,
    modified_by integer
);
 /   DROP TABLE public.cms_homepage_banner_section;
       public         heap    postgres    false            �            1259    231536 "   cms_homepage_banner_section_id_seq    SEQUENCE     �   CREATE SEQUENCE public.cms_homepage_banner_section_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 9   DROP SEQUENCE public.cms_homepage_banner_section_id_seq;
       public          postgres    false    233            �           0    0 "   cms_homepage_banner_section_id_seq    SEQUENCE OWNED BY     i   ALTER SEQUENCE public.cms_homepage_banner_section_id_seq OWNED BY public.cms_homepage_banner_section.id;
          public          postgres    false    232            �            1259    231571     cms_homepage_buyerseller_section    TABLE     R  CREATE TABLE public.cms_homepage_buyerseller_section (
    id integer NOT NULL,
    buyer_title character varying(50),
    buyer_content character varying(1000),
    buyer_redirect_link character varying(255),
    buyer_section_image character varying(255),
    seller_title character varying(50),
    seller_content character varying(1000),
    seller_redirect_link character varying(255),
    seller_section_image character varying(255),
    status integer,
    created_at timestamp with time zone,
    created_by integer,
    modified_at timestamp with time zone,
    modified_by integer
);
 4   DROP TABLE public.cms_homepage_buyerseller_section;
       public         heap    postgres    false            �            1259    231569 '   cms_homepage_buyerseller_section_id_seq    SEQUENCE     �   CREATE SEQUENCE public.cms_homepage_buyerseller_section_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 >   DROP SEQUENCE public.cms_homepage_buyerseller_section_id_seq;
       public          postgres    false    239            �           0    0 '   cms_homepage_buyerseller_section_id_seq    SEQUENCE OWNED BY     s   ALTER SEQUENCE public.cms_homepage_buyerseller_section_id_seq OWNED BY public.cms_homepage_buyerseller_section.id;
          public          postgres    false    238            �            1259    231549    cms_homepage_features_section    TABLE     U  CREATE TABLE public.cms_homepage_features_section (
    id integer NOT NULL,
    title character varying(50),
    content character varying(1000),
    icon_images character varying(255),
    status integer,
    created_at timestamp with time zone,
    created_by integer,
    modified_at timestamp with time zone,
    modified_by integer
);
 1   DROP TABLE public.cms_homepage_features_section;
       public         heap    postgres    false            �            1259    231547 $   cms_homepage_features_section_id_seq    SEQUENCE     �   CREATE SEQUENCE public.cms_homepage_features_section_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ;   DROP SEQUENCE public.cms_homepage_features_section_id_seq;
       public          postgres    false    235            �           0    0 $   cms_homepage_features_section_id_seq    SEQUENCE OWNED BY     m   ALTER SEQUENCE public.cms_homepage_features_section_id_seq OWNED BY public.cms_homepage_features_section.id;
          public          postgres    false    234            �            1259    231527    cms_menu_section    TABLE     D  CREATE TABLE public.cms_menu_section (
    id integer NOT NULL,
    menu_name character varying(50),
    menu_position integer,
    redirect_link character varying(255),
    status integer,
    created_at timestamp with time zone,
    created_by integer,
    modified_at timestamp with time zone,
    modified_by integer
);
 $   DROP TABLE public.cms_menu_section;
       public         heap    postgres    false            �            1259    231525    cms_menu_section_id_seq    SEQUENCE     �   CREATE SEQUENCE public.cms_menu_section_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.cms_menu_section_id_seq;
       public          postgres    false    231            �           0    0    cms_menu_section_id_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE public.cms_menu_section_id_seq OWNED BY public.cms_menu_section.id;
          public          postgres    false    230            �            1259    231516    cms_social_media_info    TABLE     1  CREATE TABLE public.cms_social_media_info (
    id integer NOT NULL,
    icon_images character varying(255),
    redirect_link character varying(255),
    status integer,
    created_at timestamp with time zone,
    created_by integer,
    modified_at timestamp with time zone,
    modified_by integer
);
 )   DROP TABLE public.cms_social_media_info;
       public         heap    postgres    false            �            1259    231514    cms_social_media_info_id_seq    SEQUENCE     �   CREATE SEQUENCE public.cms_social_media_info_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 3   DROP SEQUENCE public.cms_social_media_info_id_seq;
       public          postgres    false    229            �           0    0    cms_social_media_info_id_seq    SEQUENCE OWNED BY     ]   ALTER SEQUENCE public.cms_social_media_info_id_seq OWNED BY public.cms_social_media_info.id;
          public          postgres    false    228            @           1259    232011    commission_management    TABLE     �  CREATE TABLE public.commission_management (
    id integer NOT NULL,
    product_type_id integer,
    trading_type_id integer,
    commission_precentage numeric(10,0),
    effective_date timestamp with time zone,
    action character varying(1000),
    status integer,
    created_at timestamp with time zone,
    created_by integer,
    modified_at timestamp with time zone,
    modified_by integer
);
 )   DROP TABLE public.commission_management;
       public         heap    postgres    false            ?           1259    232009    commission_management_id_seq    SEQUENCE     �   CREATE SEQUENCE public.commission_management_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 3   DROP SEQUENCE public.commission_management_id_seq;
       public          postgres    false    320            �           0    0    commission_management_id_seq    SEQUENCE OWNED BY     ]   ALTER SEQUENCE public.commission_management_id_seq OWNED BY public.commission_management.id;
          public          postgres    false    319            �            1259    231623    configuration_details    TABLE     |  CREATE TABLE public.configuration_details (
    id integer NOT NULL,
    config_name character varying(150),
    "config_URL" character varying(255),
    "Config_username" character varying(150),
    config_password character varying(100),
    config_port integer,
    config_parameter character varying(1000),
    "config_MobileNumber" integer,
    config_mtype integer,
    config_message integer,
    config_secretkey character varying(100),
    status integer,
    remarks character varying(1000),
    created_at timestamp with time zone,
    created_by integer,
    modified_at timestamp with time zone,
    modified_by integer
);
 )   DROP TABLE public.configuration_details;
       public         heap    postgres    false            �            1259    231621    configuration_details_id_seq    SEQUENCE     �   CREATE SEQUENCE public.configuration_details_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 3   DROP SEQUENCE public.configuration_details_id_seq;
       public          postgres    false    249            �           0    0    configuration_details_id_seq    SEQUENCE OWNED BY     ]   ALTER SEQUENCE public.configuration_details_id_seq OWNED BY public.configuration_details.id;
          public          postgres    false    248                       1259    231777    country_details    TABLE     2  CREATE TABLE public.country_details (
    country_id integer NOT NULL,
    country_name character varying(150),
    country_code character varying(15),
    status integer,
    created_at timestamp with time zone,
    created_by integer,
    modified_at timestamp with time zone,
    modified_by integer
);
 #   DROP TABLE public.country_details;
       public         heap    postgres    false                       1259    231775    country_details_country_id_seq    SEQUENCE     �   CREATE SEQUENCE public.country_details_country_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 5   DROP SEQUENCE public.country_details_country_id_seq;
       public          postgres    false    277            �           0    0    country_details_country_id_seq    SEQUENCE OWNED BY     a   ALTER SEQUENCE public.country_details_country_id_seq OWNED BY public.country_details.country_id;
          public          postgres    false    276                       1259    231766    currency_details    TABLE     :  CREATE TABLE public.currency_details (
    currency_id integer NOT NULL,
    currency_name character varying(50),
    currency_shortcode character varying(10),
    status integer,
    created_at timestamp with time zone,
    created_by integer,
    modified_at timestamp with time zone,
    modified_by integer
);
 $   DROP TABLE public.currency_details;
       public         heap    postgres    false                       1259    231764     currency_details_currency_id_seq    SEQUENCE     �   CREATE SEQUENCE public.currency_details_currency_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 7   DROP SEQUENCE public.currency_details_currency_id_seq;
       public          postgres    false    275            �           0    0     currency_details_currency_id_seq    SEQUENCE OWNED BY     e   ALTER SEQUENCE public.currency_details_currency_id_seq OWNED BY public.currency_details.currency_id;
          public          postgres    false    274            �            1259    231483 	   dashboard    TABLE     )  CREATE TABLE public.dashboard (
    dashboard_id integer NOT NULL,
    dashboard_name character varying(50),
    "Login_user_type_id" integer,
    status integer,
    created_at timestamp with time zone,
    created_by integer,
    modified_at timestamp with time zone,
    modified_by integer
);
    DROP TABLE public.dashboard;
       public         heap    postgres    false            �            1259    231481    dashboard_dashboard_id_seq    SEQUENCE     �   CREATE SEQUENCE public.dashboard_dashboard_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 1   DROP SEQUENCE public.dashboard_dashboard_id_seq;
       public          postgres    false    223            �           0    0    dashboard_dashboard_id_seq    SEQUENCE OWNED BY     Y   ALTER SEQUENCE public.dashboard_dashboard_id_seq OWNED BY public.dashboard.dashboard_id;
          public          postgres    false    222            n           1259    232264    email_template    TABLE     *  CREATE TABLE public.email_template (
    id integer NOT NULL,
    title character varying(150),
    email_type character varying(50),
    encoding_type character varying(50),
    iso_code character varying(50),
    description character varying(1000),
    subject character varying(250),
    email_content character varying,
    attachment character varying(255),
    status integer,
    action character varying(1000),
    created_at timestamp with time zone,
    created_by integer,
    modified_at timestamp with time zone,
    modified_by integer
);
 "   DROP TABLE public.email_template;
       public         heap    postgres    false            m           1259    232262    email_template_id_seq    SEQUENCE     �   CREATE SEQUENCE public.email_template_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public.email_template_id_seq;
       public          postgres    false    366            �           0    0    email_template_id_seq    SEQUENCE OWNED BY     O   ALTER SEQUENCE public.email_template_id_seq OWNED BY public.email_template.id;
          public          postgres    false    365            /           1259    231917    group_management    TABLE     �  CREATE TABLE public.group_management (
    group_id integer NOT NULL,
    car_id integer,
    erp_id integer,
    last_integrated_date timestamp with time zone,
    group_code character varying(15),
    group_name character varying(150),
    status integer,
    created_at timestamp with time zone,
    created_by integer,
    modified_at timestamp with time zone,
    modified_by integer
);
 $   DROP TABLE public.group_management;
       public         heap    postgres    false            .           1259    231915    group_management_group_id_seq    SEQUENCE     �   CREATE SEQUENCE public.group_management_group_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 4   DROP SEQUENCE public.group_management_group_id_seq;
       public          postgres    false    303            �           0    0    group_management_group_id_seq    SEQUENCE OWNED BY     _   ALTER SEQUENCE public.group_management_group_id_seq OWNED BY public.group_management.group_id;
          public          postgres    false    302            h           1259    232231    help_management    TABLE     �  CREATE TABLE public.help_management (
    id integer NOT NULL,
    help_management_code character varying(50),
    subject_name character varying(150),
    description character varying(1000),
    module_id integer,
    keywords character varying(250),
    status integer,
    action character varying(1000),
    created_at timestamp with time zone,
    created_by integer,
    modified_at timestamp with time zone,
    modified_by integer
);
 #   DROP TABLE public.help_management;
       public         heap    postgres    false            g           1259    232229    help_management_id_seq    SEQUENCE     �   CREATE SEQUENCE public.help_management_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.help_management_id_seq;
       public          postgres    false    360            �           0    0    help_management_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE public.help_management_id_seq OWNED BY public.help_management.id;
          public          postgres    false    359            �            1259    231390    intermediator    TABLE     �  CREATE TABLE public.intermediator (
    id integer NOT NULL,
    user_id bigint,
    intermediator_code character varying(50),
    firstname character varying(50),
    lastname character varying(50),
    user_name character varying(50),
    role_id integer,
    email_id character varying(50),
    phone_number character varying(15),
    mobile_number character varying(15),
    status integer,
    image character varying(255),
    country integer,
    language_spoken character varying(50),
    address character varying(1000),
    created_at timestamp with time zone,
    created_by integer,
    modified_at timestamp with time zone,
    modified_by integer
);
 !   DROP TABLE public.intermediator;
       public         heap    postgres    false            �            1259    231388    intermediator_id_seq    SEQUENCE     �   CREATE SEQUENCE public.intermediator_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.intermediator_id_seq;
       public          postgres    false    205            �           0    0    intermediator_id_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.intermediator_id_seq OWNED BY public.intermediator.id;
          public          postgres    false    204            f           1259    232220    inventory_manangement    TABLE     �  CREATE TABLE public.inventory_manangement (
    id integer NOT NULL,
    warehouse_id integer,
    seller_id bigint,
    product_id integer,
    brand_info character varying(1000),
    model_info character varying(1000),
    car_info character varying(1000),
    price numeric(10,0),
    price_after_sale numeric(10,0),
    onhand numeric(6,0),
    e_part_qnty numeric(6,0),
    buffer_qnty numeric(6,0),
    re_order_level numeric(6,0),
    threshold_limit numeric(6,0),
    sold numeric(6,0),
    balance numeric(6,0),
    bin_loc character varying(250),
    status integer,
    action character varying(1000),
    created_at timestamp with time zone,
    created_by integer,
    modified_at timestamp with time zone,
    modified_by integer
);
 )   DROP TABLE public.inventory_manangement;
       public         heap    postgres    false            e           1259    232218    inventory_manangement_id_seq    SEQUENCE     �   CREATE SEQUENCE public.inventory_manangement_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 3   DROP SEQUENCE public.inventory_manangement_id_seq;
       public          postgres    false    358            �           0    0    inventory_manangement_id_seq    SEQUENCE OWNED BY     ]   ALTER SEQUENCE public.inventory_manangement_id_seq OWNED BY public.inventory_manangement.id;
          public          postgres    false    357                       1259    231829    jurisdiction    TABLE     j  CREATE TABLE public.jurisdiction (
    jurisdiction_id integer NOT NULL,
    name character varying(50),
    code character varying(50),
    erp_id integer,
    last_integrated_date timestamp with time zone,
    status integer,
    created_at timestamp with time zone,
    created_by integer,
    modified_at timestamp with time zone,
    modified_by integer
);
     DROP TABLE public.jurisdiction;
       public         heap    postgres    false                       1259    231827     jurisdiction_jurisdiction_id_seq    SEQUENCE     �   CREATE SEQUENCE public.jurisdiction_jurisdiction_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 7   DROP SEQUENCE public.jurisdiction_jurisdiction_id_seq;
       public          postgres    false    287            �           0    0     jurisdiction_jurisdiction_id_seq    SEQUENCE OWNED BY     e   ALTER SEQUENCE public.jurisdiction_jurisdiction_id_seq OWNED BY public.jurisdiction.jurisdiction_id;
          public          postgres    false    286                       1259    231722    make_offer_status    TABLE     +  CREATE TABLE public.make_offer_status (
    make_offer_status_id integer NOT NULL,
    offer_status_name character varying(50),
    remarks character varying(1000),
    created_at timestamp with time zone,
    created_by integer,
    modified_at timestamp with time zone,
    modified_by integer
);
 %   DROP TABLE public.make_offer_status;
       public         heap    postgres    false            
           1259    231720 *   make_offer_status_make_offer_status_id_seq    SEQUENCE     �   CREATE SEQUENCE public.make_offer_status_make_offer_status_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 A   DROP SEQUENCE public.make_offer_status_make_offer_status_id_seq;
       public          postgres    false    267            �           0    0 *   make_offer_status_make_offer_status_id_seq    SEQUENCE OWNED BY     y   ALTER SEQUENCE public.make_offer_status_make_offer_status_id_seq OWNED BY public.make_offer_status.make_offer_status_id;
          public          postgres    false    266            )           1259    231884    models    TABLE     �  CREATE TABLE public.models (
    model_id integer NOT NULL,
    brand_id integer,
    erp_id integer,
    last_integrated_date timestamp with time zone,
    model_code character varying(50),
    model_name character varying(150),
    model_api_id character varying(50),
    model_image character varying(255),
    status integer,
    created_at timestamp with time zone,
    created_by integer,
    modified_at timestamp with time zone,
    modified_by integer
);
    DROP TABLE public.models;
       public         heap    postgres    false            (           1259    231882    models_model_id_seq    SEQUENCE     �   CREATE SEQUENCE public.models_model_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.models_model_id_seq;
       public          postgres    false    297            �           0    0    models_model_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE public.models_model_id_seq OWNED BY public.models.model_id;
          public          postgres    false    296            �            1259    231472    module    TABLE     B  CREATE TABLE public.module (
    module_id integer NOT NULL,
    module_type_id integer,
    module_name character varying(50),
    module_abbr character varying(150),
    status integer,
    created_at timestamp with time zone,
    created_by integer,
    modified_at timestamp with time zone,
    modified_by integer
);
    DROP TABLE public.module;
       public         heap    postgres    false            �            1259    231470    module_module_id_seq    SEQUENCE     �   CREATE SEQUENCE public.module_module_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.module_module_id_seq;
       public          postgres    false    221            �           0    0    module_module_id_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.module_module_id_seq OWNED BY public.module.module_id;
          public          postgres    false    220            �            1259    231461    module_type    TABLE       CREATE TABLE public.module_type (
    module_type_id integer NOT NULL,
    module_type_name character varying(50),
    status integer,
    created_at timestamp with time zone,
    created_by integer,
    modified_at timestamp with time zone,
    modified_by integer
);
    DROP TABLE public.module_type;
       public         heap    postgres    false            �            1259    231459    module_type_module_type_id_seq    SEQUENCE     �   CREATE SEQUENCE public.module_type_module_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 5   DROP SEQUENCE public.module_type_module_type_id_seq;
       public          postgres    false    219            �           0    0    module_type_module_type_id_seq    SEQUENCE OWNED BY     a   ALTER SEQUENCE public.module_type_module_type_id_seq OWNED BY public.module_type.module_type_id;
          public          postgres    false    218            7           1259    231961    new_buyer_details    TABLE     O
  CREATE TABLE public.new_buyer_details (
    id bigint NOT NULL,
    user_id bigint,
    buyer_code character varying(50),
    email_verification_status boolean,
    business_name character varying(150),
    business_com_logo character varying(255),
    email_id character varying(50),
    subscription_id integer,
    subscription_effective_date timestamp with time zone,
    business_status boolean,
    billing_country integer,
    billing_state integer,
    billing_city character varying(50),
    b_address1 character varying(1000),
    b_address2 character varying(100),
    b_displayname character varying(150),
    b_phonenumber numeric(15,0),
    b_country_code character varying(10),
    jurisdiction_id integer,
    pos_id integer,
    shipping_country integer,
    shipping_state integer,
    shipping_city character varying(150),
    s_address1 character varying(1000),
    s_address2 character varying(1000),
    s_displayname character varying(150),
    s_phonenumber numeric(15,0),
    s_country_code character varying(15),
    phoneno_verified boolean,
    drop_address character varying(1000),
    googlemap_link character varying,
    payment_type_info json,
    product_type_id integer,
    trading_type_id integer,
    iv_location integer,
    iv_trade_licenese character varying(50),
    iv_res_national character varying(50),
    iv_exp_date timestamp with time zone,
    iv_country_issue integer,
    iv_f_name character varying(50),
    iv_m_name character varying(50),
    iv_l_name character varying(50),
    iv_dob timestamp with time zone,
    iv_r_bussiness_name character varying(50),
    iv_licenese_no character varying(50),
    iv_r_address character varying(1000),
    id_doc_front character varying(255),
    id_doc_back character varying(255),
    bus_doc_license character varying(255),
    registration_status integer,
    cancel_reason integer,
    vat_registration_number character varying(50),
    tax_registration_exp_date timestamp with time zone,
    vat_reg_certificate_certificate character varying(255),
    bank_benefi_name character varying,
    bank_name character varying(150),
    bank_ac_no character varying(50),
    bank_iban character varying(50),
    bank_branch_name character varying(150),
    swift_code character varying(50),
    bank_currency integer,
    bank_browse_file character varying(255),
    erp_id integer,
    last_integrated_date timestamp with time zone,
    company_buyer_representative boolean,
    t_and_c_acknowledge boolean,
    created_at timestamp with time zone,
    created_by integer,
    modified_at timestamp with time zone,
    modified_by integer
);
 %   DROP TABLE public.new_buyer_details;
       public         heap    postgres    false            6           1259    231959    new_buyer_details_id_seq    SEQUENCE     �   CREATE SEQUENCE public.new_buyer_details_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 /   DROP SEQUENCE public.new_buyer_details_id_seq;
       public          postgres    false    311            �           0    0    new_buyer_details_id_seq    SEQUENCE OWNED BY     U   ALTER SEQUENCE public.new_buyer_details_id_seq OWNED BY public.new_buyer_details.id;
          public          postgres    false    310            9           1259    231972    new_seller_details    TABLE     w
  CREATE TABLE public.new_seller_details (
    id bigint NOT NULL,
    user_id bigint,
    seller_code character varying(50),
    email_verification_status boolean,
    email_id character varying(50),
    b_multi_branch boolean,
    business_name character varying(150),
    business_com_logo character varying(255),
    subscription_id integer,
    subscription_effective_date timestamp with time zone,
    payout_shed_id integer,
    business_status boolean,
    billing_country integer,
    billing_state integer,
    billing_city character varying(50),
    b_address1 character varying(1000),
    b_address2 character varying(1000),
    jurisdiction_id integer,
    pos_id integer,
    b_displayname character varying(50),
    b_phonenumber numeric(15,0),
    b_country_code character varying(15),
    shipping_country integer,
    shipping_state integer,
    shipping_city character varying(50),
    s_address1 character varying(1000),
    s_address2 character varying(1000),
    s_displayname character varying(150),
    s_phonenumber numeric(15,0),
    s_country_code numeric(15,0),
    phoneno_verified boolean,
    drop_address character varying(1000),
    googlemap_link character varying(255),
    payment_type_info json,
    product_type_id integer,
    trading_type_id integer,
    iv_location integer,
    iv_licenese character varying(255),
    iv_national_id character varying(50),
    iv_exp_date timestamp with time zone,
    iv_country_issue integer,
    iv_f_name character varying(50),
    iv_m_name character varying(50),
    iv_l_name character varying(50),
    iv_dob timestamp with time zone,
    iv_r_bussiness_name character varying(150),
    iv_licenese_no character varying(50),
    iv_r_address character varying(1000),
    id_doc_front character varying(255),
    id_doc_back character varying(255),
    bus_doc_license character varying(255),
    registration_status integer,
    fail_reason integer,
    tax_registration_number character varying(50),
    tax_registration_exp_date timestamp with time zone,
    vat_reg_certificate character varying(255),
    bank_benefi_name character varying(150),
    bank_name character varying(50),
    bank_ac_no character varying(50),
    bank_iban character varying(50),
    bank_branch_name character varying(150),
    swift_code character varying(50),
    bank_currency integer,
    bank_browse_file character varying(255),
    company_seller_representative boolean,
    t_and_c_acknowledge boolean,
    erp_id integer,
    last_integrated_date timestamp with time zone,
    created_at timestamp with time zone,
    created_by integer,
    modified_at timestamp with time zone,
    modified_by integer
);
 &   DROP TABLE public.new_seller_details;
       public         heap    postgres    false            8           1259    231970    new_seller_details_id_seq    SEQUENCE     �   CREATE SEQUENCE public.new_seller_details_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 0   DROP SEQUENCE public.new_seller_details_id_seq;
       public          postgres    false    313            �           0    0    new_seller_details_id_seq    SEQUENCE OWNED BY     W   ALTER SEQUENCE public.new_seller_details_id_seq OWNED BY public.new_seller_details.id;
          public          postgres    false    312            \           1259    232165    newsletter_management    TABLE     �  CREATE TABLE public.newsletter_management (
    id integer NOT NULL,
    "Title" character varying(50),
    user_id character varying(50),
    registered character varying(50),
    email_id character varying(50),
    status integer,
    action character varying(1000),
    created_at timestamp with time zone,
    created_by integer,
    modified_at timestamp with time zone,
    modified_by integer
);
 )   DROP TABLE public.newsletter_management;
       public         heap    postgres    false            [           1259    232163    newsletter_management_id_seq    SEQUENCE     �   CREATE SEQUENCE public.newsletter_management_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 3   DROP SEQUENCE public.newsletter_management_id_seq;
       public          postgres    false    348            �           0    0    newsletter_management_id_seq    SEQUENCE OWNED BY     ]   ALTER SEQUENCE public.newsletter_management_id_seq OWNED BY public.newsletter_management.id;
          public          postgres    false    347            ^           1259    232176    notification    TABLE     d  CREATE TABLE public.notification (
    id integer NOT NULL,
    title character varying(50),
    advance_filter character varying(50),
    module_id integer,
    execute_on character varying(50),
    notification_type character varying(50),
    email_template integer,
    email_recipient_type character varying(50),
    email_recipient character varying(50),
    sms_template integer,
    sms_recipient_type character varying(50),
    sms_recipient character varying(50),
    push_notification_title character varying(50),
    push_notification character varying(50),
    push_notification_seller character varying(50),
    push_notification_seller_name character varying(50),
    status integer,
    action character varying(1000),
    created_at timestamp with time zone,
    created_by integer,
    modified_at timestamp with time zone,
    modified_by integer
);
     DROP TABLE public.notification;
       public         heap    postgres    false            ]           1259    232174    notification_id_seq    SEQUENCE     �   CREATE SEQUENCE public.notification_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.notification_id_seq;
       public          postgres    false    350            �           0    0    notification_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE public.notification_id_seq OWNED BY public.notification.id;
          public          postgres    false    349            d           1259    232209    offer_management    TABLE     e  CREATE TABLE public.offer_management (
    id integer NOT NULL,
    buyer_id bigint,
    seller_id bigint,
    product_id integer,
    part_no character varying(50),
    asking_amount numeric(10,0),
    asking_quantity numeric(6,0),
    seller_offer_amount numeric(10,0),
    seller_offer_quantity numeric(6,0),
    remarks character varying(1000),
    approved_by integer,
    offer_date timestamp with time zone,
    approved_date timestamp with time zone,
    seller_confirmed_date timestamp with time zone,
    rejected_date timestamp with time zone,
    rejected_reason character varying(1000),
    offer_validity timestamp with time zone,
    make_offer_status_id integer,
    status integer,
    action character varying(1000),
    created_at timestamp with time zone,
    created_by integer,
    modified_at timestamp with time zone,
    modified_by integer
);
 $   DROP TABLE public.offer_management;
       public         heap    postgres    false            c           1259    232207    offer_management_id_seq    SEQUENCE     �   CREATE SEQUENCE public.offer_management_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.offer_management_id_seq;
       public          postgres    false    356            �           0    0    offer_management_id_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE public.offer_management_id_seq OWNED BY public.offer_management.id;
          public          postgres    false    355            B           1259    232022    order_management    TABLE       CREATE TABLE public.order_management (
    order_id bigint NOT NULL,
    order_code character varying(50),
    buyer_id integer,
    user_id integer,
    method_of_payment integer,
    order_erp_id integer,
    order_date timestamp with time zone,
    order_type integer,
    order_amount numeric(10,0),
    order_status_id integer,
    total_quantity numeric(50,0),
    buyer_shipping_address character varying(255),
    googlemap_link character varying(255),
    total_vat_amount numeric(10,0),
    total_price numeric(10,0),
    last_delivery_time timestamp with time zone,
    total_discount numeric(10,0),
    total_shipping_charges numeric(10,0),
    ship_status_id integer,
    buyer_feedback character varying(1000),
    action character varying(1000),
    created_at timestamp with time zone,
    created_by integer,
    modified_at timestamp with time zone,
    modified_by integer
);
 $   DROP TABLE public.order_management;
       public         heap    postgres    false            A           1259    232020    order_management_order_id_seq    SEQUENCE     �   CREATE SEQUENCE public.order_management_order_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 4   DROP SEQUENCE public.order_management_order_id_seq;
       public          postgres    false    322            �           0    0    order_management_order_id_seq    SEQUENCE OWNED BY     _   ALTER SEQUENCE public.order_management_order_id_seq OWNED BY public.order_management.order_id;
          public          postgres    false    321            F           1259    232044    order_quote_Manage    TABLE     �  CREATE TABLE public."order_quote_Manage" (
    order_quote_id integer NOT NULL,
    order_quote_code character varying(50),
    buyer_id bigint,
    user_id integer,
    method_of_payment integer,
    order_quote_erp_id integer,
    order_quote_date timestamp with time zone,
    total_quantity numeric(6,0),
    order_type integer,
    order_amount numeric(10,0),
    order_status_id integer,
    total_vat_amount numeric(10,0),
    total_price numeric(10,0),
    buyer_shipping_address character varying(1000),
    googlemap_link character varying(255),
    action character varying(1000),
    created_at timestamp with time zone,
    created_by integer,
    modified_at timestamp with time zone,
    modified_by integer
);
 (   DROP TABLE public."order_quote_Manage";
       public         heap    postgres    false            E           1259    232042 %   order_quote_Manage_order_quote_id_seq    SEQUENCE     �   CREATE SEQUENCE public."order_quote_Manage_order_quote_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 >   DROP SEQUENCE public."order_quote_Manage_order_quote_id_seq";
       public          postgres    false    326            �           0    0 %   order_quote_Manage_order_quote_id_seq    SEQUENCE OWNED BY     s   ALTER SEQUENCE public."order_quote_Manage_order_quote_id_seq" OWNED BY public."order_quote_Manage".order_quote_id;
          public          postgres    false    325                       1259    231678    order_status    TABLE     !  CREATE TABLE public.order_status (
    order_status_id integer NOT NULL,
    order_status_name character varying(50),
    remarks character varying(1000),
    created_at timestamp with time zone,
    created_by integer,
    modified_at timestamp with time zone,
    modified_by integer
);
     DROP TABLE public.order_status;
       public         heap    postgres    false                       1259    231676     order_status_order_status_id_seq    SEQUENCE     �   CREATE SEQUENCE public.order_status_order_status_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 7   DROP SEQUENCE public.order_status_order_status_id_seq;
       public          postgres    false    259            �           0    0     order_status_order_status_id_seq    SEQUENCE OWNED BY     e   ALTER SEQUENCE public.order_status_order_status_id_seq OWNED BY public.order_status.order_status_id;
          public          postgres    false    258            �            1259    231656    order_type_details    TABLE     �   CREATE TABLE public.order_type_details (
    order_type_id integer NOT NULL,
    order_type_name character varying(50),
    created_at timestamp with time zone,
    created_by integer,
    modified_at timestamp with time zone,
    modified_by integer
);
 &   DROP TABLE public.order_type_details;
       public         heap    postgres    false            �            1259    231654 $   order_type_details_order_type_id_seq    SEQUENCE     �   CREATE SEQUENCE public.order_type_details_order_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ;   DROP SEQUENCE public.order_type_details_order_type_id_seq;
       public          postgres    false    255            �           0    0 $   order_type_details_order_type_id_seq    SEQUENCE OWNED BY     m   ALTER SEQUENCE public.order_type_details_order_type_id_seq OWNED BY public.order_type_details.order_type_id;
          public          postgres    false    254            +           1259    231895 
   parameters    TABLE     �  CREATE TABLE public.parameters (
    id integer NOT NULL,
    brand_id integer,
    model_id json,
    parameter_id character varying(50),
    parameter_name character varying(50),
    parameter_value character varying(150),
    erp_id integer,
    last_integrated_date timestamp with time zone,
    status integer,
    created_at timestamp with time zone,
    created_by integer,
    modified_at timestamp with time zone,
    modified_by integer
);
    DROP TABLE public.parameters;
       public         heap    postgres    false            *           1259    231893    parameters_id_seq    SEQUENCE     �   CREATE SEQUENCE public.parameters_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.parameters_id_seq;
       public          postgres    false    299            �           0    0    parameters_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.parameters_id_seq OWNED BY public.parameters.id;
          public          postgres    false    298            b           1259    232198    part_request_unavail    TABLE     �  CREATE TABLE public.part_request_unavail (
    id integer NOT NULL,
    part_no character varying(50),
    part_name character varying(150),
    remarks character varying(1000),
    user_id integer,
    user_type_info character varying(50),
    user_name character varying(150),
    requested_date timestamp with time zone,
    intermediator_remarks character varying(1000),
    parts_req_status_id character varying(50),
    parts_request_type character varying(50),
    status integer,
    action character varying(1000),
    created_at timestamp with time zone,
    created_by integer,
    modified_at timestamp with time zone,
    modified_by integer
);
 (   DROP TABLE public.part_request_unavail;
       public         heap    postgres    false            a           1259    232196    part_request_unavail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.part_request_unavail_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 2   DROP SEQUENCE public.part_request_unavail_id_seq;
       public          postgres    false    354            �           0    0    part_request_unavail_id_seq    SEQUENCE OWNED BY     [   ALTER SEQUENCE public.part_request_unavail_id_seq OWNED BY public.part_request_unavail.id;
          public          postgres    false    353            5           1259    231950    parts_management    TABLE     �  CREATE TABLE public.parts_management (
    id integer NOT NULL,
    car_id integer,
    group_id integer,
    sub_group_id integer,
    sub_node_id integer,
    part_number character varying(50),
    part_name character varying(150),
    part_erp_id integer,
    last_integrated_date timestamp with time zone,
    action character varying(1000),
    status integer,
    created_at timestamp with time zone,
    created_by integer,
    modified_at timestamp with time zone,
    modified_by integer
);
 $   DROP TABLE public.parts_management;
       public         heap    postgres    false            4           1259    231948    parts_management_id_seq    SEQUENCE     �   CREATE SEQUENCE public.parts_management_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.parts_management_id_seq;
       public          postgres    false    309            �           0    0    parts_management_id_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE public.parts_management_id_seq OWNED BY public.parts_management.id;
          public          postgres    false    308                       1259    231700    parts_req_status    TABLE     -  CREATE TABLE public.parts_req_status (
    parts_req_status_id integer NOT NULL,
    parts_req_status_name character varying(50),
    remarks character varying(1000),
    created_at timestamp with time zone,
    created_by integer,
    modified_at timestamp with time zone,
    modified_by integer
);
 $   DROP TABLE public.parts_req_status;
       public         heap    postgres    false                       1259    231698 (   parts_req_status_parts_req_status_id_seq    SEQUENCE     �   CREATE SEQUENCE public.parts_req_status_parts_req_status_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ?   DROP SEQUENCE public.parts_req_status_parts_req_status_id_seq;
       public          postgres    false    263            �           0    0 (   parts_req_status_parts_req_status_id_seq    SEQUENCE OWNED BY     u   ALTER SEQUENCE public.parts_req_status_parts_req_status_id_seq OWNED BY public.parts_req_status.parts_req_status_id;
          public          postgres    false    262            L           1259    232077    pay_out_schedule_management    TABLE     �  CREATE TABLE public.pay_out_schedule_management (
    id integer NOT NULL,
    pay_out_slot_name character varying(50),
    payout_shed_id integer,
    no_of_day numeric(3,0),
    order_id bigint,
    sub_order_id bigint,
    description character varying(1000),
    action character varying(1000),
    status integer,
    created_at timestamp with time zone,
    created_by integer,
    modified_at timestamp with time zone,
    modified_by integer
);
 /   DROP TABLE public.pay_out_schedule_management;
       public         heap    postgres    false            K           1259    232075 "   pay_out_schedule_management_id_seq    SEQUENCE     �   CREATE SEQUENCE public.pay_out_schedule_management_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 9   DROP SEQUENCE public.pay_out_schedule_management_id_seq;
       public          postgres    false    332            �           0    0 "   pay_out_schedule_management_id_seq    SEQUENCE OWNED BY     i   ALTER SEQUENCE public.pay_out_schedule_management_id_seq OWNED BY public.pay_out_schedule_management.id;
          public          postgres    false    331            N           1259    232088    payment_management    TABLE     �  CREATE TABLE public.payment_management (
    id integer NOT NULL,
    seller_id bigint,
    order_id bigint,
    sub_order_id bigint,
    number_of_order integer,
    total_amount numeric(10,0),
    commission_amount numeric(10,0),
    seller_amount numeric(10,0),
    payment_status_id integer,
    status integer,
    action character varying(1000),
    created_at timestamp with time zone,
    created_by integer,
    modified_at timestamp with time zone,
    modified_by integer
);
 &   DROP TABLE public.payment_management;
       public         heap    postgres    false            M           1259    232086    payment_management_id_seq    SEQUENCE     �   CREATE SEQUENCE public.payment_management_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 0   DROP SEQUENCE public.payment_management_id_seq;
       public          postgres    false    334            �           0    0    payment_management_id_seq    SEQUENCE OWNED BY     W   ALTER SEQUENCE public.payment_management_id_seq OWNED BY public.payment_management.id;
          public          postgres    false    333                       1259    231744    payment_status    TABLE     '  CREATE TABLE public.payment_status (
    payment_status_id integer NOT NULL,
    payment_status_name character varying(50),
    remarks character varying(1000),
    created_at timestamp with time zone,
    created_by integer,
    modified_at timestamp with time zone,
    modified_by integer
);
 "   DROP TABLE public.payment_status;
       public         heap    postgres    false                       1259    231742 $   payment_status_payment_status_id_seq    SEQUENCE     �   CREATE SEQUENCE public.payment_status_payment_status_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ;   DROP SEQUENCE public.payment_status_payment_status_id_seq;
       public          postgres    false    271            �           0    0 $   payment_status_payment_status_id_seq    SEQUENCE OWNED BY     m   ALTER SEQUENCE public.payment_status_payment_status_id_seq OWNED BY public.payment_status.payment_status_id;
          public          postgres    false    270            %           1259    231862    payment_type_details    TABLE       CREATE TABLE public.payment_type_details (
    payment_type_id integer NOT NULL,
    payment_type_name character varying(50),
    status integer,
    created_at timestamp with time zone,
    created_by integer,
    modified_at timestamp with time zone,
    modified_by integer
);
 (   DROP TABLE public.payment_type_details;
       public         heap    postgres    false            $           1259    231860 (   payment_type_details_payment_type_id_seq    SEQUENCE     �   CREATE SEQUENCE public.payment_type_details_payment_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ?   DROP SEQUENCE public.payment_type_details_payment_type_id_seq;
       public          postgres    false    293            �           0    0 (   payment_type_details_payment_type_id_seq    SEQUENCE OWNED BY     u   ALTER SEQUENCE public.payment_type_details_payment_type_id_seq OWNED BY public.payment_type_details.payment_type_id;
          public          postgres    false    292            �            1259    231615    payout_shed_details    TABLE       CREATE TABLE public.payout_shed_details (
    payout_shed_id integer NOT NULL,
    subscription_detail_id integer,
    status integer,
    created_at timestamp with time zone,
    created_by integer,
    modified_at timestamp with time zone,
    modified_by integer
);
 '   DROP TABLE public.payout_shed_details;
       public         heap    postgres    false            �            1259    231613 &   payout_shed_details_payout_shed_id_seq    SEQUENCE     �   CREATE SEQUENCE public.payout_shed_details_payout_shed_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 =   DROP SEQUENCE public.payout_shed_details_payout_shed_id_seq;
       public          postgres    false    247            �           0    0 &   payout_shed_details_payout_shed_id_seq    SEQUENCE OWNED BY     q   ALTER SEQUENCE public.payout_shed_details_payout_shed_id_seq OWNED BY public.payout_shed_details.payout_shed_id;
          public          postgres    false    246                       1259    231818    place_of_supply    TABLE     m  CREATE TABLE public.place_of_supply (
    pos_id integer NOT NULL,
    tax_name character varying(150),
    tax_code character varying(50),
    erp_id integer,
    last_integrated_date timestamp with time zone,
    status integer,
    created_at timestamp with time zone,
    created_by integer,
    modified_at timestamp with time zone,
    modified_by integer
);
 #   DROP TABLE public.place_of_supply;
       public         heap    postgres    false                       1259    231816    place_of_supply_pos_id_seq    SEQUENCE     �   CREATE SEQUENCE public.place_of_supply_pos_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 1   DROP SEQUENCE public.place_of_supply_pos_id_seq;
       public          postgres    false    285            �           0    0    place_of_supply_pos_id_seq    SEQUENCE OWNED BY     Y   ALTER SEQUENCE public.place_of_supply_pos_id_seq OWNED BY public.place_of_supply.pos_id;
          public          postgres    false    284            >           1259    232000    product_details    TABLE     �  CREATE TABLE public.product_details (
    product_id bigint NOT NULL,
    product_code character varying(50),
    product_erp_id integer,
    last_integrated_date timestamp with time zone,
    product_type integer,
    app_product_type integer,
    brand_info character varying(1000),
    model_info character varying(1000),
    car_info character varying(1000),
    seller_id bigint,
    genuine_part_no character varying(50),
    verify_button character varying(15),
    number_of_yr_used numeric,
    image character varying(255),
    image1 character varying(255),
    image2 character varying(255),
    image3 character varying(255),
    image4 character varying(255),
    image5 character varying(255),
    image6 character varying(255),
    image7 character varying(255),
    image8 character varying(255),
    image9 character varying(255),
    image10 character varying(255),
    weight character varying(10),
    dimension_length numeric(6,0),
    dimension_width numeric(6,0),
    dimension_height numeric(6,0),
    condition character varying(50),
    description character varying(1000),
    trading_type integer,
    manufacturer_name character varying(150),
    product_listing_status character varying(50),
    barcode_no numeric(50,0),
    aftermarket_no numeric(50,0),
    make_offer boolean,
    status integer,
    created_at timestamp with time zone,
    created_by integer,
    modified_at timestamp with time zone,
    modified_by integer
);
 #   DROP TABLE public.product_details;
       public         heap    postgres    false            =           1259    231998    product_details_product_id_seq    SEQUENCE     �   CREATE SEQUENCE public.product_details_product_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 5   DROP SEQUENCE public.product_details_product_id_seq;
       public          postgres    false    318            �           0    0    product_details_product_id_seq    SEQUENCE OWNED BY     a   ALTER SEQUENCE public.product_details_product_id_seq OWNED BY public.product_details.product_id;
          public          postgres    false    317            !           1259    231840    product_type_details    TABLE     B  CREATE TABLE public.product_type_details (
    product_type_id integer NOT NULL,
    product_type_name character varying(50),
    product_desc character varying(1000),
    status integer,
    created_at timestamp with time zone,
    created_by integer,
    modified_at timestamp with time zone,
    modified_by integer
);
 (   DROP TABLE public.product_type_details;
       public         heap    postgres    false                        1259    231838 (   product_type_details_product_type_id_seq    SEQUENCE     �   CREATE SEQUENCE public.product_type_details_product_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ?   DROP SEQUENCE public.product_type_details_product_type_id_seq;
       public          postgres    false    289            �           0    0 (   product_type_details_product_type_id_seq    SEQUENCE OWNED BY     u   ALTER SEQUENCE public.product_type_details_product_type_id_seq OWNED BY public.product_type_details.product_type_id;
          public          postgres    false    288            	           1259    231711    quote_status    TABLE     !  CREATE TABLE public.quote_status (
    quote_status_id integer NOT NULL,
    quote_status_name character varying(50),
    remarks character varying(1000),
    created_at timestamp with time zone,
    created_by integer,
    modified_at timestamp with time zone,
    modified_by integer
);
     DROP TABLE public.quote_status;
       public         heap    postgres    false                       1259    231709     quote_status_quote_status_id_seq    SEQUENCE     �   CREATE SEQUENCE public.quote_status_quote_status_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 7   DROP SEQUENCE public.quote_status_quote_status_id_seq;
       public          postgres    false    265            �           0    0     quote_status_quote_status_id_seq    SEQUENCE OWNED BY     e   ALTER SEQUENCE public.quote_status_quote_status_id_seq OWNED BY public.quote_status.quote_status_id;
          public          postgres    false    264                       1259    231755    reason_details    TABLE       CREATE TABLE public.reason_details (
    reason_id integer NOT NULL,
    reason_name character varying(150),
    status integer,
    created_at timestamp with time zone,
    created_by integer,
    modified_at timestamp with time zone,
    modified_by integer
);
 "   DROP TABLE public.reason_details;
       public         heap    postgres    false                       1259    231753    reason_details_reason_id_seq    SEQUENCE     �   CREATE SEQUENCE public.reason_details_reason_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 3   DROP SEQUENCE public.reason_details_reason_id_seq;
       public          postgres    false    273            �           0    0    reason_details_reason_id_seq    SEQUENCE OWNED BY     ]   ALTER SEQUENCE public.reason_details_reason_id_seq OWNED BY public.reason_details.reason_id;
          public          postgres    false    272                       1259    231689    registration_status    TABLE     %  CREATE TABLE public.registration_status (
    reg_status_id integer NOT NULL,
    reg_status_name character varying(150),
    remarks character varying(1000),
    created_at timestamp with time zone,
    created_by integer,
    modified_at timestamp with time zone,
    modified_by integer
);
 '   DROP TABLE public.registration_status;
       public         heap    postgres    false                       1259    231687 %   registration_status_reg_status_id_seq    SEQUENCE     �   CREATE SEQUENCE public.registration_status_reg_status_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 <   DROP SEQUENCE public.registration_status_reg_status_id_seq;
       public          postgres    false    261            �           0    0 %   registration_status_reg_status_id_seq    SEQUENCE OWNED BY     o   ALTER SEQUENCE public.registration_status_reg_status_id_seq OWNED BY public.registration_status.reg_status_id;
          public          postgres    false    260            �            1259    231494    reports    TABLE     !  CREATE TABLE public.reports (
    report_id integer NOT NULL,
    report_name character varying(50),
    "Login_user_type_id" integer,
    status integer,
    created_at timestamp with time zone,
    created_by integer,
    modified_at timestamp with time zone,
    modified_by integer
);
    DROP TABLE public.reports;
       public         heap    postgres    false            �            1259    231492    reports_report_id_seq    SEQUENCE     �   CREATE SEQUENCE public.reports_report_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public.reports_report_id_seq;
       public          postgres    false    225            �           0    0    reports_report_id_seq    SEQUENCE OWNED BY     O   ALTER SEQUENCE public.reports_report_id_seq OWNED BY public.reports.report_id;
          public          postgres    false    224            �            1259    231453    role_permission    TABLE     �  CREATE TABLE public.role_permission (
    role_permission_id integer NOT NULL,
    module_id integer,
    dashboard_id integer,
    report_id integer,
    role_id integer,
    add_access boolean,
    delete_access boolean,
    read_access boolean,
    modify_access boolean,
    export_access boolean,
    print_access boolean,
    created_at timestamp with time zone,
    created_by integer,
    modified_at timestamp with time zone,
    modified_by integer
);
 #   DROP TABLE public.role_permission;
       public         heap    postgres    false            �            1259    231451 &   role_permission_role_permission_id_seq    SEQUENCE     �   CREATE SEQUENCE public.role_permission_role_permission_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 =   DROP SEQUENCE public.role_permission_role_permission_id_seq;
       public          postgres    false    217            �           0    0 &   role_permission_role_permission_id_seq    SEQUENCE OWNED BY     q   ALTER SEQUENCE public.role_permission_role_permission_id_seq OWNED BY public.role_permission.role_permission_id;
          public          postgres    false    216            �            1259    231442    roles    TABLE     B  CREATE TABLE public.roles (
    role_id integer NOT NULL,
    role_code character varying(50),
    role_name character varying(50),
    status integer,
    action character varying(1000),
    created_at timestamp with time zone,
    created_by integer,
    modified_at timestamp with time zone,
    modified_by integer
);
    DROP TABLE public.roles;
       public         heap    postgres    false            �            1259    231440    roles_role_id_seq    SEQUENCE     �   CREATE SEQUENCE public.roles_role_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.roles_role_id_seq;
       public          postgres    false    215            �           0    0    roles_role_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.roles_role_id_seq OWNED BY public.roles.role_id;
          public          postgres    false    214            :           1259    231981    seller_branch    TABLE     �  CREATE TABLE public.seller_branch (
    "Seller_branch_id" integer,
    seller_id bigint,
    seller_branch_name character varying(50),
    branch_country integer,
    branch_state integer,
    branch_city character varying(50),
    branch_address_1 character varying(1000),
    branch_address_2 character varying(1000),
    branch_phone numeric(15,0),
    phone_country_code character varying(50),
    branch_drop_address character varying(1000),
    branch_googlemap_link character varying(1000)
);
 !   DROP TABLE public.seller_branch;
       public         heap    postgres    false            <           1259    231989    seller_warehouse    TABLE     1  CREATE TABLE public.seller_warehouse (
    warehouse_id integer NOT NULL,
    "Seller_branch_id" integer,
    seller_id bigint,
    warehouse_name character varying(150),
    warehouse_phone numeric(15,0),
    warehouse_country_code character varying(50),
    warehouse_address character varying(1000)
);
 $   DROP TABLE public.seller_warehouse;
       public         heap    postgres    false            ;           1259    231987 !   seller_warehouse_warehouse_id_seq    SEQUENCE     �   CREATE SEQUENCE public.seller_warehouse_warehouse_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 8   DROP SEQUENCE public.seller_warehouse_warehouse_id_seq;
       public          postgres    false    316            �           0    0 !   seller_warehouse_warehouse_id_seq    SEQUENCE OWNED BY     g   ALTER SEQUENCE public.seller_warehouse_warehouse_id_seq OWNED BY public.seller_warehouse.warehouse_id;
          public          postgres    false    315            l           1259    232253    seo_content    TABLE     �  CREATE TABLE public.seo_content (
    id integer NOT NULL,
    page_title character varying(50),
    meta_tag character varying(1000),
    seo_keywords character varying(1000),
    seo_content character varying(1000),
    status integer,
    action character varying(1000),
    created_at timestamp with time zone,
    created_by integer,
    modified_at timestamp with time zone,
    modified_by integer
);
    DROP TABLE public.seo_content;
       public         heap    postgres    false            k           1259    232251    seo_content_id_seq    SEQUENCE     �   CREATE SEQUENCE public.seo_content_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.seo_content_id_seq;
       public          postgres    false    364            �           0    0    seo_content_id_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE public.seo_content_id_seq OWNED BY public.seo_content.id;
          public          postgres    false    363                       1259    231667    shipment_status    TABLE     "  CREATE TABLE public.shipment_status (
    ship_status_id integer NOT NULL,
    ship_status_name character varying(50),
    remarks character varying(1000),
    created_at timestamp with time zone,
    created_by integer,
    modified_at timestamp with time zone,
    modified_by integer
);
 #   DROP TABLE public.shipment_status;
       public         heap    postgres    false                        1259    231665 "   shipment_status_ship_status_id_seq    SEQUENCE     �   CREATE SEQUENCE public.shipment_status_ship_status_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 9   DROP SEQUENCE public.shipment_status_ship_status_id_seq;
       public          postgres    false    257            �           0    0 "   shipment_status_ship_status_id_seq    SEQUENCE OWNED BY     i   ALTER SEQUENCE public.shipment_status_ship_status_id_seq OWNED BY public.shipment_status.ship_status_id;
          public          postgres    false    256            p           1259    232275    sms_template    TABLE     a  CREATE TABLE public.sms_template (
    id integer NOT NULL,
    module_id integer,
    title_name character varying(150),
    sms_content character varying(1000),
    status integer,
    action character varying(1000),
    created_at timestamp with time zone,
    created_by integer,
    modified_at timestamp with time zone,
    modified_by integer
);
     DROP TABLE public.sms_template;
       public         heap    postgres    false            o           1259    232273    sms_template_id_seq    SEQUENCE     �   CREATE SEQUENCE public.sms_template_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.sms_template_id_seq;
       public          postgres    false    368                        0    0    sms_template_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE public.sms_template_id_seq OWNED BY public.sms_template.id;
          public          postgres    false    367                       1259    231788    state_details    TABLE       CREATE TABLE public.state_details (
    id integer NOT NULL,
    country_id integer,
    state_name character varying(50),
    status integer,
    created_at timestamp with time zone,
    created_by integer,
    modified_at timestamp with time zone,
    modified_by integer
);
 !   DROP TABLE public.state_details;
       public         heap    postgres    false                       1259    231786    state_details_id_seq    SEQUENCE     �   CREATE SEQUENCE public.state_details_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.state_details_id_seq;
       public          postgres    false    279                       0    0    state_details_id_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.state_details_id_seq OWNED BY public.state_details.id;
          public          postgres    false    278            �            1259    231645    status_details    TABLE     �   CREATE TABLE public.status_details (
    status_id integer NOT NULL,
    status_name character varying(50),
    created_at timestamp with time zone,
    created_by integer,
    modified_at timestamp with time zone,
    modified_by integer
);
 "   DROP TABLE public.status_details;
       public         heap    postgres    false            �            1259    231643    status_details_status_id_seq    SEQUENCE     �   CREATE SEQUENCE public.status_details_status_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 3   DROP SEQUENCE public.status_details_status_id_seq;
       public          postgres    false    253                       0    0    status_details_status_id_seq    SEQUENCE OWNED BY     ]   ALTER SEQUENCE public.status_details_status_id_seq OWNED BY public.status_details.status_id;
          public          postgres    false    252            1           1259    231928    sub_group_management    TABLE     o  CREATE TABLE public.sub_group_management (
    sub_group_id integer NOT NULL,
    erp_id integer,
    last_integrated_date timestamp with time zone,
    group_id integer,
    sub_group_name character varying(50),
    status integer,
    created_at timestamp with time zone,
    created_by integer,
    modified_at timestamp with time zone,
    modified_by integer
);
 (   DROP TABLE public.sub_group_management;
       public         heap    postgres    false            0           1259    231926 %   sub_group_management_sub_group_id_seq    SEQUENCE     �   CREATE SEQUENCE public.sub_group_management_sub_group_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 <   DROP SEQUENCE public.sub_group_management_sub_group_id_seq;
       public          postgres    false    305                       0    0 %   sub_group_management_sub_group_id_seq    SEQUENCE OWNED BY     o   ALTER SEQUENCE public.sub_group_management_sub_group_id_seq OWNED BY public.sub_group_management.sub_group_id;
          public          postgres    false    304            3           1259    231939    sub_node_management    TABLE     �  CREATE TABLE public.sub_node_management (
    sub_node_id integer NOT NULL,
    sub_group_id integer,
    sub_node_code character varying(50),
    sub_node_name character varying(150),
    erp_id integer,
    last_integrated_date timestamp with time zone,
    status integer,
    created_at timestamp with time zone,
    created_by integer,
    modified_at timestamp with time zone,
    modified_by integer
);
 '   DROP TABLE public.sub_node_management;
       public         heap    postgres    false            2           1259    231937 #   sub_node_management_sub_node_id_seq    SEQUENCE     �   CREATE SEQUENCE public.sub_node_management_sub_node_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 :   DROP SEQUENCE public.sub_node_management_sub_node_id_seq;
       public          postgres    false    307                       0    0 #   sub_node_management_sub_node_id_seq    SEQUENCE OWNED BY     k   ALTER SEQUENCE public.sub_node_management_sub_node_id_seq OWNED BY public.sub_node_management.sub_node_id;
          public          postgres    false    306            D           1259    232033    sub_order_details    TABLE     �  CREATE TABLE public.sub_order_details (
    id bigint NOT NULL,
    order_id bigint,
    quantity numeric(6,0),
    price numeric(10,0),
    vat_amount numeric(10,0),
    total_price numeric(10,0),
    product_type_id integer,
    trading_type_id integer,
    status integer,
    seller_id integer,
    delivery_time character varying(15),
    discount numeric(10,0),
    shipping_charges numeric(10,0),
    brand_id integer,
    ship_status_id integer,
    order_status_id integer,
    sku character varying(150),
    product_id integer,
    brand_info character varying(1000),
    model_info character varying(1000),
    car_info character varying(1000),
    ship_by_date timestamp with time zone,
    deliver_by_date timestamp with time zone,
    shipped_by character varying(150),
    shipped_person character varying(150),
    shipped_person_contact_no character varying(15),
    tracking_id character varying(50),
    shipping_date timestamp with time zone,
    pickup_date timestamp with time zone,
    package_type character varying(50),
    package_dimension character varying(50),
    package_weight numeric(6,0),
    total_shipping_cost numeric(10,0),
    seller_note character varying(1000),
    buyer_feedback character varying(1000),
    total_vat_amt numeric(10,0),
    created_at timestamp with time zone,
    created_by integer,
    modified_at timestamp with time zone,
    modified_by integer
);
 %   DROP TABLE public.sub_order_details;
       public         heap    postgres    false            C           1259    232031    sub_order_details_id_seq    SEQUENCE     �   CREATE SEQUENCE public.sub_order_details_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 /   DROP SEQUENCE public.sub_order_details_id_seq;
       public          postgres    false    324                       0    0    sub_order_details_id_seq    SEQUENCE OWNED BY     U   ALTER SEQUENCE public.sub_order_details_id_seq OWNED BY public.sub_order_details.id;
          public          postgres    false    323            H           1259    232055    sub_order_quote_details    TABLE     �  CREATE TABLE public.sub_order_quote_details (
    id integer NOT NULL,
    order_quote_id integer,
    seller_id integer,
    product_id integer,
    brand_info character varying(1000),
    model_info character varying(1000),
    car_info character varying(1000),
    quantity numeric(6,0),
    product_price numeric(10,0),
    product_vat_amount numeric(10,0),
    total_vat_amount numeric(10,0),
    total_price numeric(10,0),
    image character varying(255),
    product_type_id integer,
    trading_type_id integer,
    delivery_time character varying(15),
    discount numeric(10,0),
    shipping_charges numeric(10,0),
    ship_status_id integer,
    order_status_id integer,
    sku character varying(10),
    ship_by_date timestamp with time zone,
    deliver_by_date timestamp with time zone,
    shipped_by character varying(50),
    shipped_person character varying(50),
    shipped_person_contact_no character varying(50),
    tracking_id character varying(10),
    shipping_date timestamp with time zone,
    pickup_date timestamp with time zone,
    package_type character varying(50),
    package_dimension character varying(50),
    package_weight numeric(50,0),
    total_shipping_cost numeric(10,0),
    seller_note character varying(1000),
    action character varying(1000),
    status integer,
    created_at timestamp with time zone,
    created_by integer,
    modified_at timestamp with time zone,
    modified_by integer
);
 +   DROP TABLE public.sub_order_quote_details;
       public         heap    postgres    false            G           1259    232053    sub_order_quote_details_id_seq    SEQUENCE     �   CREATE SEQUENCE public.sub_order_quote_details_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 5   DROP SEQUENCE public.sub_order_quote_details_id_seq;
       public          postgres    false    328                       0    0    sub_order_quote_details_id_seq    SEQUENCE OWNED BY     a   ALTER SEQUENCE public.sub_order_quote_details_id_seq OWNED BY public.sub_order_quote_details.id;
          public          postgres    false    327            �            1259    231593    subscription_detail    TABLE     �  CREATE TABLE public.subscription_detail (
    subscription_detail_id integer NOT NULL,
    subscription_plan_id integer,
    "Login_user_type_id" integer,
    user_id integer,
    valid_till timestamp with time zone,
    effective timestamp with time zone,
    status integer,
    action character varying(1000),
    created_at timestamp with time zone,
    created_by integer,
    modified_at timestamp with time zone,
    modified_by integer
);
 '   DROP TABLE public.subscription_detail;
       public         heap    postgres    false            �            1259    231591 .   subscription_detail_subscription_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.subscription_detail_subscription_detail_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 E   DROP SEQUENCE public.subscription_detail_subscription_detail_id_seq;
       public          postgres    false    243                       0    0 .   subscription_detail_subscription_detail_id_seq    SEQUENCE OWNED BY     �   ALTER SEQUENCE public.subscription_detail_subscription_detail_id_seq OWNED BY public.subscription_detail.subscription_detail_id;
          public          postgres    false    242            �            1259    231604    subscription_plan_details    TABLE     8  CREATE TABLE public.subscription_plan_details (
    subscription_plan_id integer NOT NULL,
    "Login_user_type_id" integer,
    subscription_name character varying(150),
    number_of_users integer,
    modules_list character varying(250),
    days character varying(6),
    price numeric(10,0),
    valid_till timestamp with time zone,
    effective timestamp with time zone,
    status integer,
    action character varying(1000),
    created_at timestamp with time zone,
    created_by integer,
    modified_at timestamp with time zone,
    modified_by integer
);
 -   DROP TABLE public.subscription_plan_details;
       public         heap    postgres    false            �            1259    231602 2   subscription_plan_details_subscription_plan_id_seq    SEQUENCE     �   CREATE SEQUENCE public.subscription_plan_details_subscription_plan_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 I   DROP SEQUENCE public.subscription_plan_details_subscription_plan_id_seq;
       public          postgres    false    245                       0    0 2   subscription_plan_details_subscription_plan_id_seq    SEQUENCE OWNED BY     �   ALTER SEQUENCE public.subscription_plan_details_subscription_plan_id_seq OWNED BY public.subscription_plan_details.subscription_plan_id;
          public          postgres    false    244            �            1259    231379    super_admin    TABLE     	  CREATE TABLE public.super_admin (
    id integer NOT NULL,
    user_id character varying(50),
    password character varying(250),
    created_at timestamp with time zone,
    created_by integer,
    modified_at timestamp with time zone,
    modified_by integer
);
    DROP TABLE public.super_admin;
       public         heap    postgres    false            �            1259    231377    super_admin_id_seq    SEQUENCE     �   CREATE SEQUENCE public.super_admin_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.super_admin_id_seq;
       public          postgres    false    203            	           0    0    super_admin_id_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE public.super_admin_id_seq OWNED BY public.super_admin.id;
          public          postgres    false    202                       1259    231807    tax_code_details    TABLE       CREATE TABLE public.tax_code_details (
    tax_code_id integer NOT NULL,
    tax_code_name character varying(50),
    status integer,
    created_at timestamp with time zone,
    created_by integer,
    modified_at timestamp with time zone,
    modified_by integer
);
 $   DROP TABLE public.tax_code_details;
       public         heap    postgres    false                       1259    231805     tax_code_details_tax_code_id_seq    SEQUENCE     �   CREATE SEQUENCE public.tax_code_details_tax_code_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 7   DROP SEQUENCE public.tax_code_details_tax_code_id_seq;
       public          postgres    false    283            
           0    0     tax_code_details_tax_code_id_seq    SEQUENCE OWNED BY     e   ALTER SEQUENCE public.tax_code_details_tax_code_id_seq OWNED BY public.tax_code_details.tax_code_id;
          public          postgres    false    282            #           1259    231851    trading_type_details    TABLE       CREATE TABLE public.trading_type_details (
    trading_type_id integer NOT NULL,
    trading_type_name character varying(50),
    status integer,
    created_at timestamp with time zone,
    created_by integer,
    modified_at timestamp with time zone,
    modified_by integer
);
 (   DROP TABLE public.trading_type_details;
       public         heap    postgres    false            "           1259    231849 (   trading_type_details_trading_type_id_seq    SEQUENCE     �   CREATE SEQUENCE public.trading_type_details_trading_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ?   DROP SEQUENCE public.trading_type_details_trading_type_id_seq;
       public          postgres    false    291                       0    0 (   trading_type_details_trading_type_id_seq    SEQUENCE OWNED BY     u   ALTER SEQUENCE public.trading_type_details_trading_type_id_seq OWNED BY public.trading_type_details.trading_type_id;
          public          postgres    false    290            �            1259    231420    user_managment    TABLE     �  CREATE TABLE public.user_managment (
    id bigint NOT NULL,
    "Login_user_type_id" integer,
    user_name character varying(150),
    user_email_id character varying(50),
    phone_no character varying(15),
    user_password character varying(250),
    two_way_auth boolean,
    status integer,
    last_login timestamp with time zone,
    created_at timestamp with time zone,
    created_by integer,
    modified_at timestamp with time zone,
    modified_by integer
);
 "   DROP TABLE public.user_managment;
       public         heap    postgres    false            �            1259    231418    user_managment_id_seq    SEQUENCE     ~   CREATE SEQUENCE public.user_managment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public.user_managment_id_seq;
       public          postgres    false    211                       0    0    user_managment_id_seq    SEQUENCE OWNED BY     O   ALTER SEQUENCE public.user_managment_id_seq OWNED BY public.user_managment.id;
          public          postgres    false    210            P           1259    232099    vin_history    TABLE     .  CREATE TABLE public.vin_history (
    id integer NOT NULL,
    vin_number character varying(50),
    vehicle_identified character varying(50),
    brand_id integer,
    model_id integer,
    user_type_info character varying(1000),
    user_name character varying(150),
    user_id integer,
    region character varying(50),
    searched_date timestamp with time zone,
    status integer,
    action character varying(1000),
    created_at timestamp with time zone,
    created_by integer,
    modified_at timestamp with time zone,
    modified_by integer
);
    DROP TABLE public.vin_history;
       public         heap    postgres    false            O           1259    232097    vin_history_id_seq    SEQUENCE     �   CREATE SEQUENCE public.vin_history_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.vin_history_id_seq;
       public          postgres    false    336                       0    0    vin_history_id_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE public.vin_history_id_seq OWNED BY public.vin_history.id;
          public          postgres    false    335            �           2604    231434 "   Login_user_type Login_user_type_id    DEFAULT     �   ALTER TABLE ONLY public."Login_user_type" ALTER COLUMN "Login_user_type_id" SET DEFAULT nextval('public."Login_user_type_Login_user_type_id_seq"'::regclass);
 U   ALTER TABLE public."Login_user_type" ALTER COLUMN "Login_user_type_id" DROP DEFAULT;
       public          postgres    false    213    212    213            �           2604    232190    Rating_review id    DEFAULT     x   ALTER TABLE ONLY public."Rating_review" ALTER COLUMN id SET DEFAULT nextval('public."Rating_review_id_seq"'::regclass);
 A   ALTER TABLE public."Rating_review" ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    351    352    352            �           2604    231802    VAT_details id    DEFAULT     t   ALTER TABLE ONLY public."VAT_details" ALTER COLUMN id SET DEFAULT nextval('public."VAT_details_id_seq"'::regclass);
 ?   ALTER TABLE public."VAT_details" ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    280    281    281            �           2604    231404 $   application_user application_user_id    DEFAULT     �   ALTER TABLE ONLY public.application_user ALTER COLUMN application_user_id SET DEFAULT nextval('public.application_user_application_user_id_seq'::regclass);
 S   ALTER TABLE public.application_user ALTER COLUMN application_user_id DROP DEFAULT;
       public          postgres    false    207    206    207            �           2604    231415    application_user_permissions id    DEFAULT     �   ALTER TABLE ONLY public.application_user_permissions ALTER COLUMN id SET DEFAULT nextval('public.application_user_permissions_id_seq'::regclass);
 N   ALTER TABLE public.application_user_permissions ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    208    209    209            �           2604    232245    audit_trail id    DEFAULT     p   ALTER TABLE ONLY public.audit_trail ALTER COLUMN id SET DEFAULT nextval('public.audit_trail_id_seq'::regclass);
 =   ALTER TABLE public.audit_trail ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    361    362    362            �           2604    231876    brands brand_id    DEFAULT     r   ALTER TABLE ONLY public.brands ALTER COLUMN brand_id SET DEFAULT nextval('public.brands_brand_id_seq'::regclass);
 >   ALTER TABLE public.brands ALTER COLUMN brand_id DROP DEFAULT;
       public          postgres    false    294    295    295            �           2604    231637    bulk_upload_details id    DEFAULT     �   ALTER TABLE ONLY public.bulk_upload_details ALTER COLUMN id SET DEFAULT nextval('public.bulk_upload_details_id_seq'::regclass);
 E   ALTER TABLE public.bulk_upload_details ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    251    250    251            �           2604    232146 (   buyer_customer_details buyer_customer_id    DEFAULT     �   ALTER TABLE ONLY public.buyer_customer_details ALTER COLUMN buyer_customer_id SET DEFAULT nextval('public.buyer_customer_details_buyer_customer_id_seq'::regclass);
 W   ALTER TABLE public.buyer_customer_details ALTER COLUMN buyer_customer_id DROP DEFAULT;
       public          postgres    false    343    344    344            �           2604    232124     buyer_saved_quote buyer_quote_id    DEFAULT     �   ALTER TABLE ONLY public.buyer_saved_quote ALTER COLUMN buyer_quote_id SET DEFAULT nextval('public.buyer_saved_quote_buyer_quote_id_seq'::regclass);
 O   ALTER TABLE public.buyer_saved_quote ALTER COLUMN buyer_quote_id DROP DEFAULT;
       public          postgres    false    339    340    340            �           2604    232135 "   buyer_saved_quote_service_items id    DEFAULT     �   ALTER TABLE ONLY public.buyer_saved_quote_service_items ALTER COLUMN id SET DEFAULT nextval('public.buyer_saved_quote_service_items_id_seq'::regclass);
 Q   ALTER TABLE public.buyer_saved_quote_service_items ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    342    341    342            �           2604    232069    car_management id    DEFAULT     v   ALTER TABLE ONLY public.car_management ALTER COLUMN id SET DEFAULT nextval('public.car_management_id_seq'::regclass);
 @   ALTER TABLE public.car_management ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    329    330    330            �           2604    231909    cars car_id    DEFAULT     j   ALTER TABLE ONLY public.cars ALTER COLUMN car_id SET DEFAULT nextval('public.cars_car_id_seq'::regclass);
 :   ALTER TABLE public.cars ALTER COLUMN car_id DROP DEFAULT;
       public          postgres    false    301    300    301            �           2604    232157    cart_details cart_item_id    DEFAULT     �   ALTER TABLE ONLY public.cart_details ALTER COLUMN cart_item_id SET DEFAULT nextval('public.cart_details_cart_item_id_seq'::regclass);
 H   ALTER TABLE public.cart_details ALTER COLUMN cart_item_id DROP DEFAULT;
       public          postgres    false    346    345    346            �           2604    232113    category_request_management id    DEFAULT     �   ALTER TABLE ONLY public.category_request_management ALTER COLUMN id SET DEFAULT nextval('public.category_request_management_id_seq'::regclass);
 M   ALTER TABLE public.category_request_management ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    337    338    338            �           2604    231736 )   category_request_status cat_req_status_id    DEFAULT     �   ALTER TABLE ONLY public.category_request_status ALTER COLUMN cat_req_status_id SET DEFAULT nextval('public.category_request_status_cat_req_status_id_seq'::regclass);
 X   ALTER TABLE public.category_request_status ALTER COLUMN cat_req_status_id DROP DEFAULT;
       public          postgres    false    269    268    269            �           2604    231508    cms_company_info id    DEFAULT     z   ALTER TABLE ONLY public.cms_company_info ALTER COLUMN id SET DEFAULT nextval('public.cms_company_info_id_seq'::regclass);
 B   ALTER TABLE public.cms_company_info ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    227    226    227            �           2604    231585    cms_footer_links id    DEFAULT     z   ALTER TABLE ONLY public.cms_footer_links ALTER COLUMN id SET DEFAULT nextval('public.cms_footer_links_id_seq'::regclass);
 B   ALTER TABLE public.cms_footer_links ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    241    240    241            �           2604    231563    cms_homepage_about_section id    DEFAULT     �   ALTER TABLE ONLY public.cms_homepage_about_section ALTER COLUMN id SET DEFAULT nextval('public.cms_homepage_about_section_id_seq'::regclass);
 L   ALTER TABLE public.cms_homepage_about_section ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    237    236    237            �           2604    231541    cms_homepage_banner_section id    DEFAULT     �   ALTER TABLE ONLY public.cms_homepage_banner_section ALTER COLUMN id SET DEFAULT nextval('public.cms_homepage_banner_section_id_seq'::regclass);
 M   ALTER TABLE public.cms_homepage_banner_section ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    233    232    233            �           2604    231574 #   cms_homepage_buyerseller_section id    DEFAULT     �   ALTER TABLE ONLY public.cms_homepage_buyerseller_section ALTER COLUMN id SET DEFAULT nextval('public.cms_homepage_buyerseller_section_id_seq'::regclass);
 R   ALTER TABLE public.cms_homepage_buyerseller_section ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    239    238    239            �           2604    231552     cms_homepage_features_section id    DEFAULT     �   ALTER TABLE ONLY public.cms_homepage_features_section ALTER COLUMN id SET DEFAULT nextval('public.cms_homepage_features_section_id_seq'::regclass);
 O   ALTER TABLE public.cms_homepage_features_section ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    234    235    235            �           2604    231530    cms_menu_section id    DEFAULT     z   ALTER TABLE ONLY public.cms_menu_section ALTER COLUMN id SET DEFAULT nextval('public.cms_menu_section_id_seq'::regclass);
 B   ALTER TABLE public.cms_menu_section ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    230    231    231            �           2604    231519    cms_social_media_info id    DEFAULT     �   ALTER TABLE ONLY public.cms_social_media_info ALTER COLUMN id SET DEFAULT nextval('public.cms_social_media_info_id_seq'::regclass);
 G   ALTER TABLE public.cms_social_media_info ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    228    229    229            �           2604    232014    commission_management id    DEFAULT     �   ALTER TABLE ONLY public.commission_management ALTER COLUMN id SET DEFAULT nextval('public.commission_management_id_seq'::regclass);
 G   ALTER TABLE public.commission_management ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    320    319    320            �           2604    231626    configuration_details id    DEFAULT     �   ALTER TABLE ONLY public.configuration_details ALTER COLUMN id SET DEFAULT nextval('public.configuration_details_id_seq'::regclass);
 G   ALTER TABLE public.configuration_details ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    249    248    249            �           2604    231780    country_details country_id    DEFAULT     �   ALTER TABLE ONLY public.country_details ALTER COLUMN country_id SET DEFAULT nextval('public.country_details_country_id_seq'::regclass);
 I   ALTER TABLE public.country_details ALTER COLUMN country_id DROP DEFAULT;
       public          postgres    false    276    277    277            �           2604    231769    currency_details currency_id    DEFAULT     �   ALTER TABLE ONLY public.currency_details ALTER COLUMN currency_id SET DEFAULT nextval('public.currency_details_currency_id_seq'::regclass);
 K   ALTER TABLE public.currency_details ALTER COLUMN currency_id DROP DEFAULT;
       public          postgres    false    274    275    275            �           2604    231486    dashboard dashboard_id    DEFAULT     �   ALTER TABLE ONLY public.dashboard ALTER COLUMN dashboard_id SET DEFAULT nextval('public.dashboard_dashboard_id_seq'::regclass);
 E   ALTER TABLE public.dashboard ALTER COLUMN dashboard_id DROP DEFAULT;
       public          postgres    false    222    223    223            �           2604    232267    email_template id    DEFAULT     v   ALTER TABLE ONLY public.email_template ALTER COLUMN id SET DEFAULT nextval('public.email_template_id_seq'::regclass);
 @   ALTER TABLE public.email_template ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    365    366    366            �           2604    231920    group_management group_id    DEFAULT     �   ALTER TABLE ONLY public.group_management ALTER COLUMN group_id SET DEFAULT nextval('public.group_management_group_id_seq'::regclass);
 H   ALTER TABLE public.group_management ALTER COLUMN group_id DROP DEFAULT;
       public          postgres    false    303    302    303            �           2604    232234    help_management id    DEFAULT     x   ALTER TABLE ONLY public.help_management ALTER COLUMN id SET DEFAULT nextval('public.help_management_id_seq'::regclass);
 A   ALTER TABLE public.help_management ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    359    360    360            �           2604    231393    intermediator id    DEFAULT     t   ALTER TABLE ONLY public.intermediator ALTER COLUMN id SET DEFAULT nextval('public.intermediator_id_seq'::regclass);
 ?   ALTER TABLE public.intermediator ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    205    204    205            �           2604    232223    inventory_manangement id    DEFAULT     �   ALTER TABLE ONLY public.inventory_manangement ALTER COLUMN id SET DEFAULT nextval('public.inventory_manangement_id_seq'::regclass);
 G   ALTER TABLE public.inventory_manangement ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    358    357    358            �           2604    231832    jurisdiction jurisdiction_id    DEFAULT     �   ALTER TABLE ONLY public.jurisdiction ALTER COLUMN jurisdiction_id SET DEFAULT nextval('public.jurisdiction_jurisdiction_id_seq'::regclass);
 K   ALTER TABLE public.jurisdiction ALTER COLUMN jurisdiction_id DROP DEFAULT;
       public          postgres    false    287    286    287            �           2604    231725 &   make_offer_status make_offer_status_id    DEFAULT     �   ALTER TABLE ONLY public.make_offer_status ALTER COLUMN make_offer_status_id SET DEFAULT nextval('public.make_offer_status_make_offer_status_id_seq'::regclass);
 U   ALTER TABLE public.make_offer_status ALTER COLUMN make_offer_status_id DROP DEFAULT;
       public          postgres    false    266    267    267            �           2604    231887    models model_id    DEFAULT     r   ALTER TABLE ONLY public.models ALTER COLUMN model_id SET DEFAULT nextval('public.models_model_id_seq'::regclass);
 >   ALTER TABLE public.models ALTER COLUMN model_id DROP DEFAULT;
       public          postgres    false    296    297    297            �           2604    231475    module module_id    DEFAULT     t   ALTER TABLE ONLY public.module ALTER COLUMN module_id SET DEFAULT nextval('public.module_module_id_seq'::regclass);
 ?   ALTER TABLE public.module ALTER COLUMN module_id DROP DEFAULT;
       public          postgres    false    220    221    221            �           2604    231464    module_type module_type_id    DEFAULT     �   ALTER TABLE ONLY public.module_type ALTER COLUMN module_type_id SET DEFAULT nextval('public.module_type_module_type_id_seq'::regclass);
 I   ALTER TABLE public.module_type ALTER COLUMN module_type_id DROP DEFAULT;
       public          postgres    false    219    218    219            �           2604    231964    new_buyer_details id    DEFAULT     |   ALTER TABLE ONLY public.new_buyer_details ALTER COLUMN id SET DEFAULT nextval('public.new_buyer_details_id_seq'::regclass);
 C   ALTER TABLE public.new_buyer_details ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    310    311    311            �           2604    231975    new_seller_details id    DEFAULT     ~   ALTER TABLE ONLY public.new_seller_details ALTER COLUMN id SET DEFAULT nextval('public.new_seller_details_id_seq'::regclass);
 D   ALTER TABLE public.new_seller_details ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    312    313    313            �           2604    232168    newsletter_management id    DEFAULT     �   ALTER TABLE ONLY public.newsletter_management ALTER COLUMN id SET DEFAULT nextval('public.newsletter_management_id_seq'::regclass);
 G   ALTER TABLE public.newsletter_management ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    348    347    348            �           2604    232179    notification id    DEFAULT     r   ALTER TABLE ONLY public.notification ALTER COLUMN id SET DEFAULT nextval('public.notification_id_seq'::regclass);
 >   ALTER TABLE public.notification ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    350    349    350            �           2604    232212    offer_management id    DEFAULT     z   ALTER TABLE ONLY public.offer_management ALTER COLUMN id SET DEFAULT nextval('public.offer_management_id_seq'::regclass);
 B   ALTER TABLE public.offer_management ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    356    355    356            �           2604    232025    order_management order_id    DEFAULT     �   ALTER TABLE ONLY public.order_management ALTER COLUMN order_id SET DEFAULT nextval('public.order_management_order_id_seq'::regclass);
 H   ALTER TABLE public.order_management ALTER COLUMN order_id DROP DEFAULT;
       public          postgres    false    321    322    322            �           2604    232047 !   order_quote_Manage order_quote_id    DEFAULT     �   ALTER TABLE ONLY public."order_quote_Manage" ALTER COLUMN order_quote_id SET DEFAULT nextval('public."order_quote_Manage_order_quote_id_seq"'::regclass);
 R   ALTER TABLE public."order_quote_Manage" ALTER COLUMN order_quote_id DROP DEFAULT;
       public          postgres    false    325    326    326            �           2604    231681    order_status order_status_id    DEFAULT     �   ALTER TABLE ONLY public.order_status ALTER COLUMN order_status_id SET DEFAULT nextval('public.order_status_order_status_id_seq'::regclass);
 K   ALTER TABLE public.order_status ALTER COLUMN order_status_id DROP DEFAULT;
       public          postgres    false    259    258    259            �           2604    231659     order_type_details order_type_id    DEFAULT     �   ALTER TABLE ONLY public.order_type_details ALTER COLUMN order_type_id SET DEFAULT nextval('public.order_type_details_order_type_id_seq'::regclass);
 O   ALTER TABLE public.order_type_details ALTER COLUMN order_type_id DROP DEFAULT;
       public          postgres    false    255    254    255            �           2604    231898    parameters id    DEFAULT     n   ALTER TABLE ONLY public.parameters ALTER COLUMN id SET DEFAULT nextval('public.parameters_id_seq'::regclass);
 <   ALTER TABLE public.parameters ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    298    299    299            �           2604    232201    part_request_unavail id    DEFAULT     �   ALTER TABLE ONLY public.part_request_unavail ALTER COLUMN id SET DEFAULT nextval('public.part_request_unavail_id_seq'::regclass);
 F   ALTER TABLE public.part_request_unavail ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    354    353    354            �           2604    231953    parts_management id    DEFAULT     z   ALTER TABLE ONLY public.parts_management ALTER COLUMN id SET DEFAULT nextval('public.parts_management_id_seq'::regclass);
 B   ALTER TABLE public.parts_management ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    309    308    309            �           2604    231703 $   parts_req_status parts_req_status_id    DEFAULT     �   ALTER TABLE ONLY public.parts_req_status ALTER COLUMN parts_req_status_id SET DEFAULT nextval('public.parts_req_status_parts_req_status_id_seq'::regclass);
 S   ALTER TABLE public.parts_req_status ALTER COLUMN parts_req_status_id DROP DEFAULT;
       public          postgres    false    263    262    263            �           2604    232080    pay_out_schedule_management id    DEFAULT     �   ALTER TABLE ONLY public.pay_out_schedule_management ALTER COLUMN id SET DEFAULT nextval('public.pay_out_schedule_management_id_seq'::regclass);
 M   ALTER TABLE public.pay_out_schedule_management ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    331    332    332            �           2604    232091    payment_management id    DEFAULT     ~   ALTER TABLE ONLY public.payment_management ALTER COLUMN id SET DEFAULT nextval('public.payment_management_id_seq'::regclass);
 D   ALTER TABLE public.payment_management ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    334    333    334            �           2604    231747     payment_status payment_status_id    DEFAULT     �   ALTER TABLE ONLY public.payment_status ALTER COLUMN payment_status_id SET DEFAULT nextval('public.payment_status_payment_status_id_seq'::regclass);
 O   ALTER TABLE public.payment_status ALTER COLUMN payment_status_id DROP DEFAULT;
       public          postgres    false    271    270    271            �           2604    231865 $   payment_type_details payment_type_id    DEFAULT     �   ALTER TABLE ONLY public.payment_type_details ALTER COLUMN payment_type_id SET DEFAULT nextval('public.payment_type_details_payment_type_id_seq'::regclass);
 S   ALTER TABLE public.payment_type_details ALTER COLUMN payment_type_id DROP DEFAULT;
       public          postgres    false    292    293    293            �           2604    231618 "   payout_shed_details payout_shed_id    DEFAULT     �   ALTER TABLE ONLY public.payout_shed_details ALTER COLUMN payout_shed_id SET DEFAULT nextval('public.payout_shed_details_payout_shed_id_seq'::regclass);
 Q   ALTER TABLE public.payout_shed_details ALTER COLUMN payout_shed_id DROP DEFAULT;
       public          postgres    false    247    246    247            �           2604    231821    place_of_supply pos_id    DEFAULT     �   ALTER TABLE ONLY public.place_of_supply ALTER COLUMN pos_id SET DEFAULT nextval('public.place_of_supply_pos_id_seq'::regclass);
 E   ALTER TABLE public.place_of_supply ALTER COLUMN pos_id DROP DEFAULT;
       public          postgres    false    285    284    285            �           2604    232003    product_details product_id    DEFAULT     �   ALTER TABLE ONLY public.product_details ALTER COLUMN product_id SET DEFAULT nextval('public.product_details_product_id_seq'::regclass);
 I   ALTER TABLE public.product_details ALTER COLUMN product_id DROP DEFAULT;
       public          postgres    false    317    318    318            �           2604    231843 $   product_type_details product_type_id    DEFAULT     �   ALTER TABLE ONLY public.product_type_details ALTER COLUMN product_type_id SET DEFAULT nextval('public.product_type_details_product_type_id_seq'::regclass);
 S   ALTER TABLE public.product_type_details ALTER COLUMN product_type_id DROP DEFAULT;
       public          postgres    false    288    289    289            �           2604    231714    quote_status quote_status_id    DEFAULT     �   ALTER TABLE ONLY public.quote_status ALTER COLUMN quote_status_id SET DEFAULT nextval('public.quote_status_quote_status_id_seq'::regclass);
 K   ALTER TABLE public.quote_status ALTER COLUMN quote_status_id DROP DEFAULT;
       public          postgres    false    264    265    265            �           2604    231758    reason_details reason_id    DEFAULT     �   ALTER TABLE ONLY public.reason_details ALTER COLUMN reason_id SET DEFAULT nextval('public.reason_details_reason_id_seq'::regclass);
 G   ALTER TABLE public.reason_details ALTER COLUMN reason_id DROP DEFAULT;
       public          postgres    false    273    272    273            �           2604    231692 !   registration_status reg_status_id    DEFAULT     �   ALTER TABLE ONLY public.registration_status ALTER COLUMN reg_status_id SET DEFAULT nextval('public.registration_status_reg_status_id_seq'::regclass);
 P   ALTER TABLE public.registration_status ALTER COLUMN reg_status_id DROP DEFAULT;
       public          postgres    false    260    261    261            �           2604    231497    reports report_id    DEFAULT     v   ALTER TABLE ONLY public.reports ALTER COLUMN report_id SET DEFAULT nextval('public.reports_report_id_seq'::regclass);
 @   ALTER TABLE public.reports ALTER COLUMN report_id DROP DEFAULT;
       public          postgres    false    224    225    225            �           2604    231456 "   role_permission role_permission_id    DEFAULT     �   ALTER TABLE ONLY public.role_permission ALTER COLUMN role_permission_id SET DEFAULT nextval('public.role_permission_role_permission_id_seq'::regclass);
 Q   ALTER TABLE public.role_permission ALTER COLUMN role_permission_id DROP DEFAULT;
       public          postgres    false    217    216    217            �           2604    231445    roles role_id    DEFAULT     n   ALTER TABLE ONLY public.roles ALTER COLUMN role_id SET DEFAULT nextval('public.roles_role_id_seq'::regclass);
 <   ALTER TABLE public.roles ALTER COLUMN role_id DROP DEFAULT;
       public          postgres    false    215    214    215            �           2604    231992    seller_warehouse warehouse_id    DEFAULT     �   ALTER TABLE ONLY public.seller_warehouse ALTER COLUMN warehouse_id SET DEFAULT nextval('public.seller_warehouse_warehouse_id_seq'::regclass);
 L   ALTER TABLE public.seller_warehouse ALTER COLUMN warehouse_id DROP DEFAULT;
       public          postgres    false    315    316    316            �           2604    232256    seo_content id    DEFAULT     p   ALTER TABLE ONLY public.seo_content ALTER COLUMN id SET DEFAULT nextval('public.seo_content_id_seq'::regclass);
 =   ALTER TABLE public.seo_content ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    364    363    364            �           2604    231670    shipment_status ship_status_id    DEFAULT     �   ALTER TABLE ONLY public.shipment_status ALTER COLUMN ship_status_id SET DEFAULT nextval('public.shipment_status_ship_status_id_seq'::regclass);
 M   ALTER TABLE public.shipment_status ALTER COLUMN ship_status_id DROP DEFAULT;
       public          postgres    false    257    256    257            �           2604    232278    sms_template id    DEFAULT     r   ALTER TABLE ONLY public.sms_template ALTER COLUMN id SET DEFAULT nextval('public.sms_template_id_seq'::regclass);
 >   ALTER TABLE public.sms_template ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    367    368    368            �           2604    231791    state_details id    DEFAULT     t   ALTER TABLE ONLY public.state_details ALTER COLUMN id SET DEFAULT nextval('public.state_details_id_seq'::regclass);
 ?   ALTER TABLE public.state_details ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    278    279    279            �           2604    231648    status_details status_id    DEFAULT     �   ALTER TABLE ONLY public.status_details ALTER COLUMN status_id SET DEFAULT nextval('public.status_details_status_id_seq'::regclass);
 G   ALTER TABLE public.status_details ALTER COLUMN status_id DROP DEFAULT;
       public          postgres    false    253    252    253            �           2604    231931 !   sub_group_management sub_group_id    DEFAULT     �   ALTER TABLE ONLY public.sub_group_management ALTER COLUMN sub_group_id SET DEFAULT nextval('public.sub_group_management_sub_group_id_seq'::regclass);
 P   ALTER TABLE public.sub_group_management ALTER COLUMN sub_group_id DROP DEFAULT;
       public          postgres    false    304    305    305            �           2604    231942    sub_node_management sub_node_id    DEFAULT     �   ALTER TABLE ONLY public.sub_node_management ALTER COLUMN sub_node_id SET DEFAULT nextval('public.sub_node_management_sub_node_id_seq'::regclass);
 N   ALTER TABLE public.sub_node_management ALTER COLUMN sub_node_id DROP DEFAULT;
       public          postgres    false    306    307    307            �           2604    232036    sub_order_details id    DEFAULT     |   ALTER TABLE ONLY public.sub_order_details ALTER COLUMN id SET DEFAULT nextval('public.sub_order_details_id_seq'::regclass);
 C   ALTER TABLE public.sub_order_details ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    323    324    324            �           2604    232058    sub_order_quote_details id    DEFAULT     �   ALTER TABLE ONLY public.sub_order_quote_details ALTER COLUMN id SET DEFAULT nextval('public.sub_order_quote_details_id_seq'::regclass);
 I   ALTER TABLE public.sub_order_quote_details ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    327    328    328            �           2604    231596 *   subscription_detail subscription_detail_id    DEFAULT     �   ALTER TABLE ONLY public.subscription_detail ALTER COLUMN subscription_detail_id SET DEFAULT nextval('public.subscription_detail_subscription_detail_id_seq'::regclass);
 Y   ALTER TABLE public.subscription_detail ALTER COLUMN subscription_detail_id DROP DEFAULT;
       public          postgres    false    242    243    243            �           2604    231607 .   subscription_plan_details subscription_plan_id    DEFAULT     �   ALTER TABLE ONLY public.subscription_plan_details ALTER COLUMN subscription_plan_id SET DEFAULT nextval('public.subscription_plan_details_subscription_plan_id_seq'::regclass);
 ]   ALTER TABLE public.subscription_plan_details ALTER COLUMN subscription_plan_id DROP DEFAULT;
       public          postgres    false    244    245    245            �           2604    231382    super_admin id    DEFAULT     p   ALTER TABLE ONLY public.super_admin ALTER COLUMN id SET DEFAULT nextval('public.super_admin_id_seq'::regclass);
 =   ALTER TABLE public.super_admin ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    202    203    203            �           2604    231810    tax_code_details tax_code_id    DEFAULT     �   ALTER TABLE ONLY public.tax_code_details ALTER COLUMN tax_code_id SET DEFAULT nextval('public.tax_code_details_tax_code_id_seq'::regclass);
 K   ALTER TABLE public.tax_code_details ALTER COLUMN tax_code_id DROP DEFAULT;
       public          postgres    false    283    282    283            �           2604    231854 $   trading_type_details trading_type_id    DEFAULT     �   ALTER TABLE ONLY public.trading_type_details ALTER COLUMN trading_type_id SET DEFAULT nextval('public.trading_type_details_trading_type_id_seq'::regclass);
 S   ALTER TABLE public.trading_type_details ALTER COLUMN trading_type_id DROP DEFAULT;
       public          postgres    false    290    291    291            �           2604    231423    user_managment id    DEFAULT     v   ALTER TABLE ONLY public.user_managment ALTER COLUMN id SET DEFAULT nextval('public.user_managment_id_seq'::regclass);
 @   ALTER TABLE public.user_managment ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    210    211    211            �           2604    232102    vin_history id    DEFAULT     p   ALTER TABLE ONLY public.vin_history ALTER COLUMN id SET DEFAULT nextval('public.vin_history_id_seq'::regclass);
 =   ALTER TABLE public.vin_history ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    335    336    336                      0    231431    Login_user_type 
   TABLE DATA           Y   COPY public."Login_user_type" ("Login_user_type_id", "Login_user_type_name") FROM stdin;
    public          postgres    false    213         �          0    232187    Rating_review 
   TABLE DATA           �   COPY public."Rating_review" (id, seller_id, rate_seller, review_seller, status, action, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    352   #      ]          0    231799    VAT_details 
   TABLE DATA           �   COPY public."VAT_details" (id, vat_percentage, effective_date, tax_code_id, pos_id, juristriction_id, status, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    281   @                0    231401    application_user 
   TABLE DATA           �   COPY public.application_user (application_user_id, user_id, portal_user_id, user_name, user_email_id, phone_no, designation, email_verification_status, phoneno_verified, status, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    207   ]                0    231412    application_user_permissions 
   TABLE DATA           �   COPY public.application_user_permissions (id, application_user_id, module_id, dashboard_id, report_id, add_access, delete_access, read_access, modify_access, export_access, print_access, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    209   z      �          0    232242    audit_trail 
   TABLE DATA           �   COPY public.audit_trail (id, module_id, field_name, old_value, new_value, user_id, time_stamp, status, audit_action, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    362   �      k          0    231873    brands 
   TABLE DATA           �   COPY public.brands (brand_id, brand_code, brand_name, erp_id, last_integrated_date, brand_api_id, brand_image, status, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    295   �      ?          0    231634    bulk_upload_details 
   TABLE DATA           �   COPY public.bulk_upload_details (id, file_name, "file_URL", product_type_id, status, remarks, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    251   �      �          0    232143    buyer_customer_details 
   TABLE DATA           �   COPY public.buyer_customer_details (buyer_customer_id, buyer_id, customer_name, email, phone, address, car_details_brand, model_id, status, action, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    344   �      �          0    232121    buyer_saved_quote 
   TABLE DATA           9  COPY public.buyer_saved_quote (buyer_quote_id, buyer_id, cart_item_id, quote_no, tax_code, buyer_customer_id, status_of_quote, quantity, price, margin_per, price_with_margin, customer_detail_name, address, car_details_brand, model_id, status, action, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    340         �          0    232132    buyer_saved_quote_service_items 
   TABLE DATA           �   COPY public.buyer_saved_quote_service_items (id, buyer_quote_id, service_item, quantity, price, tax_code, status, action, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    342   (      �          0    232066    car_management 
   TABLE DATA           �   COPY public.car_management (id, buyer_id, buyer_customer_id, car_info, brand_info, model_info, vin_no, action, status, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    330   E      q          0    231906    cars 
   TABLE DATA           �   COPY public.cars (car_id, car_name, brand_id, model_id, parameters_id, parameters_value, erp_id, last_integrated_date, status, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    301   b      �          0    232154    cart_details 
   TABLE DATA           �   COPY public.cart_details (cart_item_id, buyer_id, product_id, price, quantity, tax_code, tax_amount, final_price, checkout_status, action, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    346         �          0    232110    category_request_management 
   TABLE DATA           �   COPY public.category_request_management (id, category_code, category_name, description, cat_req_status_id, reason_for_reject, status, action, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    338   �      Q          0    231733    category_request_status 
   TABLE DATA           �   COPY public.category_request_status (cat_req_status_id, cat_req_status_name, remarks, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    269   �      '          0    231505    cms_company_info 
   TABLE DATA           �   COPY public.cms_company_info (id, company_name, company_logo, company_description, mobile_number, company_email, status, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    227   �      5          0    231582    cms_footer_links 
   TABLE DATA           �   COPY public.cms_footer_links (id, link_name, redirect_link, status, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    241   �      1          0    231560    cms_homepage_about_section 
   TABLE DATA           �   COPY public.cms_homepage_about_section (id, title, sub_title, content, redirect_link, status, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    237         -          0    231538    cms_homepage_banner_section 
   TABLE DATA           �   COPY public.cms_homepage_banner_section (id, title, content, image, status, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    233   -      3          0    231571     cms_homepage_buyerseller_section 
   TABLE DATA             COPY public.cms_homepage_buyerseller_section (id, buyer_title, buyer_content, buyer_redirect_link, buyer_section_image, seller_title, seller_content, seller_redirect_link, seller_section_image, status, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    239   J      /          0    231549    cms_homepage_features_section 
   TABLE DATA           �   COPY public.cms_homepage_features_section (id, title, content, icon_images, status, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    235   g      +          0    231527    cms_menu_section 
   TABLE DATA           �   COPY public.cms_menu_section (id, menu_name, menu_position, redirect_link, status, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    231   �      )          0    231516    cms_social_media_info 
   TABLE DATA           �   COPY public.cms_social_media_info (id, icon_images, redirect_link, status, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    229   �      �          0    232011    commission_management 
   TABLE DATA           �   COPY public.commission_management (id, product_type_id, trading_type_id, commission_precentage, effective_date, action, status, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    320   �      =          0    231623    configuration_details 
   TABLE DATA             COPY public.configuration_details (id, config_name, "config_URL", "Config_username", config_password, config_port, config_parameter, "config_MobileNumber", config_mtype, config_message, config_secretkey, status, remarks, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    249   �      Y          0    231777    country_details 
   TABLE DATA           �   COPY public.country_details (country_id, country_name, country_code, status, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    277   �      W          0    231766    currency_details 
   TABLE DATA           �   COPY public.currency_details (currency_id, currency_name, currency_shortcode, status, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    275          #          0    231483 	   dashboard 
   TABLE DATA           �   COPY public.dashboard (dashboard_id, dashboard_name, "Login_user_type_id", status, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    223   2       �          0    232264    email_template 
   TABLE DATA           �   COPY public.email_template (id, title, email_type, encoding_type, iso_code, description, subject, email_content, attachment, status, action, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    366   O       s          0    231917    group_management 
   TABLE DATA           �   COPY public.group_management (group_id, car_id, erp_id, last_integrated_date, group_code, group_name, status, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    303   l       �          0    232231    help_management 
   TABLE DATA           �   COPY public.help_management (id, help_management_code, subject_name, description, module_id, keywords, status, action, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    360   �                 0    231390    intermediator 
   TABLE DATA           �   COPY public.intermediator (id, user_id, intermediator_code, firstname, lastname, user_name, role_id, email_id, phone_number, mobile_number, status, image, country, language_spoken, address, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    205   �       �          0    232220    inventory_manangement 
   TABLE DATA           0  COPY public.inventory_manangement (id, warehouse_id, seller_id, product_id, brand_info, model_info, car_info, price, price_after_sale, onhand, e_part_qnty, buffer_qnty, re_order_level, threshold_limit, sold, balance, bin_loc, status, action, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    358   �       c          0    231829    jurisdiction 
   TABLE DATA           �   COPY public.jurisdiction (jurisdiction_id, name, code, erp_id, last_integrated_date, status, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    287   �       O          0    231722    make_offer_status 
   TABLE DATA           �   COPY public.make_offer_status (make_offer_status_id, offer_status_name, remarks, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    267   �       m          0    231884    models 
   TABLE DATA           �   COPY public.models (model_id, brand_id, erp_id, last_integrated_date, model_code, model_name, model_api_id, model_image, status, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    297   !      !          0    231472    module 
   TABLE DATA           �   COPY public.module (module_id, module_type_id, module_name, module_abbr, status, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    221   7!                0    231461    module_type 
   TABLE DATA           �   COPY public.module_type (module_type_id, module_type_name, status, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    219   T!      {          0    231961    new_buyer_details 
   TABLE DATA           z  COPY public.new_buyer_details (id, user_id, buyer_code, email_verification_status, business_name, business_com_logo, email_id, subscription_id, subscription_effective_date, business_status, billing_country, billing_state, billing_city, b_address1, b_address2, b_displayname, b_phonenumber, b_country_code, jurisdiction_id, pos_id, shipping_country, shipping_state, shipping_city, s_address1, s_address2, s_displayname, s_phonenumber, s_country_code, phoneno_verified, drop_address, googlemap_link, payment_type_info, product_type_id, trading_type_id, iv_location, iv_trade_licenese, iv_res_national, iv_exp_date, iv_country_issue, iv_f_name, iv_m_name, iv_l_name, iv_dob, iv_r_bussiness_name, iv_licenese_no, iv_r_address, id_doc_front, id_doc_back, bus_doc_license, registration_status, cancel_reason, vat_registration_number, tax_registration_exp_date, vat_reg_certificate_certificate, bank_benefi_name, bank_name, bank_ac_no, bank_iban, bank_branch_name, swift_code, bank_currency, bank_browse_file, erp_id, last_integrated_date, company_buyer_representative, t_and_c_acknowledge, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    311   q!      }          0    231972    new_seller_details 
   TABLE DATA           �  COPY public.new_seller_details (id, user_id, seller_code, email_verification_status, email_id, b_multi_branch, business_name, business_com_logo, subscription_id, subscription_effective_date, payout_shed_id, business_status, billing_country, billing_state, billing_city, b_address1, b_address2, jurisdiction_id, pos_id, b_displayname, b_phonenumber, b_country_code, shipping_country, shipping_state, shipping_city, s_address1, s_address2, s_displayname, s_phonenumber, s_country_code, phoneno_verified, drop_address, googlemap_link, payment_type_info, product_type_id, trading_type_id, iv_location, iv_licenese, iv_national_id, iv_exp_date, iv_country_issue, iv_f_name, iv_m_name, iv_l_name, iv_dob, iv_r_bussiness_name, iv_licenese_no, iv_r_address, id_doc_front, id_doc_back, bus_doc_license, registration_status, fail_reason, tax_registration_number, tax_registration_exp_date, vat_reg_certificate, bank_benefi_name, bank_name, bank_ac_no, bank_iban, bank_branch_name, swift_code, bank_currency, bank_browse_file, company_seller_representative, t_and_c_acknowledge, erp_id, last_integrated_date, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    313   �!      �          0    232165    newsletter_management 
   TABLE DATA           �   COPY public.newsletter_management (id, "Title", user_id, registered, email_id, status, action, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    348   �!      �          0    232176    notification 
   TABLE DATA           z  COPY public.notification (id, title, advance_filter, module_id, execute_on, notification_type, email_template, email_recipient_type, email_recipient, sms_template, sms_recipient_type, sms_recipient, push_notification_title, push_notification, push_notification_seller, push_notification_seller_name, status, action, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    350   �!      �          0    232209    offer_management 
   TABLE DATA           t  COPY public.offer_management (id, buyer_id, seller_id, product_id, part_no, asking_amount, asking_quantity, seller_offer_amount, seller_offer_quantity, remarks, approved_by, offer_date, approved_date, seller_confirmed_date, rejected_date, rejected_reason, offer_validity, make_offer_status_id, status, action, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    356   �!      �          0    232022    order_management 
   TABLE DATA           �  COPY public.order_management (order_id, order_code, buyer_id, user_id, method_of_payment, order_erp_id, order_date, order_type, order_amount, order_status_id, total_quantity, buyer_shipping_address, googlemap_link, total_vat_amount, total_price, last_delivery_time, total_discount, total_shipping_charges, ship_status_id, buyer_feedback, action, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    322   "      �          0    232044    order_quote_Manage 
   TABLE DATA           X  COPY public."order_quote_Manage" (order_quote_id, order_quote_code, buyer_id, user_id, method_of_payment, order_quote_erp_id, order_quote_date, total_quantity, order_type, order_amount, order_status_id, total_vat_amount, total_price, buyer_shipping_address, googlemap_link, action, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    326   "      G          0    231678    order_status 
   TABLE DATA           �   COPY public.order_status (order_status_id, order_status_name, remarks, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    259   <"      C          0    231656    order_type_details 
   TABLE DATA           ~   COPY public.order_type_details (order_type_id, order_type_name, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    255   Y"      o          0    231895 
   parameters 
   TABLE DATA           �   COPY public.parameters (id, brand_id, model_id, parameter_id, parameter_name, parameter_value, erp_id, last_integrated_date, status, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    299   v"      �          0    232198    part_request_unavail 
   TABLE DATA             COPY public.part_request_unavail (id, part_no, part_name, remarks, user_id, user_type_info, user_name, requested_date, intermediator_remarks, parts_req_status_id, parts_request_type, status, action, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    354   �"      y          0    231950    parts_management 
   TABLE DATA           �   COPY public.parts_management (id, car_id, group_id, sub_group_id, sub_node_id, part_number, part_name, part_erp_id, last_integrated_date, action, status, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    309   �"      K          0    231700    parts_req_status 
   TABLE DATA           �   COPY public.parts_req_status (parts_req_status_id, parts_req_status_name, remarks, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    263   �"      �          0    232077    pay_out_schedule_management 
   TABLE DATA           �   COPY public.pay_out_schedule_management (id, pay_out_slot_name, payout_shed_id, no_of_day, order_id, sub_order_id, description, action, status, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    332   �"      �          0    232088    payment_management 
   TABLE DATA           �   COPY public.payment_management (id, seller_id, order_id, sub_order_id, number_of_order, total_amount, commission_amount, seller_amount, payment_status_id, status, action, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    334   #      S          0    231744    payment_status 
   TABLE DATA           �   COPY public.payment_status (payment_status_id, payment_status_name, remarks, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    271   $#      i          0    231862    payment_type_details 
   TABLE DATA           �   COPY public.payment_type_details (payment_type_id, payment_type_name, status, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    293   A#      ;          0    231615    payout_shed_details 
   TABLE DATA           �   COPY public.payout_shed_details (payout_shed_id, subscription_detail_id, status, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    247   ^#      a          0    231818    place_of_supply 
   TABLE DATA           �   COPY public.place_of_supply (pos_id, tax_name, tax_code, erp_id, last_integrated_date, status, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    285   {#      �          0    232000    product_details 
   TABLE DATA           1  COPY public.product_details (product_id, product_code, product_erp_id, last_integrated_date, product_type, app_product_type, brand_info, model_info, car_info, seller_id, genuine_part_no, verify_button, number_of_yr_used, image, image1, image2, image3, image4, image5, image6, image7, image8, image9, image10, weight, dimension_length, dimension_width, dimension_height, condition, description, trading_type, manufacturer_name, product_listing_status, barcode_no, aftermarket_no, make_offer, status, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    318   �#      e          0    231840    product_type_details 
   TABLE DATA           �   COPY public.product_type_details (product_type_id, product_type_name, product_desc, status, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    289   �#      M          0    231711    quote_status 
   TABLE DATA           �   COPY public.quote_status (quote_status_id, quote_status_name, remarks, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    265   �#      U          0    231755    reason_details 
   TABLE DATA           z   COPY public.reason_details (reason_id, reason_name, status, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    273   �#      I          0    231689    registration_status 
   TABLE DATA           �   COPY public.registration_status (reg_status_id, reg_status_name, remarks, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    261   $      %          0    231494    reports 
   TABLE DATA           �   COPY public.reports (report_id, report_name, "Login_user_type_id", status, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    225   )$                0    231453    role_permission 
   TABLE DATA           �   COPY public.role_permission (role_permission_id, module_id, dashboard_id, report_id, role_id, add_access, delete_access, read_access, modify_access, export_access, print_access, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    217   F$                0    231442    roles 
   TABLE DATA           �   COPY public.roles (role_id, role_code, role_name, status, action, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    215   c$      ~          0    231981    seller_branch 
   TABLE DATA           �   COPY public.seller_branch ("Seller_branch_id", seller_id, seller_branch_name, branch_country, branch_state, branch_city, branch_address_1, branch_address_2, branch_phone, phone_country_code, branch_drop_address, branch_googlemap_link) FROM stdin;
    public          postgres    false    314   �$      �          0    231989    seller_warehouse 
   TABLE DATA           �   COPY public.seller_warehouse (warehouse_id, "Seller_branch_id", seller_id, warehouse_name, warehouse_phone, warehouse_country_code, warehouse_address) FROM stdin;
    public          postgres    false    316   �$      �          0    232253    seo_content 
   TABLE DATA           �   COPY public.seo_content (id, page_title, meta_tag, seo_keywords, seo_content, status, action, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    364   �$      E          0    231667    shipment_status 
   TABLE DATA           �   COPY public.shipment_status (ship_status_id, ship_status_name, remarks, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    257   �$      �          0    232275    sms_template 
   TABLE DATA           �   COPY public.sms_template (id, module_id, title_name, sms_content, status, action, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    368   �$      [          0    231788    state_details 
   TABLE DATA           }   COPY public.state_details (id, country_id, state_name, status, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    279   %      A          0    231645    status_details 
   TABLE DATA           r   COPY public.status_details (status_id, status_name, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    253   .%      u          0    231928    sub_group_management 
   TABLE DATA           �   COPY public.sub_group_management (sub_group_id, erp_id, last_integrated_date, group_id, sub_group_name, status, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    305   K%      w          0    231939    sub_node_management 
   TABLE DATA           �   COPY public.sub_node_management (sub_node_id, sub_group_id, sub_node_code, sub_node_name, erp_id, last_integrated_date, status, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    307   h%      �          0    232033    sub_order_details 
   TABLE DATA           D  COPY public.sub_order_details (id, order_id, quantity, price, vat_amount, total_price, product_type_id, trading_type_id, status, seller_id, delivery_time, discount, shipping_charges, brand_id, ship_status_id, order_status_id, sku, product_id, brand_info, model_info, car_info, ship_by_date, deliver_by_date, shipped_by, shipped_person, shipped_person_contact_no, tracking_id, shipping_date, pickup_date, package_type, package_dimension, package_weight, total_shipping_cost, seller_note, buyer_feedback, total_vat_amt, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    324   �%      �          0    232055    sub_order_quote_details 
   TABLE DATA           X  COPY public.sub_order_quote_details (id, order_quote_id, seller_id, product_id, brand_info, model_info, car_info, quantity, product_price, product_vat_amount, total_vat_amount, total_price, image, product_type_id, trading_type_id, delivery_time, discount, shipping_charges, ship_status_id, order_status_id, sku, ship_by_date, deliver_by_date, shipped_by, shipped_person, shipped_person_contact_no, tracking_id, shipping_date, pickup_date, package_type, package_dimension, package_weight, total_shipping_cost, seller_note, action, status, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    328   �%      7          0    231593    subscription_detail 
   TABLE DATA           �   COPY public.subscription_detail (subscription_detail_id, subscription_plan_id, "Login_user_type_id", user_id, valid_till, effective, status, action, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    243   �%      9          0    231604    subscription_plan_details 
   TABLE DATA           �   COPY public.subscription_plan_details (subscription_plan_id, "Login_user_type_id", subscription_name, number_of_users, modules_list, days, price, valid_till, effective, status, action, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    245   �%                0    231379    super_admin 
   TABLE DATA           n   COPY public.super_admin (id, user_id, password, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    203   �%      _          0    231807    tax_code_details 
   TABLE DATA           �   COPY public.tax_code_details (tax_code_id, tax_code_name, status, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    283   &      g          0    231851    trading_type_details 
   TABLE DATA           �   COPY public.trading_type_details (trading_type_id, trading_type_name, status, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    291   3&                0    231420    user_managment 
   TABLE DATA           �   COPY public.user_managment (id, "Login_user_type_id", user_name, user_email_id, phone_no, user_password, two_way_auth, status, last_login, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    211   P&      �          0    232099    vin_history 
   TABLE DATA           �   COPY public.vin_history (id, vin_number, vehicle_identified, brand_id, model_id, user_type_info, user_name, user_id, region, searched_date, status, action, created_at, created_by, modified_at, modified_by) FROM stdin;
    public          postgres    false    336   m&                 0    0 &   Login_user_type_Login_user_type_id_seq    SEQUENCE SET     W   SELECT pg_catalog.setval('public."Login_user_type_Login_user_type_id_seq"', 1, false);
          public          postgres    false    212                       0    0    Rating_review_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public."Rating_review_id_seq"', 1, false);
          public          postgres    false    351                       0    0    VAT_details_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public."VAT_details_id_seq"', 1, false);
          public          postgres    false    280                       0    0 (   application_user_application_user_id_seq    SEQUENCE SET     W   SELECT pg_catalog.setval('public.application_user_application_user_id_seq', 1, false);
          public          postgres    false    206                       0    0 #   application_user_permissions_id_seq    SEQUENCE SET     R   SELECT pg_catalog.setval('public.application_user_permissions_id_seq', 1, false);
          public          postgres    false    208                       0    0    audit_trail_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.audit_trail_id_seq', 1, false);
          public          postgres    false    361                       0    0    brands_brand_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.brands_brand_id_seq', 1, false);
          public          postgres    false    294                       0    0    bulk_upload_details_id_seq    SEQUENCE SET     I   SELECT pg_catalog.setval('public.bulk_upload_details_id_seq', 1, false);
          public          postgres    false    250                       0    0 ,   buyer_customer_details_buyer_customer_id_seq    SEQUENCE SET     [   SELECT pg_catalog.setval('public.buyer_customer_details_buyer_customer_id_seq', 1, false);
          public          postgres    false    343                       0    0 $   buyer_saved_quote_buyer_quote_id_seq    SEQUENCE SET     S   SELECT pg_catalog.setval('public.buyer_saved_quote_buyer_quote_id_seq', 1, false);
          public          postgres    false    339                       0    0 &   buyer_saved_quote_service_items_id_seq    SEQUENCE SET     U   SELECT pg_catalog.setval('public.buyer_saved_quote_service_items_id_seq', 1, false);
          public          postgres    false    341                       0    0    car_management_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.car_management_id_seq', 1, false);
          public          postgres    false    329                       0    0    cars_car_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.cars_car_id_seq', 1, false);
          public          postgres    false    300                       0    0    cart_details_cart_item_id_seq    SEQUENCE SET     L   SELECT pg_catalog.setval('public.cart_details_cart_item_id_seq', 1, false);
          public          postgres    false    345                       0    0 "   category_request_management_id_seq    SEQUENCE SET     Q   SELECT pg_catalog.setval('public.category_request_management_id_seq', 1, false);
          public          postgres    false    337                       0    0 -   category_request_status_cat_req_status_id_seq    SEQUENCE SET     \   SELECT pg_catalog.setval('public.category_request_status_cat_req_status_id_seq', 1, false);
          public          postgres    false    268                       0    0    cms_company_info_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.cms_company_info_id_seq', 1, false);
          public          postgres    false    226                       0    0    cms_footer_links_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.cms_footer_links_id_seq', 1, false);
          public          postgres    false    240                        0    0 !   cms_homepage_about_section_id_seq    SEQUENCE SET     P   SELECT pg_catalog.setval('public.cms_homepage_about_section_id_seq', 1, false);
          public          postgres    false    236            !           0    0 "   cms_homepage_banner_section_id_seq    SEQUENCE SET     Q   SELECT pg_catalog.setval('public.cms_homepage_banner_section_id_seq', 1, false);
          public          postgres    false    232            "           0    0 '   cms_homepage_buyerseller_section_id_seq    SEQUENCE SET     V   SELECT pg_catalog.setval('public.cms_homepage_buyerseller_section_id_seq', 1, false);
          public          postgres    false    238            #           0    0 $   cms_homepage_features_section_id_seq    SEQUENCE SET     S   SELECT pg_catalog.setval('public.cms_homepage_features_section_id_seq', 1, false);
          public          postgres    false    234            $           0    0    cms_menu_section_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.cms_menu_section_id_seq', 1, false);
          public          postgres    false    230            %           0    0    cms_social_media_info_id_seq    SEQUENCE SET     K   SELECT pg_catalog.setval('public.cms_social_media_info_id_seq', 1, false);
          public          postgres    false    228            &           0    0    commission_management_id_seq    SEQUENCE SET     K   SELECT pg_catalog.setval('public.commission_management_id_seq', 1, false);
          public          postgres    false    319            '           0    0    configuration_details_id_seq    SEQUENCE SET     K   SELECT pg_catalog.setval('public.configuration_details_id_seq', 1, false);
          public          postgres    false    248            (           0    0    country_details_country_id_seq    SEQUENCE SET     M   SELECT pg_catalog.setval('public.country_details_country_id_seq', 1, false);
          public          postgres    false    276            )           0    0     currency_details_currency_id_seq    SEQUENCE SET     O   SELECT pg_catalog.setval('public.currency_details_currency_id_seq', 1, false);
          public          postgres    false    274            *           0    0    dashboard_dashboard_id_seq    SEQUENCE SET     I   SELECT pg_catalog.setval('public.dashboard_dashboard_id_seq', 1, false);
          public          postgres    false    222            +           0    0    email_template_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.email_template_id_seq', 1, false);
          public          postgres    false    365            ,           0    0    group_management_group_id_seq    SEQUENCE SET     L   SELECT pg_catalog.setval('public.group_management_group_id_seq', 1, false);
          public          postgres    false    302            -           0    0    help_management_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.help_management_id_seq', 1, false);
          public          postgres    false    359            .           0    0    intermediator_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.intermediator_id_seq', 1, false);
          public          postgres    false    204            /           0    0    inventory_manangement_id_seq    SEQUENCE SET     K   SELECT pg_catalog.setval('public.inventory_manangement_id_seq', 1, false);
          public          postgres    false    357            0           0    0     jurisdiction_jurisdiction_id_seq    SEQUENCE SET     O   SELECT pg_catalog.setval('public.jurisdiction_jurisdiction_id_seq', 1, false);
          public          postgres    false    286            1           0    0 *   make_offer_status_make_offer_status_id_seq    SEQUENCE SET     Y   SELECT pg_catalog.setval('public.make_offer_status_make_offer_status_id_seq', 1, false);
          public          postgres    false    266            2           0    0    models_model_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.models_model_id_seq', 1, false);
          public          postgres    false    296            3           0    0    module_module_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.module_module_id_seq', 1, false);
          public          postgres    false    220            4           0    0    module_type_module_type_id_seq    SEQUENCE SET     M   SELECT pg_catalog.setval('public.module_type_module_type_id_seq', 1, false);
          public          postgres    false    218            5           0    0    new_buyer_details_id_seq    SEQUENCE SET     G   SELECT pg_catalog.setval('public.new_buyer_details_id_seq', 1, false);
          public          postgres    false    310            6           0    0    new_seller_details_id_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('public.new_seller_details_id_seq', 1, false);
          public          postgres    false    312            7           0    0    newsletter_management_id_seq    SEQUENCE SET     K   SELECT pg_catalog.setval('public.newsletter_management_id_seq', 1, false);
          public          postgres    false    347            8           0    0    notification_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.notification_id_seq', 1, false);
          public          postgres    false    349            9           0    0    offer_management_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.offer_management_id_seq', 1, false);
          public          postgres    false    355            :           0    0    order_management_order_id_seq    SEQUENCE SET     L   SELECT pg_catalog.setval('public.order_management_order_id_seq', 1, false);
          public          postgres    false    321            ;           0    0 %   order_quote_Manage_order_quote_id_seq    SEQUENCE SET     V   SELECT pg_catalog.setval('public."order_quote_Manage_order_quote_id_seq"', 1, false);
          public          postgres    false    325            <           0    0     order_status_order_status_id_seq    SEQUENCE SET     O   SELECT pg_catalog.setval('public.order_status_order_status_id_seq', 1, false);
          public          postgres    false    258            =           0    0 $   order_type_details_order_type_id_seq    SEQUENCE SET     S   SELECT pg_catalog.setval('public.order_type_details_order_type_id_seq', 1, false);
          public          postgres    false    254            >           0    0    parameters_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public.parameters_id_seq', 1, false);
          public          postgres    false    298            ?           0    0    part_request_unavail_id_seq    SEQUENCE SET     J   SELECT pg_catalog.setval('public.part_request_unavail_id_seq', 1, false);
          public          postgres    false    353            @           0    0    parts_management_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.parts_management_id_seq', 1, false);
          public          postgres    false    308            A           0    0 (   parts_req_status_parts_req_status_id_seq    SEQUENCE SET     W   SELECT pg_catalog.setval('public.parts_req_status_parts_req_status_id_seq', 1, false);
          public          postgres    false    262            B           0    0 "   pay_out_schedule_management_id_seq    SEQUENCE SET     Q   SELECT pg_catalog.setval('public.pay_out_schedule_management_id_seq', 1, false);
          public          postgres    false    331            C           0    0    payment_management_id_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('public.payment_management_id_seq', 1, false);
          public          postgres    false    333            D           0    0 $   payment_status_payment_status_id_seq    SEQUENCE SET     S   SELECT pg_catalog.setval('public.payment_status_payment_status_id_seq', 1, false);
          public          postgres    false    270            E           0    0 (   payment_type_details_payment_type_id_seq    SEQUENCE SET     W   SELECT pg_catalog.setval('public.payment_type_details_payment_type_id_seq', 1, false);
          public          postgres    false    292            F           0    0 &   payout_shed_details_payout_shed_id_seq    SEQUENCE SET     U   SELECT pg_catalog.setval('public.payout_shed_details_payout_shed_id_seq', 1, false);
          public          postgres    false    246            G           0    0    place_of_supply_pos_id_seq    SEQUENCE SET     I   SELECT pg_catalog.setval('public.place_of_supply_pos_id_seq', 1, false);
          public          postgres    false    284            H           0    0    product_details_product_id_seq    SEQUENCE SET     M   SELECT pg_catalog.setval('public.product_details_product_id_seq', 1, false);
          public          postgres    false    317            I           0    0 (   product_type_details_product_type_id_seq    SEQUENCE SET     W   SELECT pg_catalog.setval('public.product_type_details_product_type_id_seq', 1, false);
          public          postgres    false    288            J           0    0     quote_status_quote_status_id_seq    SEQUENCE SET     O   SELECT pg_catalog.setval('public.quote_status_quote_status_id_seq', 1, false);
          public          postgres    false    264            K           0    0    reason_details_reason_id_seq    SEQUENCE SET     K   SELECT pg_catalog.setval('public.reason_details_reason_id_seq', 1, false);
          public          postgres    false    272            L           0    0 %   registration_status_reg_status_id_seq    SEQUENCE SET     T   SELECT pg_catalog.setval('public.registration_status_reg_status_id_seq', 1, false);
          public          postgres    false    260            M           0    0    reports_report_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.reports_report_id_seq', 1, false);
          public          postgres    false    224            N           0    0 &   role_permission_role_permission_id_seq    SEQUENCE SET     U   SELECT pg_catalog.setval('public.role_permission_role_permission_id_seq', 1, false);
          public          postgres    false    216            O           0    0    roles_role_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public.roles_role_id_seq', 1, false);
          public          postgres    false    214            P           0    0 !   seller_warehouse_warehouse_id_seq    SEQUENCE SET     P   SELECT pg_catalog.setval('public.seller_warehouse_warehouse_id_seq', 1, false);
          public          postgres    false    315            Q           0    0    seo_content_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.seo_content_id_seq', 1, false);
          public          postgres    false    363            R           0    0 "   shipment_status_ship_status_id_seq    SEQUENCE SET     Q   SELECT pg_catalog.setval('public.shipment_status_ship_status_id_seq', 1, false);
          public          postgres    false    256            S           0    0    sms_template_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.sms_template_id_seq', 1, false);
          public          postgres    false    367            T           0    0    state_details_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.state_details_id_seq', 1, false);
          public          postgres    false    278            U           0    0    status_details_status_id_seq    SEQUENCE SET     K   SELECT pg_catalog.setval('public.status_details_status_id_seq', 1, false);
          public          postgres    false    252            V           0    0 %   sub_group_management_sub_group_id_seq    SEQUENCE SET     T   SELECT pg_catalog.setval('public.sub_group_management_sub_group_id_seq', 1, false);
          public          postgres    false    304            W           0    0 #   sub_node_management_sub_node_id_seq    SEQUENCE SET     R   SELECT pg_catalog.setval('public.sub_node_management_sub_node_id_seq', 1, false);
          public          postgres    false    306            X           0    0    sub_order_details_id_seq    SEQUENCE SET     G   SELECT pg_catalog.setval('public.sub_order_details_id_seq', 1, false);
          public          postgres    false    323            Y           0    0    sub_order_quote_details_id_seq    SEQUENCE SET     M   SELECT pg_catalog.setval('public.sub_order_quote_details_id_seq', 1, false);
          public          postgres    false    327            Z           0    0 .   subscription_detail_subscription_detail_id_seq    SEQUENCE SET     ]   SELECT pg_catalog.setval('public.subscription_detail_subscription_detail_id_seq', 1, false);
          public          postgres    false    242            [           0    0 2   subscription_plan_details_subscription_plan_id_seq    SEQUENCE SET     a   SELECT pg_catalog.setval('public.subscription_plan_details_subscription_plan_id_seq', 1, false);
          public          postgres    false    244            \           0    0    super_admin_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.super_admin_id_seq', 1, false);
          public          postgres    false    202            ]           0    0     tax_code_details_tax_code_id_seq    SEQUENCE SET     O   SELECT pg_catalog.setval('public.tax_code_details_tax_code_id_seq', 1, false);
          public          postgres    false    282            ^           0    0 (   trading_type_details_trading_type_id_seq    SEQUENCE SET     W   SELECT pg_catalog.setval('public.trading_type_details_trading_type_id_seq', 1, false);
          public          postgres    false    290            _           0    0    user_managment_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.user_managment_id_seq', 1, false);
          public          postgres    false    210            `           0    0    vin_history_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.vin_history_id_seq', 1, false);
          public          postgres    false    335                       2606    231439 $   Login_user_type Login_user_type_pkey 
   CONSTRAINT     x   ALTER TABLE ONLY public."Login_user_type"
    ADD CONSTRAINT "Login_user_type_pkey" PRIMARY KEY ("Login_user_type_id");
 R   ALTER TABLE ONLY public."Login_user_type" DROP CONSTRAINT "Login_user_type_pkey";
       public            postgres    false    213            �           2606    232195     Rating_review Rating_review_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY public."Rating_review"
    ADD CONSTRAINT "Rating_review_pkey" PRIMARY KEY (id);
 N   ALTER TABLE ONLY public."Rating_review" DROP CONSTRAINT "Rating_review_pkey";
       public            postgres    false    352            I           2606    231804    VAT_details VAT_details_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public."VAT_details"
    ADD CONSTRAINT "VAT_details_pkey" PRIMARY KEY (id);
 J   ALTER TABLE ONLY public."VAT_details" DROP CONSTRAINT "VAT_details_pkey";
       public            postgres    false    281                       2606    231417 >   application_user_permissions application_user_permissions_pkey 
   CONSTRAINT     |   ALTER TABLE ONLY public.application_user_permissions
    ADD CONSTRAINT application_user_permissions_pkey PRIMARY KEY (id);
 h   ALTER TABLE ONLY public.application_user_permissions DROP CONSTRAINT application_user_permissions_pkey;
       public            postgres    false    209            �           2606    231409 &   application_user application_user_pkey 
   CONSTRAINT     u   ALTER TABLE ONLY public.application_user
    ADD CONSTRAINT application_user_pkey PRIMARY KEY (application_user_id);
 P   ALTER TABLE ONLY public.application_user DROP CONSTRAINT application_user_pkey;
       public            postgres    false    207            �           2606    232250    audit_trail audit_trail_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.audit_trail
    ADD CONSTRAINT audit_trail_pkey PRIMARY KEY (id);
 F   ALTER TABLE ONLY public.audit_trail DROP CONSTRAINT audit_trail_pkey;
       public            postgres    false    362            W           2606    231881    brands brands_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY public.brands
    ADD CONSTRAINT brands_pkey PRIMARY KEY (brand_id);
 <   ALTER TABLE ONLY public.brands DROP CONSTRAINT brands_pkey;
       public            postgres    false    295            +           2606    231642 ,   bulk_upload_details bulk_upload_details_pkey 
   CONSTRAINT     j   ALTER TABLE ONLY public.bulk_upload_details
    ADD CONSTRAINT bulk_upload_details_pkey PRIMARY KEY (id);
 V   ALTER TABLE ONLY public.bulk_upload_details DROP CONSTRAINT bulk_upload_details_pkey;
       public            postgres    false    251            �           2606    232151 2   buyer_customer_details buyer_customer_details_pkey 
   CONSTRAINT        ALTER TABLE ONLY public.buyer_customer_details
    ADD CONSTRAINT buyer_customer_details_pkey PRIMARY KEY (buyer_customer_id);
 \   ALTER TABLE ONLY public.buyer_customer_details DROP CONSTRAINT buyer_customer_details_pkey;
       public            postgres    false    344            �           2606    232129 (   buyer_saved_quote buyer_saved_quote_pkey 
   CONSTRAINT     r   ALTER TABLE ONLY public.buyer_saved_quote
    ADD CONSTRAINT buyer_saved_quote_pkey PRIMARY KEY (buyer_quote_id);
 R   ALTER TABLE ONLY public.buyer_saved_quote DROP CONSTRAINT buyer_saved_quote_pkey;
       public            postgres    false    340            �           2606    232140 D   buyer_saved_quote_service_items buyer_saved_quote_service_items_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.buyer_saved_quote_service_items
    ADD CONSTRAINT buyer_saved_quote_service_items_pkey PRIMARY KEY (id);
 n   ALTER TABLE ONLY public.buyer_saved_quote_service_items DROP CONSTRAINT buyer_saved_quote_service_items_pkey;
       public            postgres    false    342            y           2606    232074 "   car_management car_management_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY public.car_management
    ADD CONSTRAINT car_management_pkey PRIMARY KEY (id);
 L   ALTER TABLE ONLY public.car_management DROP CONSTRAINT car_management_pkey;
       public            postgres    false    330            ]           2606    231914    cars cars_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY public.cars
    ADD CONSTRAINT cars_pkey PRIMARY KEY (car_id);
 8   ALTER TABLE ONLY public.cars DROP CONSTRAINT cars_pkey;
       public            postgres    false    301            �           2606    232162    cart_details cart_details_pkey 
   CONSTRAINT     f   ALTER TABLE ONLY public.cart_details
    ADD CONSTRAINT cart_details_pkey PRIMARY KEY (cart_item_id);
 H   ALTER TABLE ONLY public.cart_details DROP CONSTRAINT cart_details_pkey;
       public            postgres    false    346            �           2606    232118 <   category_request_management category_request_management_pkey 
   CONSTRAINT     z   ALTER TABLE ONLY public.category_request_management
    ADD CONSTRAINT category_request_management_pkey PRIMARY KEY (id);
 f   ALTER TABLE ONLY public.category_request_management DROP CONSTRAINT category_request_management_pkey;
       public            postgres    false    338            =           2606    231741 4   category_request_status category_request_status_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.category_request_status
    ADD CONSTRAINT category_request_status_pkey PRIMARY KEY (cat_req_status_id);
 ^   ALTER TABLE ONLY public.category_request_status DROP CONSTRAINT category_request_status_pkey;
       public            postgres    false    269                       2606    231513 &   cms_company_info cms_company_info_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.cms_company_info
    ADD CONSTRAINT cms_company_info_pkey PRIMARY KEY (id);
 P   ALTER TABLE ONLY public.cms_company_info DROP CONSTRAINT cms_company_info_pkey;
       public            postgres    false    227            !           2606    231590 &   cms_footer_links cms_footer_links_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.cms_footer_links
    ADD CONSTRAINT cms_footer_links_pkey PRIMARY KEY (id);
 P   ALTER TABLE ONLY public.cms_footer_links DROP CONSTRAINT cms_footer_links_pkey;
       public            postgres    false    241                       2606    231568 :   cms_homepage_about_section cms_homepage_about_section_pkey 
   CONSTRAINT     x   ALTER TABLE ONLY public.cms_homepage_about_section
    ADD CONSTRAINT cms_homepage_about_section_pkey PRIMARY KEY (id);
 d   ALTER TABLE ONLY public.cms_homepage_about_section DROP CONSTRAINT cms_homepage_about_section_pkey;
       public            postgres    false    237                       2606    231546 <   cms_homepage_banner_section cms_homepage_banner_section_pkey 
   CONSTRAINT     z   ALTER TABLE ONLY public.cms_homepage_banner_section
    ADD CONSTRAINT cms_homepage_banner_section_pkey PRIMARY KEY (id);
 f   ALTER TABLE ONLY public.cms_homepage_banner_section DROP CONSTRAINT cms_homepage_banner_section_pkey;
       public            postgres    false    233                       2606    231579 F   cms_homepage_buyerseller_section cms_homepage_buyerseller_section_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.cms_homepage_buyerseller_section
    ADD CONSTRAINT cms_homepage_buyerseller_section_pkey PRIMARY KEY (id);
 p   ALTER TABLE ONLY public.cms_homepage_buyerseller_section DROP CONSTRAINT cms_homepage_buyerseller_section_pkey;
       public            postgres    false    239                       2606    231557 @   cms_homepage_features_section cms_homepage_features_section_pkey 
   CONSTRAINT     ~   ALTER TABLE ONLY public.cms_homepage_features_section
    ADD CONSTRAINT cms_homepage_features_section_pkey PRIMARY KEY (id);
 j   ALTER TABLE ONLY public.cms_homepage_features_section DROP CONSTRAINT cms_homepage_features_section_pkey;
       public            postgres    false    235                       2606    231535 &   cms_menu_section cms_menu_section_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.cms_menu_section
    ADD CONSTRAINT cms_menu_section_pkey PRIMARY KEY (id);
 P   ALTER TABLE ONLY public.cms_menu_section DROP CONSTRAINT cms_menu_section_pkey;
       public            postgres    false    231                       2606    231524 0   cms_social_media_info cms_social_media_info_pkey 
   CONSTRAINT     n   ALTER TABLE ONLY public.cms_social_media_info
    ADD CONSTRAINT cms_social_media_info_pkey PRIMARY KEY (id);
 Z   ALTER TABLE ONLY public.cms_social_media_info DROP CONSTRAINT cms_social_media_info_pkey;
       public            postgres    false    229            o           2606    232019 0   commission_management commission_management_pkey 
   CONSTRAINT     n   ALTER TABLE ONLY public.commission_management
    ADD CONSTRAINT commission_management_pkey PRIMARY KEY (id);
 Z   ALTER TABLE ONLY public.commission_management DROP CONSTRAINT commission_management_pkey;
       public            postgres    false    320            )           2606    231631 0   configuration_details configuration_details_pkey 
   CONSTRAINT     n   ALTER TABLE ONLY public.configuration_details
    ADD CONSTRAINT configuration_details_pkey PRIMARY KEY (id);
 Z   ALTER TABLE ONLY public.configuration_details DROP CONSTRAINT configuration_details_pkey;
       public            postgres    false    249            E           2606    231785 $   country_details country_details_pkey 
   CONSTRAINT     j   ALTER TABLE ONLY public.country_details
    ADD CONSTRAINT country_details_pkey PRIMARY KEY (country_id);
 N   ALTER TABLE ONLY public.country_details DROP CONSTRAINT country_details_pkey;
       public            postgres    false    277            C           2606    231774 &   currency_details currency_details_pkey 
   CONSTRAINT     m   ALTER TABLE ONLY public.currency_details
    ADD CONSTRAINT currency_details_pkey PRIMARY KEY (currency_id);
 P   ALTER TABLE ONLY public.currency_details DROP CONSTRAINT currency_details_pkey;
       public            postgres    false    275                       2606    231491    dashboard dashboard_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY public.dashboard
    ADD CONSTRAINT dashboard_pkey PRIMARY KEY (dashboard_id);
 B   ALTER TABLE ONLY public.dashboard DROP CONSTRAINT dashboard_pkey;
       public            postgres    false    223            �           2606    232272 "   email_template email_template_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY public.email_template
    ADD CONSTRAINT email_template_pkey PRIMARY KEY (id);
 L   ALTER TABLE ONLY public.email_template DROP CONSTRAINT email_template_pkey;
       public            postgres    false    366            _           2606    231925 &   group_management group_management_pkey 
   CONSTRAINT     j   ALTER TABLE ONLY public.group_management
    ADD CONSTRAINT group_management_pkey PRIMARY KEY (group_id);
 P   ALTER TABLE ONLY public.group_management DROP CONSTRAINT group_management_pkey;
       public            postgres    false    303            �           2606    232239 $   help_management help_management_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY public.help_management
    ADD CONSTRAINT help_management_pkey PRIMARY KEY (id);
 N   ALTER TABLE ONLY public.help_management DROP CONSTRAINT help_management_pkey;
       public            postgres    false    360            �           2606    231398     intermediator intermediator_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public.intermediator
    ADD CONSTRAINT intermediator_pkey PRIMARY KEY (id);
 J   ALTER TABLE ONLY public.intermediator DROP CONSTRAINT intermediator_pkey;
       public            postgres    false    205            �           2606    232228 0   inventory_manangement inventory_manangement_pkey 
   CONSTRAINT     n   ALTER TABLE ONLY public.inventory_manangement
    ADD CONSTRAINT inventory_manangement_pkey PRIMARY KEY (id);
 Z   ALTER TABLE ONLY public.inventory_manangement DROP CONSTRAINT inventory_manangement_pkey;
       public            postgres    false    358            O           2606    231837    jurisdiction jurisdiction_pkey 
   CONSTRAINT     i   ALTER TABLE ONLY public.jurisdiction
    ADD CONSTRAINT jurisdiction_pkey PRIMARY KEY (jurisdiction_id);
 H   ALTER TABLE ONLY public.jurisdiction DROP CONSTRAINT jurisdiction_pkey;
       public            postgres    false    287            ;           2606    231730 (   make_offer_status make_offer_status_pkey 
   CONSTRAINT     x   ALTER TABLE ONLY public.make_offer_status
    ADD CONSTRAINT make_offer_status_pkey PRIMARY KEY (make_offer_status_id);
 R   ALTER TABLE ONLY public.make_offer_status DROP CONSTRAINT make_offer_status_pkey;
       public            postgres    false    267            Y           2606    231892    models models_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY public.models
    ADD CONSTRAINT models_pkey PRIMARY KEY (model_id);
 <   ALTER TABLE ONLY public.models DROP CONSTRAINT models_pkey;
       public            postgres    false    297                       2606    231480    module module_pkey 
   CONSTRAINT     W   ALTER TABLE ONLY public.module
    ADD CONSTRAINT module_pkey PRIMARY KEY (module_id);
 <   ALTER TABLE ONLY public.module DROP CONSTRAINT module_pkey;
       public            postgres    false    221                       2606    231469    module_type module_type_pkey 
   CONSTRAINT     f   ALTER TABLE ONLY public.module_type
    ADD CONSTRAINT module_type_pkey PRIMARY KEY (module_type_id);
 F   ALTER TABLE ONLY public.module_type DROP CONSTRAINT module_type_pkey;
       public            postgres    false    219            g           2606    231969 (   new_buyer_details new_buyer_details_pkey 
   CONSTRAINT     f   ALTER TABLE ONLY public.new_buyer_details
    ADD CONSTRAINT new_buyer_details_pkey PRIMARY KEY (id);
 R   ALTER TABLE ONLY public.new_buyer_details DROP CONSTRAINT new_buyer_details_pkey;
       public            postgres    false    311            i           2606    231980 *   new_seller_details new_seller_details_pkey 
   CONSTRAINT     h   ALTER TABLE ONLY public.new_seller_details
    ADD CONSTRAINT new_seller_details_pkey PRIMARY KEY (id);
 T   ALTER TABLE ONLY public.new_seller_details DROP CONSTRAINT new_seller_details_pkey;
       public            postgres    false    313            �           2606    232173 0   newsletter_management newsletter_management_pkey 
   CONSTRAINT     n   ALTER TABLE ONLY public.newsletter_management
    ADD CONSTRAINT newsletter_management_pkey PRIMARY KEY (id);
 Z   ALTER TABLE ONLY public.newsletter_management DROP CONSTRAINT newsletter_management_pkey;
       public            postgres    false    348            �           2606    232184    notification notification_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.notification
    ADD CONSTRAINT notification_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY public.notification DROP CONSTRAINT notification_pkey;
       public            postgres    false    350            �           2606    232217 &   offer_management offer_management_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.offer_management
    ADD CONSTRAINT offer_management_pkey PRIMARY KEY (id);
 P   ALTER TABLE ONLY public.offer_management DROP CONSTRAINT offer_management_pkey;
       public            postgres    false    356            q           2606    232030 &   order_management order_management_pkey 
   CONSTRAINT     j   ALTER TABLE ONLY public.order_management
    ADD CONSTRAINT order_management_pkey PRIMARY KEY (order_id);
 P   ALTER TABLE ONLY public.order_management DROP CONSTRAINT order_management_pkey;
       public            postgres    false    322            u           2606    232052 *   order_quote_Manage order_quote_Manage_pkey 
   CONSTRAINT     x   ALTER TABLE ONLY public."order_quote_Manage"
    ADD CONSTRAINT "order_quote_Manage_pkey" PRIMARY KEY (order_quote_id);
 X   ALTER TABLE ONLY public."order_quote_Manage" DROP CONSTRAINT "order_quote_Manage_pkey";
       public            postgres    false    326            3           2606    231686    order_status order_status_pkey 
   CONSTRAINT     i   ALTER TABLE ONLY public.order_status
    ADD CONSTRAINT order_status_pkey PRIMARY KEY (order_status_id);
 H   ALTER TABLE ONLY public.order_status DROP CONSTRAINT order_status_pkey;
       public            postgres    false    259            /           2606    231664 *   order_type_details order_type_details_pkey 
   CONSTRAINT     s   ALTER TABLE ONLY public.order_type_details
    ADD CONSTRAINT order_type_details_pkey PRIMARY KEY (order_type_id);
 T   ALTER TABLE ONLY public.order_type_details DROP CONSTRAINT order_type_details_pkey;
       public            postgres    false    255            [           2606    231903    parameters parameters_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.parameters
    ADD CONSTRAINT parameters_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY public.parameters DROP CONSTRAINT parameters_pkey;
       public            postgres    false    299            �           2606    232206 .   part_request_unavail part_request_unavail_pkey 
   CONSTRAINT     l   ALTER TABLE ONLY public.part_request_unavail
    ADD CONSTRAINT part_request_unavail_pkey PRIMARY KEY (id);
 X   ALTER TABLE ONLY public.part_request_unavail DROP CONSTRAINT part_request_unavail_pkey;
       public            postgres    false    354            e           2606    231958 &   parts_management parts_management_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.parts_management
    ADD CONSTRAINT parts_management_pkey PRIMARY KEY (id);
 P   ALTER TABLE ONLY public.parts_management DROP CONSTRAINT parts_management_pkey;
       public            postgres    false    309            7           2606    231708 &   parts_req_status parts_req_status_pkey 
   CONSTRAINT     u   ALTER TABLE ONLY public.parts_req_status
    ADD CONSTRAINT parts_req_status_pkey PRIMARY KEY (parts_req_status_id);
 P   ALTER TABLE ONLY public.parts_req_status DROP CONSTRAINT parts_req_status_pkey;
       public            postgres    false    263            {           2606    232085 <   pay_out_schedule_management pay_out_schedule_management_pkey 
   CONSTRAINT     z   ALTER TABLE ONLY public.pay_out_schedule_management
    ADD CONSTRAINT pay_out_schedule_management_pkey PRIMARY KEY (id);
 f   ALTER TABLE ONLY public.pay_out_schedule_management DROP CONSTRAINT pay_out_schedule_management_pkey;
       public            postgres    false    332            }           2606    232096 *   payment_management payment_management_pkey 
   CONSTRAINT     h   ALTER TABLE ONLY public.payment_management
    ADD CONSTRAINT payment_management_pkey PRIMARY KEY (id);
 T   ALTER TABLE ONLY public.payment_management DROP CONSTRAINT payment_management_pkey;
       public            postgres    false    334            ?           2606    231752 "   payment_status payment_status_pkey 
   CONSTRAINT     o   ALTER TABLE ONLY public.payment_status
    ADD CONSTRAINT payment_status_pkey PRIMARY KEY (payment_status_id);
 L   ALTER TABLE ONLY public.payment_status DROP CONSTRAINT payment_status_pkey;
       public            postgres    false    271            U           2606    231870 .   payment_type_details payment_type_details_pkey 
   CONSTRAINT     y   ALTER TABLE ONLY public.payment_type_details
    ADD CONSTRAINT payment_type_details_pkey PRIMARY KEY (payment_type_id);
 X   ALTER TABLE ONLY public.payment_type_details DROP CONSTRAINT payment_type_details_pkey;
       public            postgres    false    293            '           2606    231620 ,   payout_shed_details payout_shed_details_pkey 
   CONSTRAINT     v   ALTER TABLE ONLY public.payout_shed_details
    ADD CONSTRAINT payout_shed_details_pkey PRIMARY KEY (payout_shed_id);
 V   ALTER TABLE ONLY public.payout_shed_details DROP CONSTRAINT payout_shed_details_pkey;
       public            postgres    false    247            M           2606    231826 $   place_of_supply place_of_supply_pkey 
   CONSTRAINT     f   ALTER TABLE ONLY public.place_of_supply
    ADD CONSTRAINT place_of_supply_pkey PRIMARY KEY (pos_id);
 N   ALTER TABLE ONLY public.place_of_supply DROP CONSTRAINT place_of_supply_pkey;
       public            postgres    false    285            m           2606    232008 $   product_details product_details_pkey 
   CONSTRAINT     j   ALTER TABLE ONLY public.product_details
    ADD CONSTRAINT product_details_pkey PRIMARY KEY (product_id);
 N   ALTER TABLE ONLY public.product_details DROP CONSTRAINT product_details_pkey;
       public            postgres    false    318            Q           2606    231848 .   product_type_details product_type_details_pkey 
   CONSTRAINT     y   ALTER TABLE ONLY public.product_type_details
    ADD CONSTRAINT product_type_details_pkey PRIMARY KEY (product_type_id);
 X   ALTER TABLE ONLY public.product_type_details DROP CONSTRAINT product_type_details_pkey;
       public            postgres    false    289            9           2606    231719    quote_status quote_status_pkey 
   CONSTRAINT     i   ALTER TABLE ONLY public.quote_status
    ADD CONSTRAINT quote_status_pkey PRIMARY KEY (quote_status_id);
 H   ALTER TABLE ONLY public.quote_status DROP CONSTRAINT quote_status_pkey;
       public            postgres    false    265            A           2606    231763 "   reason_details reason_details_pkey 
   CONSTRAINT     g   ALTER TABLE ONLY public.reason_details
    ADD CONSTRAINT reason_details_pkey PRIMARY KEY (reason_id);
 L   ALTER TABLE ONLY public.reason_details DROP CONSTRAINT reason_details_pkey;
       public            postgres    false    273            5           2606    231697 ,   registration_status registration_status_pkey 
   CONSTRAINT     u   ALTER TABLE ONLY public.registration_status
    ADD CONSTRAINT registration_status_pkey PRIMARY KEY (reg_status_id);
 V   ALTER TABLE ONLY public.registration_status DROP CONSTRAINT registration_status_pkey;
       public            postgres    false    261                       2606    231502    reports reports_pkey 
   CONSTRAINT     Y   ALTER TABLE ONLY public.reports
    ADD CONSTRAINT reports_pkey PRIMARY KEY (report_id);
 >   ALTER TABLE ONLY public.reports DROP CONSTRAINT reports_pkey;
       public            postgres    false    225            	           2606    231458 $   role_permission role_permission_pkey 
   CONSTRAINT     r   ALTER TABLE ONLY public.role_permission
    ADD CONSTRAINT role_permission_pkey PRIMARY KEY (role_permission_id);
 N   ALTER TABLE ONLY public.role_permission DROP CONSTRAINT role_permission_pkey;
       public            postgres    false    217                       2606    231450    roles roles_pkey 
   CONSTRAINT     S   ALTER TABLE ONLY public.roles
    ADD CONSTRAINT roles_pkey PRIMARY KEY (role_id);
 :   ALTER TABLE ONLY public.roles DROP CONSTRAINT roles_pkey;
       public            postgres    false    215            k           2606    231997 &   seller_warehouse seller_warehouse_pkey 
   CONSTRAINT     n   ALTER TABLE ONLY public.seller_warehouse
    ADD CONSTRAINT seller_warehouse_pkey PRIMARY KEY (warehouse_id);
 P   ALTER TABLE ONLY public.seller_warehouse DROP CONSTRAINT seller_warehouse_pkey;
       public            postgres    false    316            �           2606    232261    seo_content seo_content_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.seo_content
    ADD CONSTRAINT seo_content_pkey PRIMARY KEY (id);
 F   ALTER TABLE ONLY public.seo_content DROP CONSTRAINT seo_content_pkey;
       public            postgres    false    364            1           2606    231675 $   shipment_status shipment_status_pkey 
   CONSTRAINT     n   ALTER TABLE ONLY public.shipment_status
    ADD CONSTRAINT shipment_status_pkey PRIMARY KEY (ship_status_id);
 N   ALTER TABLE ONLY public.shipment_status DROP CONSTRAINT shipment_status_pkey;
       public            postgres    false    257            �           2606    232283    sms_template sms_template_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.sms_template
    ADD CONSTRAINT sms_template_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY public.sms_template DROP CONSTRAINT sms_template_pkey;
       public            postgres    false    368            G           2606    231796     state_details state_details_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public.state_details
    ADD CONSTRAINT state_details_pkey PRIMARY KEY (id);
 J   ALTER TABLE ONLY public.state_details DROP CONSTRAINT state_details_pkey;
       public            postgres    false    279            -           2606    231653 "   status_details status_details_pkey 
   CONSTRAINT     g   ALTER TABLE ONLY public.status_details
    ADD CONSTRAINT status_details_pkey PRIMARY KEY (status_id);
 L   ALTER TABLE ONLY public.status_details DROP CONSTRAINT status_details_pkey;
       public            postgres    false    253            a           2606    231936 .   sub_group_management sub_group_management_pkey 
   CONSTRAINT     v   ALTER TABLE ONLY public.sub_group_management
    ADD CONSTRAINT sub_group_management_pkey PRIMARY KEY (sub_group_id);
 X   ALTER TABLE ONLY public.sub_group_management DROP CONSTRAINT sub_group_management_pkey;
       public            postgres    false    305            c           2606    231947 ,   sub_node_management sub_node_management_pkey 
   CONSTRAINT     s   ALTER TABLE ONLY public.sub_node_management
    ADD CONSTRAINT sub_node_management_pkey PRIMARY KEY (sub_node_id);
 V   ALTER TABLE ONLY public.sub_node_management DROP CONSTRAINT sub_node_management_pkey;
       public            postgres    false    307            s           2606    232041 (   sub_order_details sub_order_details_pkey 
   CONSTRAINT     f   ALTER TABLE ONLY public.sub_order_details
    ADD CONSTRAINT sub_order_details_pkey PRIMARY KEY (id);
 R   ALTER TABLE ONLY public.sub_order_details DROP CONSTRAINT sub_order_details_pkey;
       public            postgres    false    324            w           2606    232063 4   sub_order_quote_details sub_order_quote_details_pkey 
   CONSTRAINT     r   ALTER TABLE ONLY public.sub_order_quote_details
    ADD CONSTRAINT sub_order_quote_details_pkey PRIMARY KEY (id);
 ^   ALTER TABLE ONLY public.sub_order_quote_details DROP CONSTRAINT sub_order_quote_details_pkey;
       public            postgres    false    328            #           2606    231601 ,   subscription_detail subscription_detail_pkey 
   CONSTRAINT     ~   ALTER TABLE ONLY public.subscription_detail
    ADD CONSTRAINT subscription_detail_pkey PRIMARY KEY (subscription_detail_id);
 V   ALTER TABLE ONLY public.subscription_detail DROP CONSTRAINT subscription_detail_pkey;
       public            postgres    false    243            %           2606    231612 8   subscription_plan_details subscription_plan_details_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.subscription_plan_details
    ADD CONSTRAINT subscription_plan_details_pkey PRIMARY KEY (subscription_plan_id);
 b   ALTER TABLE ONLY public.subscription_plan_details DROP CONSTRAINT subscription_plan_details_pkey;
       public            postgres    false    245            �           2606    231387    super_admin super_admin_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.super_admin
    ADD CONSTRAINT super_admin_pkey PRIMARY KEY (id);
 F   ALTER TABLE ONLY public.super_admin DROP CONSTRAINT super_admin_pkey;
       public            postgres    false    203            K           2606    231815 &   tax_code_details tax_code_details_pkey 
   CONSTRAINT     m   ALTER TABLE ONLY public.tax_code_details
    ADD CONSTRAINT tax_code_details_pkey PRIMARY KEY (tax_code_id);
 P   ALTER TABLE ONLY public.tax_code_details DROP CONSTRAINT tax_code_details_pkey;
       public            postgres    false    283            S           2606    231859 .   trading_type_details trading_type_details_pkey 
   CONSTRAINT     y   ALTER TABLE ONLY public.trading_type_details
    ADD CONSTRAINT trading_type_details_pkey PRIMARY KEY (trading_type_id);
 X   ALTER TABLE ONLY public.trading_type_details DROP CONSTRAINT trading_type_details_pkey;
       public            postgres    false    291                       2606    231428 "   user_managment user_managment_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY public.user_managment
    ADD CONSTRAINT user_managment_pkey PRIMARY KEY (id);
 L   ALTER TABLE ONLY public.user_managment DROP CONSTRAINT user_managment_pkey;
       public            postgres    false    211                       2606    232107    vin_history vin_history_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.vin_history
    ADD CONSTRAINT vin_history_pkey PRIMARY KEY (id);
 F   ALTER TABLE ONLY public.vin_history DROP CONSTRAINT vin_history_pkey;
       public            postgres    false    336            r           2606    232519 '   Rating_review Rating_review_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public."Rating_review"
    ADD CONSTRAINT "Rating_review_status_fkey" FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 U   ALTER TABLE ONLY public."Rating_review" DROP CONSTRAINT "Rating_review_status_fkey";
       public          postgres    false    352    253    3373            �           2606    233505 ,   VAT_details VAT_details_jurisdiction_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public."VAT_details"
    ADD CONSTRAINT "VAT_details_jurisdiction_id_fkey" FOREIGN KEY (juristriction_id) REFERENCES public.jurisdiction(jurisdiction_id) NOT VALID;
 Z   ALTER TABLE ONLY public."VAT_details" DROP CONSTRAINT "VAT_details_jurisdiction_id_fkey";
       public          postgres    false    287    3407    281            �           2606    233500 #   VAT_details VAT_details_pos_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public."VAT_details"
    ADD CONSTRAINT "VAT_details_pos_id_fkey" FOREIGN KEY (pos_id) REFERENCES public.place_of_supply(pos_id) NOT VALID;
 Q   ALTER TABLE ONLY public."VAT_details" DROP CONSTRAINT "VAT_details_pos_id_fkey";
       public          postgres    false    3405    285    281            �           2606    232384 #   VAT_details VAT_details_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public."VAT_details"
    ADD CONSTRAINT "VAT_details_status_fkey" FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 Q   ALTER TABLE ONLY public."VAT_details" DROP CONSTRAINT "VAT_details_status_fkey";
       public          postgres    false    253    3373    281            �           2606    233495 %   VAT_details VAT_details_tax_code_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public."VAT_details"
    ADD CONSTRAINT "VAT_details_tax_code_fkey" FOREIGN KEY (tax_code_id) REFERENCES public.tax_code_details(tax_code_id) NOT VALID;
 S   ALTER TABLE ONLY public."VAT_details" DROP CONSTRAINT "VAT_details_tax_code_fkey";
       public          postgres    false    3403    281    283            �           2606    232729 R   application_user_permissions application_user_permissions_application_user_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.application_user_permissions
    ADD CONSTRAINT application_user_permissions_application_user_id_fkey FOREIGN KEY (application_user_id) REFERENCES public.application_user(application_user_id);
 |   ALTER TABLE ONLY public.application_user_permissions DROP CONSTRAINT application_user_permissions_application_user_id_fkey;
       public          postgres    false    209    207    3327            �           2606    232829 I   application_user_permissions application_user_permissions_created_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.application_user_permissions
    ADD CONSTRAINT application_user_permissions_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.user_managment(id);
 s   ALTER TABLE ONLY public.application_user_permissions DROP CONSTRAINT application_user_permissions_created_by_fkey;
       public          postgres    false    209    211    3331            �           2606    232744 K   application_user_permissions application_user_permissions_dashboard_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.application_user_permissions
    ADD CONSTRAINT application_user_permissions_dashboard_id_fkey FOREIGN KEY (dashboard_id) REFERENCES public.dashboard(dashboard_id);
 u   ALTER TABLE ONLY public.application_user_permissions DROP CONSTRAINT application_user_permissions_dashboard_id_fkey;
       public          postgres    false    209    223    3343            �           2606    232834 J   application_user_permissions application_user_permissions_modified_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.application_user_permissions
    ADD CONSTRAINT application_user_permissions_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.user_managment(id);
 t   ALTER TABLE ONLY public.application_user_permissions DROP CONSTRAINT application_user_permissions_modified_by_fkey;
       public          postgres    false    209    211    3331            �           2606    232734 H   application_user_permissions application_user_permissions_module_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.application_user_permissions
    ADD CONSTRAINT application_user_permissions_module_id_fkey FOREIGN KEY (module_id) REFERENCES public.module(module_id);
 r   ALTER TABLE ONLY public.application_user_permissions DROP CONSTRAINT application_user_permissions_module_id_fkey;
       public          postgres    false    209    221    3341            �           2606    232739 H   application_user_permissions application_user_permissions_report_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.application_user_permissions
    ADD CONSTRAINT application_user_permissions_report_id_fkey FOREIGN KEY (report_id) REFERENCES public.reports(report_id);
 r   ALTER TABLE ONLY public.application_user_permissions DROP CONSTRAINT application_user_permissions_report_id_fkey;
       public          postgres    false    209    225    3345            �           2606    232409 -   application_user application_user_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.application_user
    ADD CONSTRAINT application_user_status_fkey FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 W   ALTER TABLE ONLY public.application_user DROP CONSTRAINT application_user_status_fkey;
       public          postgres    false    207    253    3373            �           2606    233204 '   audit_trail audit_trail_created_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.audit_trail
    ADD CONSTRAINT audit_trail_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.user_managment(id);
 Q   ALTER TABLE ONLY public.audit_trail DROP CONSTRAINT audit_trail_created_by_fkey;
       public          postgres    false    362    211    3331            �           2606    233209 (   audit_trail audit_trail_modified_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.audit_trail
    ADD CONSTRAINT audit_trail_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.user_managment(id);
 R   ALTER TABLE ONLY public.audit_trail DROP CONSTRAINT audit_trail_modified_by_fkey;
       public          postgres    false    211    362    3331            �           2606    233399 &   audit_trail audit_trail_module_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.audit_trail
    ADD CONSTRAINT audit_trail_module_id_fkey FOREIGN KEY (module_id) REFERENCES public.module(module_id);
 P   ALTER TABLE ONLY public.audit_trail DROP CONSTRAINT audit_trail_module_id_fkey;
       public          postgres    false    221    362    3341            �           2606    232549 #   audit_trail audit_trail_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.audit_trail
    ADD CONSTRAINT audit_trail_status_fkey FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 M   ALTER TABLE ONLY public.audit_trail DROP CONSTRAINT audit_trail_status_fkey;
       public          postgres    false    362    253    3373            �           2606    232699    brands brands_created_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.brands
    ADD CONSTRAINT brands_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.user_managment(id);
 G   ALTER TABLE ONLY public.brands DROP CONSTRAINT brands_created_by_fkey;
       public          postgres    false    295    211    3331            �           2606    232704    brands brands_modified_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.brands
    ADD CONSTRAINT brands_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.user_managment(id);
 H   ALTER TABLE ONLY public.brands DROP CONSTRAINT brands_modified_by_fkey;
       public          postgres    false    295    211    3331            �           2606    232414    brands brands_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.brands
    ADD CONSTRAINT brands_status_fkey FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 C   ALTER TABLE ONLY public.brands DROP CONSTRAINT brands_status_fkey;
       public          postgres    false    295    253    3373            �           2606    232579 <   bulk_upload_details bulk_upload_details_product_type_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.bulk_upload_details
    ADD CONSTRAINT bulk_upload_details_product_type_id_fkey FOREIGN KEY (product_type_id) REFERENCES public.product_type_details(product_type_id);
 f   ALTER TABLE ONLY public.bulk_upload_details DROP CONSTRAINT bulk_upload_details_product_type_id_fkey;
       public          postgres    false    251    289    3409            �           2606    232574 3   bulk_upload_details bulk_upload_details_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.bulk_upload_details
    ADD CONSTRAINT bulk_upload_details_status_fkey FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 ]   ALTER TABLE ONLY public.bulk_upload_details DROP CONSTRAINT bulk_upload_details_status_fkey;
       public          postgres    false    251    253    3373            j           2606    232504 ;   buyer_customer_details buyer_customer_details_buyer_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.buyer_customer_details
    ADD CONSTRAINT buyer_customer_details_buyer_id_fkey FOREIGN KEY (buyer_id) REFERENCES public.new_buyer_details(id);
 e   ALTER TABLE ONLY public.buyer_customer_details DROP CONSTRAINT buyer_customer_details_buyer_id_fkey;
       public          postgres    false    344    311    3431            i           2606    232499 9   buyer_customer_details buyer_customer_details_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.buyer_customer_details
    ADD CONSTRAINT buyer_customer_details_status_fkey FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 c   ALTER TABLE ONLY public.buyer_customer_details DROP CONSTRAINT buyer_customer_details_status_fkey;
       public          postgres    false    344    253    3373            b           2606    233319 1   buyer_saved_quote buyer_saved_quote_buyer_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.buyer_saved_quote
    ADD CONSTRAINT buyer_saved_quote_buyer_id_fkey FOREIGN KEY (buyer_id) REFERENCES public.application_user(application_user_id);
 [   ALTER TABLE ONLY public.buyer_saved_quote DROP CONSTRAINT buyer_saved_quote_buyer_id_fkey;
       public          postgres    false    3327    340    207            d           2606    233329 :   buyer_saved_quote buyer_saved_quote_car_details_brand_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.buyer_saved_quote
    ADD CONSTRAINT buyer_saved_quote_car_details_brand_fkey FOREIGN KEY (car_details_brand) REFERENCES public.brands(brand_id);
 d   ALTER TABLE ONLY public.buyer_saved_quote DROP CONSTRAINT buyer_saved_quote_car_details_brand_fkey;
       public          postgres    false    340    295    3415            e           2606    233334 5   buyer_saved_quote buyer_saved_quote_cart_item_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.buyer_saved_quote
    ADD CONSTRAINT buyer_saved_quote_cart_item_id_fkey FOREIGN KEY (cart_item_id) REFERENCES public.cart_details(cart_item_id);
 _   ALTER TABLE ONLY public.buyer_saved_quote DROP CONSTRAINT buyer_saved_quote_cart_item_id_fkey;
       public          postgres    false    346    340    3465            c           2606    233324 1   buyer_saved_quote buyer_saved_quote_model_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.buyer_saved_quote
    ADD CONSTRAINT buyer_saved_quote_model_id_fkey FOREIGN KEY (model_id) REFERENCES public.models(model_id);
 [   ALTER TABLE ONLY public.buyer_saved_quote DROP CONSTRAINT buyer_saved_quote_model_id_fkey;
       public          postgres    false    297    340    3417            h           2606    233344 S   buyer_saved_quote_service_items buyer_saved_quote_service_items_buyer_quote_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.buyer_saved_quote_service_items
    ADD CONSTRAINT buyer_saved_quote_service_items_buyer_quote_id_fkey FOREIGN KEY (buyer_quote_id) REFERENCES public.buyer_saved_quote(buyer_quote_id);
 }   ALTER TABLE ONLY public.buyer_saved_quote_service_items DROP CONSTRAINT buyer_saved_quote_service_items_buyer_quote_id_fkey;
       public          postgres    false    340    342    3459            g           2606    232494 K   buyer_saved_quote_service_items buyer_saved_quote_service_items_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.buyer_saved_quote_service_items
    ADD CONSTRAINT buyer_saved_quote_service_items_status_fkey FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 u   ALTER TABLE ONLY public.buyer_saved_quote_service_items DROP CONSTRAINT buyer_saved_quote_service_items_status_fkey;
       public          postgres    false    342    253    3373            a           2606    232489 /   buyer_saved_quote buyer_saved_quote_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.buyer_saved_quote
    ADD CONSTRAINT buyer_saved_quote_status_fkey FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 Y   ALTER TABLE ONLY public.buyer_saved_quote DROP CONSTRAINT buyer_saved_quote_status_fkey;
       public          postgres    false    340    253    3373            f           2606    233339 8   buyer_saved_quote buyer_saved_quote_status_of_quote_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.buyer_saved_quote
    ADD CONSTRAINT buyer_saved_quote_status_of_quote_fkey FOREIGN KEY (status_of_quote) REFERENCES public.quote_status(quote_status_id);
 b   ALTER TABLE ONLY public.buyer_saved_quote DROP CONSTRAINT buyer_saved_quote_status_of_quote_fkey;
       public          postgres    false    265    3385    340            N           2606    232724 4   car_management car_management_buyer_customer_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.car_management
    ADD CONSTRAINT car_management_buyer_customer_id_fkey FOREIGN KEY (buyer_customer_id) REFERENCES public.buyer_customer_details(buyer_customer_id);
 ^   ALTER TABLE ONLY public.car_management DROP CONSTRAINT car_management_buyer_customer_id_fkey;
       public          postgres    false    330    344    3463            M           2606    232719 +   car_management car_management_buyer_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.car_management
    ADD CONSTRAINT car_management_buyer_id_fkey FOREIGN KEY (buyer_id) REFERENCES public.new_buyer_details(id);
 U   ALTER TABLE ONLY public.car_management DROP CONSTRAINT car_management_buyer_id_fkey;
       public          postgres    false    330    311    3431            K           2606    232709 -   car_management car_management_created_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.car_management
    ADD CONSTRAINT car_management_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.user_managment(id);
 W   ALTER TABLE ONLY public.car_management DROP CONSTRAINT car_management_created_by_fkey;
       public          postgres    false    330    211    3331            L           2606    232714 .   car_management car_management_modified_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.car_management
    ADD CONSTRAINT car_management_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.user_managment(id);
 X   ALTER TABLE ONLY public.car_management DROP CONSTRAINT car_management_modified_by_fkey;
       public          postgres    false    330    211    3331            J           2606    232464 )   car_management car_management_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.car_management
    ADD CONSTRAINT car_management_status_fkey FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 S   ALTER TABLE ONLY public.car_management DROP CONSTRAINT car_management_status_fkey;
       public          postgres    false    330    253    3373            �           2606    233069    cars cars_brand_id_fkey    FK CONSTRAINT     ~   ALTER TABLE ONLY public.cars
    ADD CONSTRAINT cars_brand_id_fkey FOREIGN KEY (brand_id) REFERENCES public.brands(brand_id);
 A   ALTER TABLE ONLY public.cars DROP CONSTRAINT cars_brand_id_fkey;
       public          postgres    false    301    295    3415            �           2606    233064    cars cars_created_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.cars
    ADD CONSTRAINT cars_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.user_managment(id);
 C   ALTER TABLE ONLY public.cars DROP CONSTRAINT cars_created_by_fkey;
       public          postgres    false    3331    211    301            �           2606    233079    cars cars_model_id_fkey    FK CONSTRAINT     ~   ALTER TABLE ONLY public.cars
    ADD CONSTRAINT cars_model_id_fkey FOREIGN KEY (model_id) REFERENCES public.models(model_id);
 A   ALTER TABLE ONLY public.cars DROP CONSTRAINT cars_model_id_fkey;
       public          postgres    false    301    3417    297            �           2606    233059    cars cars_modified_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.cars
    ADD CONSTRAINT cars_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.user_managment(id);
 D   ALTER TABLE ONLY public.cars DROP CONSTRAINT cars_modified_by_fkey;
       public          postgres    false    3331    301    211            �           2606    233074    cars cars_parameters_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.cars
    ADD CONSTRAINT cars_parameters_id_fkey FOREIGN KEY (parameters_id) REFERENCES public.parameters(id);
 F   ALTER TABLE ONLY public.cars DROP CONSTRAINT cars_parameters_id_fkey;
       public          postgres    false    301    3419    299            �           2606    232429    cars cars_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.cars
    ADD CONSTRAINT cars_status_fkey FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 ?   ALTER TABLE ONLY public.cars DROP CONSTRAINT cars_status_fkey;
       public          postgres    false    301    253    3373            `           2606    233159 N   category_request_management category_request_management_cat_req_status_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.category_request_management
    ADD CONSTRAINT category_request_management_cat_req_status_id_fkey FOREIGN KEY (cat_req_status_id) REFERENCES public.category_request_status(cat_req_status_id);
 x   ALTER TABLE ONLY public.category_request_management DROP CONSTRAINT category_request_management_cat_req_status_id_fkey;
       public          postgres    false    3389    338    269            ^           2606    233149 G   category_request_management category_request_management_created_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.category_request_management
    ADD CONSTRAINT category_request_management_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.super_admin(id);
 q   ALTER TABLE ONLY public.category_request_management DROP CONSTRAINT category_request_management_created_by_fkey;
       public          postgres    false    3323    338    203            ]           2606    233144 H   category_request_management category_request_management_modified_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.category_request_management
    ADD CONSTRAINT category_request_management_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.super_admin(id);
 r   ALTER TABLE ONLY public.category_request_management DROP CONSTRAINT category_request_management_modified_by_fkey;
       public          postgres    false    338    3323    203            _           2606    233154 N   category_request_management category_request_management_reason_for_reject_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.category_request_management
    ADD CONSTRAINT category_request_management_reason_for_reject_fkey FOREIGN KEY (reason_for_reject) REFERENCES public.reason_details(reason_id);
 x   ALTER TABLE ONLY public.category_request_management DROP CONSTRAINT category_request_management_reason_for_reject_fkey;
       public          postgres    false    3393    273    338            \           2606    232484 C   category_request_management category_request_management_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.category_request_management
    ADD CONSTRAINT category_request_management_status_fkey FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 m   ALTER TABLE ONLY public.category_request_management DROP CONSTRAINT category_request_management_status_fkey;
       public          postgres    false    338    253    3373            �           2606    232819 1   cms_company_info cms_company_info_created_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.cms_company_info
    ADD CONSTRAINT cms_company_info_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.user_managment(id);
 [   ALTER TABLE ONLY public.cms_company_info DROP CONSTRAINT cms_company_info_created_by_fkey;
       public          postgres    false    227    211    3331            �           2606    232824 2   cms_company_info cms_company_info_modified_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.cms_company_info
    ADD CONSTRAINT cms_company_info_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.user_managment(id);
 \   ALTER TABLE ONLY public.cms_company_info DROP CONSTRAINT cms_company_info_modified_by_fkey;
       public          postgres    false    227    211    3331            �           2606    232349 -   cms_company_info cms_company_info_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.cms_company_info
    ADD CONSTRAINT cms_company_info_status_fkey FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 W   ALTER TABLE ONLY public.cms_company_info DROP CONSTRAINT cms_company_info_status_fkey;
       public          postgres    false    227    253    3373            �           2606    232749 1   cms_footer_links cms_footer_links_created_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.cms_footer_links
    ADD CONSTRAINT cms_footer_links_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.user_managment(id);
 [   ALTER TABLE ONLY public.cms_footer_links DROP CONSTRAINT cms_footer_links_created_by_fkey;
       public          postgres    false    211    241    3331            �           2606    232754 2   cms_footer_links cms_footer_links_modified_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.cms_footer_links
    ADD CONSTRAINT cms_footer_links_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.user_managment(id);
 \   ALTER TABLE ONLY public.cms_footer_links DROP CONSTRAINT cms_footer_links_modified_by_fkey;
       public          postgres    false    211    241    3331            �           2606    232314 -   cms_footer_links cms_footer_links_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.cms_footer_links
    ADD CONSTRAINT cms_footer_links_status_fkey FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 W   ALTER TABLE ONLY public.cms_footer_links DROP CONSTRAINT cms_footer_links_status_fkey;
       public          postgres    false    3373    253    241            �           2606    232769 E   cms_homepage_about_section cms_homepage_about_section_created_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.cms_homepage_about_section
    ADD CONSTRAINT cms_homepage_about_section_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.user_managment(id);
 o   ALTER TABLE ONLY public.cms_homepage_about_section DROP CONSTRAINT cms_homepage_about_section_created_by_fkey;
       public          postgres    false    237    211    3331            �           2606    232774 F   cms_homepage_about_section cms_homepage_about_section_modified_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.cms_homepage_about_section
    ADD CONSTRAINT cms_homepage_about_section_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.user_managment(id);
 p   ALTER TABLE ONLY public.cms_homepage_about_section DROP CONSTRAINT cms_homepage_about_section_modified_by_fkey;
       public          postgres    false    237    211    3331            �           2606    232324 A   cms_homepage_about_section cms_homepage_about_section_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.cms_homepage_about_section
    ADD CONSTRAINT cms_homepage_about_section_status_fkey FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 k   ALTER TABLE ONLY public.cms_homepage_about_section DROP CONSTRAINT cms_homepage_about_section_status_fkey;
       public          postgres    false    237    253    3373            �           2606    232789 G   cms_homepage_banner_section cms_homepage_banner_section_created_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.cms_homepage_banner_section
    ADD CONSTRAINT cms_homepage_banner_section_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.user_managment(id);
 q   ALTER TABLE ONLY public.cms_homepage_banner_section DROP CONSTRAINT cms_homepage_banner_section_created_by_fkey;
       public          postgres    false    233    211    3331            �           2606    232794 H   cms_homepage_banner_section cms_homepage_banner_section_modified_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.cms_homepage_banner_section
    ADD CONSTRAINT cms_homepage_banner_section_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.user_managment(id);
 r   ALTER TABLE ONLY public.cms_homepage_banner_section DROP CONSTRAINT cms_homepage_banner_section_modified_by_fkey;
       public          postgres    false    233    211    3331            �           2606    232334 C   cms_homepage_banner_section cms_homepage_banner_section_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.cms_homepage_banner_section
    ADD CONSTRAINT cms_homepage_banner_section_status_fkey FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 m   ALTER TABLE ONLY public.cms_homepage_banner_section DROP CONSTRAINT cms_homepage_banner_section_status_fkey;
       public          postgres    false    253    3373    233            �           2606    232759 Q   cms_homepage_buyerseller_section cms_homepage_buyerseller_section_created_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.cms_homepage_buyerseller_section
    ADD CONSTRAINT cms_homepage_buyerseller_section_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.user_managment(id);
 {   ALTER TABLE ONLY public.cms_homepage_buyerseller_section DROP CONSTRAINT cms_homepage_buyerseller_section_created_by_fkey;
       public          postgres    false    239    211    3331            �           2606    232764 R   cms_homepage_buyerseller_section cms_homepage_buyerseller_section_modified_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.cms_homepage_buyerseller_section
    ADD CONSTRAINT cms_homepage_buyerseller_section_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.user_managment(id);
 |   ALTER TABLE ONLY public.cms_homepage_buyerseller_section DROP CONSTRAINT cms_homepage_buyerseller_section_modified_by_fkey;
       public          postgres    false    239    211    3331            �           2606    232319 M   cms_homepage_buyerseller_section cms_homepage_buyerseller_section_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.cms_homepage_buyerseller_section
    ADD CONSTRAINT cms_homepage_buyerseller_section_status_fkey FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 w   ALTER TABLE ONLY public.cms_homepage_buyerseller_section DROP CONSTRAINT cms_homepage_buyerseller_section_status_fkey;
       public          postgres    false    253    3373    239            �           2606    232779 K   cms_homepage_features_section cms_homepage_features_section_created_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.cms_homepage_features_section
    ADD CONSTRAINT cms_homepage_features_section_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.user_managment(id);
 u   ALTER TABLE ONLY public.cms_homepage_features_section DROP CONSTRAINT cms_homepage_features_section_created_by_fkey;
       public          postgres    false    235    211    3331            �           2606    232784 L   cms_homepage_features_section cms_homepage_features_section_modified_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.cms_homepage_features_section
    ADD CONSTRAINT cms_homepage_features_section_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.user_managment(id);
 v   ALTER TABLE ONLY public.cms_homepage_features_section DROP CONSTRAINT cms_homepage_features_section_modified_by_fkey;
       public          postgres    false    235    211    3331            �           2606    232329 G   cms_homepage_features_section cms_homepage_features_section_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.cms_homepage_features_section
    ADD CONSTRAINT cms_homepage_features_section_status_fkey FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 q   ALTER TABLE ONLY public.cms_homepage_features_section DROP CONSTRAINT cms_homepage_features_section_status_fkey;
       public          postgres    false    235    3373    253            �           2606    232799 1   cms_menu_section cms_menu_section_created_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.cms_menu_section
    ADD CONSTRAINT cms_menu_section_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.user_managment(id);
 [   ALTER TABLE ONLY public.cms_menu_section DROP CONSTRAINT cms_menu_section_created_by_fkey;
       public          postgres    false    231    211    3331            �           2606    232804 2   cms_menu_section cms_menu_section_modified_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.cms_menu_section
    ADD CONSTRAINT cms_menu_section_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.user_managment(id);
 \   ALTER TABLE ONLY public.cms_menu_section DROP CONSTRAINT cms_menu_section_modified_by_fkey;
       public          postgres    false    231    211    3331            �           2606    232339 -   cms_menu_section cms_menu_section_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.cms_menu_section
    ADD CONSTRAINT cms_menu_section_status_fkey FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 W   ALTER TABLE ONLY public.cms_menu_section DROP CONSTRAINT cms_menu_section_status_fkey;
       public          postgres    false    231    3373    253            �           2606    232809 ;   cms_social_media_info cms_social_media_info_created_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.cms_social_media_info
    ADD CONSTRAINT cms_social_media_info_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.user_managment(id);
 e   ALTER TABLE ONLY public.cms_social_media_info DROP CONSTRAINT cms_social_media_info_created_by_fkey;
       public          postgres    false    211    229    3331            �           2606    232814 <   cms_social_media_info cms_social_media_info_modified_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.cms_social_media_info
    ADD CONSTRAINT cms_social_media_info_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.user_managment(id);
 f   ALTER TABLE ONLY public.cms_social_media_info DROP CONSTRAINT cms_social_media_info_modified_by_fkey;
       public          postgres    false    211    229    3331            �           2606    232344 7   cms_social_media_info cms_social_media_info_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.cms_social_media_info
    ADD CONSTRAINT cms_social_media_info_status_fkey FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 a   ALTER TABLE ONLY public.cms_social_media_info DROP CONSTRAINT cms_social_media_info_status_fkey;
       public          postgres    false    229    253    3373            ,           2606    233124 ;   commission_management commission_management_created_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.commission_management
    ADD CONSTRAINT commission_management_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.user_managment(id);
 e   ALTER TABLE ONLY public.commission_management DROP CONSTRAINT commission_management_created_by_fkey;
       public          postgres    false    211    3331    320            -           2606    233129 <   commission_management commission_management_modified_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.commission_management
    ADD CONSTRAINT commission_management_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.user_managment(id);
 f   ALTER TABLE ONLY public.commission_management DROP CONSTRAINT commission_management_modified_by_fkey;
       public          postgres    false    211    3331    320            +           2606    233119 @   commission_management commission_management_product_type_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.commission_management
    ADD CONSTRAINT commission_management_product_type_id_fkey FOREIGN KEY (product_type_id) REFERENCES public.product_type_details(product_type_id);
 j   ALTER TABLE ONLY public.commission_management DROP CONSTRAINT commission_management_product_type_id_fkey;
       public          postgres    false    320    289    3409            *           2606    232459 7   commission_management commission_management_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.commission_management
    ADD CONSTRAINT commission_management_status_fkey FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 a   ALTER TABLE ONLY public.commission_management DROP CONSTRAINT commission_management_status_fkey;
       public          postgres    false    320    253    3373            �           2606    232359 7   configuration_details configuration_details_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.configuration_details
    ADD CONSTRAINT configuration_details_status_fkey FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 a   ALTER TABLE ONLY public.configuration_details DROP CONSTRAINT configuration_details_status_fkey;
       public          postgres    false    253    3373    249            �           2606    232619 /   country_details country_details_created_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.country_details
    ADD CONSTRAINT country_details_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.user_managment(id);
 Y   ALTER TABLE ONLY public.country_details DROP CONSTRAINT country_details_created_by_fkey;
       public          postgres    false    277    211    3331            �           2606    232624 0   country_details country_details_modified_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.country_details
    ADD CONSTRAINT country_details_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.user_managment(id);
 Z   ALTER TABLE ONLY public.country_details DROP CONSTRAINT country_details_modified_by_fkey;
       public          postgres    false    277    211    3331            �           2606    232374 +   country_details country_details_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.country_details
    ADD CONSTRAINT country_details_status_fkey FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 U   ALTER TABLE ONLY public.country_details DROP CONSTRAINT country_details_status_fkey;
       public          postgres    false    253    3373    277            �           2606    232609 1   currency_details currency_details_created_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.currency_details
    ADD CONSTRAINT currency_details_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.user_managment(id);
 [   ALTER TABLE ONLY public.currency_details DROP CONSTRAINT currency_details_created_by_fkey;
       public          postgres    false    275    211    3331            �           2606    232614 2   currency_details currency_details_modified_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.currency_details
    ADD CONSTRAINT currency_details_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.user_managment(id);
 \   ALTER TABLE ONLY public.currency_details DROP CONSTRAINT currency_details_modified_by_fkey;
       public          postgres    false    275    211    3331            �           2606    232369 -   currency_details currency_details_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.currency_details
    ADD CONSTRAINT currency_details_status_fkey FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 W   ALTER TABLE ONLY public.currency_details DROP CONSTRAINT currency_details_status_fkey;
       public          postgres    false    3373    253    275            �           2606    233194 -   email_template email_template_created_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.email_template
    ADD CONSTRAINT email_template_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.super_admin(id);
 W   ALTER TABLE ONLY public.email_template DROP CONSTRAINT email_template_created_by_fkey;
       public          postgres    false    3323    366    203            �           2606    233199 .   email_template email_template_modified_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.email_template
    ADD CONSTRAINT email_template_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.super_admin(id);
 X   ALTER TABLE ONLY public.email_template DROP CONSTRAINT email_template_modified_by_fkey;
       public          postgres    false    3323    203    366            �           2606    232559 )   email_template email_template_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.email_template
    ADD CONSTRAINT email_template_status_fkey FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 S   ALTER TABLE ONLY public.email_template DROP CONSTRAINT email_template_status_fkey;
       public          postgres    false    366    253    3373                       2606    233169 -   group_management group_management_car_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.group_management
    ADD CONSTRAINT group_management_car_id_fkey FOREIGN KEY (car_id) REFERENCES public.cars(car_id);
 W   ALTER TABLE ONLY public.group_management DROP CONSTRAINT group_management_car_id_fkey;
       public          postgres    false    3421    303    301            �           2606    232839 1   group_management group_management_created_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.group_management
    ADD CONSTRAINT group_management_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.user_managment(id);
 [   ALTER TABLE ONLY public.group_management DROP CONSTRAINT group_management_created_by_fkey;
       public          postgres    false    211    303    3331                        2606    232844 2   group_management group_management_modified_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.group_management
    ADD CONSTRAINT group_management_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.user_managment(id);
 \   ALTER TABLE ONLY public.group_management DROP CONSTRAINT group_management_modified_by_fkey;
       public          postgres    false    211    303    3331            �           2606    232434 -   group_management group_management_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.group_management
    ADD CONSTRAINT group_management_status_fkey FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 W   ALTER TABLE ONLY public.group_management DROP CONSTRAINT group_management_status_fkey;
       public          postgres    false    303    253    3373                       2606    233024 /   help_management help_management_created_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.help_management
    ADD CONSTRAINT help_management_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.user_managment(id);
 Y   ALTER TABLE ONLY public.help_management DROP CONSTRAINT help_management_created_by_fkey;
       public          postgres    false    3331    211    360            �           2606    233029 0   help_management help_management_modified_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.help_management
    ADD CONSTRAINT help_management_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.user_managment(id);
 Z   ALTER TABLE ONLY public.help_management DROP CONSTRAINT help_management_modified_by_fkey;
       public          postgres    false    3331    211    360            �           2606    233389 .   help_management help_management_module_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.help_management
    ADD CONSTRAINT help_management_module_id_fkey FOREIGN KEY (module_id) REFERENCES public.module(module_id);
 X   ALTER TABLE ONLY public.help_management DROP CONSTRAINT help_management_module_id_fkey;
       public          postgres    false    360    3341    221            ~           2606    232544 +   help_management help_management_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.help_management
    ADD CONSTRAINT help_management_status_fkey FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 U   ALTER TABLE ONLY public.help_management DROP CONSTRAINT help_management_status_fkey;
       public          postgres    false    360    253    3373            �           2606    233009 (   intermediator intermediator_country_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.intermediator
    ADD CONSTRAINT intermediator_country_fkey FOREIGN KEY (country) REFERENCES public.country_details(country_id);
 R   ALTER TABLE ONLY public.intermediator DROP CONSTRAINT intermediator_country_fkey;
       public          postgres    false    277    205    3397            �           2606    232404 '   intermediator intermediator_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.intermediator
    ADD CONSTRAINT intermediator_status_fkey FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 Q   ALTER TABLE ONLY public.intermediator DROP CONSTRAINT intermediator_status_fkey;
       public          postgres    false    205    253    3373            �           2606    233459 (   intermediator intermediator_user_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.intermediator
    ADD CONSTRAINT intermediator_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.user_managment(id);
 R   ALTER TABLE ONLY public.intermediator DROP CONSTRAINT intermediator_user_id_fkey;
       public          postgres    false    3331    211    205            {           2606    233034 ;   inventory_manangement inventory_manangement_created_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.inventory_manangement
    ADD CONSTRAINT inventory_manangement_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.user_managment(id);
 e   ALTER TABLE ONLY public.inventory_manangement DROP CONSTRAINT inventory_manangement_created_by_fkey;
       public          postgres    false    211    358    3331            |           2606    233039 <   inventory_manangement inventory_manangement_modified_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.inventory_manangement
    ADD CONSTRAINT inventory_manangement_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.user_managment(id);
 f   ALTER TABLE ONLY public.inventory_manangement DROP CONSTRAINT inventory_manangement_modified_by_fkey;
       public          postgres    false    211    3331    358            }           2606    233054 :   inventory_manangement inventory_manangement_seller_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.inventory_manangement
    ADD CONSTRAINT inventory_manangement_seller_id_fkey FOREIGN KEY (seller_id) REFERENCES public.new_seller_details(id);
 d   ALTER TABLE ONLY public.inventory_manangement DROP CONSTRAINT inventory_manangement_seller_id_fkey;
       public          postgres    false    3433    313    358            y           2606    232539 7   inventory_manangement inventory_manangement_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.inventory_manangement
    ADD CONSTRAINT inventory_manangement_status_fkey FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 a   ALTER TABLE ONLY public.inventory_manangement DROP CONSTRAINT inventory_manangement_status_fkey;
       public          postgres    false    358    253    3373            z           2606    232594 =   inventory_manangement inventory_manangement_warehouse_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.inventory_manangement
    ADD CONSTRAINT inventory_manangement_warehouse_id_fkey FOREIGN KEY (warehouse_id) REFERENCES public.seller_warehouse(warehouse_id);
 g   ALTER TABLE ONLY public.inventory_manangement DROP CONSTRAINT inventory_manangement_warehouse_id_fkey;
       public          postgres    false    358    316    3435            �           2606    232679    models models_created_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.models
    ADD CONSTRAINT models_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.user_managment(id);
 G   ALTER TABLE ONLY public.models DROP CONSTRAINT models_created_by_fkey;
       public          postgres    false    297    211    3331            �           2606    232684    models models_modified_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.models
    ADD CONSTRAINT models_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.user_managment(id);
 H   ALTER TABLE ONLY public.models DROP CONSTRAINT models_modified_by_fkey;
       public          postgres    false    297    211    3331            �           2606    232419    models models_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.models
    ADD CONSTRAINT models_status_fkey FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 C   ALTER TABLE ONLY public.models DROP CONSTRAINT models_status_fkey;
       public          postgres    false    297    253    3373            �           2606    232569    module module_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.module
    ADD CONSTRAINT module_status_fkey FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 C   ALTER TABLE ONLY public.module DROP CONSTRAINT module_status_fkey;
       public          postgres    false    221    253    3373                       2606    232884 6   new_buyer_details new_buyer_details_bank_currency_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.new_buyer_details
    ADD CONSTRAINT new_buyer_details_bank_currency_fkey FOREIGN KEY (bank_currency) REFERENCES public.currency_details(currency_id);
 `   ALTER TABLE ONLY public.new_buyer_details DROP CONSTRAINT new_buyer_details_bank_currency_fkey;
       public          postgres    false    275    311    3395                       2606    232929 8   new_buyer_details new_buyer_details_billing_country_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.new_buyer_details
    ADD CONSTRAINT new_buyer_details_billing_country_fkey FOREIGN KEY (billing_country) REFERENCES public.country_details(country_id);
 b   ALTER TABLE ONLY public.new_buyer_details DROP CONSTRAINT new_buyer_details_billing_country_fkey;
       public          postgres    false    277    311    3397                       2606    232924 6   new_buyer_details new_buyer_details_billing_state_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.new_buyer_details
    ADD CONSTRAINT new_buyer_details_billing_state_fkey FOREIGN KEY (billing_state) REFERENCES public.state_details(id);
 `   ALTER TABLE ONLY public.new_buyer_details DROP CONSTRAINT new_buyer_details_billing_state_fkey;
       public          postgres    false    279    3399    311                       2606    232889 6   new_buyer_details new_buyer_details_cancel_reason_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.new_buyer_details
    ADD CONSTRAINT new_buyer_details_cancel_reason_fkey FOREIGN KEY (cancel_reason) REFERENCES public.reason_details(reason_id);
 `   ALTER TABLE ONLY public.new_buyer_details DROP CONSTRAINT new_buyer_details_cancel_reason_fkey;
       public          postgres    false    3393    273    311                       2606    232879 3   new_buyer_details new_buyer_details_created_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.new_buyer_details
    ADD CONSTRAINT new_buyer_details_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.user_managment(id);
 ]   ALTER TABLE ONLY public.new_buyer_details DROP CONSTRAINT new_buyer_details_created_by_fkey;
       public          postgres    false    211    311    3331                       2606    232894 9   new_buyer_details new_buyer_details_iv_country_issue_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.new_buyer_details
    ADD CONSTRAINT new_buyer_details_iv_country_issue_fkey FOREIGN KEY (iv_country_issue) REFERENCES public.country_details(country_id);
 c   ALTER TABLE ONLY public.new_buyer_details DROP CONSTRAINT new_buyer_details_iv_country_issue_fkey;
       public          postgres    false    3397    311    277                       2606    232899 4   new_buyer_details new_buyer_details_iv_location_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.new_buyer_details
    ADD CONSTRAINT new_buyer_details_iv_location_fkey FOREIGN KEY (iv_location) REFERENCES public.country_details(country_id);
 ^   ALTER TABLE ONLY public.new_buyer_details DROP CONSTRAINT new_buyer_details_iv_location_fkey;
       public          postgres    false    3397    311    277                       2606    232874 4   new_buyer_details new_buyer_details_modified_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.new_buyer_details
    ADD CONSTRAINT new_buyer_details_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.user_managment(id);
 ^   ALTER TABLE ONLY public.new_buyer_details DROP CONSTRAINT new_buyer_details_modified_by_fkey;
       public          postgres    false    3331    311    211                       2606    232909 8   new_buyer_details new_buyer_details_product_type_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.new_buyer_details
    ADD CONSTRAINT new_buyer_details_product_type_id_fkey FOREIGN KEY (product_type_id) REFERENCES public.product_type_details(product_type_id);
 b   ALTER TABLE ONLY public.new_buyer_details DROP CONSTRAINT new_buyer_details_product_type_id_fkey;
       public          postgres    false    289    3409    311                       2606    232914 9   new_buyer_details new_buyer_details_shipping_country_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.new_buyer_details
    ADD CONSTRAINT new_buyer_details_shipping_country_fkey FOREIGN KEY (shipping_country) REFERENCES public.country_details(country_id);
 c   ALTER TABLE ONLY public.new_buyer_details DROP CONSTRAINT new_buyer_details_shipping_country_fkey;
       public          postgres    false    277    311    3397                       2606    232919 7   new_buyer_details new_buyer_details_shipping_state_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.new_buyer_details
    ADD CONSTRAINT new_buyer_details_shipping_state_fkey FOREIGN KEY (shipping_state) REFERENCES public.state_details(id);
 a   ALTER TABLE ONLY public.new_buyer_details DROP CONSTRAINT new_buyer_details_shipping_state_fkey;
       public          postgres    false    3399    279    311                       2606    232934 8   new_buyer_details new_buyer_details_subscription_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.new_buyer_details
    ADD CONSTRAINT new_buyer_details_subscription_id_fkey FOREIGN KEY (subscription_id) REFERENCES public.subscription_plan_details(subscription_plan_id);
 b   ALTER TABLE ONLY public.new_buyer_details DROP CONSTRAINT new_buyer_details_subscription_id_fkey;
       public          postgres    false    311    3365    245                       2606    232904 8   new_buyer_details new_buyer_details_trading_type_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.new_buyer_details
    ADD CONSTRAINT new_buyer_details_trading_type_id_fkey FOREIGN KEY (trading_type_id) REFERENCES public.trading_type_details(trading_type_id);
 b   ALTER TABLE ONLY public.new_buyer_details DROP CONSTRAINT new_buyer_details_trading_type_id_fkey;
       public          postgres    false    291    3411    311                       2606    233454 0   new_buyer_details new_buyer_details_user_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.new_buyer_details
    ADD CONSTRAINT new_buyer_details_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.user_managment(id);
 Z   ALTER TABLE ONLY public.new_buyer_details DROP CONSTRAINT new_buyer_details_user_id_fkey;
       public          postgres    false    3331    211    311            %           2606    232994 8   new_seller_details new_seller_details_bank_currency_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.new_seller_details
    ADD CONSTRAINT new_seller_details_bank_currency_fkey FOREIGN KEY (bank_currency) REFERENCES public.currency_details(currency_id);
 b   ALTER TABLE ONLY public.new_seller_details DROP CONSTRAINT new_seller_details_bank_currency_fkey;
       public          postgres    false    313    275    3395                       2606    232949 :   new_seller_details new_seller_details_billing_country_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.new_seller_details
    ADD CONSTRAINT new_seller_details_billing_country_fkey FOREIGN KEY (billing_country) REFERENCES public.country_details(country_id);
 d   ALTER TABLE ONLY public.new_seller_details DROP CONSTRAINT new_seller_details_billing_country_fkey;
       public          postgres    false    313    3397    277                       2606    232954 8   new_seller_details new_seller_details_billing_state_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.new_seller_details
    ADD CONSTRAINT new_seller_details_billing_state_fkey FOREIGN KEY (billing_state) REFERENCES public.state_details(id);
 b   ALTER TABLE ONLY public.new_seller_details DROP CONSTRAINT new_seller_details_billing_state_fkey;
       public          postgres    false    3399    313    279            $           2606    232989 6   new_seller_details new_seller_details_fail_reason_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.new_seller_details
    ADD CONSTRAINT new_seller_details_fail_reason_fkey FOREIGN KEY (fail_reason) REFERENCES public.reason_details(reason_id);
 `   ALTER TABLE ONLY public.new_seller_details DROP CONSTRAINT new_seller_details_fail_reason_fkey;
       public          postgres    false    3393    313    273            #           2606    232984 ;   new_seller_details new_seller_details_iv_country_issue_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.new_seller_details
    ADD CONSTRAINT new_seller_details_iv_country_issue_fkey FOREIGN KEY (iv_country_issue) REFERENCES public.country_details(country_id);
 e   ALTER TABLE ONLY public.new_seller_details DROP CONSTRAINT new_seller_details_iv_country_issue_fkey;
       public          postgres    false    277    3397    313            "           2606    232979 6   new_seller_details new_seller_details_iv_location_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.new_seller_details
    ADD CONSTRAINT new_seller_details_iv_location_fkey FOREIGN KEY (iv_location) REFERENCES public.country_details(country_id);
 `   ALTER TABLE ONLY public.new_seller_details DROP CONSTRAINT new_seller_details_iv_location_fkey;
       public          postgres    false    277    313    3397                       2606    232944 9   new_seller_details new_seller_details_payout_shed_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.new_seller_details
    ADD CONSTRAINT new_seller_details_payout_shed_id_fkey FOREIGN KEY (payout_shed_id) REFERENCES public.payout_shed_details(payout_shed_id);
 c   ALTER TABLE ONLY public.new_seller_details DROP CONSTRAINT new_seller_details_payout_shed_id_fkey;
       public          postgres    false    3367    313    247                        2606    232969 :   new_seller_details new_seller_details_product_type_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.new_seller_details
    ADD CONSTRAINT new_seller_details_product_type_id_fkey FOREIGN KEY (product_type_id) REFERENCES public.product_type_details(product_type_id);
 d   ALTER TABLE ONLY public.new_seller_details DROP CONSTRAINT new_seller_details_product_type_id_fkey;
       public          postgres    false    313    289    3409                       2606    232959 ;   new_seller_details new_seller_details_shipping_country_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.new_seller_details
    ADD CONSTRAINT new_seller_details_shipping_country_fkey FOREIGN KEY (shipping_country) REFERENCES public.country_details(country_id);
 e   ALTER TABLE ONLY public.new_seller_details DROP CONSTRAINT new_seller_details_shipping_country_fkey;
       public          postgres    false    3397    313    277                       2606    232964 9   new_seller_details new_seller_details_shipping_state_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.new_seller_details
    ADD CONSTRAINT new_seller_details_shipping_state_fkey FOREIGN KEY (shipping_state) REFERENCES public.state_details(id);
 c   ALTER TABLE ONLY public.new_seller_details DROP CONSTRAINT new_seller_details_shipping_state_fkey;
       public          postgres    false    3399    313    279                       2606    232939 :   new_seller_details new_seller_details_subscription_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.new_seller_details
    ADD CONSTRAINT new_seller_details_subscription_id_fkey FOREIGN KEY (subscription_id) REFERENCES public.subscription_plan_details(subscription_plan_id);
 d   ALTER TABLE ONLY public.new_seller_details DROP CONSTRAINT new_seller_details_subscription_id_fkey;
       public          postgres    false    3365    313    245            !           2606    232974 :   new_seller_details new_seller_details_trading_type_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.new_seller_details
    ADD CONSTRAINT new_seller_details_trading_type_id_fkey FOREIGN KEY (trading_type_id) REFERENCES public.trading_type_details(trading_type_id);
 d   ALTER TABLE ONLY public.new_seller_details DROP CONSTRAINT new_seller_details_trading_type_id_fkey;
       public          postgres    false    291    3411    313            &           2606    233449 2   new_seller_details new_seller_details_user_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.new_seller_details
    ADD CONSTRAINT new_seller_details_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.user_managment(id);
 \   ALTER TABLE ONLY public.new_seller_details DROP CONSTRAINT new_seller_details_user_id_fkey;
       public          postgres    false    3331    313    211            l           2606    233134 ;   newsletter_management newsletter_management_created_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.newsletter_management
    ADD CONSTRAINT newsletter_management_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.user_managment(id);
 e   ALTER TABLE ONLY public.newsletter_management DROP CONSTRAINT newsletter_management_created_by_fkey;
       public          postgres    false    211    348    3331            m           2606    233139 <   newsletter_management newsletter_management_modified_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.newsletter_management
    ADD CONSTRAINT newsletter_management_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.user_managment(id);
 f   ALTER TABLE ONLY public.newsletter_management DROP CONSTRAINT newsletter_management_modified_by_fkey;
       public          postgres    false    3331    211    348            k           2606    232509 7   newsletter_management newsletter_management_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.newsletter_management
    ADD CONSTRAINT newsletter_management_status_fkey FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 a   ALTER TABLE ONLY public.newsletter_management DROP CONSTRAINT newsletter_management_status_fkey;
       public          postgres    false    348    253    3373            p           2606    233379 -   notification notification_email_template_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.notification
    ADD CONSTRAINT notification_email_template_fkey FOREIGN KEY (email_template) REFERENCES public.email_template(id);
 W   ALTER TABLE ONLY public.notification DROP CONSTRAINT notification_email_template_fkey;
       public          postgres    false    3485    350    366            o           2606    233374 (   notification notification_module_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.notification
    ADD CONSTRAINT notification_module_id_fkey FOREIGN KEY (module_id) REFERENCES public.module(module_id);
 R   ALTER TABLE ONLY public.notification DROP CONSTRAINT notification_module_id_fkey;
       public          postgres    false    3341    350    221            q           2606    233384 +   notification notification_sms_template_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.notification
    ADD CONSTRAINT notification_sms_template_fkey FOREIGN KEY (sms_template) REFERENCES public.sms_template(id);
 U   ALTER TABLE ONLY public.notification DROP CONSTRAINT notification_sms_template_fkey;
       public          postgres    false    3487    368    350            n           2606    232514 %   notification notification_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.notification
    ADD CONSTRAINT notification_status_fkey FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 O   ALTER TABLE ONLY public.notification DROP CONSTRAINT notification_status_fkey;
       public          postgres    false    350    253    3373            w           2606    233439 /   offer_management offer_management_buyer_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.offer_management
    ADD CONSTRAINT offer_management_buyer_id_fkey FOREIGN KEY (buyer_id) REFERENCES public.new_buyer_details(id);
 Y   ALTER TABLE ONLY public.offer_management DROP CONSTRAINT offer_management_buyer_id_fkey;
       public          postgres    false    3431    311    356            u           2606    233044 1   offer_management offer_management_created_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.offer_management
    ADD CONSTRAINT offer_management_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.user_managment(id);
 [   ALTER TABLE ONLY public.offer_management DROP CONSTRAINT offer_management_created_by_fkey;
       public          postgres    false    211    356    3331            v           2606    233049 2   offer_management offer_management_modified_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.offer_management
    ADD CONSTRAINT offer_management_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.user_managment(id);
 \   ALTER TABLE ONLY public.offer_management DROP CONSTRAINT offer_management_modified_by_fkey;
       public          postgres    false    3331    211    356            x           2606    233444 0   offer_management offer_management_seller_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.offer_management
    ADD CONSTRAINT offer_management_seller_id_fkey FOREIGN KEY (seller_id) REFERENCES public.new_seller_details(id);
 Z   ALTER TABLE ONLY public.offer_management DROP CONSTRAINT offer_management_seller_id_fkey;
       public          postgres    false    3433    313    356            t           2606    232529 -   offer_management offer_management_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.offer_management
    ADD CONSTRAINT offer_management_status_fkey FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 W   ALTER TABLE ONLY public.offer_management DROP CONSTRAINT offer_management_status_fkey;
       public          postgres    false    356    253    3373            3           2606    233419 /   order_management order_management_buyer_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.order_management
    ADD CONSTRAINT order_management_buyer_id_fkey FOREIGN KEY (buyer_id) REFERENCES public.new_buyer_details(id);
 Y   ALTER TABLE ONLY public.order_management DROP CONSTRAINT order_management_buyer_id_fkey;
       public          postgres    false    3431    322    311            /           2606    233219 1   order_management order_management_created_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.order_management
    ADD CONSTRAINT order_management_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.user_managment(id);
 [   ALTER TABLE ONLY public.order_management DROP CONSTRAINT order_management_created_by_fkey;
       public          postgres    false    322    211    3331            5           2606    233434 8   order_management order_management_method_of_payment_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.order_management
    ADD CONSTRAINT order_management_method_of_payment_fkey FOREIGN KEY (method_of_payment) REFERENCES public.payment_type_details(payment_type_id);
 b   ALTER TABLE ONLY public.order_management DROP CONSTRAINT order_management_method_of_payment_fkey;
       public          postgres    false    3413    293    322            .           2606    233214 2   order_management order_management_modified_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.order_management
    ADD CONSTRAINT order_management_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.user_managment(id);
 \   ALTER TABLE ONLY public.order_management DROP CONSTRAINT order_management_modified_by_fkey;
       public          postgres    false    322    211    3331            0           2606    233224 6   order_management order_management_order_status_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.order_management
    ADD CONSTRAINT order_management_order_status_id_fkey FOREIGN KEY (order_status_id) REFERENCES public.order_status(order_status_id);
 `   ALTER TABLE ONLY public.order_management DROP CONSTRAINT order_management_order_status_id_fkey;
       public          postgres    false    322    259    3379            4           2606    233424 1   order_management order_management_order_type_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.order_management
    ADD CONSTRAINT order_management_order_type_fkey FOREIGN KEY (order_type) REFERENCES public.order_type_details(order_type_id);
 [   ALTER TABLE ONLY public.order_management DROP CONSTRAINT order_management_order_type_fkey;
       public          postgres    false    255    3375    322            1           2606    233229 5   order_management order_management_ship_status_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.order_management
    ADD CONSTRAINT order_management_ship_status_id_fkey FOREIGN KEY (ship_status_id) REFERENCES public.shipment_status(ship_status_id);
 _   ALTER TABLE ONLY public.order_management DROP CONSTRAINT order_management_ship_status_id_fkey;
       public          postgres    false    322    257    3377            2           2606    233409 .   order_management order_management_user_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.order_management
    ADD CONSTRAINT order_management_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.new_seller_details(id);
 X   ALTER TABLE ONLY public.order_management DROP CONSTRAINT order_management_user_id_fkey;
       public          postgres    false    322    313    3433            ?           2606    233274 3   order_quote_Manage order_quote_Manage_buyer_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public."order_quote_Manage"
    ADD CONSTRAINT "order_quote_Manage_buyer_id_fkey" FOREIGN KEY (buyer_id) REFERENCES public.new_buyer_details(id);
 a   ALTER TABLE ONLY public."order_quote_Manage" DROP CONSTRAINT "order_quote_Manage_buyer_id_fkey";
       public          postgres    false    311    326    3431            A           2606    233349 4   order_quote_Manage order_quote_Manage_buyer_id_fkey1    FK CONSTRAINT     �   ALTER TABLE ONLY public."order_quote_Manage"
    ADD CONSTRAINT "order_quote_Manage_buyer_id_fkey1" FOREIGN KEY (buyer_id) REFERENCES public.application_user(application_user_id);
 b   ALTER TABLE ONLY public."order_quote_Manage" DROP CONSTRAINT "order_quote_Manage_buyer_id_fkey1";
       public          postgres    false    3327    326    207            B           2606    233354 <   order_quote_Manage order_quote_Manage_method_of_payment_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public."order_quote_Manage"
    ADD CONSTRAINT "order_quote_Manage_method_of_payment_fkey" FOREIGN KEY (method_of_payment) REFERENCES public.payment_type_details(payment_type_id);
 j   ALTER TABLE ONLY public."order_quote_Manage" DROP CONSTRAINT "order_quote_Manage_method_of_payment_fkey";
       public          postgres    false    3413    293    326            >           2606    233269 :   order_quote_Manage order_quote_Manage_order_status_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public."order_quote_Manage"
    ADD CONSTRAINT "order_quote_Manage_order_status_id_fkey" FOREIGN KEY (order_status_id) REFERENCES public.order_status(order_status_id);
 h   ALTER TABLE ONLY public."order_quote_Manage" DROP CONSTRAINT "order_quote_Manage_order_status_id_fkey";
       public          postgres    false    326    259    3379            C           2606    233429 5   order_quote_Manage order_quote_Manage_order_type_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public."order_quote_Manage"
    ADD CONSTRAINT "order_quote_Manage_order_type_fkey" FOREIGN KEY (order_type) REFERENCES public.order_type_details(order_type_id);
 c   ALTER TABLE ONLY public."order_quote_Manage" DROP CONSTRAINT "order_quote_Manage_order_type_fkey";
       public          postgres    false    326    255    3375            @           2606    233279 2   order_quote_Manage order_quote_Manage_user_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public."order_quote_Manage"
    ADD CONSTRAINT "order_quote_Manage_user_id_fkey" FOREIGN KEY (user_id) REFERENCES public.user_managment(id);
 `   ALTER TABLE ONLY public."order_quote_Manage" DROP CONSTRAINT "order_quote_Manage_user_id_fkey";
       public          postgres    false    326    3331    211            �           2606    232639 %   parameters parameters_created_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.parameters
    ADD CONSTRAINT parameters_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.user_managment(id);
 O   ALTER TABLE ONLY public.parameters DROP CONSTRAINT parameters_created_by_fkey;
       public          postgres    false    299    211    3331            �           2606    232644 &   parameters parameters_modified_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.parameters
    ADD CONSTRAINT parameters_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.user_managment(id);
 P   ALTER TABLE ONLY public.parameters DROP CONSTRAINT parameters_modified_by_fkey;
       public          postgres    false    299    211    3331            �           2606    232424 !   parameters parameters_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.parameters
    ADD CONSTRAINT parameters_status_fkey FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 K   ALTER TABLE ONLY public.parameters DROP CONSTRAINT parameters_status_fkey;
       public          postgres    false    299    253    3373            s           2606    232524 5   part_request_unavail part_request_unavail_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.part_request_unavail
    ADD CONSTRAINT part_request_unavail_status_fkey FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 _   ALTER TABLE ONLY public.part_request_unavail DROP CONSTRAINT part_request_unavail_status_fkey;
       public          postgres    false    354    253    3373            
           2606    232689 1   parts_management parts_management_created_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.parts_management
    ADD CONSTRAINT parts_management_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.user_managment(id);
 [   ALTER TABLE ONLY public.parts_management DROP CONSTRAINT parts_management_created_by_fkey;
       public          postgres    false    309    211    3331                       2606    232694 2   parts_management parts_management_modified_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.parts_management
    ADD CONSTRAINT parts_management_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.user_managment(id);
 \   ALTER TABLE ONLY public.parts_management DROP CONSTRAINT parts_management_modified_by_fkey;
       public          postgres    false    309    211    3331            	           2606    232449 -   parts_management parts_management_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.parts_management
    ADD CONSTRAINT parts_management_status_fkey FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 W   ALTER TABLE ONLY public.parts_management DROP CONSTRAINT parts_management_status_fkey;
       public          postgres    false    309    253    3373            P           2606    233014 G   pay_out_schedule_management pay_out_schedule_management_created_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.pay_out_schedule_management
    ADD CONSTRAINT pay_out_schedule_management_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.user_managment(id);
 q   ALTER TABLE ONLY public.pay_out_schedule_management DROP CONSTRAINT pay_out_schedule_management_created_by_fkey;
       public          postgres    false    332    3331    211            Q           2606    233019 H   pay_out_schedule_management pay_out_schedule_management_modified_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.pay_out_schedule_management
    ADD CONSTRAINT pay_out_schedule_management_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.user_managment(id);
 r   ALTER TABLE ONLY public.pay_out_schedule_management DROP CONSTRAINT pay_out_schedule_management_modified_by_fkey;
       public          postgres    false    332    211    3331            R           2606    233309 E   pay_out_schedule_management pay_out_schedule_management_order_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.pay_out_schedule_management
    ADD CONSTRAINT pay_out_schedule_management_order_id_fkey FOREIGN KEY (order_id) REFERENCES public.order_management(order_id);
 o   ALTER TABLE ONLY public.pay_out_schedule_management DROP CONSTRAINT pay_out_schedule_management_order_id_fkey;
       public          postgres    false    322    332    3441            O           2606    232469 C   pay_out_schedule_management pay_out_schedule_management_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.pay_out_schedule_management
    ADD CONSTRAINT pay_out_schedule_management_status_fkey FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 m   ALTER TABLE ONLY public.pay_out_schedule_management DROP CONSTRAINT pay_out_schedule_management_status_fkey;
       public          postgres    false    332    253    3373            S           2606    233314 I   pay_out_schedule_management pay_out_schedule_management_sub_order_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.pay_out_schedule_management
    ADD CONSTRAINT pay_out_schedule_management_sub_order_id_fkey FOREIGN KEY (sub_order_id) REFERENCES public.sub_order_details(id);
 s   ALTER TABLE ONLY public.pay_out_schedule_management DROP CONSTRAINT pay_out_schedule_management_sub_order_id_fkey;
       public          postgres    false    332    3443    324            U           2606    233089 5   payment_management payment_management_created_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.payment_management
    ADD CONSTRAINT payment_management_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.user_managment(id);
 _   ALTER TABLE ONLY public.payment_management DROP CONSTRAINT payment_management_created_by_fkey;
       public          postgres    false    3331    211    334            V           2606    233094 6   payment_management payment_management_modified_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.payment_management
    ADD CONSTRAINT payment_management_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.user_managment(id);
 `   ALTER TABLE ONLY public.payment_management DROP CONSTRAINT payment_management_modified_by_fkey;
       public          postgres    false    211    334    3331            X           2606    233104 3   payment_management payment_management_order_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.payment_management
    ADD CONSTRAINT payment_management_order_id_fkey FOREIGN KEY (order_id) REFERENCES public.order_management(order_id);
 ]   ALTER TABLE ONLY public.payment_management DROP CONSTRAINT payment_management_order_id_fkey;
       public          postgres    false    322    3441    334            Z           2606    233114 <   payment_management payment_management_payment_status_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.payment_management
    ADD CONSTRAINT payment_management_payment_status_id_fkey FOREIGN KEY (payment_status_id) REFERENCES public.payment_status(payment_status_id);
 f   ALTER TABLE ONLY public.payment_management DROP CONSTRAINT payment_management_payment_status_id_fkey;
       public          postgres    false    334    271    3391            W           2606    233099 4   payment_management payment_management_seller_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.payment_management
    ADD CONSTRAINT payment_management_seller_id_fkey FOREIGN KEY (seller_id) REFERENCES public.new_seller_details(id);
 ^   ALTER TABLE ONLY public.payment_management DROP CONSTRAINT payment_management_seller_id_fkey;
       public          postgres    false    3433    313    334            T           2606    232474 1   payment_management payment_management_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.payment_management
    ADD CONSTRAINT payment_management_status_fkey FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 [   ALTER TABLE ONLY public.payment_management DROP CONSTRAINT payment_management_status_fkey;
       public          postgres    false    334    253    3373            Y           2606    233109 7   payment_management payment_management_sub_order_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.payment_management
    ADD CONSTRAINT payment_management_sub_order_id_fkey FOREIGN KEY (sub_order_id) REFERENCES public.sub_order_details(id);
 a   ALTER TABLE ONLY public.payment_management DROP CONSTRAINT payment_management_sub_order_id_fkey;
       public          postgres    false    3443    334    324            �           2606    232399 5   payment_type_details payment_type_details_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.payment_type_details
    ADD CONSTRAINT payment_type_details_status_fkey FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 _   ALTER TABLE ONLY public.payment_type_details DROP CONSTRAINT payment_type_details_status_fkey;
       public          postgres    false    293    253    3373            �           2606    232584 7   payout_shed_details payout_shed_details_created_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.payout_shed_details
    ADD CONSTRAINT payout_shed_details_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.user_managment(id);
 a   ALTER TABLE ONLY public.payout_shed_details DROP CONSTRAINT payout_shed_details_created_by_fkey;
       public          postgres    false    247    211    3331            �           2606    232589 8   payout_shed_details payout_shed_details_modified_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.payout_shed_details
    ADD CONSTRAINT payout_shed_details_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.user_managment(id);
 b   ALTER TABLE ONLY public.payout_shed_details DROP CONSTRAINT payout_shed_details_modified_by_fkey;
       public          postgres    false    247    211    3331            �           2606    233304 ;   payout_shed_details payout_shed_details_payout_shed_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.payout_shed_details
    ADD CONSTRAINT payout_shed_details_payout_shed_id_fkey FOREIGN KEY (payout_shed_id) REFERENCES public.pay_out_schedule_management(id);
 e   ALTER TABLE ONLY public.payout_shed_details DROP CONSTRAINT payout_shed_details_payout_shed_id_fkey;
       public          postgres    false    247    3451    332            �           2606    232354 3   payout_shed_details payout_shed_details_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.payout_shed_details
    ADD CONSTRAINT payout_shed_details_status_fkey FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 ]   ALTER TABLE ONLY public.payout_shed_details DROP CONSTRAINT payout_shed_details_status_fkey;
       public          postgres    false    3373    247    253            �           2606    233084 C   payout_shed_details payout_shed_details_subscription_detail_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.payout_shed_details
    ADD CONSTRAINT payout_shed_details_subscription_detail_id_fkey FOREIGN KEY (subscription_detail_id) REFERENCES public.subscription_plan_details(subscription_plan_id);
 m   ALTER TABLE ONLY public.payout_shed_details DROP CONSTRAINT payout_shed_details_subscription_detail_id_fkey;
       public          postgres    false    3365    247    245            )           2606    232454 +   product_details product_details_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.product_details
    ADD CONSTRAINT product_details_status_fkey FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 U   ALTER TABLE ONLY public.product_details DROP CONSTRAINT product_details_status_fkey;
       public          postgres    false    318    253    3373            �           2606    232659 9   product_type_details product_type_details_created_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.product_type_details
    ADD CONSTRAINT product_type_details_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.user_managment(id);
 c   ALTER TABLE ONLY public.product_type_details DROP CONSTRAINT product_type_details_created_by_fkey;
       public          postgres    false    289    211    3331            �           2606    232664 :   product_type_details product_type_details_modified_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.product_type_details
    ADD CONSTRAINT product_type_details_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.user_managment(id);
 d   ALTER TABLE ONLY public.product_type_details DROP CONSTRAINT product_type_details_modified_by_fkey;
       public          postgres    false    289    211    3331            �           2606    232389 5   product_type_details product_type_details_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.product_type_details
    ADD CONSTRAINT product_type_details_status_fkey FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 _   ALTER TABLE ONLY public.product_type_details DROP CONSTRAINT product_type_details_status_fkey;
       public          postgres    false    289    253    3373            �           2606    232599 -   reason_details reason_details_created_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.reason_details
    ADD CONSTRAINT reason_details_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.user_managment(id);
 W   ALTER TABLE ONLY public.reason_details DROP CONSTRAINT reason_details_created_by_fkey;
       public          postgres    false    273    211    3331            �           2606    232604 .   reason_details reason_details_modified_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.reason_details
    ADD CONSTRAINT reason_details_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.user_managment(id);
 X   ALTER TABLE ONLY public.reason_details DROP CONSTRAINT reason_details_modified_by_fkey;
       public          postgres    false    273    211    3331            �           2606    232364 )   reason_details reason_details_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.reason_details
    ADD CONSTRAINT reason_details_status_fkey FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 S   ALTER TABLE ONLY public.reason_details DROP CONSTRAINT reason_details_status_fkey;
       public          postgres    false    273    253    3373            �           2606    233369 .   role_permission role_permission_module_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.role_permission
    ADD CONSTRAINT role_permission_module_id_fkey FOREIGN KEY (module_id) REFERENCES public.module(module_id);
 X   ALTER TABLE ONLY public.role_permission DROP CONSTRAINT role_permission_module_id_fkey;
       public          postgres    false    217    221    3341            �           2606    233404 ,   role_permission role_permission_role_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.role_permission
    ADD CONSTRAINT role_permission_role_id_fkey FOREIGN KEY (role_id) REFERENCES public.roles(role_id);
 V   ALTER TABLE ONLY public.role_permission DROP CONSTRAINT role_permission_role_id_fkey;
       public          postgres    false    215    3335    217            �           2606    232849    roles roles_created_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.roles
    ADD CONSTRAINT roles_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.user_managment(id);
 E   ALTER TABLE ONLY public.roles DROP CONSTRAINT roles_created_by_fkey;
       public          postgres    false    215    211    3331            �           2606    232854    roles roles_modified_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.roles
    ADD CONSTRAINT roles_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.user_managment(id);
 F   ALTER TABLE ONLY public.roles DROP CONSTRAINT roles_modified_by_fkey;
       public          postgres    false    211    215    3331            �           2606    232534    roles roles_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.roles
    ADD CONSTRAINT roles_status_fkey FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 A   ALTER TABLE ONLY public.roles DROP CONSTRAINT roles_status_fkey;
       public          postgres    false    215    253    3373            '           2606    232999 /   seller_branch seller_branch_branch_country_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.seller_branch
    ADD CONSTRAINT seller_branch_branch_country_fkey FOREIGN KEY (branch_country) REFERENCES public.country_details(country_id);
 Y   ALTER TABLE ONLY public.seller_branch DROP CONSTRAINT seller_branch_branch_country_fkey;
       public          postgres    false    277    3397    314            (           2606    233004 -   seller_branch seller_branch_branch_state_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.seller_branch
    ADD CONSTRAINT seller_branch_branch_state_fkey FOREIGN KEY (branch_state) REFERENCES public.state_details(id);
 W   ALTER TABLE ONLY public.seller_branch DROP CONSTRAINT seller_branch_branch_state_fkey;
       public          postgres    false    314    3399    279            �           2606    232859 '   seo_content seo_content_created_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.seo_content
    ADD CONSTRAINT seo_content_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.user_managment(id);
 Q   ALTER TABLE ONLY public.seo_content DROP CONSTRAINT seo_content_created_by_fkey;
       public          postgres    false    364    211    3331            �           2606    232864 (   seo_content seo_content_modified_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.seo_content
    ADD CONSTRAINT seo_content_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.user_managment(id);
 R   ALTER TABLE ONLY public.seo_content DROP CONSTRAINT seo_content_modified_by_fkey;
       public          postgres    false    364    211    3331            �           2606    232554 #   seo_content seo_content_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.seo_content
    ADD CONSTRAINT seo_content_status_fkey FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 M   ALTER TABLE ONLY public.seo_content DROP CONSTRAINT seo_content_status_fkey;
       public          postgres    false    364    253    3373            �           2606    233184 )   sms_template sms_template_created_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.sms_template
    ADD CONSTRAINT sms_template_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.user_managment(id);
 S   ALTER TABLE ONLY public.sms_template DROP CONSTRAINT sms_template_created_by_fkey;
       public          postgres    false    368    3331    211            �           2606    233189 *   sms_template sms_template_modified_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.sms_template
    ADD CONSTRAINT sms_template_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.user_managment(id);
 T   ALTER TABLE ONLY public.sms_template DROP CONSTRAINT sms_template_modified_by_fkey;
       public          postgres    false    368    3331    211            �           2606    233394 (   sms_template sms_template_module_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.sms_template
    ADD CONSTRAINT sms_template_module_id_fkey FOREIGN KEY (module_id) REFERENCES public.module(module_id);
 R   ALTER TABLE ONLY public.sms_template DROP CONSTRAINT sms_template_module_id_fkey;
       public          postgres    false    3341    221    368            �           2606    232564 %   sms_template sms_template_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.sms_template
    ADD CONSTRAINT sms_template_status_fkey FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 O   ALTER TABLE ONLY public.sms_template DROP CONSTRAINT sms_template_status_fkey;
       public          postgres    false    368    253    3373            �           2606    232869 +   state_details state_details_country_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.state_details
    ADD CONSTRAINT state_details_country_id_fkey FOREIGN KEY (country_id) REFERENCES public.country_details(country_id);
 U   ALTER TABLE ONLY public.state_details DROP CONSTRAINT state_details_country_id_fkey;
       public          postgres    false    277    279    3397            �           2606    232649 +   state_details state_details_created_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.state_details
    ADD CONSTRAINT state_details_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.user_managment(id);
 U   ALTER TABLE ONLY public.state_details DROP CONSTRAINT state_details_created_by_fkey;
       public          postgres    false    279    211    3331            �           2606    232654 ,   state_details state_details_modified_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.state_details
    ADD CONSTRAINT state_details_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.user_managment(id);
 V   ALTER TABLE ONLY public.state_details DROP CONSTRAINT state_details_modified_by_fkey;
       public          postgres    false    279    211    3331            �           2606    232379 '   state_details state_details_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.state_details
    ADD CONSTRAINT state_details_status_fkey FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 Q   ALTER TABLE ONLY public.state_details DROP CONSTRAINT state_details_status_fkey;
       public          postgres    false    279    3373    253                       2606    232629 9   sub_group_management sub_group_management_created_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.sub_group_management
    ADD CONSTRAINT sub_group_management_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.user_managment(id);
 c   ALTER TABLE ONLY public.sub_group_management DROP CONSTRAINT sub_group_management_created_by_fkey;
       public          postgres    false    305    211    3331                       2606    233164 7   sub_group_management sub_group_management_group_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.sub_group_management
    ADD CONSTRAINT sub_group_management_group_id_fkey FOREIGN KEY (group_id) REFERENCES public.group_management(group_id);
 a   ALTER TABLE ONLY public.sub_group_management DROP CONSTRAINT sub_group_management_group_id_fkey;
       public          postgres    false    305    303    3423                       2606    232634 :   sub_group_management sub_group_management_modified_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.sub_group_management
    ADD CONSTRAINT sub_group_management_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.user_managment(id);
 d   ALTER TABLE ONLY public.sub_group_management DROP CONSTRAINT sub_group_management_modified_by_fkey;
       public          postgres    false    211    305    3331                       2606    232439 5   sub_group_management sub_group_management_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.sub_group_management
    ADD CONSTRAINT sub_group_management_status_fkey FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 _   ALTER TABLE ONLY public.sub_group_management DROP CONSTRAINT sub_group_management_status_fkey;
       public          postgres    false    305    253    3373                       2606    233174 7   sub_node_management sub_node_management_created_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.sub_node_management
    ADD CONSTRAINT sub_node_management_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.user_managment(id);
 a   ALTER TABLE ONLY public.sub_node_management DROP CONSTRAINT sub_node_management_created_by_fkey;
       public          postgres    false    211    3331    307                       2606    233179 8   sub_node_management sub_node_management_modified_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.sub_node_management
    ADD CONSTRAINT sub_node_management_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.user_managment(id);
 b   ALTER TABLE ONLY public.sub_node_management DROP CONSTRAINT sub_node_management_modified_by_fkey;
       public          postgres    false    211    3331    307                       2606    232444 3   sub_node_management sub_node_management_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.sub_node_management
    ADD CONSTRAINT sub_node_management_status_fkey FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 ]   ALTER TABLE ONLY public.sub_node_management DROP CONSTRAINT sub_node_management_status_fkey;
       public          postgres    false    307    253    3373            ;           2606    233259 1   sub_order_details sub_order_details_brand_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.sub_order_details
    ADD CONSTRAINT sub_order_details_brand_id_fkey FOREIGN KEY (brand_id) REFERENCES public.brands(brand_id);
 [   ALTER TABLE ONLY public.sub_order_details DROP CONSTRAINT sub_order_details_brand_id_fkey;
       public          postgres    false    295    324    3415            =           2606    233414 1   sub_order_details sub_order_details_order_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.sub_order_details
    ADD CONSTRAINT sub_order_details_order_id_fkey FOREIGN KEY (order_id) REFERENCES public.order_management(order_id);
 [   ALTER TABLE ONLY public.sub_order_details DROP CONSTRAINT sub_order_details_order_id_fkey;
       public          postgres    false    324    322    3441            6           2606    233234 8   sub_order_details sub_order_details_order_status_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.sub_order_details
    ADD CONSTRAINT sub_order_details_order_status_id_fkey FOREIGN KEY (order_status_id) REFERENCES public.order_status(order_status_id);
 b   ALTER TABLE ONLY public.sub_order_details DROP CONSTRAINT sub_order_details_order_status_id_fkey;
       public          postgres    false    324    259    3379            <           2606    233264 3   sub_order_details sub_order_details_product_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.sub_order_details
    ADD CONSTRAINT sub_order_details_product_id_fkey FOREIGN KEY (product_id) REFERENCES public.product_details(product_id);
 ]   ALTER TABLE ONLY public.sub_order_details DROP CONSTRAINT sub_order_details_product_id_fkey;
       public          postgres    false    324    318    3437            7           2606    233239 8   sub_order_details sub_order_details_product_type_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.sub_order_details
    ADD CONSTRAINT sub_order_details_product_type_id_fkey FOREIGN KEY (product_type_id) REFERENCES public.product_type_details(product_type_id);
 b   ALTER TABLE ONLY public.sub_order_details DROP CONSTRAINT sub_order_details_product_type_id_fkey;
       public          postgres    false    324    289    3409            :           2606    233254 2   sub_order_details sub_order_details_seller_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.sub_order_details
    ADD CONSTRAINT sub_order_details_seller_id_fkey FOREIGN KEY (seller_id) REFERENCES public.new_seller_details(id);
 \   ALTER TABLE ONLY public.sub_order_details DROP CONSTRAINT sub_order_details_seller_id_fkey;
       public          postgres    false    324    313    3433            9           2606    233249 7   sub_order_details sub_order_details_ship_status_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.sub_order_details
    ADD CONSTRAINT sub_order_details_ship_status_id_fkey FOREIGN KEY (ship_status_id) REFERENCES public.shipment_status(ship_status_id);
 a   ALTER TABLE ONLY public.sub_order_details DROP CONSTRAINT sub_order_details_ship_status_id_fkey;
       public          postgres    false    257    3377    324            8           2606    233244 8   sub_order_details sub_order_details_trading_type_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.sub_order_details
    ADD CONSTRAINT sub_order_details_trading_type_id_fkey FOREIGN KEY (trading_type_id) REFERENCES public.trading_type_details(trading_type_id);
 b   ALTER TABLE ONLY public.sub_order_details DROP CONSTRAINT sub_order_details_trading_type_id_fkey;
       public          postgres    false    3411    324    291            E           2606    233289 D   sub_order_quote_details sub_order_quote_details_order_status_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.sub_order_quote_details
    ADD CONSTRAINT sub_order_quote_details_order_status_id_fkey FOREIGN KEY (order_status_id) REFERENCES public.order_status(order_status_id);
 n   ALTER TABLE ONLY public.sub_order_quote_details DROP CONSTRAINT sub_order_quote_details_order_status_id_fkey;
       public          postgres    false    259    328    3379            G           2606    233299 ?   sub_order_quote_details sub_order_quote_details_product_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.sub_order_quote_details
    ADD CONSTRAINT sub_order_quote_details_product_id_fkey FOREIGN KEY (product_id) REFERENCES public.product_details(product_id);
 i   ALTER TABLE ONLY public.sub_order_quote_details DROP CONSTRAINT sub_order_quote_details_product_id_fkey;
       public          postgres    false    3437    328    318            H           2606    233359 D   sub_order_quote_details sub_order_quote_details_product_type_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.sub_order_quote_details
    ADD CONSTRAINT sub_order_quote_details_product_type_id_fkey FOREIGN KEY (product_type_id) REFERENCES public.product_type_details(product_type_id);
 n   ALTER TABLE ONLY public.sub_order_quote_details DROP CONSTRAINT sub_order_quote_details_product_type_id_fkey;
       public          postgres    false    289    3409    328            D           2606    233284 >   sub_order_quote_details sub_order_quote_details_seller_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.sub_order_quote_details
    ADD CONSTRAINT sub_order_quote_details_seller_id_fkey FOREIGN KEY (seller_id) REFERENCES public.new_seller_details(id);
 h   ALTER TABLE ONLY public.sub_order_quote_details DROP CONSTRAINT sub_order_quote_details_seller_id_fkey;
       public          postgres    false    313    3433    328            F           2606    233294 C   sub_order_quote_details sub_order_quote_details_ship_status_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.sub_order_quote_details
    ADD CONSTRAINT sub_order_quote_details_ship_status_id_fkey FOREIGN KEY (ship_status_id) REFERENCES public.shipment_status(ship_status_id);
 m   ALTER TABLE ONLY public.sub_order_quote_details DROP CONSTRAINT sub_order_quote_details_ship_status_id_fkey;
       public          postgres    false    328    3377    257            I           2606    233364 D   sub_order_quote_details sub_order_quote_details_trading_type_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.sub_order_quote_details
    ADD CONSTRAINT sub_order_quote_details_trading_type_id_fkey FOREIGN KEY (trading_type_id) REFERENCES public.trading_type_details(trading_type_id);
 n   ALTER TABLE ONLY public.sub_order_quote_details DROP CONSTRAINT sub_order_quote_details_trading_type_id_fkey;
       public          postgres    false    3411    291    328            �           2606    232284 7   subscription_detail subscription_detail_created_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.subscription_detail
    ADD CONSTRAINT subscription_detail_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.user_managment(id);
 a   ALTER TABLE ONLY public.subscription_detail DROP CONSTRAINT subscription_detail_created_by_fkey;
       public          postgres    false    243    211    3331            �           2606    232294 8   subscription_detail subscription_detail_modified_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.subscription_detail
    ADD CONSTRAINT subscription_detail_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.user_managment(id);
 b   ALTER TABLE ONLY public.subscription_detail DROP CONSTRAINT subscription_detail_modified_by_fkey;
       public          postgres    false    243    3331    211            �           2606    232309 C   subscription_detail subscription_detail_subscription_detail_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.subscription_detail
    ADD CONSTRAINT subscription_detail_subscription_detail_id_fkey FOREIGN KEY (subscription_detail_id) REFERENCES public.subscription_plan_details(subscription_plan_id);
 m   ALTER TABLE ONLY public.subscription_detail DROP CONSTRAINT subscription_detail_subscription_detail_id_fkey;
       public          postgres    false    243    245    3365            �           2606    232304 4   subscription_detail subscription_detail_user_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.subscription_detail
    ADD CONSTRAINT subscription_detail_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.user_managment(id);
 ^   ALTER TABLE ONLY public.subscription_detail DROP CONSTRAINT subscription_detail_user_id_fkey;
       public          postgres    false    243    3331    211            �           2606    232289 C   subscription_plan_details subscription_plan_details_created_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.subscription_plan_details
    ADD CONSTRAINT subscription_plan_details_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.user_managment(id);
 m   ALTER TABLE ONLY public.subscription_plan_details DROP CONSTRAINT subscription_plan_details_created_by_fkey;
       public          postgres    false    211    245    3331            �           2606    232299 D   subscription_plan_details subscription_plan_details_modified_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.subscription_plan_details
    ADD CONSTRAINT subscription_plan_details_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.user_managment(id);
 n   ALTER TABLE ONLY public.subscription_plan_details DROP CONSTRAINT subscription_plan_details_modified_by_fkey;
       public          postgres    false    245    3331    211            �           2606    232669 9   trading_type_details trading_type_details_created_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.trading_type_details
    ADD CONSTRAINT trading_type_details_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.user_managment(id);
 c   ALTER TABLE ONLY public.trading_type_details DROP CONSTRAINT trading_type_details_created_by_fkey;
       public          postgres    false    291    211    3331            �           2606    232674 :   trading_type_details trading_type_details_modified_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.trading_type_details
    ADD CONSTRAINT trading_type_details_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.user_managment(id);
 d   ALTER TABLE ONLY public.trading_type_details DROP CONSTRAINT trading_type_details_modified_by_fkey;
       public          postgres    false    291    211    3331            �           2606    232394 5   trading_type_details trading_type_details_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.trading_type_details
    ADD CONSTRAINT trading_type_details_status_fkey FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 _   ALTER TABLE ONLY public.trading_type_details DROP CONSTRAINT trading_type_details_status_fkey;
       public          postgres    false    291    253    3373            �           2606    233464 5   user_managment user_managment_Login_user_type_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.user_managment
    ADD CONSTRAINT "user_managment_Login_user_type_id_fkey" FOREIGN KEY ("Login_user_type_id") REFERENCES public."Login_user_type"("Login_user_type_id");
 a   ALTER TABLE ONLY public.user_managment DROP CONSTRAINT "user_managment_Login_user_type_id_fkey";
       public          postgres    false    213    211    3333            [           2606    232479 #   vin_history vin_history_status_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.vin_history
    ADD CONSTRAINT vin_history_status_fkey FOREIGN KEY (status) REFERENCES public.status_details(status_id);
 M   ALTER TABLE ONLY public.vin_history DROP CONSTRAINT vin_history_status_fkey;
       public          postgres    false    336    253    3373                  x������ � �      �      x������ � �      ]      x������ � �            x������ � �            x������ � �      �      x������ � �      k      x������ � �      ?      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      q      x������ � �      �      x������ � �      �      x������ � �      Q      x������ � �      '      x������ � �      5      x������ � �      1      x������ � �      -      x������ � �      3      x������ � �      /      x������ � �      +      x������ � �      )      x������ � �      �      x������ � �      =      x������ � �      Y      x������ � �      W      x������ � �      #      x������ � �      �      x������ � �      s      x������ � �      �      x������ � �            x������ � �      �      x������ � �      c      x������ � �      O      x������ � �      m      x������ � �      !      x������ � �            x������ � �      {      x������ � �      }      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      G      x������ � �      C      x������ � �      o      x������ � �      �      x������ � �      y      x������ � �      K      x������ � �      �      x������ � �      �      x������ � �      S      x������ � �      i      x������ � �      ;      x������ � �      a      x������ � �      �      x������ � �      e      x������ � �      M      x������ � �      U      x������ � �      I      x������ � �      %      x������ � �            x������ � �            x������ � �      ~      x������ � �      �      x������ � �      �      x������ � �      E      x������ � �      �      x������ � �      [      x������ � �      A      x������ � �      u      x������ � �      w      x������ � �      �      x������ � �      �      x������ � �      7      x������ � �      9      x������ � �            x������ � �      _      x������ � �      g      x������ � �            x������ � �      �      x������ � �     
var express = require('express');
var router = express.Router();
var pool = require('../config/db');

router.post('/create', (req, res) => {
  console.log("module Input", req.body);

  var module_type_id = req.body.module_type_id;
  var module_name = req.body.module_name;
  var module_abbr = req.body.module_abbr;
  var status = req.body.status;
  var created_at = new Date();
  var created_by = 1;
  var modified_at = new Date();
  var modified_by = 1

  var country_query = {
    text: 'INSERT INTO module (module_type_id,module_name,module_abbr,status,created_at,created_by,modified_at,modified_by) VALUES ($1,$2,$3,$4,$5,$6,$7,$8);',
    values: [module_type_id,module_name,module_abbr,status,created_at,created_by,modified_at,modified_by]
  }
  pool.query(country_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [], code: 500 });
    } else {
      res.json({ success: true, msg: "New Module Created Successfully", data: req.rows, code: 200 });
    }
  });
});

router.put('/update/:id', (req, res) => {
  var country = req.body.country_name;
  var country_code = req.body.country_code;
  var created_at = req.body.created_at;
  var status = req.body.status;
  var modified_at = new Date();
  var created_by = 1;
  var modified_by = 1;
  var id = req.params.id;
  var country_query = {
    text: 'UPDATE module SET country_name=$1, created_at=$2, modified_at=$3 ,created_by=$4, modified_by=$5, status=$7, country_code=$8  WHERE id = $6;',
    values: [country, created_at, modified_at, created_by, modified_by, id,status, country_code]
  }
  pool.query(country_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database" });
    } else {
      res.json({ success: true, msg: "Module Updated Successfully", data: req.rows });
    }
  });
});


router.put('/status_update/:id', (req, res) => {
    var status = req.body.status;
    var module_id = req.params.id;
    var country_query = {
      text: 'UPDATE module SET status=$1 WHERE module_id = $2;',
      values: [status,module_id]
    }
    pool.query(country_query, (err, req) => {
      if (err) {
        console.log(err.stack);
        res.json({ success: false, msg: "Error in database" });
      } else {
        res.json({ success: true, msg: "Module Updated Successfully", data: req.rows });
      }
    });
  });

router.delete('/delete/:id', (req, res) => {
  var id = req.params.id;
  var country_query = {
    text: 'DELETE FROM module WHERE id= $1',
    values: [id]
  }
  pool.query(country_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database" });
    } else {
      res.json({ success: true, msg: "Module Deleted Successfully" });
    }
  });
});


router.get('/getlist', (req, res) => {
  var country_query = {
    text: 'SELECT *  FROM module',
  }
  pool.query(country_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database" });
    } else {
      res.json({ success: true, msg: 'Loading Module', data: req.rows });
    }
  });
});

router.get('/getby_id/:id', (req, res) => {
  var id = req.params.id;
  var country_query = {
    text: 'SELECT *  FROM module WHERE id = $1',
    values: [id]
  }
  pool.query(country_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database" });
    } else {
      if (req.rows.length > 0) {
        res.json({ success: true, msg: "Loading data", data: req.rows[0], code: 200 });
      } else {
        res.json({ success: true, msg: "No data found", data: [], code: 200 });
      }
    }
  });
});

module.exports = router;
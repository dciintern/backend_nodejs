var express = require('express');
var router = express.Router();
var pool = require('../config/db');
var multer  = require('multer');
var path = require('path');

// PROFILE FILE STORE & VIEW //

var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './files/cms')
  },
  filename: function (req, file, cb) {
    cb(null, new Date().getTime()+file.originalname)
  }
})

var upload = multer({ storage: storage }).single('file');

router.get('/viewimage', (req,res)=>{
  filepath = path.join(__dirname, '../files/cms') + '/' + req.query.filename;
  res.sendFile(filepath);
});

router.post('/file',(req,res,next)=>{
  upload(req,res,function (err) {
    if(err) {
      res.json({
        success: false, message: 'error while uploading....',
        error: err
      });
    }
    else{
      res.json ({success: true, message:'uploaded successfully....',uploadedfile:req.file.filename});
    }
  });
});

// INFO CMS

router.post('/info_cms/create', (req, res) => {
  var contact = req.body.contact;
  var company_name = req.body.company_name;
  var description = req.body.description;
  var playstore_link = req.body.playstore_link;
  var applestore_link = req.body.applestore_link;
  var logo = req.body.logo;
  var email = req.body.email;
  var created_at = new Date();
  var modified_at = new Date();
  var created_by = 1;
  var modified_by = 1;

  var info_cms_query = {
    text: 'INSERT INTO info_cms (email,conatct,created_at,modified_at,created_by,modified_by,logo,applestore_link,playstore_link) VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11);',
    values: [email, contact, created_at, modified_at, created_by, modified_by, company_name, description,logo,applestore_link,playstore_link]
  }
  pool.query(info_cms_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [] });
    } else {
      res.json({ success: true, msg: "New Info Created Successfully", data: req.rows });
    }
  });
});
router.put('/info_cms/update/:id', (req, res) => {
  var contact = req.body.contact;
  var company_name = req.body.company_name;
  var description = req.body.description;
  var logo = req.body.logo;
  var email = req.body.email;
  var playstore_link = req.body.playstore_link;
  var applestore_link = req.body.applestore_link;
  var created_at = req.body.created_at;
  var modified_at = new Date();
  var created_by = 1;
  var modified_by = 1;
  var id = req.params.id;
  var info_cms_query = {
    text: 'UPDATE info_cms SET email=$1, contact=$2, created_at=$3, modified_at=$4 ,created_by=$5, modified_by=$6 , company_name=$8 , description=$9, logo=$10 ,applestore_link=$11, playstore_link=$12  WHERE id = $7;',
    values: [email, contact, created_at, modified_at, created_by, modified_by, id, company_name, description, logo,applestore_link,playstore_link]
  }
  pool.query(info_cms_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [] });
    } else {
      res.json({ success: true, msg: "Company Profile Updated Successfully", data: req.rows });
    }
  })

});
router.delete('/info_cms/delete/:id', (req, res) => {
  var id = req.params.id;
  var info_cms_query = {
    text: 'DELETE FROM info_cms WHERE id= $1',
    values: [id]
  }
  pool.query(info_cms_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [] });
    } else {
      res.json({ success: true, msg: "State Deleted Successfully", data: req.rows });
    }
  })
});
router.get('/info_cms/getlist', (req, res) => {
  var info_cms_query = {
    text: 'SELECT *  FROM info_cms',
  }
  pool.query(info_cms_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [] });
    } else {
      if (req.rows.length > 0) {
        res.json({ success: true, msg: "Loadind State", data: req.rows });
      } else {
        res.json({ success: true, msg: "No data Found", data: [] });
      }
    }
  })

});
router.get('/info_cms/getby_id/:id', (req, res) => {
  var id = req.params.id;
  var info_cms_query = {
    text: 'SELECT *  FROM info_cms WHERE id = $1',
    values: [id]
  }
  pool.query(info_cms_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [], code: 500 });
    } else {
      if (req.rows.length > 0) {
        res.json({ success: true, msg: "Loadind State", data: req.rows[0] });
      } else {
        res.json({ success: true, msg: "No data Found", data: [] });
      }
    }
  })
});


// BANNER CMS

// BANNER FILE STORE & VIEW //

var storage2 = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './files/banner')
  },
  filename: function (req, file, cb) {
    cb(null, new Date().getTime()+file.originalname)
  }
})

var upload2 = multer({ storage: storage2 }).single('file');

router.get('/banner_cms/viewimage', (req,res)=>{
  filepath = path.join(__dirname, '../files/banner') + '/' + req.query.filename;
  res.sendFile(filepath);
});

router.post('/banner_cms/file',(req,res,next)=>{
  upload2(req,res,function (err) {
    if(err) {
      res.json({
        success: false, message: 'error while uploading....',
        error: err
      });
    }
    else{
      res.json ({success: true, message:'uploaded successfully....',uploadedfile:req.file.filename});
    }
  });
});

router.post('/banner_cms/create', (req, res) => {
    var title = req.body.title;
    var content = req.body.content;
    var image = req.body.image;
    var status = req.body.status;
    var created_at = new Date();
    var modified_at = new Date();
    var created_by = 1;
    var modified_by = 1;
  
    var banner_cms_query = {
      text: 'INSERT INTO banner_cms (title,content,image,status,created_at,modified_at,created_by,modified_by) VALUES ($1,$2,$3,$4,$5,$6,$7,$8);',
      values: [title,content,image, status, created_at, modified_at, created_by, modified_by]
    }
    pool.query(banner_cms_query, (err, req) => {
      if (err) {
        console.log(err.stack);
        res.json({ success: false, msg: "Error in database", data: [] });
      } else {
        res.json({ success: true, msg: "New Banner Created Successfully", data: req.rows });
      }
    });
  });

  router.put('/banner_cms/update/:id', (req, res) => {
    var title = req.body.title;
    var content = req.body.content;
    var image = req.body.image;
    var status = req.body.status;
    var created_at = req.body.created_at;
    var modified_at = new Date();
    var created_by = 1;
    var modified_by = 1;
    var id = req.params.id;
    var banner_cms_query = {
      text: 'UPDATE banner_cms SET title=$1, content=$2, image=$3, status=$4, created_at=$5, modified_at=$6 ,created_by=$7, modified_by=$8  WHERE id = $9;',
      values: [title,content,image, status, created_at, modified_at, created_by, modified_by, id]
    }
    pool.query(banner_cms_query, (err, req) => {
      if (err) {
        console.log(err.stack);
        res.json({ success: false, msg: "Error in database", data: [] });
      } else {
        res.json({ success: true, msg: "Banner Updated Successfully", data: req.rows });
      }
    });
  });

  router.delete('/banner_cms/delete/:id', (req, res) => {
    var id = req.params.id;
    var banner_cms_query = {
      text: 'DELETE FROM banner_cms WHERE id= $1',
      values: [id]
    }
    pool.query(banner_cms_query, (err, req) => {
      if (err) {
        console.log(err.stack);
        res.json({ success: false, msg: "Error in database", data: [] });
      } else {
        res.json({ success: true, msg: "Banner Deleted Successfully", data: req.rows });
      }
    });
  });

  router.get('/banner_cms/getlist', (req, res) => {
    var banner_cms_query = {
      text: 'SELECT cd.id, cd.title, cd.content, cd.image, cd.status, cd.created_at, cd.created_by, cd.modified_at, cd.modified_by, um.user_name FROM banner_cms cd inner join user_managment um ON cd.created_by = um.id',
    }
    pool.query(banner_cms_query, (err, req) => {
      if (err) {
        console.log(err.stack);
        res.json({ success: false, msg: "Error in database", data: [] });
      } else {
        if (req.rows.length > 0) {
          res.json({ success: true, msg: "Loadind Banners", data: req.rows });
        } else {
          res.json({ success: true, msg: "No data Found", data: [] });
        }
      }
    });
  });

  router.get('/banner_cms/getby_id/:id', (req, res) => {
    var id = req.params.id;
    var info_cms_query = {
      text: 'SELECT *  FROM banner_cms WHERE id = $1',
      values: [id]
    }
    pool.query(info_cms_query, (err, req) => {
      if (err) {
        console.log(err.stack);
        res.json({ success: false, msg: "Error in database", data: [], code: 500 });
      } else {
        if (req.rows.length > 0) {
          res.json({ success: true, msg: "Loadind Banners", data: req.rows[0] });
        } else {
          res.json({ success: true, msg: "No data Found", data: [] });
        }
      }
    });
  });


// FEATURES CMS


// FEATURES FILE STORE & VIEW //

var storage3 = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './files/features')
  },
  filename: function (req, file, cb) {
    cb(null, new Date().getTime()+file.originalname)
  }
})

var upload3 = multer({ storage: storage3 }).single('file');

router.get('/features_cms/viewimage', (req,res)=>{
  filepath = path.join(__dirname, '../files/features') + '/' + req.query.filename;
  res.sendFile(filepath);
});

router.post('/features_cms/file',(req,res,next)=>{
  upload3(req,res,function (err) {
    if(err) {
      res.json({
        success: false, message: 'error while uploading....',
        error: err
      });
    }
    else{
      res.json ({success: true, message:'uploaded successfully....',uploadedfile:req.file.filename});
    }
  });
});

router.post('/features_cms/create', (req, res) => {
  var icon = req.body.icon;
  var title = req.body.title;
  var content = req.body.content;
  var status = req.body.status;
  var created_at = new Date();
  var modified_at = new Date();
  var created_by = 1;
  var modified_by = 1;

  var features_cms_query = {
    text: 'INSERT INTO features_cms (title,content,icon,status,created_at,modified_at,created_by,modified_by) VALUES ($1,$2,$3,$4,$5,$6,$7,$8);',
    values: [title,content,icon,status,created_at, modified_at, created_by, modified_by]
  }
  pool.query(features_cms_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [] });
    } else {
      res.json({ success: true, msg: "New features Created Successfully", data: req.rows });
    }
  });
});
router.put('/features_cms/update/:id', (req, res) => {
  var icon = req.body.icon;
  var title = req.body.title;
  var content = req.body.content;
  var status = req.body.status;
  var created_at = req.body.created_at;
  var modified_at = new Date();
  var created_by = 1;
  var modified_by = 1;
  var id = req.params.id;
  var features_cms_query = {
    text: 'UPDATE features_cms SET icon=$1, title=$2, content=$3, status=$4, created_at=$5, modified_at=$6 ,created_by=$7, modified_by=$8  WHERE id = $9;',
    values: [icon, title, content, status, created_at, modified_at, created_by, modified_by, id]
  }
  pool.query(features_cms_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [] });
    } else {
      res.json({ success: true, msg: "Features Updated Successfully", data: req.rows });
    }
  })

});
router.delete('/features_cms/delete/:id', (req, res) => {
  var id = req.params.id;
  var features_cms_query = {
    text: 'DELETE FROM features_cms WHERE id= $1',
    values: [id]
  }
  pool.query(features_cms_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [] });
    } else {
      res.json({ success: true, msg: "Features Deleted Successfully", data: req.rows });
    }
  })
});
router.get('/features_cms/getlist', (req, res) => {
  var features_cms_query = {
    text: 'SELECT cd.id, cd.title, cd.content, cd.icon, cd.status, cd.created_at, cd.created_by, cd.modified_at, cd.modified_by, um.user_name FROM features_cms cd inner join user_managment um ON cd.created_by = um.id',
  }
  pool.query(features_cms_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [] });
    } else {
      if (req.rows.length > 0) {
        res.json({ success: true, msg: "Loadind Features", data: req.rows });
      } else {
        res.json({ success: true, msg: "No data Found", data: [] });
      }
    }
  })

});
router.get('/features_cms/getby_id/:id', (req, res) => {
  var id = req.params.id;
  var features_cms_query = {
    text: 'SELECT *  FROM features_cms WHERE id = $1',
    values: [id]
  }
  pool.query(features_cms_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [], code: 500 });
    } else {
      if (req.rows.length > 0) {
        res.json({ success: true, msg: "Loading data", data: req.rows[0], code: 200 });
      } else {
        res.json({ success: true, msg: "No data found", data: [], code: 200 });
      }
    }
  })
});

// INFO CMS

router.post('/hero_section_cms/create', (req, res) => {
  var title = req.body.title;
  var sub_title = req.body.sub_title;
  var content = req.body.content;
  var link = req.body.link;
  var created_at = new Date();
  var modified_at = new Date();
  var created_by = 1;
  var modified_by = 1;

  var hero_section_cms_query = {
    text: 'INSERT INTO hero_section_cms (title,content,link ,created_at,modified_at,created_by,modified_by,sub_title) VALUES ($1,$2,$3,$4,$5,$6,$7,$8);',
    values: [title, content,link, created_at, modified_at, created_by, modified_by, sub_title]
  }
  pool.query(hero_section_cms_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [] });
    } else {
      res.json({ success: true, msg: "New Hero Section Created Successfully", data: req.rows });
    }
  });
});

router.put('/hero_section_cms/update/:id', (req, res) => {
  var title = req.body.title;
  var sub_title = req.body.sub_title;
  var content = req.body.content;
  var link = req.body.link;
  var created_at = new Date();
  var modified_at = new Date();
  var created_by = 1;
  var modified_by = 1;
  var id = req.params.id;
  var hero_section_cms_query = {
    text: 'UPDATE hero_section_cms SET title=$1, content=$2, link=$3, created_at=$4, modified_at=$5 ,created_by=$6, modified_by=$7, sub_title=$9  WHERE id = $8;',
    values: [title, content, link, created_at, modified_at, created_by, modified_by, id, sub_title]
  }
  pool.query(hero_section_cms_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [] });
    } else {
      res.json({ success: true, msg: "Hero Section Updated Successfully", data: req.rows });
    }
  });
});

router.delete('/hero_section_cms/delete/:id', (req, res) => {
  var id = req.params.id;
  var hero_section_cms_query = {
    text: 'DELETE FROM hero_section_cms WHERE id= $1',
    values: [id]
  }
  pool.query(hero_section_cms_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [] });
    } else {
      res.json({ success: true, msg: "Hero Section Deleted Successfully", data: req.rows });
    }
  });
});

router.get('/hero_section_cms/getlist', (req, res) => {
  var hero_section_cms_query = {
    text: 'SELECT *  FROM hero_section_cms',
  }
  pool.query(hero_section_cms_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [] });
    } else {
      if (req.rows.length > 0) {
        res.json({ success: true, msg: "Loadind Hero Section", data: req.rows });
      } else {
        res.json({ success: true, msg: "No data Found", data: [] });
      }
    }
  });
});
router.get('/hero_section_cms/getby_id/:id', (req, res) => {
  var id = req.params.id;
  var hero_section_cms_query = {
    text: 'SELECT *  FROM hero_section_cms WHERE id = $1',
    values: [id]
  }
  pool.query(hero_section_cms_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [], code: 500 });
    } else {
      if (req.rows.length > 0) {
        res.json({ success: true, msg: "Loadind Hero Section", data: req.rows[0] });
      } else {
        res.json({ success: true, msg: "No data Found", data: [] });
      }
    }
  });
});




// SERVICES CMS

// SERVICES FILE STORE & VIEW //

var storage4 = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './files/service')
  },
  filename: function (req, file, cb) {
    cb(null, new Date().getTime()+file.originalname)
  }
})

var upload4 = multer({ storage: storage4 }).single('file');

router.get('/services_cms/viewimage', (req,res)=>{
  filepath = path.join(__dirname, '../files/service') + '/' + req.query.filename;
  res.sendFile(filepath);
});

router.post('/services_cms/file',(req,res,next)=>{
  upload4(req,res,function (err) {
    if(err) {
      res.json({
        success: false, message: 'error while uploading....',
        error: err
      });
    }
    else{
      res.json ({success: true, message:'uploaded successfully....',uploadedfile:req.file.filename});
    }
  });
});

router.post('/services_cms/create', (req, res) => {
  var title = req.body.title;
  var content = req.body.content;
  var icon = req.body.icon;
  var status = req.body.status;
  var created_at = new Date();
  var modified_at = new Date();
  var created_by = 1;
  var modified_by = 1;

  var services_cms_query = {
    text: 'INSERT INTO services_cms (title,content,icon ,created_at,modified_at,created_by,modified_by,status) VALUES ($1,$2,$3,$4,$5,$6,$7,$8);',
    values: [title, content,icon, created_at, modified_at, created_by, modified_by,status]
  }
  pool.query(services_cms_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [] });
    } else {
      res.json({ success: true, msg: "New Service Created Successfully", data: req.rows });
    }
  });
});

router.put('/services_cms/update/:id', (req, res) => {
  var title = req.body.title;
  var content = req.body.content;
  var icon = req.body.icon;
  var status = req.body.status;
  var created_at = req.body.created_at;
  var modified_at = new Date();
  var created_by = 1;
  var modified_by = 1;
  var id = req.params.id;
  var services_cms_query = {
    text: 'UPDATE services_cms SET title=$1, content=$2, icon=$3, created_at=$4, modified_at=$5 ,created_by=$6, modified_by=$7, status=$9  WHERE id = $8;',
    values: [title, content, icon, created_at, modified_at, created_by, modified_by, id,status]
  }
  pool.query(services_cms_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [] });
    } else {
      res.json({ success: true, msg: "Service Updated Successfully", data: req.rows });
    }
  });
});

router.delete('/services_cms/delete/:id', (req, res) => {
  var id = req.params.id;
  var services_cms_query = {
    text: 'DELETE FROM services_cms WHERE id= $1',
    values: [id]
  }
  pool.query(services_cms_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [] });
    } else {
      res.json({ success: true, msg: "Service Deleted Successfully", data: req.rows });
    }
  });
});

router.get('/services_cms/getlist', (req, res) => {
  var services_cms_query = {
    text: 'SELECT cd.id, cd.title, cd.content, cd.icon, cd.status, cd.created_at, cd.created_by, cd.modified_at, cd.modified_by, um.user_name FROM services_cms cd inner join user_managment um ON cd.created_by = um.id',
  }
  pool.query(services_cms_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [] });
    } else {
      if (req.rows.length > 0) {
        res.json({ success: true, msg: "Loadind Service", data: req.rows });
      } else {
        res.json({ success: true, msg: "No data Found", data: [] });
      }
    }
  });
});

router.get('/services_cms/getby_id/:id', (req, res) => {
  var id = req.params.id;
  var services_cms_query = {
    text: 'SELECT *  FROM services_cms WHERE id = $1',
    values: [id]
  }
  pool.query(services_cms_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [], code: 500 });
    } else {
      if (req.rows.length > 0) {
        res.json({ success: true, msg: "Loadind Service", data: req.rows[0] });
      } else {
        res.json({ success: true, msg: "No data Found", data: [] });
      }
    }
  });
});




// BUYER & SELLER CMS


// BUYER & SELLER FILE STORE & VIEW //

var storage5 = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './files/buyer_seller')
  },
  filename: function (req, file, cb) {
    cb(null, new Date().getTime()+file.originalname)
  }
})

var upload5 = multer({ storage: storage5 }).single('file');

router.get('/buyer_seller_cms/viewimage', (req,res)=>{
  filepath = path.join(__dirname, '../files/buyer_seller') + '/' + req.query.filename;
  res.sendFile(filepath);
});

router.post('/buyer_seller_cms/file',(req,res,next)=>{
  upload5(req,res,function (err) {
    if(err) {
      res.json({
        success: false, message: 'error while uploading....',
        error: err
      });
    }
    else{
      res.json ({success: true, message:'uploaded successfully....',uploadedfile:req.file.filename});
    }
  });
});

router.post('/buyer_seller_cms/create', (req, res) => {
  var buyer_title = req.body.buyer_title;
  var buyer_content = req.body.buyer_content;
  var buyer_link = req.body.buyer_link;
  var buyer_image = req.body.buyer_image;
  var seller_title = req.body.seller_title;
  var seller_content = req.body.seller_content;
  var seller_link = req.body.seller_link;
  var seller_image = req.body.seller_image;
  var created_at = new Date();
  var modified_at = new Date();
  var created_by = 1;
  var modified_by = 1;

  var buyer_seller_cms_query = {
    text: 'INSERT INTO buyer_seller_cms (buyer_title, buyer_content, buyer_link, buyer_image, seller_title, seller_content, seller_link, seller_image, created_at, modified_at, created_by, modified_by) VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12);',
    values: [buyer_title, buyer_content, buyer_link, buyer_image, seller_title, seller_content, seller_link, seller_image, created_at, modified_at, created_by, modified_by]
  }
  pool.query(buyer_seller_cms_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [] });
    } else {
      res.json({ success: true, msg: "Buyer Seller Info Created Successfully", data: req.rows });
    }
  });
});
router.put('/buyer_seller_cms/update/:id', (req, res) => {
  var buyer_title = req.body.buyer_title;
  var buyer_content = req.body.buyer_content;
  var buyer_link = req.body.buyer_link;
  var buyer_image = req.body.buyer_image;
  var seller_title = req.body.seller_title;
  var seller_content = req.body.seller_content;
  var seller_link = req.body.seller_link;
  var seller_image = req.body.seller_image;
  var created_at = new Date(req.body.created_at);
  var modified_at = new Date();
  var created_by = 1;
  var modified_by = 1;
  var id = req.params.id;
  var buyer_seller_cms_query = {
    text: 'UPDATE buyer_seller_cms SET buyer_title=$1, buyer_content=$2, buyer_link=$3, buyer_image=$4, seller_title=$5, seller_content=$6, seller_link=$7, seller_image=$8, created_at=$9, modified_at=$10, created_by=$11, modified_by=$12 WHERE id = $13;',
    values: [buyer_title, buyer_content, buyer_link, buyer_image, seller_title, seller_content, seller_link, seller_image, created_at, modified_at, created_by, modified_by, id]
  }
  pool.query(buyer_seller_cms_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [] });
    } else {
      res.json({ success: true, msg: "Buyer Seller Info Updated Successfully", data: req.rows });
    }
  })

});


router.get('/buyer_seller_cms/getby_id/:id', (req, res) => {
  var id = req.params.id;
  var info_cms_query = {
    text: 'SELECT *  FROM buyer_seller_cms WHERE id = $1',
    values: [id]
  }
  pool.query(info_cms_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [], code: 500 });
    } else {
      if (req.rows.length > 0) {
        res.json({ success: true, msg: "Loadind Buyer Seller Info", data: req.rows[0] });
      } else {
        res.json({ success: true, msg: "No data Found", data: [] });
      }
    }
  })
});


// SOCIAL MEDIA LINKS CMS


// SOCIAL MEDIA LINKS FILE STORE & VIEW //

var storage6 = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './files/social_media_links')
  },
  filename: function (req, file, cb) {
    cb(null, new Date().getTime()+file.originalname)
  }
})

var upload6 = multer({ storage: storage6 }).single('file');

router.get('/social_media_links_cms/viewimage', (req,res)=>{
  filepath = path.join(__dirname, '../files/social_media_links') + '/' + req.query.filename;
  res.sendFile(filepath);
});

router.post('/social_media_links_cms/file',(req,res,next)=>{
  upload6(req,res,function (err) {
    if(err) {
      res.json({
        success: false, message: 'error while uploading....',
        error: err
      });
    }
    else{
      res.json ({success: true, message:'uploaded successfully....',uploadedfile:req.file.filename});
    }
  });
});

router.post('/social_media_links_cms/create', (req, res) => {
  var icon = req.body.icon;
  var link = req.body.link;
  var status = req.body.status;
  var created_at = new Date();
  var modified_at = new Date();
  var created_by = 1;
  var modified_by = 1;

  var social_media_links_cms_query = {
    text: 'INSERT INTO social_media_links_cms (link,icon,status,created_at,modified_at,created_by,modified_by) VALUES ($1,$2,$3,$4,$5,$6,$7);',
    values: [link,icon,status,created_at, modified_at, created_by, modified_by]
  }
  pool.query(social_media_links_cms_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [] });
    } else {
      res.json({ success: true, msg: "New features Created Successfully", data: req.rows });
    }
  });
});
router.put('/social_media_links_cms/update/:id', (req, res) => {
  var icon = req.body.icon;
  var link = req.body.link;
  var status = req.body.status;
  var created_at = req.body.created_at;
  var modified_at = new Date();
  var created_by = 1;
  var modified_by = 1;
  var id = req.params.id;
  var social_media_links_cms_query = {
    text: 'UPDATE social_media_links_cms SET icon=$1, link=$2, status=$3, created_at=$4, modified_at=$5 ,created_by=$6, modified_by=$7  WHERE id = $8;',
    values: [icon, link, status, created_at, modified_at, created_by, modified_by, id]
  }
  pool.query(social_media_links_cms_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [] });
    } else {
      res.json({ success: true, msg: "Features Updated Successfully", data: req.rows });
    }
  })

});
router.delete('/social_media_links_cms/delete/:id', (req, res) => {
  var id = req.params.id;
  var social_media_links_cms_query = {
    text: 'DELETE FROM social_media_links_cms WHERE id= $1',
    values: [id]
  }
  pool.query(social_media_links_cms_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [] });
    } else {
      res.json({ success: true, msg: "Features Deleted Successfully", data: req.rows });
    }
  })
});
router.get('/social_media_links_cms/getlist', (req, res) => {
  var social_media_links_cms_query = {
    text: 'SELECT cd.id, cd.link, cd.icon, cd.status, cd.created_at, cd.created_by, cd.modified_at, cd.modified_by, um.user_name FROM social_media_links_cms cd inner join user_managment um ON cd.created_by = um.id',
  }
  pool.query(social_media_links_cms_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [] });
    } else {
      if (req.rows.length > 0) {
        res.json({ success: true, msg: "Loadind Features", data: req.rows });
      } else {
        res.json({ success: true, msg: "No data Found", data: [] });
      }
    }
  })

});
router.get('/social_media_links_cms/getby_id/:id', (req, res) => {
  var id = req.params.id;
  var social_media_links_cms_query = {
    text: 'SELECT *  FROM social_media_links_cms WHERE id = $1',
    values: [id]
  }
  pool.query(social_media_links_cms_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [], code: 500 });
    } else {
      if (req.rows.length > 0) {
        res.json({ success: true, msg: "Loading data", data: req.rows[0], code: 200 });
      } else {
        res.json({ success: true, msg: "No data found", data: [], code: 200 });
      }
    }
  })
});

// SOCIAL MEDIA LINKS CMS


// USEFULL LINKS CMS

router.post('/useful_links_cms/create', (req, res) => {
  var label = req.body.label;
  var link = req.body.link;
  var status = req.body.status;
  var created_at = new Date();
  var modified_at = new Date();
  var created_by = 1;
  var modified_by = 1;

  var useful_links_cms_query = {
    text: 'INSERT INTO useful_links_cms (link,label,status,created_at,modified_at,created_by,modified_by) VALUES ($1,$2,$3,$4,$5,$6,$7);',
    values: [link,label,status,created_at, modified_at, created_by, modified_by]
  }
  pool.query(useful_links_cms_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [] });
    } else {
      res.json({ success: true, msg: "New features Created Successfully", data: req.rows });
    }
  });
});
router.put('/useful_links_cms/update/:id', (req, res) => {
  var label = req.body.label;
  var link = req.body.link;
  var status = req.body.status;
  var created_at = req.body.created_at;
  var modified_at = new Date();
  var created_by = 1;
  var modified_by = 1;
  var id = req.params.id;
  var useful_links_cms_query = {
    text: 'UPDATE useful_links_cms SET label=$1, link=$2, status=$3, created_at=$4, modified_at=$5 ,created_by=$6, modified_by=$7  WHERE id = $8;',
    values: [label, link, status, created_at, modified_at, created_by, modified_by, id]
  }
  pool.query(useful_links_cms_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [] });
    } else {
      res.json({ success: true, msg: "Features Updated Successfully", data: req.rows });
    }
  })

});
router.delete('/useful_links_cms/delete/:id', (req, res) => {
  var id = req.params.id;
  var useful_links_cms_query = {
    text: 'DELETE FROM useful_links_cms WHERE id= $1',
    values: [id]
  }
  pool.query(useful_links_cms_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [] });
    } else {
      res.json({ success: true, msg: "Features Deleted Successfully", data: req.rows });
    }
  })
});
router.get('/useful_links_cms/getlist', (req, res) => {
  var useful_links_cms_query = {
    text: 'SELECT cd.id, cd.link, cd.label, cd.status, cd.created_at, cd.created_by, cd.modified_at, cd.modified_by, um.user_name FROM useful_links_cms cd inner join user_managment um ON cd.created_by = um.id',
  }
  pool.query(useful_links_cms_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [] });
    } else {
      if (req.rows.length > 0) {
        res.json({ success: true, msg: "Loadind Features", data: req.rows });
      } else {
        res.json({ success: true, msg: "No data Found", data: [] });
      }
    }
  })

});
router.get('/useful_links_cms/getby_id/:id', (req, res) => {
  var id = req.params.id;
  var useful_links_cms_query = {
    text: 'SELECT *  FROM useful_links_cms WHERE id = $1',
    values: [id]
  }
  pool.query(useful_links_cms_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [], code: 500 });
    } else {
      if (req.rows.length > 0) {
        res.json({ success: true, msg: "Loading data", data: req.rows[0], code: 200 });
      } else {
        res.json({ success: true, msg: "No data found", data: [], code: 200 });
      }
    }
  })
});

// USEFULL LINKS CMS


// NAV-LINKS LINKS CMS

router.post('/nav_links_cms/create', (req, res) => {
  var label = req.body.label;
  var link = req.body.link;
  var status = req.body.status;
  var created_at = new Date();
  var modified_at = new Date();
  var created_by = 1;
  var modified_by = 1;

  var nav_links_cms_query = {
    text: 'INSERT INTO nav_links_cms (link,label,status,created_at,modified_at,created_by,modified_by) VALUES ($1,$2,$3,$4,$5,$6,$7);',
    values: [link,label,status,created_at, modified_at, created_by, modified_by]
  }
  pool.query(nav_links_cms_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [] });
    } else {
      res.json({ success: true, msg: "New features Created Successfully", data: req.rows });
    }
  });
});
router.put('/nav_links_cms/update/:id', (req, res) => {
  var label = req.body.label;
  var link = req.body.link;
  var status = req.body.status;
  var created_at = req.body.created_at;
  var modified_at = new Date();
  var created_by = 1;
  var modified_by = 1;
  var id = req.params.id;
  var nav_links_cms_query = {
    text: 'UPDATE nav_links_cms SET label=$1, link=$2, status=$3, created_at=$4, modified_at=$5 ,created_by=$6, modified_by=$7  WHERE id = $8;',
    values: [label, link, status, created_at, modified_at, created_by, modified_by, id]
  }
  pool.query(nav_links_cms_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [] });
    } else {
      res.json({ success: true, msg: "Features Updated Successfully", data: req.rows });
    }
  })

});
router.delete('/nav_links_cms/delete/:id', (req, res) => {
  var id = req.params.id;
  var nav_links_cms_query = {
    text: 'DELETE FROM nav_links_cms WHERE id= $1',
    values: [id]
  }
  pool.query(nav_links_cms_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [] });
    } else {
      res.json({ success: true, msg: "Features Deleted Successfully", data: req.rows });
    }
  })
});
router.get('/nav_links_cms/getlist', (req, res) => {
  var nav_links_cms_query = {
    text: 'SELECT cd.id, cd.link, cd.label, cd.status, cd.created_at, cd.created_by, cd.modified_at, cd.modified_by, um.user_name FROM nav_links_cms cd inner join user_managment um ON cd.created_by = um.id ORDER BY created_at asc',
  }
  pool.query(nav_links_cms_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [] });
    } else {
      if (req.rows.length > 0) {
        res.json({ success: true, msg: "Loadind Features", data: req.rows });
      } else {
        res.json({ success: true, msg: "No data Found", data: [] });
      }
    }
  })

});
router.get('/nav_links_cms/getby_id/:id', (req, res) => {
  var id = req.params.id;
  var nav_links_cms_query = {
    text: 'SELECT *  FROM nav_links_cms WHERE id = $1',
    values: [id]
  }
  pool.query(nav_links_cms_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [], code: 500 });
    } else {
      if (req.rows.length > 0) {
        res.json({ success: true, msg: "Loading data", data: req.rows[0], code: 200 });
      } else {
        res.json({ success: true, msg: "No data found", data: [], code: 200 });
      }
    }
  });
});

// NAV-LINKS CMS

// Mobile Sections LINKS CMS


// Mobile Sections FILE STORE & VIEW //

var storage6 = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './files/mobile_section')
  },
  filename: function (req, file, cb) {
    cb(null, new Date().getTime()+file.originalname)
  }
})

var upload6 = multer({ storage: storage6 }).single('file');

router.get('/mobile_section_cms/viewimage', (req,res)=>{
  filepath = path.join(__dirname, '../files/mobile_section') + '/' + req.query.filename;
  res.sendFile(filepath);
});

router.post('/mobile_section_cms/file',(req,res,next)=>{
  upload6(req,res,function (err) {
    if(err) {
      res.json({
        success: false, message: 'error while uploading....',
        error: err
      });
    }
    else{
      res.json ({success: true, message:'uploaded successfully....',uploadedfile:req.file.filename});
    }
  });
});

router.post('/mobile_section_cms/create', (req, res) => {
  var title = req.body.title;
  var content = req.body.content;
  var image = req.body.image;
  var status = req.body.status;
  var created_at = new Date();
  var modified_at = new Date();
  var created_by = 1;
  var modified_by = 1;

  var mobile_section_cms_query = {
    text: 'INSERT INTO mobile_section_cms (content,title,status,created_at,modified_at,created_by,modified_by, image) VALUES ($1,$2,$3,$4,$5,$6,$7,$8);',
    values: [content,title,status,created_at, modified_at, created_by, modified_by, image]
  }
  pool.query(mobile_section_cms_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [] });
    } else {
      res.json({ success: true, msg: "New Mobile Section Created Successfully", data: req.rows });
    }
  });
});
router.put('/mobile_section_cms/update/:id', (req, res) => {
  var title = req.body.title;
  var content = req.body.content;
  var image = req.body.image;
  var status = req.body.status;
  var created_at = req.body.created_at;
  var modified_at = new Date();
  var created_by = 1;
  var modified_by = 1;
  var id = req.params.id;
  var mobile_section_cms_query = {
    text: 'UPDATE mobile_section_cms SET content=$1, title=$2, status=$3, created_at=$4, modified_at=$5 ,created_by=$6, modified_by=$7, image=$9  WHERE id = $8;',
    values: [content,title, status, created_at, modified_at, created_by, modified_by, id, image]
  }
  pool.query(mobile_section_cms_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [] });
    } else {
      res.json({ success: true, msg: "Mobile Section Updated Successfully", data: req.rows });
    }
  })

});
router.delete('/mobile_section_cms/delete/:id', (req, res) => {
  var id = req.params.id;
  var mobile_section_cms_query = {
    text: 'DELETE FROM mobile_section_cms WHERE id= $1',
    values: [id]
  }
  pool.query(mobile_section_cms_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [] });
    } else {
      res.json({ success: true, msg: "Mobile Section Deleted Successfully", data: req.rows });
    }
  })
});
router.get('/mobile_section_cms/getlist', (req, res) => {
  var mobile_section_cms_query = {
    text: 'SELECT *  FROM mobile_section_cms',
  }
  pool.query(mobile_section_cms_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [] });
    } else {
      if (req.rows.length > 0) {
        res.json({ success: true, msg: "Loadind Mobile Section", data: req.rows });
      } else {
        res.json({ success: true, msg: "No data Found", data: [] });
      }
    }
  })

});
router.get('/mobile_section_cms/getby_id/:id', (req, res) => {
  var id = req.params.id;
  var mobile_section_cms_query = {
    text: 'SELECT *  FROM mobile_section_cms WHERE id = $1',
    values: [id]
  }
  pool.query(mobile_section_cms_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [], code: 500 });
    } else {
      if (req.rows.length > 0) {
        res.json({ success: true, msg: "Loading data", data: req.rows[0], code: 200 });
      } else {
        res.json({ success: true, msg: "No data found", data: [], code: 200 });
      }
    }
  })
});

// Mobile Section LINKS CMS

module.exports = router;
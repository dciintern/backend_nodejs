var express = require('express');
var router = express.Router();
var pool = require('../config/db');

router.post('/configuration-details/create', (req, res) => {

  var config_name = req.body.config_name;
  var config_URL = req.body.config_URL;
  var config_username = req.body.config_username;
  var config_password = req.body.config_password;
  var config_port = req.body.config_port;
  var config_parameter = req.body.config_parameter;
  var config_MobileNumber = req.body.config_MobileNumber;
  var config_mtype = req.body.config_mtype;
  var config_message = req.body.config_message;
  var config_secretkey = req.body.config_secretkey;
  var status = req.body.status;
  var remarks = req.body.remarks;
  var created_at = new Date();
  var modified_at = new Date();
  var created_by = 1;
  var modified_by = 1;
  var configuartion_TEMPATE_query = {
    text: 'INSERT INTO configuration_details (config_name, config_URL, config_username, config_password, config_port, config_parameter, config_MobileNumber, config_mtype, config_message, config_secretkey, status, remarks, created_at, modified_at, created_by, modified_by) VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16);',
    values: [config_name, config_URL, config_username, config_password, config_port, config_parameter, config_MobileNumber, config_mtype, config_message, config_secretkey, status, remarks, created_at, modified_at, created_by, modified_by]
  }
  pool.query(configuartion_TEMPATE_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [], code: 500 });
    } else {
      res.json({ success: true, msg: "New configuartion Template Created Successfully", data: req.rows, code: 200 });
    }
  });
});

router.put('/configuration-details/update/:id', (req, res) => {
    var id = req.body.id;
    var config_name = req.body.config_name;
    var config_URL = req.body.config_URL;
    var config_username = req.body.config_username;
    var config_password = req.body.config_password;
    var config_port = req.body.config_port;
    var config_parameter = req.body.config_parameter;
    var config_MobileNumber = req.body.config_MobileNumber;
    var config_mtype = req.body.config_mtype;
    var config_message = req.body.config_message;
    var config_secretkey = req.body.config_secretkey;
    var status = req.body.status;
    var remarks = req.body.remarks;
    var created_at = req.body.created_at;
    var modified_at = new Date();
    var created_by = 1;
    var modified_by = 1;
  var configuartion_TEMPATE_query = {
    text: 'UPDATE configuration_details SET config_name=$1, config_URL=$2, config_username=$3, config_password=$4, config_port=$5, config_parameter=$6, config_MobileNumber=$7, config_mtype=$8, config_message=$9, config_secretkey=$10, status=$11, remarks=$12, created_at=$13, modified_at=$14, created_by=$15, modified_by=$16  WHERE id = $17;',
    values: [config_name, config_URL, config_username, config_password, config_port, config_parameter, config_MobileNumber, config_mtype, config_message, config_secretkey, status, remarks, created_at, modified_at, created_by, modified_by,id]
  }
  pool.query(configuartion_TEMPATE_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database" });
    } else {
      res.json({ success: true, msg: "configuartion Template Updated Successfully", data: req.rows });
    }
  });
});

router.delete('/configuration-details/delete/:id', (req, res) => {
  var id = req.params.id;
  var configuartion_TEMPATE_query = {
    text: 'DELETE FROM configuration_details WHERE id= $1',
    values: [id]
  }
  pool.query(configuartion_TEMPATE_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database" });
    } else {
      res.json({ success: true, msg: "configuartion Template Deleted Successfully" });
    }
  });
});


router.get('/configuration-details/getlist', (req, res) => {
  var configuartion_TEMPATE_query = {
    text: 'SELECT *, cd.created_at, cd.created_by, cd.modified_at, cd.modified_by, um.user_name FROM configuration_details cd inner join user_managment um ON cd.created_by = um.id',
  }
  pool.query(configuartion_TEMPATE_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database" });
    } else {
      res.json({ success: true, msg: 'Loading configuartion Template', data: req.rows });
    }
  });
});

router.get('/configuration-details/getby_id/:id', (req, res) => {
  var id = req.params.id;
  var configuartion_TEMPATE_query = {
    text: 'SELECT *  FROM configuration_details WHERE id = $1',
    values: [id]
  }
  pool.query(configuartion_TEMPATE_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database" });
    } else {
      if (req.rows.length > 0) {
        res.json({ success: true, msg: "Loading data", data: req.rows[0], code: 200 });
      } else {
        res.json({ success: true, msg: "No data found", data: [], code: 200 });
      }
    }
  });
});

module.exports = router;
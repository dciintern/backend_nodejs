var express = require('express');
var router = express.Router();
var pool = require('../config/db');


// Category Request Status API  starts here...//

router.post('/category_request/create', (req, res) => {
  var cat_req_status_name = req.body.cat_req_status_name;
  var remarks = req.body.remarks;
  var status = req.body.status;
  var created_at = new Date();
  var modified_at = new Date();
  var created_by = 1;
  var modified_by = 1;
  var category_request_status_query = {
    text: 'INSERT INTO category_request_status (cat_req_status_name,remarks,status,created_at,modified_at,created_by,modified_by) VALUES ($1,$2,$3,$4,$5,$6,$7);',
    values: [cat_req_status_name,remarks,status,created_at,modified_at,created_by,modified_by]
  }
  pool.query(category_request_status_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [], code: 500 });
    } else {
      res.json({ success: true, msg: "Created Successfully", data: req.rows, code: 200 });
    }
  })
});

router.put('/category_request/update/:id', (req, res) => {
  var cat_req_status_id = req.body.cat_req_status_id;
  var cat_req_status_name = req.body.cat_req_status_name;
  var remarks = req.body.remarks;
  var status = req.body.status;
  var created_at = req.body.created_at;
  var modified_at = new Date();
  var created_by = 1;
  var modified_by = 1;
  var category_request_status_query = {
    text: 'UPDATE category_request_status SET cat_req_status_name=$2,status=$3,remarks=$4,created_at=$5,modified_at=$6,created_by=$7,modified_by=$8  WHERE cat_req_status_id = $1;',
    values: [cat_req_status_id,cat_req_status_name,remarks,status,created_at,modified_at,created_by,modified_by]
  }
  pool.query(category_request_status_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [], code: 500 });
    } else {
      res.json({ success: true, msg: "Updated Successfully", data: req.rows, code: 200 });
    }
  })

});

router.delete('/category_request/delete/:id', (req, res) => {
  var id = req.params.id;
  var category_request_status_query = {
    text: 'DELETE FROM category_request_status WHERE cat_req_status_id= $1',
    values: [id]
  }
  pool.query(category_request_status_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [] });
    } else {
      res.json({ success: true, msg: "Deleted Successfully", data: req.rows });
    }
  });

});
router.get('/category_request/getlist', (req, res) => {
  var status_query = {
    text: 'SELECT cd.cat_req_status_id, cd.cat_req_status_name, cd.remarks, cd.status, cd.created_at, cd.created_by, cd.modified_at, cd.modified_by, um.user_name FROM category_request_status cd inner join user_managment um ON cd.created_by = um.id',
  }
  pool.query(status_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [] });
    } else {
      if (req.rows.length > 0) {
        res.json({ success: true, msg: "Loadind Status", data: req.rows });
      } else {
        res.json({ success: true, msg: "No data Found", data: [] });
      }
    }
  });

});
router.get('/category_request/getby_id/:id', (req, res) => {
  var id = req.params.id;
  var status_query = {
    text: 'SELECT *  FROM category_request_status WHERE cat_req_status_id = $1',
    values: [id]
  }
  pool.query(status_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [], code: 500 });
    } else {
      if (req.rows.length > 0) {
        res.json({ success: true, msg: "Loading data", data: req.rows[0], code: 200 });
      } else {
        res.json({ success: true, msg: "No data found", data: [], code: 200 });
      }
    }
  });

});

// Category Request Status API  ends here...//



// Make Offer Status API  starts here...//

router.post('/make_offer/create', (req, res) => {
  var offer_status_name = req.body.offer_status_name;
  var remarks = req.body.remarks;
  var status = req.body.status;
  var created_at = new Date();
  var modified_at = new Date();
  var created_by = 1;
  var modified_by = 1;
  var make_offer_status_query = {
    text: 'INSERT INTO make_offer_status (offer_status_name,remarks,status,created_at,modified_at,created_by,modified_by) VALUES ($1,$2,$3,$4,$5,$6,$7);',
    values: [offer_status_name,remarks,status,created_at,modified_at,created_by,modified_by]
  }
  pool.query(make_offer_status_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [], code: 500 });
    } else {
      res.json({ success: true, msg: "Created Successfully", data: req.rows, code: 200 });
    }
  })
});

router.put('/make_offer/update/:id', (req, res) => {
  var make_offer_status_id = req.body.make_offer_status_id;
  var offer_status_name = req.body.offer_status_name;
  var remarks = req.body.remarks;
  var status = req.body.status;
  var created_at = req.body.created_at;
  var modified_at = new Date();
  var created_by = 1;
  var modified_by = 1;
  var make_offer_status_query = {
    text: 'UPDATE make_offer_status SET offer_status_name=$2,remarks=$3,status=$4,created_at=$5,modified_at=$6,created_by=$7,modified_by=$8  WHERE make_offer_status_id = $1;',
    values: [make_offer_status_id,offer_status_name,remarks,status,created_at,modified_at,created_by,modified_by]
  }
  pool.query(make_offer_status_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [], code: 500 });
    } else {
      res.json({ success: true, msg: "Updated Successfully", data: req.rows, code: 200 });
    }
  })

});

router.delete('/make_offer/delete/:id', (req, res) => {
  var id = req.params.id;
  var make_offer_status_query = {
    text: 'DELETE FROM make_offer_status WHERE make_offer_status_id= $1',
    values: [id]
  }
  pool.query(make_offer_status_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [] });
    } else {
      res.json({ success: true, msg: "Deleted Successfully", data: req.rows });
    }
  });

});
router.get('/make_offer/getlist', (req, res) => {
  var status_query = {
    text: 'SELECT cd.make_offer_status_id, cd.offer_status_name, cd.remarks, cd.status, cd.created_at, cd.created_by, cd.modified_at, cd.modified_by, um.user_name FROM make_offer_status cd inner join user_managment um ON cd.created_by = um.id',
  }
  pool.query(status_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [] });
    } else {
      if (req.rows.length > 0) {
        res.json({ success: true, msg: "Loadind Status", data: req.rows });
      } else {
        res.json({ success: true, msg: "No data Found", data: [] });
      }
    }
  });

});
router.get('/make_offer/getby_id/:id', (req, res) => {
  var id = req.params.id;
  var status_query = {
    text: 'SELECT *  FROM make_offer_status WHERE make_offer_status_id = $1',
    values: [id]
  }
  pool.query(status_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [], code: 500 });
    } else {
      if (req.rows.length > 0) {
        res.json({ success: true, msg: "Loading data", data: req.rows[0], code: 200 });
      } else {
        res.json({ success: true, msg: "No data found", data: [], code: 200 });
      }
    }
  });

});

// Make Offer Status API  ends here...//


// Order Status API  starts here...//

router.post('/order/create', (req, res) => {
  var order_status_name = req.body.order_status_name;
  var remarks = req.body.remarks;
  var status = req.body.status;
  var created_at = new Date();
  var modified_at = new Date();
  var created_by = 1;
  var modified_by = 1;
  var order_status_query = {
    text: 'INSERT INTO order_status (order_status_name,remarks,status,created_at,modified_at,created_by,modified_by) VALUES ($1,$2,$3,$4,$5,$6,$7);',
    values: [order_status_name,remarks,status,created_at,modified_at,created_by,modified_by]
  }
  pool.query(order_status_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [], code: 500 });
    } else {
      res.json({ success: true, msg: "Created Successfully", data: req.rows, code: 200 });
    }
  })
});

router.put('/order/update/:id', (req, res) => {
  var order_status_id = req.body.order_status_id;
  var order_status_name = req.body.order_status_name;
  var remarks = req.body.remarks;
  var status = req.body.status;
  var created_at = req.body.created_at;
  var modified_at = new Date();
  var created_by = 1;
  var modified_by = 1;
  var order_status_query = {
    text: 'UPDATE order_status SET order_status_name=$2,status=$3,remarks=$4,created_at=$5,modified_at=$6,created_by=$7,modified_by=$8  WHERE order_status_id = $1;',
    values: [order_status_id,order_status_name,status,remarks,created_at,modified_at,created_by,modified_by]
  }
  pool.query(order_status_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [], code: 500 });
    } else {
      res.json({ success: true, msg: "Updated Successfully", data: req.rows, code: 200 });
    }
  })

});

router.delete('/order/delete/:id', (req, res) => {
  var id = req.params.id;
  var order_status_query = {
    text: 'DELETE FROM order_status WHERE order_status_id= $1',
    values: [id]
  }
  pool.query(order_status_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [] });
    } else {
      res.json({ success: true, msg: "Deleted Successfully", data: req.rows });
    }
  });

});
router.get('/order/getlist', (req, res) => {
  var status_query = {
    text: 'SELECT cd.order_status_id, cd.order_status_name, cd.remarks, cd.status, cd.created_at, cd.created_by, cd.modified_at, cd.modified_by, um.user_name FROM order_status cd inner join user_managment um ON cd.created_by = um.id',
  }
  pool.query(status_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [] });
    } else {
      if (req.rows.length > 0) {
        res.json({ success: true, msg: "Loadind Status", data: req.rows });
      } else {
        res.json({ success: true, msg: "No data Found", data: [] });
      }
    }
  });

});
router.get('/order/getby_id/:id', (req, res) => {
  var id = req.params.id;
  var status_query = {
    text: 'SELECT *  FROM order_status WHERE order_status_id = $1',
    values: [id]
  }
  pool.query(status_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [], code: 500 });
    } else {
      if (req.rows.length > 0) {
        res.json({ success: true, msg: "Loading data", data: req.rows[0], code: 200 });
      } else {
        res.json({ success: true, msg: "No data found", data: [], code: 200 });
      }
    }
  });

});

// Order Status API  ends here...//


// Parts Request Status API  starts here...//

router.post('/parts_req/create', (req, res) => {
  var parts_req_status_name = req.body.parts_req_status_name;
  var remarks = req.body.remarks;
  var status = req.body.status;
  var created_at = new Date();
  var modified_at = new Date();
  var created_by = 1;
  var modified_by = 1;
  var parts_req_status_query = {
    text: 'INSERT INTO parts_req_status (parts_req_status_name,remarks,status,created_at,modified_at,created_by,modified_by) VALUES ($1,$2,$3,$4,$5,$6,$7);',
    values: [parts_req_status_name,remarks,status,created_at,modified_at,created_by,modified_by]
  }
  pool.query(parts_req_status_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [], code: 500 });
    } else {
      res.json({ success: true, msg: "Created Successfully", data: req.rows, code: 200 });
    }
  })
});

router.put('/parts_req/update/:id', (req, res) => {
  var parts_req_status_id = req.body.parts_req_status_id;
  var parts_req_status_name = req.body.parts_req_status_name;
  var remarks = req.body.remarks;
  var status = req.body.status;
  var created_at = req.body.created_at;
  var modified_at = new Date();
  var created_by = 1;
  var modified_by = 1;
  var parts_req_status_query = {
    text: 'UPDATE parts_req_status SET parts_req_status_name=$2,status=$3,remarks=$4,created_at=$5,modified_at=$6,created_by=$7,modified_by=$8  WHERE parts_req_status_id = $1;',
    values: [parts_req_status_id,parts_req_status_name,status,remarks,created_at,modified_at,created_by,modified_by]
  }
  pool.query(parts_req_status_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [], code: 500 });
    } else {
      res.json({ success: true, msg: "Updated Successfully", data: req.rows, code: 200 });
    }
  })

});

router.delete('/parts_req/delete/:id', (req, res) => {
  var id = req.params.id;
  var parts_req_status_query = {
    text: 'DELETE FROM parts_req_status WHERE parts_req_status_id= $1',
    values: [id]
  }
  pool.query(parts_req_status_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [] });
    } else {
      res.json({ success: true, msg: "Deleted Successfully", data: req.rows });
    }
  });

});
router.get('/parts_req/getlist', (req, res) => {
  var status_query = {
    text: 'SELECT cd.parts_req_status_id, cd.parts_req_status_name, cd.remarks, cd.status, cd.created_at, cd.created_by, cd.modified_at, cd.modified_by, um.user_name FROM parts_req_status cd inner join user_managment um ON cd.created_by = um.id',
  }
  pool.query(status_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [] });
    } else {
      if (req.rows.length > 0) {
        res.json({ success: true, msg: "Loadind Status", data: req.rows });
      } else {
        res.json({ success: true, msg: "No data Found", data: [] });
      }
    }
  });

});
router.get('/parts_req/getby_id/:id', (req, res) => {
  var id = req.params.id;
  var status_query = {
    text: 'SELECT *  FROM parts_req_status WHERE parts_req_status_id = $1',
    values: [id]
  }
  pool.query(status_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [], code: 500 });
    } else {
      if (req.rows.length > 0) {
        res.json({ success: true, msg: "Loading data", data: req.rows[0], code: 200 });
      } else {
        res.json({ success: true, msg: "No data found", data: [], code: 200 });
      }
    }
  });

});

// Parts Request Status API  ends here...//


// Payment Status API  starts here...//

router.post('/payment/create', (req, res) => {
  var payment_status_name = req.body.payment_status_name;
  var remarks = req.body.remarks;
  var status = req.body.status;
  var created_at = new Date();
  var modified_at = new Date();
  var created_by = 1;
  var modified_by = 1;
  var payment_status_query = {
    text: 'INSERT INTO payment_status (payment_status_name,remarks,status,created_at,modified_at,created_by,modified_by) VALUES ($1,$2,$3,$4,$5,$6,$7);',
    values: [payment_status_name,remarks,status,created_at,modified_at,created_by,modified_by]
  }
  pool.query(payment_status_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [], code: 500 });
    } else {
      res.json({ success: true, msg: "Created Successfully", data: req.rows, code: 200 });
    }
  })
});

router.put('/payment/update/:id', (req, res) => {
  var payment_status_id = req.body.payment_status_id;
  var payment_status_name = req.body.payment_status_name;
  var remarks = req.body.remarks;
  var status = req.body.status;
  var created_at = req.body.created_at;
  var modified_at = new Date();
  var created_by = 1;
  var modified_by = 1;
  var payment_status_query = {
    text: 'UPDATE payment_status SET payment_status_name=$2,status=$3,remarks=$4,created_at=$5,modified_at=$6,created_by=$7,modified_by=$8  WHERE payment_status_id = $1;',
    values: [payment_status_id,payment_status_name,status,remarks,created_at,modified_at,created_by,modified_by]
  }
  pool.query(payment_status_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [], code: 500 });
    } else {
      res.json({ success: true, msg: "Updated Successfully", data: req.rows, code: 200 });
    }
  })

});

router.delete('/payment/delete/:id', (req, res) => {
  var id = req.params.id;
  var payment_status_query = {
    text: 'DELETE FROM payment_status WHERE payment_status_id= $1',
    values: [id]
  }
  pool.query(payment_status_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [] });
    } else {
      res.json({ success: true, msg: "Deleted Successfully", data: req.rows });
    }
  });

});
router.get('/payment/getlist', (req, res) => {
  var status_query = {
    text: 'SELECT cd.payment_status_id, cd.payment_status_name, cd.remarks, cd.status, cd.created_at, cd.created_by, cd.modified_at, cd.modified_by, um.user_name FROM payment_status cd inner join user_managment um ON cd.created_by = um.id',
  }
  pool.query(status_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [] });
    } else {
      if (req.rows.length > 0) {
        res.json({ success: true, msg: "Loadind Status", data: req.rows });
      } else {
        res.json({ success: true, msg: "No data Found", data: [] });
      }
    }
  });

});
router.get('/payment/getby_id/:id', (req, res) => {
  var id = req.params.id;
  var status_query = {
    text: 'SELECT *  FROM payment_status WHERE payment_status_id = $1',
    values: [id]
  }
  pool.query(status_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [], code: 500 });
    } else {
      if (req.rows.length > 0) {
        res.json({ success: true, msg: "Loading data", data: req.rows[0], code: 200 });
      } else {
        res.json({ success: true, msg: "No data found", data: [], code: 200 });
      }
    }
  });

});

// Payment Status API  ends here...//


// Quote Status API  starts here...//

router.post('/quote/create', (req, res) => {
  var quote_status_name = req.body.quote_status_name;
  var remarks = req.body.remarks;
  var status = req.body.status;
  var created_at = new Date();
  var modified_at = new Date();
  var created_by = 1;
  var modified_by = 1;
  var quote_status_query = {
    text: 'INSERT INTO quote_status (quote_status_name,remarks,status,created_at,modified_at,created_by,modified_by) VALUES ($1,$2,$3,$4,$5,$6,$7);',
    values: [quote_status_name,remarks,status,created_at,modified_at,created_by,modified_by]
  }
  pool.query(quote_status_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [], code: 500 });
    } else {
      res.json({ success: true, msg: "Created Successfully", data: req.rows, code: 200 });
    }
  })
});

router.put('/quote/update/:id', (req, res) => {
  var quote_status_id = req.body.quote_status_id;
  var quote_status_name = req.body.quote_status_name;
  var remarks = req.body.remarks;
  var status = req.body.status;
  var created_at = req.body.created_at;
  var modified_at = new Date();
  var created_by = 1;
  var modified_by = 1;
  var quote_status_query = {
    text: 'UPDATE quote_status SET quote_status_name=$2,status=$3,remarks=$4,created_at=$5,modified_at=$6,created_by=$7,modified_by=$8  WHERE quote_status_id = $1;',
    values: [quote_status_id,quote_status_name,status,remarks,created_at,modified_at,created_by,modified_by]
  }
  pool.query(quote_status_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [], code: 500 });
    } else {
      res.json({ success: true, msg: "Updated Successfully", data: req.rows, code: 200 });
    }
  })

});

router.delete('/quote/delete/:id', (req, res) => {
  var id = req.params.id;
  var quote_status_query = {
    text: 'DELETE FROM quote_status WHERE quote_status_id= $1',
    values: [id]
  }
  pool.query(quote_status_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [] });
    } else {
      res.json({ success: true, msg: "Deleted Successfully", data: req.rows });
    }
  });

});
router.get('/quote/getlist', (req, res) => {
  var status_query = {
    text: 'SELECT cd.quote_status_id, cd.quote_status_name, cd.remarks, cd.status, cd.created_at, cd.created_by, cd.modified_at, cd.modified_by, um.user_name FROM quote_status cd inner join user_managment um ON cd.created_by = um.id',
  }
  pool.query(status_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [] });
    } else {
      if (req.rows.length > 0) {
        res.json({ success: true, msg: "Loadind Status", data: req.rows });
      } else {
        res.json({ success: true, msg: "No data Found", data: [] });
      }
    }
  });

});
router.get('/quote/getby_id/:id', (req, res) => {
  var id = req.params.id;
  var status_query = {
    text: 'SELECT *  FROM quote_status WHERE quote_status_id = $1',
    values: [id]
  }
  pool.query(status_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [], code: 500 });
    } else {
      if (req.rows.length > 0) {
        res.json({ success: true, msg: "Loading data", data: req.rows[0], code: 200 });
      } else {
        res.json({ success: true, msg: "No data found", data: [], code: 200 });
      }
    }
  });

});

// Quote Status API  ends here...//


// Registration Status API  starts here...//

router.post('/registration/create', (req, res) => {
  var reg_status_name = req.body.reg_status_name;
  var remarks = req.body.remarks;
  var status = req.body.status;
  var created_at = new Date();
  var modified_at = new Date();
  var created_by = 1;
  var modified_by = 1;
  var registration_status_query = {
    text: 'INSERT INTO registration_status (reg_status_name,remarks,status,created_at,modified_at,created_by,modified_by) VALUES ($1,$2,$3,$4,$5,$6,$7);',
    values: [reg_status_name,remarks,status,created_at,modified_at,created_by,modified_by]
  }
  pool.query(registration_status_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [], code: 500 });
    } else {
      res.json({ success: true, msg: "Created Successfully", data: req.rows, code: 200 });
    }
  });
});

router.put('/registration/update/:id', (req, res) => {
  var reg_status_id = req.body.reg_status_id;
  var reg_status_name = req.body.reg_status_name;
  var remarks = req.body.remarks;
  var status = req.body.status;
  var created_at = req.body.created_at;
  var modified_at = new Date();
  var created_by = 1;
  var modified_by = 1;
  var registration_status_query = {
    text: 'UPDATE registration_status SET reg_status_name=$2,status=$3,remarks=$4,created_at=$5,modified_at=$6,created_by=$7,modified_by=$8  WHERE reg_status_id = $1;',
    values: [reg_status_id,reg_status_name,status,remarks,created_at,modified_at,created_by,modified_by]
  }
  pool.query(registration_status_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [], code: 500 });
    } else {
      res.json({ success: true, msg: "Updated Successfully", data: req.rows, code: 200 });
    }
  });
});

router.delete('/registration/delete/:id', (req, res) => {
  var id = req.params.id;
  var registration_status_query = {
    text: 'DELETE FROM registration_status WHERE reg_status_id= $1',
    values: [id]
  }
  pool.query(registration_status_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [] });
    } else {
      res.json({ success: true, msg: "Deleted Successfully", data: req.rows });
    }
  });
});
router.get('/registration/getlist', (req, res) => {
  var registration_status_query = {
    text: 'SELECT cd.reg_status_id, cd.reg_status_name, cd.remarks, cd.status, cd.created_at, cd.created_by, cd.modified_at, cd.modified_by, um.user_name FROM registration_status cd inner join user_managment um ON cd.created_by = um.id',
  }
  pool.query(registration_status_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [] });
    } else {
      if (req.rows.length > 0) {
        res.json({ success: true, msg: "Loadind Status", data: req.rows });
      } else {
        res.json({ success: true, msg: "No data Found", data: [] });
      }
    }
  });
});
router.get('/registration/getby_id/:id', (req, res) => {
  var id = req.params.id;
  var registration_status_query = {
    text: 'SELECT *  FROM registration_status WHERE reg_status_id = $1',
    values: [id]
  }
  pool.query(registration_status_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [], code: 500 });
    } else {
      if (req.rows.length > 0) {
        res.json({ success: true, msg: "Loading data", data: req.rows[0], code: 200 });
      } else {
        res.json({ success: true, msg: "No data found", data: [], code: 200 });
      }
    }
  });
});

// Registration Status API  ends here...//


// Shipment Status API  starts here...//

router.post('/shipment/create', (req, res) => {
  var shipment_status_name = req.body.shipment_status_name;
  var remarks = req.body.remarks;
  var status = req.body.status;
  var created_at = new Date();
  var modified_at = new Date();
  var created_by = 1;
  var modified_by = 1;
  var shipment_status_query = {
    text: 'INSERT INTO shipment_status (shipment_status_name,remarks,status,created_at,modified_at,created_by,modified_by) VALUES ($1,$2,$3,$4,$5,$6,$7);',
    values: [shipment_status_name,remarks,status,created_at,modified_at,created_by,modified_by]
  }
  pool.query(shipment_status_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [], code: 500 });
    } else {
      res.json({ success: true, msg: "Created Successfully", data: req.rows, code: 200 });
    }
  })
});

router.put('/shipment/update/:id', (req, res) => {
  var shipment_status_id = req.body.shipment_status_id;
  var shipment_status_name = req.body.shipment_status_name;
  var remarks = req.body.remarks;
  var status = req.body.status;
  var created_at = req.body.created_at;
  var modified_at = new Date();
  var created_by = 1;
  var modified_by = 1;
  var shipment_status_query = {
    text: 'UPDATE shipment_status SET shipment_status_name=$2,status=$3,remarks=$4,created_at=$5,modified_at=$6,created_by=$7,modified_by=$8  WHERE shipment_status_id = $1;',
    values: [shipment_status_id,shipment_status_name,status,remarks,created_at,modified_at,created_by,modified_by]
  }
  pool.query(shipment_status_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [], code: 500 });
    } else {
      res.json({ success: true, msg: "Updated Successfully", data: req.rows, code: 200 });
    }
  })

});

router.delete('/shipment/delete/:id', (req, res) => {
  var id = req.params.id;
  var shipment_status_query = {
    text: 'DELETE FROM shipment_status WHERE shipment_status_id= $1',
    values: [id]
  }
  pool.query(shipment_status_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [] });
    } else {
      res.json({ success: true, msg: "Deleted Successfully", data: req.rows });
    }
  });

});
router.get('/shipment/getlist', (req, res) => {
  var status_query = {
    text: 'SELECT cd.shipment_status_id, cd.shipment_status_name, cd.remarks, cd.status, cd.created_at, cd.created_by, cd.modified_at, cd.modified_by, um.user_name FROM shipment_status cd inner join user_managment um ON cd.created_by = um.id',
  }
  pool.query(status_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [] });
    } else {
      if (req.rows.length > 0) {
        res.json({ success: true, msg: "Loadind Status", data: req.rows });
      } else {
        res.json({ success: true, msg: "No data Found", data: [] });
      }
    }
  });

});
router.get('/shipment/getby_id/:id', (req, res) => {
  var id = req.params.id;
  var status_query = {
    text: 'SELECT *  FROM shipment_status WHERE shipment_status_id = $1',
    values: [id]
  }
  pool.query(status_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [], code: 500 });
    } else {
      if (req.rows.length > 0) {
        res.json({ success: true, msg: "Loading data", data: req.rows[0], code: 200 });
      } else {
        res.json({ success: true, msg: "No data found", data: [], code: 200 });
      }
    }
  });

});

// shipment Status API  ends here...//

module.exports = router;
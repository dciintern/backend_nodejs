var express = require('express');
var router = express.Router();
var pool = require('../config/db');

router.post('/create', (req, res) => {
  console.log(req.body);
  var module_id = req.body.module_id;
  var title_name = req.body.title_name;
  var sms_content = req.body.sms_content;
  var status = req.body.status;
  var action = req.body.action;
  var created_at = new Date();
  var modified_at = new Date();
  var created_by = 1;
  var modified_by = 1;
  var SMS_TEMPATE_query = {
    text: 'INSERT INTO sms_template (module_id,created_at,modified_at,created_by,modified_by,status,title_name,sms_content,action) VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9);',
    values: [module_id, created_at, modified_at, created_by, modified_by,status,title_name,sms_content,action]
  }
  pool.query(SMS_TEMPATE_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [], code: 500 });
    } else {
      res.json({ success: true, msg: "New SMS Template Created Successfully", data: req.rows, code: 200 });
    }
  });
});

router.put('/update/:id', (req, res) => {
  var module_id = req.body.module_id;
  var title_name = req.body.title_name;
  var sms_content = req.body.sms_content;
  var status = req.body.status;
  var action = req.body.action;
  var modified_at = new Date();
  var created_by = 1;
  var modified_by = 1;
  var id = req.params.id;
  var SMS_TEMPATE_query = {
    text: 'UPDATE sms_template SET module_id=$1, created_at=$2, modified_at=$3 ,created_by=$4, modified_by=$5, status=$7, title_name=$8, sms_content=$9, action=$10  WHERE id = $6;',
    values: [module_id, created_at, modified_at, created_by, modified_by, id,status, title_name,sms_content,action]
  }
  pool.query(SMS_TEMPATE_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database" });
    } else {
      res.json({ success: true, msg: "SMS Template Updated Successfully", data: req.rows });
    }
  });
});

router.delete('/delete/:id', (req, res) => {
  var id = req.params.id;
  var SMS_TEMPATE_query = {
    text: 'DELETE FROM sms_template WHERE id= $1',
    values: [id]
  }
  pool.query(SMS_TEMPATE_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database" });
    } else {
      res.json({ success: true, msg: "SMS Template Deleted Successfully" });
    }
  });
});


router.get('/getlist', (req, res) => {
  var SMS_TEMPATE_query = {
    text: 'SELECT *, cd.created_at, cd.created_by, cd.modified_at, cd.modified_by, um.user_name FROM sms_template cd inner join user_managment um ON cd.created_by = um.id',
  }
  pool.query(SMS_TEMPATE_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database" });
    } else {
      res.json({ success: true, msg: 'Loading SMS Template', data: req.rows });
    }
  });
});

router.get('/getby_id/:id', (req, res) => {
  var id = req.params.id;
  var SMS_TEMPATE_query = {
    text: 'SELECT *  FROM sms_template WHERE id = $1',
    values: [id]
  }
  pool.query(SMS_TEMPATE_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database" });
    } else {
      if (req.rows.length > 0) {
        res.json({ success: true, msg: "Loading data", data: req.rows[0], code: 200 });
      } else {
        res.json({ success: true, msg: "No data found", data: [], code: 200 });
      }
    }
  });
});

module.exports = router;
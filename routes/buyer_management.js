var express = require('express');
var router = express.Router();
var pool = require('../config/db');


// BUYER MANAGEMENT API  starts here...//

router.post('/create', (req, res) => {
    var user_id = req.body.remarks;
    var buyer_code = req.body.remarks;
    var email_verification_status = req.body.remarks;
    var business_name = req.body.remarks;
    var business_com_logo = req.body.remarks;
    var email_id = req.body.remarks;
    var subscription_id = req.body.remarks;
    var subscription_effective_date = req.body.remarks;
    var business_status = req.body.remarks;
    var billing_country = req.body.remarks;
    var billing_state = req.body.remarks;
    var billing_city = req.body.remarks;
    var b_address1 = req.body.remarks;
    var b_address2 = req.body.remarks;
    var b_displayname = req.body.remarks;
    var b_phonenumber = req.body.remarks;
    var b_country_code = req.body.remarks;
    var jurisdiction_id = req.body.remarks;
    var pos_id = req.body.remarks;
    var shipping_country = req.body.remarks;
    var shipping_state = req.body.remarks;
    var shipping_city = req.body.remarks;
    var s_address1 = req.body.remarks;
    var s_address2 = req.body.remarks;
    var s_displayname = req.body.remarks;
    var s_phonenumber = req.body.remarks;
    var s_country_code = req.body.remarks;
    var phoneno_verified = req.body.remarks;
    var drop_address = req.body.remarks;
    var googlemap_link = req.body.remarks;
    var payment_type_info= req.body.remarks; 
    var product_type_id = req.body.remarks;
    var trading_type_id = req.body.remarks;
    var iv_location = req.body.remarks;
    var iv_trade_licenese = req.body.remarks;
    var iv_res_national = req.body.remarks;
    var iv_exp_date = req.body.remarks;
    var iv_country_issue = req.body.remarks;
    var iv_f_name = req.body.remarks;
    var iv_m_name = req.body.remarks;
    var iv_l_name = req.body.remarks;
    var iv_dob = req.body.remarks;
    var iv_r_bussiness_name = req.body.remarks;
    var iv_licenese_no = req.body.remarks;
    var iv_r_address = req.body.remarks;
    var id_doc_front = req.body.remarks;
    var id_doc_back = req.body.remarks;
    var bus_doc_license = req.body.remarks;
    var registration_status = req.body.remarks;
    var cancel_reason = req.body.remarks;
    var vat_registration_number = req.body.remarks;
    var tax_registration_exp_date = req.body.remarks;
    var vat_reg_certificate_certificate = req.body.remarks;
    var bank_benefi_name = req.body.remarks;
    var bank_name = req.body.remarks;
    var bank_ac_no = req.body.remarks;
    var bank_iban = req.body.remarks;
    var bank_branch_name = req.body.remarks;
    var swift_code = req.body.remarks;
    var bank_currency = req.body.remarks;
    var bank_browse_file = req.body.remarks;
    var erp_id = req.body.remarks;
    var last_integrated_date = req.body.remarks;
    var company_buyer_representative = req.body.remarks;
    var t_and_c_acknowledge = req.body.remarks;
    var created_at = new Date();
    var created_by = 1;
    var modified_at = new Date();
    var modified_by = 1;
  var shipment_status_query = {
    text: 'INSERT INTO new_buyer_details (user_id, buyer_code, email_verification_status ,business_name, business_com_logo , email_id ,subscription_id ,ubscription_effective_date ,business_status ,billing_country ,billing_state ,billing_city  , b_address1 , b_address2 , b_displayname , b_phonenumber , b_country_code , jurisdiction_id , pos_id , shipping_country ,shipping_state ,shipping_city ,s_address1 ,s_address2 ,s_displayname ,s_phonenumber ,s_country_code ,phoneno_verified ,drop_address ,googlemap_link ,payment_type_info ,product_type_id ,trading_type_id ,iv_location ,iv_trade_licenese ,iv_res_national ,iv_exp_date ,iv_country_issue , iv_f_name , iv_m_name , iv_l_name , iv_dob , iv_r_bussiness_name ,iv_licenese_no ,iv_r_address ,id_doc_front ,id_doc_back ,bus_doc_license ,registration_status ,cancel_reason ,vat_registration_number ,tax_registration_exp_date ,vat_reg_certificate_certificate ,bank_benefi_name ,bank_name ,bank_ac_no ,bank_iban ,bank_branch_name ,swift_code ,bank_currency ,bank_browse_file ,erp_id ,last_integrated_date ,company_buyer_representative ,t_and_c_acknowledge ,created_at ,created_by ,modified_at ,modified_by ) VALUES ($1,$2,$3,$4,$5,$6,$7);',
    values: [user_id, buyer_code, email_verification_status ,business_name, business_com_logo , email_id ,subscription_id ,ubscription_effective_date ,business_status ,billing_country ,billing_state ,billing_city  , b_address1 , b_address2 , b_displayname , b_phonenumber , b_country_code , jurisdiction_id , pos_id , shipping_country ,shipping_state ,shipping_city ,s_address1 ,s_address2 ,s_displayname ,s_phonenumber ,s_country_code ,phoneno_verified ,drop_address ,googlemap_link ,payment_type_info ,product_type_id ,trading_type_id ,iv_location ,iv_trade_licenese ,iv_res_national ,iv_exp_date ,iv_country_issue , iv_f_name , iv_m_name , iv_l_name , iv_dob , iv_r_bussiness_name ,iv_licenese_no ,iv_r_address ,id_doc_front ,id_doc_back ,bus_doc_license ,registration_status ,cancel_reason ,vat_registration_number ,tax_registration_exp_date ,vat_reg_certificate_certificate ,bank_benefi_name ,bank_name ,bank_ac_no ,bank_iban ,bank_branch_name ,swift_code ,bank_currency ,bank_browse_file ,erp_id ,last_integrated_date ,company_buyer_representative ,t_and_c_acknowledge ,created_at ,created_by ,modified_at ,modified_by ]
  }
  pool.query(shipment_status_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [], code: 500 });
    } else {
      res.json({ success: true, msg: "New Category Request Status Created Successfully", data: req.rows, code: 200 });
    }
  })
});

router.put('/update/:id', (req, res) => {
  var shipment_status_id = req.body.shipment_status_id;
  var shipment_status_name = req.body.shipment_status_name;
  var remarks = req.body.remarks;
  var status = req.body.status;
  var created_at = req.body.created_at;
  var modified_at = new Date();
  var created_by = 1;
  var modified_by = 1;
  var shipment_status_query = {
    text: 'UPDATE shipment_status SET shipment_status_name=$2,status=$3,remarks=$4,created_at=$5,modified_at=$6,created_by=$7,modified_by=$8  WHERE shipment_status_id = $1;',
    values: [shipment_status_id,shipment_status_name,status,remarks,created_at,modified_at,created_by,modified_by]
  }
  pool.query(shipment_status_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [], code: 500 });
    } else {
      res.json({ success: true, msg: "Category Request Status Updated Successfully", data: req.rows, code: 200 });
    }
  })

});

router.delete('/delete/:id', (req, res) => {
  var id = req.params.id;
  var shipment_status_query = {
    text: 'DELETE FROM shipment_status WHERE shipment_status_id= $1',
    values: [id]
  }
  pool.query(shipment_status_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [] });
    } else {
      res.json({ success: true, msg: "Category Request Status Deleted Successfully", data: req.rows });
    }
  });

});
router.get('/getlist', (req, res) => {
  var status_query = {
    text: 'SELECT *  FROM shipment_status',
  }
  pool.query(status_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [] });
    } else {
      if (req.rows.length > 0) {
        res.json({ success: true, msg: "Loadind Category Request Status", data: req.rows });
      } else {
        res.json({ success: true, msg: "No data Found", data: [] });
      }
    }
  });

});
router.get('/getby_id/:id', (req, res) => {
  var id = req.params.id;
  var status_query = {
    text: 'SELECT *  FROM shipment_status WHERE shipment_status_id = $1',
    values: [id]
  }
  pool.query(status_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [], code: 500 });
    } else {
      if (req.rows.length > 0) {
        res.json({ success: true, msg: "Loading data", data: req.rows[0], code: 200 });
      } else {
        res.json({ success: true, msg: "No data found", data: [], code: 200 });
      }
    }
  });

});

// BUYER MANAGEMENT API  ends here...//

module.exports = router;


    
var express = require('express');
var router = express.Router();
var pool = require('../config/db');

router.post('/create', (req, res) => {
  var role_name = req.body.role_name;
  var role_code = req.body.role_code;
  var action = req.body.action;
  var status = req.body.status;
  var created_at = new Date();
  var modified_at = new Date();
  var created_by = 1;
  var modified_by = 1;
  var role_query = {
    text: 'INSERT INTO roles (role_name,role_code,action,status,created_at,modified_at,created_by,modified_by) VALUES ($1,$2,$3,$4,$5,$6,$7,$8);',
    values: [role_name,role_code,action,status, created_at, modified_at, created_by, modified_by]
  }
  pool.query(role_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [], code: 500 });
    } else {
      res.json({ success: true, msg: "Created Successfully", data: req.rows, code: 200 });
    }
  })
});
router.put('/update/:id', (req, res) => {
  var role_name = req.body.role_name;
  var role_code = req.body.role_code;
  var action = req.body.action;
  var status = req.body.status;
  var created_at = new Date();
  var modified_at = new Date();
  var created_by = 1;
  var modified_by = 1;
  var id = req.params.id;
  var role_query = {
    text: 'UPDATE roles SET role_name=$1,role_code=$2 ,action=$3 ,status=$4 ,created_at=$5 ,modified_at=$6 ,created_by=$7 ,modified_by=$8  WHERE id = $9;',
    values: [role_name, role_code, action, status, created_at, modified_at, created_by, modified_by, id]
  }
  pool.query(role_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [], code: 500 });
    } else {
      res.json({ success: true, msg: "Role Updated Succesfully", data:req.rows, code:200 });
    }
  })

});
router.delete('/delete/:id', (req, res) => {
  var id = req.params.id;
  var role_query = {
    text: 'DELETE FROM roles WHERE id= $1',
    values: [id]
  }
  pool.query(role_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: req.rows, code: 500 });
    } else {
      res.json({ success: true, msg: "Role Deleted Succesfully", data:req.rows, code:200 });
    }
  })
});
router.get('/getlist', (req, res) => {
  var role_query = {
    text: 'SELECT cd.id, cd.role_name, cd.status, cd.created_at, cd.created_by, cd.modified_at, cd.modified_by, um.user_name FROM roles cd inner join user_managment um ON cd.created_by = um.id',
  }
  pool.query(role_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: req.rows, code: 500 });
    } else {
      if (req.rowCount > 0) {
        res.json({ success: true, msg: "Loading List", data:req.rows, code:200 });
      } else {
        res.json({ success: true, msg: "No data found", data:[], code:200 });
      }
      
    }
  })

});
router.get('/getby_id/:id', (req, res) => {
  var id = req.params.id;
  var role_query = {
    text: 'SELECT *  FROM roles WHERE id = $1',
    values: [id]
  }
  pool.query(role_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: req.rows, code: 500 });
    } else {
      if (req.rows.length > 0) {
        res.json({ success: true, msg: "Loading data", data: req.rows[0], code: 200 });
      } else {
        res.json({ success: true, msg: "No data found", data: [], code: 200 });
      }
    }
  })
});

module.exports = router;
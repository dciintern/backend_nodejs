var express = require('express');
var router = express.Router();
var pool = require('../config/db');

router.post('/create', (req, res) => {
  var login_user_type_name = req.body.login_user_type_name;
  var status = req.body.status;
  var created_at = new Date();
  var modified_at = new Date();
  var created_by = 1;
  var modified_by = 1;
  var Login_user_type_query = {
    text: 'INSERT INTO login_user_type (login_user_type_name,created_at,modified_at,created_by,modified_by,status) VALUES ($1,$2,$3,$4,$5,$6);',
    values: [login_user_type_name, created_at, modified_at, created_by, modified_by,status]
  }
  pool.query(Login_user_type_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [], code: 500 });
    } else {
      res.json({ success: true, msg: "Created Successfully", data: req.rows, code: 200 });
    }
  })
});
router.put('/update/:id', (req, res) => {
  var login_user_type_name = req.body.login_user_type_name;
  var status = req.body.status;
  var created_at = req.body.created_at;
  var modified_at = new Date();
  var created_by = 1;
  var modified_by = 1;
  var login_user_type_id = req.params.id;
  var Login_user_type_query = {
    text: 'UPDATE login_user_type SET login_user_type_name=$1, created_at=$2, modified_at=$3 ,created_by=$4, modified_by=$5,status=$7  WHERE login_user_type_id = $6;',
    values: [login_user_type_name, created_at, modified_at, created_by, modified_by, login_user_type_id,status]
  }
  pool.query(Login_user_type_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [] });
    } else {
      res.json({ success: true, msg: "Updated Successfully", data: req.rows });
    }
  })

});
router.delete('/delete/:id', (req, res) => {
  var id = req.params.id;
  var Login_user_type_query = {
    text: 'DELETE FROM login_user_type WHERE login_user_type_id= $1',
    values: [id]
  }
  pool.query(Login_user_type_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [] });
    } else {
      res.json({ success: true, msg: "Deleted Successfully", data: req.rows });
    }
  })
});
router.get('/getlist', (req, res) => {
  var Login_user_type_query = {
    text: 'SELECT cd.login_user_type_id, cd.login_user_type_name, cd.status, cd.created_at, cd.created_by, cd.modified_at, cd.modified_by, um.user_name FROM login_user_type cd inner join user_managment um ON cd.created_by = um.id',
  }
  pool.query(Login_user_type_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [] });
    } else {
      res.json({ success: true, msg: "Loading Currencies", data: req.rows });
    }
  })

});
router.get('/getby_id/:id', (req, res) => {
  var id = req.params.id;
  var Login_user_type_query = {
    text: 'SELECT *  FROM login_user_type WHERE login_user_type_id = $1',
    values: [id]
  }
  pool.query(Login_user_type_query, (err, req) => {
    if (err) {
      console.log(err.stack);
      res.json({ success: false, msg: "Error in database", data: [] });
    } else {
      if (req.rows.length > 0) {
        res.json({ success: true, msg: "Loading data", data: req.rows[0], code: 200 });
      } else {
        res.json({ success: true, msg: "No data found", data: [], code: 200 });
      }
    }
  })
});

module.exports = router;
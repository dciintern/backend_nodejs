const express = require('express');
const http = require('http');

const bodyParser = require('body-parser');
const cors = require('cors');
const fs = require('fs')
const app = express();
const server = http.createServer(app);
const port = 3000;

app.use(cors());
app.use(bodyParser.json());

const auth = require('./routes/auth');
const buyerManagement = require('./routes/buyer_management');
const buyerDetails = require('./routes/buyerDetails');
const statusManagement = require('./routes/status_management');
const country = require('./routes/country');
const state = require('./routes/state');
const city = require('./routes/city');
const payout = require('./routes/payout-schdule');
const currency = require('./routes/currency');
const productType = require('./routes/product-type');
const reason = require('./routes/reason');
const role = require('./routes/role');
const usertype = require('./routes/user_type');
const tradingtype = require('./routes/tradingType');
const subscription_management = require('./routes/subscription_management');
const cms_main = require('./routes/cms_main');
const sms_template = require('./routes/sms_template');
const template_management = require('./routes/template-management');
const role_permission = require('./routes/role_permission');
const module_management = require('./routes/module_management');


app.use('/api/auth', auth);
app.use('/api/buyer_management', buyerManagement);
app.use('/api/buyer_details', buyerDetails);
app.use('/api/country-management', country);
app.use('/api/state-management', state);
app.use('/api/city-management', city);
app.use('/api/payout-management', payout);
app.use('/api/currency-management', currency);
app.use('/api/productType-management', productType);
app.use('/api/reason-management', reason);
app.use('/api/user-type-management', usertype);
app.use('/api/trading-type-management', tradingtype);
app.use('/api/role-management', role);
app.use('/api/cms-main', cms_main);
app.use('/api/status-management', statusManagement);
app.use('/api/subscription-plan-management', subscription_management);
app.use('/api/sms_template', sms_template);
app.use('/api/template-management', template_management);
app.use('/api/role_permission', role_permission);
app.use('/api/module_management', module_management);

server.listen(port,function(err){
    if(err) 
    console.log("Server Startup Error: "+err);
    else
    console.log("Server running @port: "+port);
});

app.get('/',function(req,res){
    res.writeHead(200,{'Content-Type':'text/html'});
    const sendpage = fs.createReadStream(__dirname + '/index.html','utf8');
    sendpage.pipe(res);
});